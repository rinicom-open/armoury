#include <algorithm>
#include <armoury/json.hpp>
#include <doctest/doctest.h>

struct StructForJsonTestA
{
    int x{};
    float y{};
    std::string s{};
    std::vector<std::string> stringVec{};
    std::array<int64_t, 3> arr{{}};
    std::array<double, 3> doubles{{}};

  private:
    friend class arms::json::Accessor<StructForJsonTestA>;

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, "x", x);
        store(dom, "y", y);
        store<Requirement::OPTIONAL>(dom, "s", s);
        store<Requirement::OPTIONAL>(dom, "stringVec", stringVec);
        store(dom, "arr", arr);
        store(dom, "doubles", doubles);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load(dom, "x", x);
        load<Requirement::OPTIONAL>(dom, "y", y);
        load<Requirement::OPTIONAL>(dom, "s", s);
        load<Requirement::OPTIONAL>(dom, "stringVec", stringVec);
        load(dom, "arr", arr);
        load<Requirement::OPTIONAL>(dom, "doubles", doubles);
    }
};

struct StructForJsonTestB
{
    int x{};
    float y{};
    std::string s{};
    std::vector<std::string> stringVec{};
    std::array<StructForJsonTestA, 2> arrOfStruct{};
    int cArray[3]{1, 2, 3};
    int cArray2[4]{9, 8, 7, 6};
    uint8_t cArray3[4]{9, 8, 7, 6};
    static constexpr char const *stringLiteral{"literal"};

  private:
    friend class arms::json::Accessor<StructForJsonTestB>;

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, "x", x);
        store(dom, "y", y);
        store(dom, "s", s);
        store<Requirement::OPTIONAL>(dom, "stringVec", stringVec);
        store(dom, "arrOfStruct", arrOfStruct);
        store(dom, "cArray", std::span(cArray));
        store(dom, "cArray2", cArray2);
        store(dom, "cArray3", cArray3);
        store(dom, "stringLiteralAsArray", std::span(stringLiteral, std::strlen(stringLiteral)));
        store(dom, "stringLiteral", stringLiteral);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load(dom, "x", x);
        load<Requirement::OPTIONAL>(dom, "y", y);
        load<Requirement::OPTIONAL>(dom, "s", s);
        load<Requirement::OPTIONAL>(dom, "stringVec", stringVec);
        load<Requirement::OPTIONAL>(dom, "arrOfStruct", arrOfStruct);
        load<Requirement::OPTIONAL>(dom, "cArray", std::span(cArray));
        load<Requirement::OPTIONAL>(dom, "cArray2", cArray2);
        load<Requirement::OPTIONAL>(dom, "cArray3", cArray3);
        // Cannot write to stringLiteral
    }
};

struct StructForJsonTestC
{
    StructForJsonTestA structA{};

  private:
    friend class arms::json::Accessor<StructForJsonTestC>;

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, "structA", structA);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load<Requirement::OPTIONAL>(dom, "structA", structA);
    }
};

struct StructOfNumbers
{
    unsigned char uc = 255;
    signed char sc = -128;
    unsigned short us = 65535;
    signed short ss = 32767;
    std::uint64_t ui64 = std::numeric_limits<std::uint64_t>::max();
    std::int64_t si64 = std::numeric_limits<std::int64_t>::lowest();

    bool operator==(StructOfNumbers const &other) const
    {
        return (uc == other.uc && sc == other.sc && us == other.us && ss == other.ss && ui64 == other.ui64 &&
                si64 == other.si64);
    }

  private:
    friend class arms::json::Accessor<StructOfNumbers>;

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, "uc", uc);
        store(dom, "sc", sc);
        store(dom, "us", us);
        store(dom, "ss", ss);
        store(dom, "ui64", ui64);
        store(dom, "si64", si64);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load(dom, "uc", uc);
        load(dom, "sc", sc);
        load(dom, "us", us);
        load(dom, "ss", ss);
        load(dom, "ui64", ui64);
        load(dom, "si64", si64);
    }
};

struct GenericData
{
    template <typename D>
    void toJson(D &dom) const
    {
        arms::json::store(dom, "g", g);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        arms::json::load(dom, "g", g);
    }

    arms::json::GenericJsonObject g;
};

TEST_CASE("Reading JSON into StructForJsonTestA [constructFromJson]")
{
    std::string testJsonA = R"(
        {
            "x": 123,
            "y": 14.0,
            "s": "test string",
            "stringVec": [ "this", "is", "a", "vector", "of", "strings" ],
            "arr": [ 1, -10, 100000000000 ]
        }
    )";

    REQUIRE_NOTHROW((void)arms::json::makeWithJson<StructForJsonTestA>(testJsonA));
    StructForJsonTestA testA = arms::json::makeWithJson<StructForJsonTestA>(testJsonA);
    REQUIRE((testA.x == 123));
    REQUIRE((testA.y == 14.0f));
    REQUIRE((testA.s == "test string"));
    REQUIRE((testA.stringVec == std::vector<std::string>{"this", "is", "a", "vector", "of", "strings"}));
    REQUIRE((testA.arr == std::array<int64_t, 3>{1, -10, 100000000000}));

    std::string badTestJsonA = R"(
        {
            "x": 123,
            "y": 14.0,
            "s": "test string",
            "stringVec": [ "this", "is", "a", "vector", "of", 11111111111111111111111 ],
            "arr": [ 1, -10, 100000000000 ]
        }
    )";
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestA>(badTestJsonA));
}

TEST_CASE("Get array when object is expected [constructFromJson]")
{
    std::string testJsonA = R"(
        [ 1, -10, 100000000000 ]
    )";
    std::string testJsonC = R"(
        {
            "structA": [ 1, -10, 100000000000 ]
        }
    )";

    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestA>(testJsonA));
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestC>(testJsonC));
}

TEST_CASE("Get object when array is expected [constructFromJson]")
{
    std::string testJsonA = R"(
        {
            "x": 123,
            "y": 14.0,
            "s": "test string",
            "stringVec": "this",
            "arr": {"first": 1, "second": -10, "third": 100000000000}
        }
    )";

    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestA>(testJsonA));
}

struct StringLiteralTest
{
    std::string stringLiteral{};

  private:
    friend class arms::json::Accessor<StringLiteralTest>;

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, "stringLiteral", stringLiteral);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load(dom, "stringLiteral", stringLiteral);
    }
};

TEST_CASE("Reading JSON into StructForJsonTestB [constructFromJson]")
{
    std::string testJsonB = R"(
        {
            "x": -123,
            "y": 0,
            "s": "Note, the float above has no decimal point in this JSON",
            "stringVec": [ "this", "is", "a", "vector", "of", "strings",
                           "with a long string in it, to test allocation"],
            "arrOfStruct": [
                {
                    "x": 123,
                    "y": 14.0,
                    "s": "This is the first sub-json",
                    "stringVec": [ "string" ],
                    "arr": [ 1, -10, 100000000000 ]
                },
                {
                    "x": 123,
                    "y": 14.0,
                    "s": "this is the second sub-json",
                    "stringVec": [ "hello" ],
                    "arr": [ 1, -10, 100000000000 ]
                }
            ],
            "cArray": [9, 8, 7],
            "cArray2": [0, 1, 2, 3],
            "cArray3": [0, 1, 2, 3]
        }
    )";

    std::string testJsonBWrong = R"(
        {
            "cArray": [0, 1, 2, 3, 4]
        }
    )";

    std::string testJsonBWrong2 = R"(
        {
            "cArray": []
        }
    )";
    std::string testJsonBWrong3 = R"(
        {
            "cArray2": [9,8,7,6,5,4]
        }
    )";
    std::string testJsonBWrong4 = R"(
        {
            "cArray2": []
        }
    )";

    REQUIRE_NOTHROW((void)arms::json::makeWithJson<StructForJsonTestB>(testJsonB));
    StructForJsonTestB testB = arms::json::makeWithJson<StructForJsonTestB>(testJsonB);
    std::string jsonString = arms::json::toJsonString(testB);
    testB = arms::json::makeWithJson<StructForJsonTestB>(jsonString);
    REQUIRE((testB.x == -123));
    REQUIRE((testB.y == 0.0f));
    REQUIRE((testB.s == "Note, the float above has no decimal point in this JSON"));
    REQUIRE((testB.stringVec == std::vector<std::string>{"this", "is", "a", "vector", "of", "strings",
                                                         "with a long string in it, to test allocation"}));
    REQUIRE((testB.arrOfStruct.at(1).s == "this is the second sub-json"));
    REQUIRE((testB.cArray[1] == 8));
    REQUIRE((testB.cArray2[1] == 1));
    REQUIRE((testB.cArray3[1] == 1));

    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestB>(testJsonBWrong));
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestB>(testJsonBWrong2));
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestB>(testJsonBWrong3));
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestB>(testJsonBWrong4));

    StringLiteralTest literalTest = arms::json::makeWithJson<StringLiteralTest>(jsonString);
    REQUIRE((literalTest.stringLiteral == std::string("literal")));
}

TEST_CASE("Update data from JSON [updateFromJson]")
{
    std::string goodTestJsonB = R"(
        {
            "x": -123,
            "y": 999.0,
            "s": "goodTestJsonB",
            "stringVec": [ "this", "is", "a", "vector", "of", "strings"],
            "arrOfStruct": [
                {
                    "x": 123,
                    "y": 14.0,
                    "s": "This is the first sub-json",
                    "stringVec": [ "string" ],
                    "arr": [ 1, -10, 100000000000 ]
                },
                {
                    "x": 123,
                    "y": 15.0,
                    "s": "this is the second sub-json",
                    "stringVec": [ "hello" ],
                    "arr": [ 1, -10, 100000000000 ]
                }
            ]
        }
    )";
    std::string badTestJsonB = R"(
        {
            "x": -123,
            "y": 666.0,
            "s": "badTestJsonB",
            "stringVec": [ "this", "is", "a", "vector", "of", "strings"],
            "arrOfStruct": [
                {
                    "x": 123,
                    "y": 1.0,
                    "s": "This is the first sub-json",
                    "stringVec": [ "string" ],
                    "arr": [ 1, -10, 100000000000 ]
                },
                {
                    "x": 123,
                    "y": 1.0,
                    "s": "the array is too long",
                    "stringVec": [ "hello" ],
                    "arr": [ 1, -10, 100, 100, 100, 100 ]
                }
            ]
        }
    )";
    std::string goodTestJsonB_v2 = R"(
        {
            "x": 222,
            "s": "goodTestJsonB_v2",
            "arrOfStruct": [
                {
                    "x": 123,
                    "y": null,
                    "arr": [ 1, 1, 1 ]
                },
                {
                    "x": 123,
                    "y": 10.0,
                    "s": "the array is fine",
                    "stringVec": [ "hello" ],
                    "arr": [ 1, -10, 100 ]
                }
            ]
        }
    )";

    StructForJsonTestB testB{};
    REQUIRE_NOTHROW(testB = arms::json::makeWithJson<StructForJsonTestB>(goodTestJsonB));
    REQUIRE((testB.y == 999.f));
    REQUIRE((testB.s == "goodTestJsonB"));
    REQUIRE((testB.arrOfStruct.at(1).s == "this is the second sub-json"));

    REQUIRE_THROWS(testB = arms::json::makeWithJson<StructForJsonTestB>(badTestJsonB, testB));
    REQUIRE((testB.y == 999.f));
    REQUIRE((testB.s == "goodTestJsonB"));
    REQUIRE((testB.arrOfStruct.at(1).s == "this is the second sub-json"));

    REQUIRE_NOTHROW(testB = arms::json::makeWithJson<StructForJsonTestB>(goodTestJsonB_v2, testB));
    REQUIRE((testB.y == 999.f));
    REQUIRE((testB.s == "goodTestJsonB_v2"));
    REQUIRE((testB.arrOfStruct.at(0).s == "This is the first sub-json"));
    REQUIRE((testB.arrOfStruct.at(0).y == 14.0f));
    REQUIRE((testB.arrOfStruct.at(1).s == "the array is fine"));
    REQUIRE((testB.arrOfStruct.at(1).y == 10.0f));
}

TEST_CASE("Int and float compatibility [constructFromJson]")
{
    auto doTest = [](std::string const &json) -> void {
        REQUIRE_NOTHROW((void)arms::json::makeWithJson<StructForJsonTestA>(json));
        StructForJsonTestA testA = arms::json::makeWithJson<StructForJsonTestA>(json);
        REQUIRE((testA.x == 123));
        REQUIRE((testA.y == 14.0f));
        REQUIRE((testA.arr == std::array<int64_t, 3>{1, 2, 3}));
        REQUIRE((testA.doubles == std::array<double, 3>{4.0, 5.0, 6.0}));
    };

    doTest(R"(
        {
            "x": 123,
            "y": 14.0,
            "arr": [ 1, 2, 3 ],
            "doubles": [ 4.0, 5.0, 6.0 ]
        }
    )");

    doTest(R"(
        {
            "x": 123,
            "y": 14,
            "arr": [ 1, 2, 3 ],
            "doubles": [ 4, 5, 6 ]
        }
    )");

    doTest(R"(
        {
            "x": 123.0,
            "y": 14.0,
            "arr": [ 1.0, 2.0, 3.0 ],
            "doubles": [ 4.0, 5.0, 6.0 ]
        }
    )");

    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestA>(R"(
        {
            "x": 123123123123123,
            "y": 14.0,
            "arr": [ 1, 2, 3 ],
            "doubles": [ 4.0, 5.0, 6.0 ]
        }
    )"));

    REQUIRE_THROWS((void)arms::json::makeWithJson<StructForJsonTestA>(R"(
        {
            "x": 123,
            "y": 100000000000000000000000000000000000000000,
            "arr": [ 1, 2, 3 ],
            "doubles": [ 4.0, 5.0, 6.0 ]
        }
    )"));
}

TEST_CASE("Integer ranges [Integer ranges]")
{
    StructOfNumbers son{};
    std::string text{arms::json::toJsonString(son)};
    StructOfNumbers sonReturned{arms::json::makeWithJson<StructOfNumbers>(text)};
    REQUIRE((son == sonReturned));
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructOfNumbers>(R"(
        {
            "uc": 256,
            "sc": 0,
            "us": 0,
            "ss": 0,
            "ui64": 0,
            "si64": 0
        }
     )"));
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructOfNumbers>(R"(
        {
            "uc": 0,
            "sc": -130,
            "us": 0,
            "ss": 0,
            "ui64": 0,
            "si64": 0
        }
     )"));
    REQUIRE_THROWS((void)arms::json::makeWithJson<StructOfNumbers>(R"(
        {
            "uc": 0,
            "sc": 0,
            "us": 0,
            "ss": 32888,
            "ui64": 0,
            "si64": 0
        }
     )"));
}

TEST_CASE("Preserving generic JSON data")
{
    GenericData gen;
    std::string json = arms::json::toJsonString(gen);
    std::cout << json << std::endl;
    auto it = std::remove_if(json.begin(), json.end(), [](auto c) { return std::isspace(c); });
    json.erase(it, json.end());
    REQUIRE(("{}" == json));
    REQUIRE_NOTHROW(gen = arms::json::makeWithJson<GenericData>(R"({"g":[{"hello":"world"}]})"));
    std::cout << json << std::endl;
    GenericData gen2{gen};
    REQUIRE((arms::json::toJsonString(gen) == arms::json::toJsonString(gen2)));
    GenericData gen3;
    gen3 = gen;
    REQUIRE((arms::json::toJsonString(gen) == arms::json::toJsonString(gen3)));

    REQUIRE_THROWS(gen = arms::json::makeWithJson<GenericData>(R"({"g":[{"hello":"world","boo"}]})"));
}
