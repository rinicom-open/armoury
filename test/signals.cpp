#include <armoury/signals.hpp>
#include <doctest/doctest.h>
#include <future>

SCENARIO("signal handler installation")
{
    GIVEN("an unknown signal handling state")
    {
        WHEN("initSignals has been called")
        {
            arms::signals::initSignals();

            THEN("handlers for SIGINT, SIGTERM and SIGQUIT are set to quitHandler")
            {
                struct sigaction sa = {};
                struct sigaction newSa = {};

                sigemptyset(&newSa.sa_mask);
                newSa.sa_flags = SA_RESTART;
                newSa.sa_handler = SIG_IGN;

                int rc = sigaction(SIGINT, &newSa, &sa);
                CHECK((rc == 0));
                CHECK(((sa.sa_flags & SA_RESTART) == SA_RESTART));
                CHECK((sa.sa_handler == arms::signals::quitHandler));

                rc = sigaction(SIGTERM, &newSa, &sa);
                CHECK((rc == 0));
                CHECK(((sa.sa_flags & SA_RESTART) == SA_RESTART));
                CHECK((sa.sa_handler == arms::signals::quitHandler));

                rc = sigaction(SIGQUIT, &newSa, &sa);
                CHECK((rc == 0));
                CHECK(((sa.sa_flags & SA_RESTART) == SA_RESTART));
                CHECK((sa.sa_handler == arms::signals::quitHandler));
            }
            THEN("handler for g_customInterruptSignal is set to customSignalHandlerCaller")
            {
                struct sigaction sa = {};
                struct sigaction newSa = {};
                sigemptyset(&newSa.sa_mask);

                newSa.sa_flags = SA_RESTART;
                newSa.sa_handler = SIG_IGN;

                int rc = sigaction(arms::signals::g_customInterruptSignal, &newSa, &sa);
                CHECK((rc == 0));
                CHECK(((sa.sa_flags & SA_RESTART) == 0));
                CHECK((sa.sa_handler == arms::signals::customSignalHandlerCaller));
            }
        }
    }
}

SCENARIO("Program exit")
{
    GIVEN("A program")
    {
        arms::signals::initSignals();
        auto waitingThreadFunc = []() { arms::signals::waitForProgramExitRequest(); };
        std::future exitRequestListener = std::async(std::launch::async, waitingThreadFunc);

        THEN("It should correctly respond to calls to requestProgramExit")
        {
            // Do not exit before requested
            CHECK_FALSE(arms::signals::shouldExit());
            CHECK((std::future_status::timeout == exitRequestListener.wait_for(std::chrono::seconds(1))));

            // Exit when requested
            arms::signals::requestProgramExit();
            CHECK((std::future_status::ready == exitRequestListener.wait_for(std::chrono::seconds(5))));
            CHECK((arms::signals::shouldExit()));

            // Ensure completion of test
            arms::signals::requestProgramExit();
            if (std::future_status::timeout == exitRequestListener.wait_for(std::chrono::seconds(20)))
            {
                exit(1);
            }
        }
    }
}
