#include <armoury/strings.hpp>
#include <doctest/doctest.h>

TEST_CASE("tryFormat")
{
    REQUIRE((arms::strings::tryFormat("abc{}{}", 123, "321") == std::string("abc123321")));
    REQUIRE_NOTHROW(arms::strings::tryFormat("{}{}{}"));
    REQUIRE_NOTHROW(arms::strings::tryFormat("", 123, "321"));
}
