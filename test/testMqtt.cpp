#include <armoury/misc.hpp>
#include <armoury/mqtt.hpp>
#include <armoury/mqtt/loadconfig.hpp>
#include <doctest/doctest.h>
#include <tuple>
#include <unordered_map>
#include <variant>

using namespace arms::mqtt;

SCENARIO("Figuring out a sensible default port for mqtt")
{
    GIVEN("An MqttInfo instance")
    {
        MqttInfo info{};
        WHEN("it doesn't have TLS info")
        {
            REQUIRE(!info.tlsSupport);
            THEN("the default port is 1883")
            {
                CHECK((info.getPort() == 1883));
            }
            AND_WHEN("the default port is overriden")
            {
                info.setPort(1234);
                THEN("the configured port value is used")
                {
                    CHECK((info.getPort() == 1234));
                }
            }
        }
        WHEN("it has TLS info")
        {
            info.tlsSupport = PSKBasedTLSSupport{"key", "identity"};
            REQUIRE(info.tlsSupport);
            THEN("the default port is 8883")
            {
                CHECK((info.getPort() == 8883));
            }
            AND_WHEN("the default port is overriden")
            {
                info.setPort(1234);
                THEN("the configured port value is used")
                {
                    CHECK((info.getPort() == 1234));
                }
            }
        }
    }
}

SCENARIO("Legal keep-alive values")
{
    GIVEN("An MqttInfo instance")
    {
        MqttInfo info{};
        WHEN("The keep-alive isn't configured")
        {
            THEN("It has a legal default")
            {
                auto const keepAlive = info.getKeepAlive();
                info.setKeepAlive(keepAlive);
                CHECK((keepAlive == info.getKeepAlive()));
            }
        }
        WHEN("The keep-alive is set to zero")
        {
            THEN("It has a legal value")
            {
                /**
                 * The source code of mosquitto (in lib/connect.c) allows a keep-alive value of 0.
                 * This is so that keep-alives can be disabled.
                 **/
                info.setKeepAlive(0);
                CHECK((info.getKeepAlive() == 0));
            }
        }
        WHEN("The keep-alive is set between 1 and 4")
        {
            info.setKeepAlive(2);
            THEN("This illegal value is rounds up to the closest legal keep-alive value")
            {
                /**
                 * The restriction on keep-alive values in mosquitto is documented in the return value section of
                 * https://mosquitto.org/api/files/mosquitto-h.html#mosquitto_connect_bind_v5
                 **/
                CHECK((info.getKeepAlive() > 2));
                auto const keepAlive = info.getKeepAlive();
                info.setKeepAlive(keepAlive);
                CHECK((keepAlive == info.getKeepAlive()));
            }
        }
        WHEN("The keep-alive is set to greater than 65535")
        {
            info.setKeepAlive(65536);
            THEN("This illegal value is set to the closest legal keep-alive value")
            {
                CHECK((info.getKeepAlive() == 65535));
            }
        }
    }
}

namespace
{
struct MockConfigSetting
{
    using Value = std::variant<std::string, bool>;
    std::unordered_map<std::string, Value> groupSettings;

    template <typename T>
    bool lookupValue(std::string const &name, T &value) const
    {
        static_assert(std::is_same_v<T, std::string> or std::is_same_v<T, bool>);
        if (auto found = groupSettings.find(name); found != std::end(groupSettings))
        {
            value = std::get<T>(found->second);
            return true;
        }
        else
        {
            return false;
        }
    }
    bool exists(std::string const &name) const
    {
        return groupSettings.contains(name);
    }
    bool isGroup() const
    {
        return true;
    }
};

MockConfigSetting makeTLSCertSettings(std::string pathType, std::string path,
                                      std::optional<ClientCredentials> credentials, bool insecure = false)
{
    MockConfigSetting result{};
    result.groupSettings["type"] = "cert";
    result.groupSettings["ca_path_type"] = std::move(pathType);
    result.groupSettings["ca_path"] = std::move(path);
    if (credentials)
    {
        result.groupSettings["client_cert"] = credentials->certfile;
        result.groupSettings["client_key"] = credentials->keyfile;
        result.groupSettings["client_key_password"] = credentials->password;
    }
    result.groupSettings.emplace("insecure", insecure);
    return result;
}

MockConfigSetting makePSKSettings(std::string key, std::string identity)
{
    MockConfigSetting result{};
    result.groupSettings["type"] = "psk";
    result.groupSettings["key"] = std::move(key);
    result.groupSettings["identity"] = std::move(identity);
    return result;
}

using ConfigValueType = std::variant<std::string, int, MockConfigSetting, bool>;

struct MockConfigLoader
{
    std::unordered_map<std::string, ConfigValueType> config{};
    template <typename T>
    bool getSetting(T &var, std::string const &setting) const
    {
        if (auto found = config.find(setting); found != std::end(config))
        {
            return std::visit(
                [&var](auto const &value) {
                    using StoredValueType = std::decay_t<decltype(value)>;
                    if constexpr (std::is_same_v<StoredValueType, T>)
                    {
                        var = value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                found->second);
        }
        else
        {
            return false;
        }
    }

    template <typename T>
    bool getSetting(std::optional<T> &var, std::string const &setting) const
    {
        if (auto found = config.find(setting); found != std::end(config))
        {
            return std::visit(
                [&var](auto const &value) {
                    using StoredValueType = std::decay_t<decltype(value)>;
                    using RequestedValueType = T;
                    if constexpr (std::is_same_v<StoredValueType, RequestedValueType>)
                    {
                        var = value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                found->second);
        }
        else
        {
            return false;
        }
    }

    bool getSetting(std::optional<TLSSupportFromConfig> &var, std::string const &setting) const
    {
        if (auto found = config.find(setting); found != std::end(config))
        {
            return std::visit(
                [&var](auto const &value) {
                    using StoredValueType = std::decay_t<decltype(value)>;
                    if constexpr (std::is_same_v<StoredValueType, MockConfigSetting>)
                    {
                        var = static_cast<TLSSupportFromConfig>(value);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                },
                found->second);
        }
        else
        {
            return false;
        }
    }
};

} // namespace

SCENARIO("Load MQTT config")
{
    GIVEN("A mock config")
    {
        MockConfigLoader loader{};
        loader.config["mqtt_username"] = "foo";
        loader.config["mqtt_password"] = "bar";
        loader.config["mqtt_host"] = "localtoast";
        loader.config["mqtt_port"] = 1234;
        loader.config["mqtt_keep_alive"] = 567;
        loader.config["mqtt_bind_address"] = "bind_to_this";

        WHEN("The config is loaded")
        {
            MqttInfo info{};
            loadConfig(loader, info);
            THEN("The MqttInfo object has the expected values")
            {
                REQUIRE(info.login);
                CHECK((info.login->username == "foo"));
                CHECK((info.login->password == "bar"));
                CHECK((info.host == "localtoast"));
                CHECK((info.getPort() == 1234));
                CHECK((info.getKeepAlive() == 567));
                CHECK((info.bind == "bind_to_this"));
            }
        }

        WHEN("The config has TLS info")
        {
            loader.config["mqtt_tls_client_auth"] = makeTLSCertSettings(
                "file", "path_to_ca", ClientCredentials{"client_cert_file", "client_key_file", "client_password"});
            MqttInfo info{};
            loadConfig(loader, info);
            THEN("The MqttInfo object has the expected values")
            {
                REQUIRE(info.tlsSupport);
                REQUIRE(std::holds_alternative<CertificateBasedTLSSupport>(*info.tlsSupport));
                constexpr auto password_length = std::string_view{"client_password"}.length();
                std::array<char, password_length + 1> buffer{};
                auto result = std::get<CertificateBasedTLSSupport>(*info.tlsSupport)
                                  .getClientKeyPassword(buffer.data(), buffer.size());
                CHECK((result == password_length));
                CHECK((std::string_view{buffer.data()} == "client_password"));
                CHECK_FALSE(std::get<CertificateBasedTLSSupport>(*info.tlsSupport).isInsecure());
            }
        }

        WHEN("The config has PSK info")
        {
            loader.config["mqtt_tls_client_auth"] = makePSKSettings("bah", "humbug");
            MqttInfo info{};
            loadConfig(loader, info);
            THEN("The MqttInfo object has the expected values")
            {
                REQUIRE(info.tlsSupport);
                REQUIRE(std::holds_alternative<PSKBasedTLSSupport>(*info.tlsSupport));
                CHECK((std::get<PSKBasedTLSSupport>(*info.tlsSupport).key == "bah"));
                CHECK((std::get<PSKBasedTLSSupport>(*info.tlsSupport).identity == "humbug"));
            }
        }

        WHEN("Certificate checking is disabled in TLS config")
        {
            bool const disabled{true};
            loader.config["mqtt_tls_client_auth"] = makeTLSCertSettings(
                "file", "path_to_ca", ClientCredentials{"client_cert_file", "client_key_file", "client_password"},
                disabled);
            MqttInfo info{};
            loadConfig(loader, info);
            THEN("TLS is disabled in the loaded config")
            {
                CHECK(std::get<CertificateBasedTLSSupport>(*info.tlsSupport).isInsecure());
            }
        }
        WHEN("Persistent sessions are requested by the config")
        {
            loader.config.emplace("mqtt_clean_session", false);
            MqttInfo info{};
            loadConfig(loader, info);
            THEN("Persistent session is set on the mqtt info")
            {
                CHECK((info.sessionType == SessionType::Persistent));
                CHECK_FALSE(info.getClientInfo().cleanSession);
            }
        }
        WHEN("Clean sessions are requested by the config")
        {
            loader.config.emplace("mqtt_clean_session", true);
            MqttInfo info{};
            loadConfig(loader, info);
            THEN("Clean session is set on the mqtt info")
            {
                CHECK((info.sessionType == SessionType::Clean));
                CHECK(info.getClientInfo().cleanSession);
            }
        }
    }
    GIVEN("An empty mock config")
    {
        MockConfigLoader loader{};
        WHEN("The config is loaded")
        {
            MqttInfo info{};
            loadConfig(loader, info);
            THEN("The MqttInfo object has the default values")
            {
                MqttInfo defaultConfig{};
                CHECK((info == defaultConfig));
                CHECK_FALSE(info.tlsSupport);
                CHECK_FALSE(info.login);
                CHECK((info.sessionType == SessionType::Clean));
                CHECK(arms::enumUnderlyingValue(info.sessionType));
            }
        }
    }
}

SCENARIO("Cloning properties")
{
    GIVEN("Publish message properties")
    {
        PublishMessageProperties properties{};

        std::string const responseTopic{"a test response topic"};
        properties.setResponseTopic(responseTopic);

        std::string const correlationData{"test correlation data"};
        properties.setCorrelationData(correlationData);

        WHEN("The properties are cloned")
        {
            PublishMessageProperties clone{properties};
            THEN("The clone has the expected values")
            {
                ResponseData reader{clone()};
                CHECK(reader.hasResponseTopic());
                CHECK(reader.cloneResponseTopic() == responseTopic);
                CHECK(reader.matchesCorrelationData(correlationData.data()));
            }
            THEN("The source values are unchanged")
            {
                ResponseData reader{properties()};
                CHECK(reader.hasResponseTopic());
                CHECK(reader.cloneResponseTopic() == responseTopic);
                CHECK(reader.matchesCorrelationData(correlationData.data()));
            }
        }
    }
}

SCENARIO("Cloning response data")
{
    GIVEN("MQTT response data")
    {
        PublishMessageProperties properties{};
        std::string const responseTopic{"another test response topic"};
        properties.setResponseTopic(responseTopic);

        uint32_t const correlationData{0xbeefdead};
        properties.setCorrelationData(&correlationData, sizeof(correlationData));

        ResponseData data{properties()};
        WHEN("The response data is cloned")
        {
            ResponseData clone{data};
            THEN("The clone has the expected values")
            {
                CHECK(data.hasResponseTopic());
                CHECK(data.cloneResponseTopic() == responseTopic);
                CHECK(data.matchesCorrelationData(&correlationData));
            }
        }
    }
}

SCENARIO("Move assignment of properties")
{
    GIVEN("Two sets of publish message properties")
    {
        PublishMessageProperties a{};

        std::string const responseTopicA{"a_topic"};
        a.setResponseTopic(responseTopicA);

        std::string const correlationDataA{"a_data"};
        a.setCorrelationData(correlationDataA);

        PublishMessageProperties b{};

        std::string const responseTopicB{"b_topic"};
        b.setResponseTopic(responseTopicB);

        std::string const correlationDataB{"b_data"};
        b.setCorrelationData(correlationDataB);

        WHEN("One property set is moved into the other")
        {
            a = std::move(b);
            THEN("The assigned-to property has the expected values")
            {
                ResponseData reader{a()};
                CHECK(reader.hasResponseTopic());
                CHECK(reader.cloneResponseTopic() == responseTopicB);
                CHECK(reader.matchesCorrelationData(correlationDataB.data()));
            }
            THEN("The source values are empty")
            {
                ResponseData reader{b()};
                CHECK(not reader.hasResponseTopic());
                CHECK(not reader.hasCorrelationData());
            }
        }
    }
}

SCENARIO("Empty response topics and correlation data")
{
    GIVEN("Publish properties with no response topic or correlation data")
    {
        PublishMessageProperties properties{};
        WHEN("Checking the response topic")
        {
            ResponseData reader{properties()};
            THEN("The response topic is empty")
            {
                CHECK((false == reader.hasResponseTopic()));
                CHECK(reader.cloneResponseTopic().empty());
            }
        }
        WHEN("Checking the correlation data")
        {
            ResponseData reader{properties()};
            THEN("The correlation data is empty")
            {
                CHECK((false == reader.hasCorrelationData()));
                CHECK(reader.correlationDataView().empty());
            }
        }
    }
    GIVEN("Publish properties with correlation data of size 0")
    {
        PublishMessageProperties properties{};
        uint8_t noddy{};
        properties.setCorrelationData(&noddy, 0);
        WHEN("Checking the correlation data")
        {
            ResponseData reader{properties()};
            THEN("The correlation data is empty")
            {
                CHECK((false == reader.hasCorrelationData()));
                CHECK(reader.correlationDataView().empty());
            }
        }
    }
    GIVEN("Publish properties with an empty response topic string")
    {
        PublishMessageProperties properties{};
        properties.setResponseTopic("");

        THEN("Obaining the MQTT properites throws an exception for library versions >= 2.0.20")
        {
            int major, minor, revision;
            int version = mosquitto_lib_version(&major, &minor, &revision);
            CHECK((major == LIBMOSQUITTO_MAJOR));
            CHECK((minor >= LIBMOSQUITTO_MINOR));
            if (version >= 2000020)
                CHECK_THROWS(properties());
            else
                CHECK_NOTHROW(properties());
        }
    }
}

SCENARIO("Compute QoS for persistent and clean sessions")
{
    MqttInfo info{};
    GIVEN("Clean session properties")
    {
        info.sessionType = SessionType::Clean;
        WHEN("Computing the QoS")
        {
            THEN("QoS is 0")
            {
                CHECK((info.getQoS() == 0));
            }
        }
    }
    GIVEN("Persistent session properties")
    {
        info.sessionType = SessionType::Persistent;
        WHEN("Computing the QoS")
        {
            THEN("QoS is 1")
            {
                CHECK((info.getQoS() == 1));
            }
        }
    }
}

SCENARIO("Client name handling for clean and persistent sessions")
{
    GIVEN("A persistent session with no client name")
    {
        MqttInfo info{};
        info.clientName.clear();
        info.sessionType = SessionType::Persistent;

        WHEN("Getting the session info")
        {
            THEN("An error is detected")
            {
                CHECK_THROWS(info.getClientInfo());
            }
        }
    }
    GIVEN("A clean session with no client name")
    {
        MqttInfo info{};
        info.clientName.clear();
        info.sessionType = SessionType::Clean;

        WHEN("Getting the session info")
        {
            THEN("No error is detected")
            {
                CHECK_NOTHROW(info.getClientInfo());
            }
        }
    }
    GIVEN("A persistent session with a client name")
    {
        MqttInfo info{};
        info.clientName = "foo";
        info.sessionType = SessionType::Persistent;

        WHEN("Getting the session info")
        {
            THEN("No error is detected")
            {
                CHECK_NOTHROW(info.getClientInfo());
            }
        }
    }
    GIVEN("A clean session with a client name")
    {
        MqttInfo info{};
        info.clientName = "bar";
        info.sessionType = SessionType::Clean;

        WHEN("Getting the session info")
        {
            THEN("No error is detected")
            {
                CHECK_NOTHROW(info.getClientInfo());
            }
        }
    }
    GIVEN("ClientInfo")
    {
        ClientInfo info{};
        WHEN("Not configured for a clean session")
        {
            info.cleanSession = false;
            AND_WHEN("The name is 23 characters in length")
            {
                info.name = "abcdefghijklmnopqrstuvw";
                REQUIRE(info.name.length() == 23);
                THEN("The configured name is used for the client id")
                {
                    CHECK(info.getId() == "abcdefghijklmnopqrstuvw");
                }
            }
            AND_WHEN("The name is over 23 characters in length")
            {
                info.name = "abcdefghijklmnopqrstuvwxyz";
                REQUIRE(info.name.length() > 23);
                THEN("The first 23 characters of the configured name are used for the client id")
                {
                    CHECK(info.getId() == "abcdefghijklmnopqrstuvw");
                }
            }
        }
        WHEN("Configured for a clean session")
        {
            info.cleanSession = true;
            AND_WHEN("The name is 6 characters in length")
            {
                info.name = "123456";
                REQUIRE(info.name.length() == 6);

                THEN("A 23 character id is generated")
                {
                    CHECK(info.getId().length() == 23);
                }
                THEN("The first characters match the configured name")
                {
                    CHECK(info.getId().find("123456") == 0);
                }
            }
            AND_WHEN("The name is over 6 characters in length")
            {
                info.name = "1234567";
                REQUIRE(info.name.length() > 6);

                THEN("A 23 character id is generated")
                {
                    CHECK(info.getId().length() == 23);
                }
                THEN("The first 6 characters match the configured name")
                {
                    CHECK(info.getId().find("123456") == 0);
                }
            }
        }
    }
}

SCENARIO("Prevent duplicate response topic and correlation data properties")
{
    GIVEN("MQTT publish properties")
    {
        PublishMessageProperties properties{};

        WHEN("A response topic has been added")
        {
            std::string const first_response_topic{"topic_one"};
            properties.setResponseTopic(first_response_topic);

            REQUIRE(MOSQ_ERR_SUCCESS == properties.checkProperties());
            REQUIRE_NOTHROW(properties());
            REQUIRE(properties());

            THEN("Another response topic cannot be added")
            {
                std::string const second_response_topic{"topic_two"};
                properties.setResponseTopic(second_response_topic);

                CHECK_FALSE(MOSQ_ERR_SUCCESS == properties.checkProperties());
                CHECK_THROWS(properties());
            }
        }
        WHEN("Correlation data has been added")
        {
            std::string const correlation_data{"test correlation data"};
            properties.setCorrelationData(correlation_data);

            REQUIRE(MOSQ_ERR_SUCCESS == properties.checkProperties());
            REQUIRE_NOTHROW(properties());
            REQUIRE(properties());

            THEN("More correlation data cannot be added")
            {
                std::string const more_correlation_data{"more correlation data"};
                properties.setCorrelationData(more_correlation_data);

                CHECK_FALSE(MOSQ_ERR_SUCCESS == properties.checkProperties());
                CHECK_THROWS(properties());
            }
        }
        WHEN("No properties have been set")
        {
            THEN("The properties are valid")
            {
                CHECK(MOSQ_ERR_SUCCESS == properties.checkProperties());
                CHECK_NOTHROW(properties());
            }
        }
    }
    GIVEN("MQTT connect properties")
    {
        ConnectProperties properties{};

        WHEN("The expiry has been set")
        {
            properties.setSessionExpiry(1234);

            REQUIRE(MOSQ_ERR_SUCCESS == properties.checkProperties());
            REQUIRE_NOTHROW(properties());
            REQUIRE(properties());

            THEN("The expiry cannot be set again")
            {
                properties.setSessionToNeverExpire();

                CHECK_FALSE(MOSQ_ERR_SUCCESS == properties.checkProperties());
                CHECK_THROWS(properties());
            }
        }
        WHEN("No properties have been set")
        {
            THEN("The properties are valid")
            {
                CHECK(MOSQ_ERR_SUCCESS == properties.checkProperties());
                CHECK_NOTHROW(properties());
            }
        }
    }
}
