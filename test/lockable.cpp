#include <armoury/RingBuffer.hpp>
#include <doctest/doctest.h>
#include <iostream>

using namespace arms;

TEST_CASE("lockable")
{
    Lockable<int> i{3};

    {
        auto [locked] = makeLocked<WriteLock>(i);
        REQUIRE((locked == 3));
        std::cout << "Value of locked data is " << locked << std::endl;
        locked.get() = 6;
    }

    {
        auto [locked] = makeLocked<ReadLock>(i);
        REQUIRE((locked == 6));
        std::cout << "Value of locked data is " << locked << std::endl;
        auto locked2 = std::move(locked);
        REQUIRE((locked2 == 6));
        REQUIRE_THROWS((void)(locked == 6));
    }

    std::cout << "Size of stack-based Lockable<int> : " << sizeof(Lockable<int>) << " bytes" << std::endl;
    std::cout << "Size of stack-based Lockable<std::array<int,4>> : " << sizeof(Lockable<std::array<int, 4>>)
              << " bytes" << std::endl;
}

TEST_CASE("Batch in ring buffer")
{
    RingBuffer<int64_t, 10> ring;

    {
        auto optionalLocked = ring.leader();
        if (optionalLocked.has_value())
        {
            REQUIRE((optionalLocked.value() == 0));
            optionalLocked.value().get() = 10;
        }
    }

    {
        auto optionalLocked = ring.follower();
        if (optionalLocked.has_value())
        {
            REQUIRE((optionalLocked.value() == 10));
        }
    }

    std::cout << "Size of stack-based RingBuffer<int64_t, 10>: " << sizeof(RingBuffer<int64_t, 10>) << " bytes"
              << std::endl;
}
