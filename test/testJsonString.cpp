#include <armoury/json.hpp>
#include <doctest/doctest.h>
#include <limits>

namespace
{
struct BasicTestType
{
    int a;
    bool b;
    float f;
    std::string s;

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, "a", a);
        store(dom, "b", b);
        store(dom, "f", f);
        store(dom, "s", s);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load(dom, "a", a);
        load(dom, "b", b);
        load(dom, "f", f);
        load(dom, "s", s);
    }

    friend bool operator==(BasicTestType const &lhs, BasicTestType const &rhs)
    {
        return std::tie(lhs.a, lhs.b, lhs.f, lhs.s) == std::tie(rhs.a, rhs.b, rhs.f, rhs.s);
    }
};

template <typename T>
struct TestWrapper
{
    std::string key;
    T value{};

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, key.c_str(), value);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load(dom, key.c_str(), value);
    }
};

// allow the test wrapper to deduce its T from the 2nd argument (CTAD)
template <typename T>
TestWrapper(std::string, T) -> TestWrapper<T>;

} // namespace

TEST_CASE("Basic writing JSON and reading it back")
{
    const BasicTestType original{1, true, 1.23f, "Hello, json"};
    std::string originalJson = arms::json::toJsonString(original);

    BasicTestType restored = arms::json::makeWithJson<BasicTestType>(originalJson);
    REQUIRE((original == restored));
}

TEST_CASE("Pretty writing JSON and reading it back")
{
    const BasicTestType original{1, true, 1.23f, "Hello, json"};
    std::string originalJson = arms::json::toPrettyJsonString(original);

    BasicTestType restored = arms::json::makeWithJson<BasicTestType>(originalJson);
    REQUIRE((original == restored));
}

TEST_CASE("NaN handling")
{
    static_assert(std::numeric_limits<double>::has_quiet_NaN);
    std::array const doubles{std::numeric_limits<double>::quiet_NaN(), 23.0};
    auto doublesToJson = [&]() {
        std::string originalJson = arms::json::toJsonString(TestWrapper{"data", doubles});
        return originalJson;
    };
    REQUIRE_THROWS(doublesToJson());
}

TEST_CASE("Infinity handling")
{
    static_assert(std::numeric_limits<double>::has_infinity);
    std::array const doubles{42.0, std::numeric_limits<double>::infinity(), 23.0,
                             -std::numeric_limits<double>::infinity()};
    auto doublesToJson = [&]() {
        std::string originalJson = arms::json::toJsonString(TestWrapper{"data", doubles});
        return originalJson;
    };
    REQUIRE_THROWS(doublesToJson());
}

namespace
{
template <arms::json::Requirement Store = arms::json::Requirement::REQUIRED,
          arms::json::Requirement Load = arms::json::Requirement::REQUIRED>
struct TestOptional
{
    std::optional<int> a;

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store<Store>(dom, "a", a);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load<Load>(dom, "a", a);
    }
};

template <arms::json::Requirement Load>
using TestOptionalLoad = TestOptional<arms::json::Requirement::REQUIRED, Load>;
} // namespace

TEST_CASE("Optional handling")
{
    using namespace arms::json;
    // when storing, an OPTIONAL optional field with no value will do nothing
    {
        TestOptional<Requirement::OPTIONAL> const original{};
        std::string originalJson = arms::json::toJsonString(original);
        REQUIRE((originalJson.find('a') == std::string::npos));
        auto restored = arms::json::makeWithJson<TestOptionalLoad<Requirement::OPTIONAL>>(originalJson);
        REQUIRE((original.a == restored.a));
    }
    // when storing, a REQUIRED optional field with no value will behave like OPTIONAL and do nothing
    {
        TestOptional<> const original{};
        std::string originalJson = arms::json::toJsonString(original);
        REQUIRE((originalJson.find('a') == std::string::npos));
        auto restored = arms::json::makeWithJson<TestOptionalLoad<Requirement::OPTIONAL>>(originalJson);
        REQUIRE((original.a == restored.a));
    }
    // when storing, an IGNORE_ERRORS optional field with no value will do nothing
    {
        TestOptional<Requirement::IGNORE_ERRORS> const original{};
        std::string originalJson = arms::json::toJsonString(original);
        REQUIRE((originalJson.find('a') == std::string::npos));
        auto restored = arms::json::makeWithJson<TestOptionalLoad<Requirement::OPTIONAL>>(originalJson);
        REQUIRE((original.a == restored.a));
    }
    // when loading, a REQUIRED optional field is supported if the field has a value
    {
        TestOptional<Requirement::OPTIONAL> const original{23};
        std::string originalJson = arms::json::toJsonString(original);
        auto restored = arms::json::makeWithJson<TestOptional<>>(originalJson);
        REQUIRE((original.a == restored.a));
    }
    // when loading, a REQUIRED optional field will throw if the field doesn't have a value
    {
        TestOptional<Requirement::OPTIONAL> const original{};
        std::string originalJson = arms::json::toJsonString(original);
        auto l = [originalJson]() {
            [[maybe_unused]] auto restored = arms::json::makeWithJson<TestOptional<>>(originalJson);
        };
        REQUIRE_THROWS(l());
    }
    // when loading, an IGNORE_ERRORS optional field will ignore if the field doesn't have a value
    {
        TestOptional<Requirement::OPTIONAL> const original{};
        std::string originalJson = arms::json::toJsonString(original);
        auto restored = arms::json::makeWithJson<TestOptionalLoad<Requirement::IGNORE_ERRORS>>(originalJson);
        REQUIRE((original.a == restored.a));
    }
}

TEST_CASE("Junk handling")
{
    auto test = [](std::string const &json) {
        BasicTestType restored = arms::json::makeWithJson<BasicTestType>(json);
        return restored;
    };

    std::string saneJson = R"(
        {
            "a": 42,
            "b": true,
            "f": 1.2,
            "s": "hello"
        }
    )";
    REQUIRE_NOTHROW(test(saneJson));

    std::string extraneousCommaJson = R"(
        {
            "a": 42,
            "b": true,
            "f": 1.2,
            "s": "hello",
        }
    )";
    REQUIRE_THROWS(test(extraneousCommaJson));

    std::string quotedNumberJson = R"(
        {
            "a": 42,
            "b": true,
            "f": "1.2",
            "s": "hello"
        }
    )";
    REQUIRE_THROWS(test(quotedNumberJson));

    std::string nanJson = R"(
        {
            "a": 42,
            "b": true,
            "f": NaN,
            "s": "hello"
        }
    )";
    REQUIRE_THROWS(test(nanJson));

    std::string infJson = R"(
        {
            "a": 42,
            "b": true,
            "f": Infinity,
            "s": "hello"
        }
    )";
    REQUIRE_THROWS(test(infJson));

    std::string negativeInfJson = R"(
        {
            "a": 42,
            "b": true,
            "f": -Infinity,
            "s": "hello"
        }
    )";
    REQUIRE_THROWS(test(negativeInfJson));

    std::string negativeZeroJson = R"(
        {
            "a": -0,
            "b": true,
            "f": 3.4,
            "s": "hello"
        }
    )";
    REQUIRE_NOTHROW(test(negativeZeroJson));
}

TEST_CASE("Limit handling")
{
    auto testRestoreImpl = [](auto value, auto cf) {
        using ValueType = decltype(value);
        std::string valueJson = arms::json::toJsonString(TestWrapper{"value", value});
        TestWrapper<ValueType> restored = arms::json::makeWithJson<TestWrapper<ValueType>>(valueJson, "value");
        return cf(value, restored.value);
    };
    auto testRestore = [testRestoreImpl](auto value) {
        return testRestoreImpl(value, [](const auto &lhs, const auto &rhs) { return lhs == rhs; });
    };

    REQUIRE(testRestore(std::numeric_limits<int64_t>::lowest()));
    REQUIRE(testRestore(std::numeric_limits<int64_t>::max()));

    REQUIRE(testRestore(std::numeric_limits<uint64_t>::lowest()));
    REQUIRE(testRestore(std::numeric_limits<uint64_t>::max()));

    auto testFloatRestore = [testRestoreImpl](auto value) {
        return testRestoreImpl(value, [](const auto &lhs, const auto &rhs) { return arms::relaxedEqual(lhs, rhs); });
    };

    REQUIRE(testFloatRestore(std::numeric_limits<double>::lowest()));
    REQUIRE(testFloatRestore(std::numeric_limits<double>::max()));

    // Rapid json converts floats to doubles before writing to a json string.
    // When reading back, the values can be outside the legal range of floats.
    // Testing here that this error is detected.
    REQUIRE_THROWS(testFloatRestore(std::numeric_limits<float>::max()));
    REQUIRE_THROWS(testFloatRestore(std::numeric_limits<float>::lowest()));

    REQUIRE(testFloatRestore(std::nextafter(std::numeric_limits<float>::max(), 0)));
    REQUIRE(testFloatRestore(std::nextafter(std::numeric_limits<float>::lowest(), 0)));
}

SCENARIO("Handling non-null terminated strings")
{
    GIVEN("Some json")
    {
        std::string testJson = R"(
        {
            "a": 42,
            "b": true,
            "f": 1.2,
            "s": "hello"
        })";
        WHEN("That json is part of a larger string")
        {
            auto const jsonLength = testJson.size(); // doesn't include null-termination
            testJson.append(' ', 20u);
            THEN("Parsing the json sub-string shouldn't fail")
            {
                CHECK_NOTHROW(
                    (void)arms::json::makeWithJson<BasicTestType>(std::string_view{testJson.data(), jsonLength}));
            }
        }
    }
}
