#include <armoury/json.hpp>
#include <doctest/doctest.h>

TEST_CASE("Test behaviour of an RpcRequest that takes no args")
{
    using namespace arms::json;
    RpcRequest<void> voidRequest{"void"};
    REQUIRE_NOTHROW(voidRequest.call([]() {}));

    auto serializeVoidRequest = [&]() {
        auto originalRequestJson = arms::json::toJsonString(voidRequest);
        return originalRequestJson;
    };
    REQUIRE_NOTHROW(serializeVoidRequest());

    // Setting anything other than no args on RpcRequest<void> would be a compilation
    // error
    voidRequest.setArgs();
    REQUIRE_NOTHROW(voidRequest.call([]() {}));

    auto originalRequestJson = serializeVoidRequest();
    arms::json::RpcRequest restoredRequest = arms::json::makeWithJson<RpcRequest<void>>(originalRequestJson, "void");
    bool sideEffect{false};
    REQUIRE_NOTHROW(restoredRequest.call([&sideEffect]() { sideEffect = true; }));
    REQUIRE(sideEffect);
}

namespace
{
// To serialize a vector, you need a wrapper with a toJson method
template <typename T>
struct VectorJsonWrapper
{
    std::vector<T> value{};

    template <typename D>
    void toJson(D &dom) const
    {
        using namespace arms::json;
        store(dom, "values", value);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        using namespace arms::json;
        load(dom, "values", value);
    }
};
} // namespace
TEST_CASE("Test behaviour of an RpcRequest with empty vector args")
{
    auto callable = [](VectorJsonWrapper<int> const &v) -> std::vector<int> { return v.value; };
    arms::json::RpcRequest originalRequest{"callable", +callable};
    originalRequest.setArgs(VectorJsonWrapper<int>{});
    REQUIRE_NOTHROW(originalRequest.call(callable));

    auto originalRequestJson = arms::json::toJsonString(originalRequest);
    arms::json::RpcRequest restoredRequest =
        arms::json::makeWithJson<decltype(originalRequest)>(originalRequestJson, "callable");
    REQUIRE_NOTHROW(restoredRequest.call(callable));
}

TEST_CASE("Test behaviour of an RpcRequest with vector args")
{
    auto callable = [](VectorJsonWrapper<int> const &v) -> std::vector<int> { return v.value; };
    arms::json::RpcRequest originalRequest{"callable", +callable};
    const std::vector<int> args{5, 4, 3, 2, 1};
    originalRequest.setArgs(VectorJsonWrapper<int>{args});
    // REQUIRE(originalRequest.call(callable) == args);

    auto originalRequestJson = arms::json::toJsonString(originalRequest);
    arms::json::RpcRequest restoredRequest =
        arms::json::makeWithJson<decltype(originalRequest)>(originalRequestJson, "callable");
    // REQUIRE(restoredRequest.call(callable) == args);
}

TEST_CASE("Test handling of arguments to RpcRequest")
{
    // Most misuse is caught at compile time, so can mostly only test the happy path
    auto callable = [](int i, std::string const &s) -> std::string { return std::to_string(i + 10) + s; };

    arms::json::RpcRequest originalRequest{"callable", std::function(callable)};
    REQUIRE(originalRequest.getMethod() == "callable");

    originalRequest.setArgs(2, "four");
    REQUIRE(originalRequest.getArgs());

    int j{0};
    auto callable2 = [&j](int i, std::string const &) -> std::string {
        j += i;
        return std::to_string(j);
    };

    originalRequest.setArgs(0, "zero");
    auto result = originalRequest.call(callable2);
    REQUIRE(j == 0);
    // REQUIRE(result == "0");

    originalRequest.setArgs(7, "seven");
    result = originalRequest.call(callable2);
    REQUIRE(j == 7);
    // REQUIRE(result == "7");

    auto originalRequestJson = arms::json::toJsonString(originalRequest);
    arms::json::RpcRequest restoredRequest =
        arms::json::makeWithJson<decltype(originalRequest)>(originalRequestJson, "callable");

    REQUIRE(restoredRequest.getMethod() == "callable");
    REQUIRE(restoredRequest.getArgs());
    result = restoredRequest.call(callable2);
    REQUIRE(j == 14);
    // REQUIRE(result == "14");
}

TEST_CASE("Test RpcRequest property mismatches")
{
    auto callable = [](int i, std::string const &s) -> std::string { return std::to_string(i + 10) + s; };

    arms::json::RpcRequest originalRequest{"callable", std::function(callable)};
    auto originalRequestJson = arms::json::toJsonString(originalRequest);

    auto tryMismatchedName = [originalRequestJson] {
        arms::json::RpcRequest restoredRequest =
            arms::json::makeWithJson<decltype(originalRequest)>(originalRequestJson, "mismatched name");
        return restoredRequest;
    };
    REQUIRE_THROWS(tryMismatchedName());
}

TEST_CASE("Test id serialisation")
{
    auto callable = [](int i, std::string const &s) -> std::string { return std::to_string(i + 10) + s; };

    arms::json::RpcRequest originalRequest{"callable", +callable};
    originalRequest.id = 54321;
    auto originalRequestJson = arms::json::toJsonString(originalRequest);

    arms::json::RpcRequest restoredRequest =
        arms::json::makeWithJson<decltype(originalRequest)>(originalRequestJson, "callable");
    REQUIRE(originalRequest.id == restoredRequest.id);
    REQUIRE_FALSE(originalRequest.id == std::uint8_t(123));
    REQUIRE_FALSE(originalRequest.id == "lll");
    REQUIRE_FALSE(originalRequest.id == std::string("pp"));
    REQUIRE_FALSE(originalRequest.id == 0);

    originalRequest.id = std::string{"five four three two one"};
    originalRequestJson = arms::json::toJsonString(originalRequest);
    arms::json::RpcRequest anotherRestoredRequest =
        arms::json::makeWithJson<decltype(originalRequest)>(originalRequestJson, "callable");
    REQUIRE(originalRequest.id == anotherRestoredRequest.id);
    REQUIRE(originalRequest.id != restoredRequest.id);
}

TEST_CASE("Test RpcRequest movable arg decay")
{
    auto callable = [](std::string &&) -> std::string { return "nothing of consequence"; };
    arms::json::RpcRequest originalRequest{"callable", +callable};

    std::string const arg = "request has its own copies of args";
    originalRequest.setArgs(arg);

    // We cannot call with the original callable because it takes an rvalue reference, and
    // the request's cached arguments cannot (and should not) be stolen.
    auto r1 = originalRequest.call([](std::string s) { return s; });
    // REQUIRE(r1 == arg);

    auto r2 = originalRequest.call([](std::string const &s) { return s; });
    // REQUIRE(r2 == arg);
}

TEST_CASE("Test callable exceptions propagate")
{
    auto callable = [](std::string s) -> std::string { return s; };
    arms::json::RpcRequest originalRequest{"callable", +callable};
    originalRequest.setArgs("some argument");

    std::function<std::string(std::string)> throwingFunction{};
    REQUIRE_THROWS(originalRequest.call(throwingFunction));
}
