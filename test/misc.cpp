#include <armoury/misc.hpp>
#include <doctest/doctest.h>
#include <iomanip>
#include <iostream>

TEST_CASE("Addition in Range")
{
    REQUIRE(arms::additionInRange<uint8_t>(int8_t(-128), 128));
    REQUIRE(arms::additionInRange<uint8_t>(128, int8_t(-128)));
    REQUIRE(arms::additionInRange<uint8_t>(127, 128));
    REQUIRE(arms::additionInRange<uint8_t>(int8_t(-128), 383));
    REQUIRE(arms::additionInRange<uint8_t>(382, int8_t(-128)));
    REQUIRE_FALSE(arms::additionInRange<uint8_t>(int8_t(-128), 384));
    REQUIRE_FALSE(arms::additionInRange<uint8_t>(int8_t(-128), 127));
    REQUIRE_FALSE(arms::additionInRange<uint8_t>(128, 128));

    REQUIRE(arms::additionInRange<int>(std::numeric_limits<int>::max(), std::numeric_limits<int>::lowest()));
    REQUIRE(arms::additionInRange<int>(0, std::numeric_limits<int>::max()));
    REQUIRE(arms::additionInRange<int>(std::numeric_limits<int>::lowest(), 0));
    REQUIRE_FALSE(arms::additionInRange<int>(std::numeric_limits<int>::lowest(), -1));
    REQUIRE_FALSE(arms::additionInRange<int>(std::numeric_limits<int>::max(), 1));
}

TEST_CASE("MQTT IDs have correct length and contain only allowed characters")
{
    arms::RandomIdGenerator rid;
    REQUIRE((rid.newId().size() == 23));
    REQUIRE((rid.newId(18).size() == 18));
    REQUIRE((rid.newId().find_first_of("\\|`¬!\"£$%^&*()-_=+[{]};:'@#~,<.>/? \n") == std::string::npos));
}

TEST_CASE("Correct moduloSum are computed")
{
    REQUIRE((arms::moduloSum(10, 0) == 0));
    REQUIRE((arms::moduloSum(10, -1) == 9));
    REQUIRE((arms::moduloSum(10, 1, -1) == 0));
    REQUIRE((arms::moduloSum(10, 255) == 5));
    REQUIRE((arms::moduloSum(255, 10) == 10));
    REQUIRE((arms::moduloSum(0, 10) == 0));
    REQUIRE((arms::moduloSum(10, 100'000'000'003) == 3));
    REQUIRE((arms::moduloSum(10, 100'000'000'003, static_cast<std::int8_t>(-1)) == 2));
    REQUIRE((arms::moduloSum(25, 1'000'000'000'000'003, static_cast<std::int8_t>(-4)) == 24));
    REQUIRE((arms::moduloSum(25, -1'000'000'000'000'003, static_cast<std::int8_t>(4)) == 1));
    REQUIRE((arms::moduloSum(10, static_cast<std::uint32_t>(4'000'000'001),
                             static_cast<std::uint32_t>(4'000'000'002)) == 3));
    REQUIRE((arms::moduloSum(4'000'000'000, static_cast<std::uint32_t>(3'000'000'000),
                             static_cast<std::uint32_t>(3'000'000'000)) == 2'000'000'000));
    REQUIRE((arms::moduloSum(10, static_cast<std::int64_t>(5'000'000'000'000'000'001),
                             static_cast<std::int64_t>(5'000'000'000'000'000'002)) == 3));
}

TEST_CASE("Test of relaxedEqual for floating point numbers")
{
    auto typeStr = [](auto f) -> std::string {
        if constexpr (std::is_integral_v<decltype(f)>)
        {
            return "integral";
        }
        else if constexpr (std::is_same_v<std::decay_t<decltype(f)>, float>)
        {
            return "float";
        }
        else if constexpr (std::is_same_v<std::decay_t<decltype(f)>, double>)
        {
            return "double";
        }
        else if constexpr (std::is_same_v<std::decay_t<decltype(f)>, long double>)
        {
            return "long double";
        }
        else
        {
            return "unknown type";
        }
    };

    auto equalityTest = [&typeStr](auto f1, auto f2) -> bool {
        bool const result1 = arms::relaxedEqual(f1, f2);
        bool const result2 = arms::relaxedEqual(f2, f1);
        std::cout << std::boolalpha << std::setprecision(std::numeric_limits<long double>::digits10 + 1) << "Test if "
                  << f1 << "(" << typeStr(f1) << ")"
                  << " == " << f2 << "(" << typeStr(f2) << ")"
                  << " ->  operator== " << (f1 == f2) << "   relaxedEqual " << result1 << std::endl;
        REQUIRE((result1 == result2));
        return result1;
    };

    REQUIRE(equalityTest(255.0f, 255));
    REQUIRE(equalityTest(std::numeric_limits<int64_t>::max(), static_cast<float>(std::numeric_limits<int64_t>::max())));
    REQUIRE(equalityTest(static_cast<long double>(std::numeric_limits<int64_t>::max()),
                         static_cast<float>(std::numeric_limits<int64_t>::max())));
    REQUIRE(equalityTest(std::numeric_limits<float>::max(), static_cast<double>(std::numeric_limits<float>::max())));
    REQUIRE(equalityTest(std::numeric_limits<float>::max(),
                         std::nextafter(static_cast<double>(std::numeric_limits<float>::max()), 0.0)));
    long double pi = 3.14159265358979323846264338327950288419716939937510l;
    REQUIRE(equalityTest(static_cast<float>(pi), pi));
    REQUIRE(equalityTest(static_cast<float>(pi), std::nextafter(static_cast<float>(pi), 0.0f)));
    REQUIRE_FALSE(
        equalityTest(std::nextafter(static_cast<float>(pi), 10.0f), std::nextafter(static_cast<float>(pi), 0.0f)));
}

template <typename I, typename F>
void testFloatingToIntegerConversionRange()
{
    I i1{std::numeric_limits<I>::lowest()};
    I i2{std::numeric_limits<I>::max()};
    F f1{std::nextafter(static_cast<F>(i1), static_cast<F>(0.0))};
    F f2{std::nextafter(static_cast<F>(i2), static_cast<F>(0.0))};
    auto r1{arms::safeFloatingToIntegerConversion<I>(f1)};
    auto r2{arms::safeFloatingToIntegerConversion<I>(f2)};

    REQUIRE(r1.has_value());
    REQUIRE(r2.has_value());

    auto accurate = [](auto r, auto i) -> bool {
        if (r.has_value())
        {
            double acc = std::abs(static_cast<double>(r.value()) / static_cast<double>(i) - 1.0);
            return (acc < 0.000001);
        }
        return false;
    };

    if constexpr (std::is_signed_v<I>)
    {
        REQUIRE(accurate(r1, i1));
    }
    REQUIRE(accurate(r2, i2));
}

TEST_CASE("Integer to floating conversions")
{
    testFloatingToIntegerConversionRange<std::uint8_t, double>();
    testFloatingToIntegerConversionRange<std::int64_t, double>();
    testFloatingToIntegerConversionRange<std::uint64_t, double>();
    testFloatingToIntegerConversionRange<std::uint32_t, float>();
    testFloatingToIntegerConversionRange<std::int32_t, float>();
    testFloatingToIntegerConversionRange<std::int64_t, float>();
    testFloatingToIntegerConversionRange<std::uint64_t, float>();

    REQUIRE((arms::safeFloatingToIntegerConversion<std::uint8_t>(255.0).value_or(0) == 255));

    REQUIRE((arms::safeFloatingToIntegerConversion<bool>(1.0).value_or(false) == true));
    REQUIRE((arms::safeFloatingToIntegerConversion<bool>(0.0).value_or(true) == false));

    REQUIRE_FALSE(arms::safeFloatingToIntegerConversion<std::uint8_t>(256.0).has_value());
    REQUIRE_FALSE(arms::safeFloatingToIntegerConversion<std::uint8_t>(-1.0).has_value());
    REQUIRE_FALSE(
        arms::safeFloatingToIntegerConversion<std::uint64_t>(std::numeric_limits<double>::quiet_NaN()).has_value());
    REQUIRE_FALSE(
        arms::safeFloatingToIntegerConversion<std::uint64_t>(std::numeric_limits<double>::infinity()).has_value());
}
