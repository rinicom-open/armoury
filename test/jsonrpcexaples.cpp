#include <armoury/jsonrpc.hpp>
#include <doctest/doctest.h>

int subtract(int minuEnd, int subtrahEnd)
{
    return (minuEnd - subtrahEnd);
}

int sum(int sum1, int sum2, int sum3)
{
    return (sum1 + sum2 + sum3);
}

void update(int one, int two, int three, int four, int five)
{
    std::cout << one + two + three + four + five << std::endl;
}

struct C
{
    int updateTestVar{0};
    int notifyHelloTestVar{0};

    int subtract(int minuEnd, int subtrahEnd) const
    {
        return (minuEnd - subtrahEnd);
    }

    int sum(int sum1, int sum2, int sum3)
    {
        return (sum1 + sum2 + sum3);
    }

    void update(int one, int two, int three, int four, int five)
    {
        updateTestVar = one + two + three + four + five;
    }

    void notify_hello(int one)
    {
        notifyHelloTestVar = one;
    }

    std::tuple<std::string, int> get_data()
    {
        return {"Hello", 5};
    }
};

using namespace arms::json;

// clang-format off
using Api1 = Methods<
    Rpc<"subtract", int(Param<"minuend", int>, Param<"subtrahend", int>)>,
    Rpc<"sum", int(Param<"sum1", int>, Param<"sum2", int>, Param<"sum3", int>)>,
    Rpc<"update", void(Param<"one", int>, Param<"two", int>, Param<"three", int>, Param<"four", int>, Param<"five", int>)>,
    Rpc<"notify_hello", void(Param<"one", int>)>,
    Rpc<"get_data", std::tuple<std::string, int>()>
>;
// clang-format on

TEST_CASE("RPCSpec-Positional-Parameters")
{
    C c;
    std::string testInvalidFunctionRequestString{
        R"({"jsonrpc": "2.0", "method": "subtract", "params": [42, 23], "id": 1})"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");
    REQUIRE((batch.responses.size() == 1));
    auto response = extractResponse<Api1, "subtract">(1, responseJson);
    REQUIRE(response.has_value());
    REQUIRE(response->result.has_value());
    REQUIRE_FALSE(response->error.has_value());
    REQUIRE((response->result.value() == 19));

    testInvalidFunctionRequestString = R"({"jsonrpc": "2.0", "method": "subtract", "params": [23, 42], "id": 2})";
    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    responseJson = batch.finaliseAndMakeResponseJson().value_or("");
    REQUIRE((batch.responses.size() == 1));
    response = extractResponse<Api1, "subtract">(2, responseJson);
    REQUIRE(response.has_value());
    REQUIRE(response->result.has_value());
    REQUIRE_FALSE(response->error.has_value());
    REQUIRE((response->result.value() == -19));
}

TEST_CASE("RPCSpec-Named-Parameters")
{
    C c;
    std::string testInvalidFunctionRequestString{
        R"({"jsonrpc": "2.0", "method": "subtract", "params": {"subtrahend": 23, "minuend": 42}, "id": 3})"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);

    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");
    REQUIRE((batch.responses.size() == 1));
    auto response = extractResponse<Api1, "subtract">(3, responseJson);
    REQUIRE(response.has_value());
    REQUIRE(response->result.has_value());
    REQUIRE_FALSE(response->error.has_value());
    REQUIRE((response->result.value() == 19));

    testInvalidFunctionRequestString =
        R"({"jsonrpc": "2.0", "method": "subtract", "params": {"minuend": 42, "subtrahend": 23}, "id": 4})";

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);

    responseJson = batch.finaliseAndMakeResponseJson().value_or("");
    REQUIRE((batch.responses.size() == 1));
    response = extractResponse<Api1, "subtract">(4, responseJson);
    REQUIRE(response.has_value());
    REQUIRE(response->result.has_value());
    REQUIRE_FALSE(response->error.has_value());
    REQUIRE((response->result.value() == 19));
}

TEST_CASE("RPCSpec-Notification")
{
    C c;
    std::string testInvalidFunctionRequestString{R"({"jsonrpc": "2.0", "method": "update", "params": [1,2,3,4,5]})"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{Func<Api1, "update">{&C::update, &c}};

    REQUIRE((c.updateTestVar == 0));
    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    batch.finaliseResponse();

    REQUIRE((batch.responses.size() == 0));
    REQUIRE((c.updateTestVar == 15));
}

TEST_CASE("RPCSpec-NonExistent-Method")
{
    C c;
    std::string testInvalidFunctionRequestString{R"({"jsonrpc": "2.0", "method": "foobar", "id": "1"})"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");

    REQUIRE((batch.responses.size() == 1));
    auto response = extractResponse<Api1, "subtract">("1", responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());
    REQUIRE((response->error->code == -32601));
    REQUIRE((response->error->message == "Method not found"));
}

TEST_CASE("RPCSpec-Invalid-JSON")
{
    C c;
    std::string testInvalidFunctionRequestString{R"({"jsonrpc": "2.0", "method": "foobar, "params": "bar", "baz])"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");

    REQUIRE((batch.responses.size() == 1));
    auto response = extractResponse<Api1, "subtract">({}, responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());

    // TODO: Formalise error codes and messages
    // REQUIRE(response->error->code == -32700);
}

// "Type error: method" == "Invalid Request"
TEST_CASE("RPCSpec-Invalid-Request")
{
    C c;
    std::string testInvalidFunctionRequestString{R"({"jsonrpc": "2.0", "method": 1, "params": "bar"})"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");

    REQUIRE((batch.responses.size() == 1));
    auto response = extractResponse<Api1, "subtract">({}, responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());

    // TODO: Formalise error codes and messages
    // REQUIRE(response->error->code == -32600);
    // REQUIRE(response->error->message == "Invalid Request");
}

TEST_CASE("RPCSpec-Empty-Array")
{
    C c;
    std::string testInvalidFunctionRequestString{R"([])"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");

    REQUIRE((batch.responses.size() == 1));
    auto response = extractResponse<Api1, "subtract">({}, responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());
    // TODO: Formalise error codes and messages
    // REQUIRE(response->error->code == -32600);
    // REQUIRE(response->error->message == "Invalid Request");
}

// -32000 == -32600
TEST_CASE("RPCSpec-Invalid-Batch-Not-Empty")
{
    C c;
    std::string testInvalidFunctionRequestString{R"([1])"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");

    REQUIRE((batch.responses.size() == 1));
    auto response = extractResponse<Api1, "subtract">({}, responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());
    // TODO: Formalise error codes and messages
    // REQUIRE(response->error->code == -32600);
    // REQUIRE(response->error->message == "Invalid Request");
}

TEST_CASE("RPCSpec-Invalid-Batch")
{
    C c;
    std::string testInvalidFunctionRequestString{R"([1,2,3])"};
    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 1> callees{
        Func<Api1, "subtract">{&C::subtract, &c},
    };

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    batch.finaliseResponse();
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");

    REQUIRE((batch.responses.size() == 3));
    auto response = extractResponse<Api1, "subtract">({}, responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());
    // TODO: Formalise error codes and messages
    // REQUIRE(response->error->code == -32600);
    // REQUIRE(response->error->message == "Invalid Request");
}

TEST_CASE("RPCSpec-Call-Batch")
{
    C c;
    std::string testInvalidFunctionRequestString{
        R"([{"jsonrpc": "2.0", "method": "sum", "params": [1,2,4], "id": "1"},{"jsonrpc": "2.0", "method": "notify_hello", "params": [7]},{"jsonrpc": "2.0", "method": "subtract", "params": [42,23], "id": "2"},{"foo": "boo"},{"jsonrpc": "2.0", "method": "foo.get", "params": {"name": "myself"}, "id": "5"},{"jsonrpc": "2.0", "method": "get_data", "id": "9"}])"};

    using namespace arms::json;
    arms::json::Batch<Api1> batch;

    Procedures<Api1, 4> callees{Func<Api1, "subtract">{&C::subtract, &c}, Func<Api1, "sum">{&C::sum, &c},
                                Func<Api1, "notify_hello">{&C::notify_hello, &c},
                                Func<Api1, "get_data">{&C::get_data, &c}};

    batch = arms::json::Batch<Api1>(testInvalidFunctionRequestString);
    callees.processBatch(batch);
    batch.finaliseResponse();
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");

    REQUIRE((batch.responses.size() == 5));

    // std::cout << "Testing get_data tuple function call" << std::endl;
    auto tupleResponse = extractResponse<Api1, "get_data">("9", responseJson);
    std::string r;
    int i = 0;
    if (tupleResponse.has_value() && tupleResponse->result.has_value())
    {
        r = std::get<0>(tupleResponse->result.value());
        i = std::get<1>(tupleResponse->result.value());
    }
    REQUIRE(tupleResponse.has_value());
    REQUIRE(tupleResponse->result.has_value());
    REQUIRE_FALSE(tupleResponse->error.has_value());
    REQUIRE((r == "Hello"));
    REQUIRE((i == 5));

    // std::cout << "Testing sum function call" << std::endl;
    auto response = extractResponse<Api1, "sum">("1", responseJson);
    REQUIRE(response.has_value());
    REQUIRE(response->result.has_value());
    REQUIRE_FALSE(response->error.has_value());
    REQUIRE((response->result.value() == 7));

    // std::cout << "Testing subtract function call" << std::endl;
    response = extractResponse<Api1, "subtract">("2", responseJson);
    REQUIRE(response.has_value());
    REQUIRE(response->result.has_value());
    REQUIRE_FALSE(response->error.has_value());
    REQUIRE((response->result.value() == 19));

    // std::cout << "Testing method not found error function call" << std::endl;
    response = extractResponse<Api1, "sum">("5", responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());
    // TODO: Formalise error codes and messages
    // REQUIRE(response->error->code == -32601);
    // REQUIRE(response->error->message == "Method not found");

    // std::cout << "Testing method not found error function call" << std::endl;
    response = extractResponse<Api1, "sum">({}, responseJson);
    REQUIRE(response.has_value());
    REQUIRE_FALSE(response->result.has_value());
    REQUIRE(response->error.has_value());
    // TODO: Formalise error codes and messages
    // REQUIRE(response->error->code == -32600);
    // REQUIRE(response->error->message == "Invalid Request");
}
