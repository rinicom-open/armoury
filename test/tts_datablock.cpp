#include <armoury/tts_datablocks.hpp>
#include <doctest/doctest.h>
#include <iostream>

std::string dataBlockDescription()
{
    return {R"(

// User defined types, UDTs must be surrounded with quotation marks
TYPE "JOYSTICK"
VERSION : 0.1
   STRUCT
      PosX : Real;
 // // // //  This is a single line comment // // // //
      PosY : Real; (*
                      This is a
                      multiline
                       comment
                                *)
      PosZ : Real;
      HasPos : Bool;
      NextScene : Bool;
      PrevScene : Bool;
      NextPanel : Bool;
      PrevPanel:Bool;             // missing spaces around colon
      PrimarySource : Bool        // missing semi-colon
      NextFallbackSource : Bool;
   END_STRUCT
END_TYPE

DATA_BLOCK "Rinicom"
{ S7_Optimized_Access := 'FALSE' }
VERSION : 0.1
NON_RETAIN
   STRUCT
      Data : UInt;
      Joystick : "JOYSTICK";
   END_STRUCT;
END_DATA_BLOCK

    )"};
}

inline constexpr arms::datablock::DateTime validTestDateTime()
{
    arms::datablock::DateTime dt{};
    dt.nanosecond = 987'654'321;
    dt.weekday = 7;
    dt.year = 51;
    dt.month = 4;
    dt.day = 4;
    dt.hour = 10;
    dt.minute = 45;
    dt.second = 30;
    return dt;
}

TEST_CASE("Correct date-time strings are computed")
{
    arms::datablock::DateTime dt = validTestDateTime();
    REQUIRE(dt.toString() == "2021-04-04 Sun 10:45:30.987654321");

    // Incorrect weekday should be ignored in favour of rest of date values
    dt = validTestDateTime();
    dt.weekday = 8;
    REQUIRE(dt.toString() == "2021-04-04 Sun 10:45:30.987654321");

    // Out of normal range values are corrected
    dt = validTestDateTime();
    dt.nanosecond = 1;
    dt.weekday = 0;
    dt.year = 9;
    dt.month = 13;
    dt.day = 22;
    dt.hour = 44;
    dt.minute = 54;
    dt.second = 90;
    REQUIRE(dt.toString() == "1980-01-23 Wed 20:55:30.000000001");
}

TEST_CASE("Invalid date-times are identified")
{
    arms::datablock::DateTime dt = validTestDateTime();
    REQUIRE(dt.isValid());

    // Allow weekday of 0 and 7 to be valid for Sunday
    dt = validTestDateTime();
    dt.weekday = 0;
    REQUIRE(dt.isValid());

    dt = validTestDateTime();
    dt.month = 0;
    REQUIRE_FALSE(dt.isValid());

    dt = validTestDateTime();
    dt.nanosecond = 1'000'000'001;
    REQUIRE_FALSE(dt.isValid());

    dt = validTestDateTime();
    dt.month = 13;
    REQUIRE_FALSE(dt.isValid());

    dt = validTestDateTime();
    dt.hour = 25;
    REQUIRE_FALSE(dt.isValid());

    dt = validTestDateTime();
    dt.second = 61;
    REQUIRE_FALSE(dt.isValid());
}

TEST_CASE("Timezone of the computer doesn't affect date-time")
{
    char const *origTz = std::getenv("TZ");

    auto test = [](char const *tz) -> void {
        setenv("TZ", tz, 1);
        tzset();
        arms::datablock::DateTime dt = validTestDateTime();
        REQUIRE(dt.isValid());
        REQUIRE(dt.toString() == "2021-04-04 Sun 10:45:30.987654321");
    };

    test("Europe/London");
    test("Europe/Rome");
    test("Europe/Vienna");
    test("America/Panama");
    test("Etc/GMT");
    test("Etc/GMT+12");
    if (origTz)
    {
        // Restores original timezone
        test(origTz);
    }
}

TEST_CASE("Element type compatibility check")
{
    REQUIRE(arms::datablock::isCompatibleType<bool>(arms::datablock::ElementType::BOOL));
    REQUIRE(arms::datablock::isCompatibleType<std::byte>(arms::datablock::ElementType::BYTE));
    REQUIRE(arms::datablock::isCompatibleType<float>(arms::datablock::ElementType::REAL));
    REQUIRE(arms::datablock::isCompatibleType<arms::datablock::DateTime>(arms::datablock::ElementType::DTL));
    REQUIRE(arms::datablock::isCompatibleType<char>(arms::datablock::ElementType::CHAR));
    REQUIRE_FALSE(arms::datablock::isCompatibleType<bool>(arms::datablock::ElementType::REAL));
    REQUIRE_FALSE(arms::datablock::isCompatibleType<int>(arms::datablock::ElementType::REAL));
    REQUIRE_FALSE(arms::datablock::isCompatibleType<double>(arms::datablock::ElementType::STRUCT));
    REQUIRE_FALSE(arms::datablock::isCompatibleType<float>(arms::datablock::ElementType::USINT));
}

TEST_CASE("Trimming whitespace and unprintable characters")
{
    std::string testToken = "  ,%o   p \rkm,\t \t\vj\nd14 23\"\"\"    ";
    std::string desired = R"(,%o   p km, jd14 23""")";
    arms::datablock::internal::trimWhitespaceAndUnprintable(testToken);
    REQUIRE((testToken == desired));
}

TEST_CASE("Find if a string starts with another string")
{
    REQUIRE(arms::datablock::internal::stringStartsWith("The quick fox", "The "));
    REQUIRE(arms::datablock::internal::stringStartsWith(" ", " "));
    REQUIRE_FALSE(arms::datablock::internal::stringStartsWith("The quick fox", "fox"));
    REQUIRE_FALSE(arms::datablock::internal::stringStartsWith(" ", "hello"));
    REQUIRE_FALSE(arms::datablock::internal::stringStartsWith(" ", "     "));
}

TEST_CASE("Parser can read the datablock description string buffer")
{
    arms::datablock::Parser parser{dataBlockDescription()};

    std::optional<arms::datablock::Descriptor> rinicomDescriptor = parser.findDescriptor("\"Rinicom\"");
    REQUIRE(rinicomDescriptor.has_value());

    std::optional<arms::datablock::Descriptor> joystickXDescriptor = parser.findDescriptor("\"Rinicom\".Joystick.PosX");
    REQUIRE(joystickXDescriptor.has_value());

    std::optional<arms::datablock::Descriptor> badDescriptor = parser.findDescriptor("\"Rinicom\".nonExistant");
    REQUIRE_FALSE(badDescriptor.has_value());
}

TEST_CASE("Write and read from datablock using descriptor")
{
    arms::datablock::Parser parser{dataBlockDescription()};
    arms::datablock::Descriptor rinicom = parser.findDescriptor("\"Rinicom\"").value();
    std::size_t rinicomSizeBits = rinicom.size() + rinicom.pad();
    // Allocate some storage bytes for test data.
    std::vector<std::byte> dataBlock(rinicomSizeBits / 8); // DATA_BLOCK ends on even byte

    std::optional<arms::datablock::Descriptor> joystickXDescriptor = parser.findDescriptor("\"Rinicom\".Joystick.PosX");
    REQUIRE(joystickXDescriptor.has_value());
    float result{0.0f};
    float requireResult{0.14f};
    if (joystickXDescriptor.has_value())
    {
        joystickXDescriptor->write(dataBlock, requireResult);
        joystickXDescriptor->read(dataBlock, result);
    }
    REQUIRE((result == requireResult));
}
