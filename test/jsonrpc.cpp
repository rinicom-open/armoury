#include <armoury/RingBuffer.hpp>
#include <armoury/jsonrpc.hpp>
#include <armoury/time.hpp>
#include <doctest/doctest.h>

std::string func_1(std::array<int, 3> arr)
{
    return std::to_string(arr[1] + 1);
}

std::string func_2(int i, std::string s)
{
    return std::to_string(i + 10) + s;
}

std::string func_1_aborted(std::array<int, 3> arr)
{
    throw arms::json::AbortProcedureCallHandling();
    return std::to_string(arr[1] + 1);
}

std::string func_2_aborted(int i, std::string s)
{
    throw arms::json::AbortProcedureCallHandling();
    return std::to_string(i + 10) + s;
}

struct C
{
    std::string func_2(int i, std::string s) const
    {
        if (i == 666)
        {
            throw std::runtime_error(s);
        }
        return std::to_string(i + j) + s;
    }

    void func_3()
    {
        std::cout << "Function 3, with no return value has been called." << std::endl;
    }

    void func_4(bool b)
    {
        std::cout << "Function 4: " << (b ? "true" : "false") << std::endl;
    }

    int j = 20;
};

using namespace arms::json;

// clang-format off
using Api1 = Methods<
    Rpc<"func 1", std::string(Param<"anIntArray",std::array<int,3>>)>,
    Rpc<"func 2", std::string(Param<"anInt",int>, Param<"aString",std::string>)>,
    Rpc<"func 3", void()>
>;
using Api2 = Methods<
    Rpc<"func 4", void(Param<"aBool",bool>)>
>;
using Api1Incompatible = Methods<
    Rpc<"func 3", void(Param<"aBool",bool>)>
>;
// clang-format on

TEST_CASE("Unique method names in APIs")
{
    REQUIRE((Rpc<"func 3", void(Param<"aBool", bool>)>::countMatchingMethodNames<Api1>() == 1));
    REQUIRE((Rpc<"an unused name", void(Param<"aBool", bool>)>::countMatchingMethodNames<Api1>() == 0));

    // Should not compile, duplicate methods names in combined API
    // arms::json::Batch<Api1, Api1> batch;
}

TEST_CASE("Compatible and incompatible APIs")
{
    REQUIRE(Rpc<"func 3", void()>::isCompatible<Rpc<"func 3", void()>>());
    REQUIRE_FALSE(Rpc<"func 3", void()>::isCompatible<Rpc<"func 3", void(Param<"aBool", bool>)>>());
    REQUIRE(Rpc<"func 3", void()>::isCompatible<Api1>());
    REQUIRE_FALSE(Rpc<"func 3", void(Param<"aBool", bool>)>::isCompatible<Api1>());
    REQUIRE(Api1::isCompatible<Api1>());
    REQUIRE(Api1::isCompatible<Api2>());
    REQUIRE_FALSE(Api1::isCompatible<Api1Incompatible>());

    // Should not compile. Although there are no duplicate names in the batch's combined API, there are incompatible
    // APIs between the batch and procedures
    /*
    arms::json::Batch<Api1Incompatible, Api2> batch;
    arms::json::Procedures<Api1, 1> callees{arms::json::Func<Api1, "func 1">{&func_1}};
    REQUIRE_NOTHROW(callees.processBatch(batch));
    */
}

TEST_CASE("Notifications")
{
    using namespace arms::json;
    C c;
    Procedures<Api1, 2> callees{Func<Api1, "func 2">{&C::func_2, &c}, Func<Api1, "func 3">{&C::func_3, &c}};

    auto testNotification = [&](std::string const &str) {
        arms::json::Batch<Api1, Api2> batch{str};
        REQUIRE_NOTHROW(callees.processBatch(batch));
        auto response = batch.finaliseAndMakeResponseJson();
        std::cout << "Notification response: " << response.value_or("<Empty>") << std::endl;
        REQUIRE_FALSE(response.has_value());
    };

    std::string testNotificationStr{R"({"jsonrpc": "2.0", "method": "func 3", "params": [123,"hello"]})"};
    std::string testDaemonicNotificationStr{R"(
    [
        {"jsonrpc": "2.0", "method": "func 2", "params": [666,"boo"]},
        {"jsonrpc": "2.0", "method": "func 4", "params": [true]}
    ]
    )"};

    testNotification(testNotificationStr);
    testNotification(testDaemonicNotificationStr);
}

TEST_CASE("Void result")
{
    using namespace arms::json;
    C c;
    Procedures<Api1, 1> callees{Func<Api1, "func 3">{&C::func_3, &c}};

    std::string testRequest{R"({"jsonrpc": "2.0", "method": "func 3", "params": [123,"hello"], "id": "the id"})"};
    arms::json::Batch<Api1> batch{testRequest};
    REQUIRE_NOTHROW(callees.processBatch(batch));
    auto responseJson = batch.finaliseAndMakeResponseJson();
    REQUIRE(responseJson.has_value());
    REQUIRE((responseJson.value_or(std::string()).find("result") != std::string::npos));
    std::cout << "Void result json: " << responseJson.value_or("ERROR") << std::endl;
    auto response = arms::json::extractResponse<Api1, "func 3">("the id", responseJson.value_or(""));
    REQUIRE(response.has_value());
    REQUIRE((response.has_value() && response->result.has_value()));
}

TEST_CASE("BadBatch")
{
    std::string badBatch = "[1,2,3]";

    using namespace arms::json;
    C c;
    Procedures<Api1, 1> callees{Func<Api1, "func 2">{&C::func_2, &c}};

    arms::json::Batch<Api1> batch{badBatch};
    callees.processBatch(batch);
    std::string responseJson = batch.finaliseAndMakeResponseJson().value_or("");
    std::cout << responseJson << std::endl;

    auto response = extractResponse<Api1, "func 2">({}, responseJson);
    REQUIRE((-32000 == response.value().error.value().code));
    std::cout << response.value().error.value().message << std::endl;
}

TEST_CASE("Callees")
{
    C c;
    using namespace arms::json;

    Procedures<Api1, 2> callees{Func<Api1, "func 2">{&C::func_2, &c}, Func<Api1, "func 1">{&func_1}};
    Procedures<Api1, 2> abortingCallees{Func<Api1, "func 2">{&func_2_aborted, &c},
                                        Func<Api1, "func 1">{&func_1_aborted}};

    arms::json::Batch<Api1> batch;
    batch.addRequest<"func 2">(10, 13, "oeirfioerfj");
    batch.addRequest<"func 2">({}, 23, "j");
    batch.addRequest<"func 1">("kopkpok", std::array<int, 3>{12, 13, 14});
    batch.addRequest<"func 3">(7);

    std::cout << "\nBefore Processing : \n";
    std::cout << "Requests: " << batch.makeRequestJson().value_or("Nothing!") << "\n";
    std::cout << "Response: " << batch.finaliseAndMakeResponseJson().value_or("Nothing!") << "\n";

    {
        // Methods that abort should not modify the batch
        abortingCallees.processBatch(batch);
        arms::time::ScopedTimer timer{"processBatch 1"};
        callees.processBatch(batch);
    }

    std::cout << "\nAfter Processing : \n";
    std::cout << "Requests: " << batch.makeRequestJson().value_or("Nothing!") << "\n";
    std::cout << "Response: " << batch.finaliseAndMakeResponseJson().value_or("Nothing!") << "\n";

    std::string testRequestString{R"(
      [
        {
          "jsonrpc": "2.0",
          "method": "func 1",
          "params": [[100, 101, 102]],
          "id": "params is array"
        },
        {
          "jsonrpc": "2.0",
          "method": "erferferf",
          "params": [[100, 101, 102]],
          "id": "bad method"
        },
        {
          "jsonrpc": "2.0",
          "method": "asdfasdf",
          "params": [[100, 101, 102]],
          "id": ["broken id and bad method"]
        },
        {
          "jsonrpc": "2.0",
          "method": "func 1",
          "params": [[100, 101, 102]],
          "id": ["broken id"]
        },
        {
          "jsonrpc": "2.0",
          "method": "func 1",
          "params": {
              "anIntArray":[100, 101, 102]
          },
          "id": null
        }
      ]

    )"};

    {
        arms::time::ScopedTimer timer{"initFromRequestJson"};
        batch.initFromRequestJson(testRequestString);
    }

    std::cout << "\nFrom testRequestString : \n";
    std::cout << "Requests: " << batch.makeRequestJson().value_or("Nothing!") << "\n";
    std::cout << "Response: " << batch.finaliseAndMakeResponseJson().value_or("Nothing!") << "\n";

    {
        arms::time::ScopedTimer timer{"processBatch 2"};
        callees.processBatch(batch);
    }
    std::string responseJson;

    {
        arms::time::ScopedTimer timer{"makeResponseJson"};
        responseJson = batch.finaliseAndMakeResponseJson().value_or("Nothing!");
    }

    std::cout << "\nAfter processing testRequestString : \n";
    std::cout << "Requests: " << batch.makeRequestJson().value_or("Nothing!") << "\n";
    std::cout << "Response: " << responseJson << "\n";

    {
        arms::time::ScopedTimer timer{"extractResponse"};
        auto response = extractResponse<Api1, "func 1">("params is array", responseJson);
        if (response.has_value())
        {
            std::cout << "Client got the result: " << response.value().result.value_or("Nothing")
                      << " from request 'params is array'\n";
        }
    }
}

TEST_CASE("RpcId")
{
    arms::json::RpcId id{"boo"};
    REQUIRE((id == "boo"));
}

TEST_CASE("Batch in ring buffer")
{
    arms::RingBuffer<arms::json::Batch<Api1>, 10> ring;

    {
        auto locked = ring.leader();
        if (locked.has_value())
        {
            locked.value().get().addRequest<"func 2">("boo", 5, "hello");
        }
    }

    {
        auto locked = ring.follower<arms::WriteLock>();
        if (locked.has_value())
        {
            Procedures<Api1, 1> callees{Func<Api1, "func 2">{&func_2}};
            callees.processBatch(locked.value().get());
        }
    }
}

namespace
{
template <typename T>
class Wrapper
{
  public:
    T data;

    Wrapper(auto... u) : data(std::move(u)...)
    {
    }

  private:
    friend class Accessor<Wrapper>;

    template <Serialisable...>
    friend class helpers::Variant;

    void toJson(auto &dom) const
    {
        using namespace arms::json;
        store(dom, "value", data);
    }

    void fromJson(auto const &dom)
    {
        using namespace arms::json;
        load(dom, "value", data);
    }
};

class S
{
  public:
    int i;
    float f;

  private:
    friend class Accessor<S>;

    template <Serialisable...>
    friend class helpers::Variant;

    void toJson(auto &dom) const
    {
        using namespace arms::json;
        store(dom, "i", i);
        store(dom, "f", f);
    }

    void fromJson(auto const &dom)
    {
        using namespace arms::json;
        load(dom, "i", i);
        load(dom, "f", f);
    }
};

} // namespace

SCENARIO("Variant construction")
{
    using string_t = Wrapper<std::string>;
    using int_t = Wrapper<int>;
    using float_t = Wrapper<float>;

    GIVEN("A variant")
    {
        using variant_type = arms::json::helpers::Variant<string_t, int_t, float_t>;

        variant_type original{string_t{"hello"}};
        REQUIRE(std::holds_alternative<string_t>(original.data));

        WHEN("The variant is copied")
        {
            variant_type copy{original};
            THEN("The copy constructor isn't inhibited")
            {
                CHECK(std::holds_alternative<string_t>(copy.data));
            }
        }

        WHEN("The variant is converted to JSON")
        {
            auto json = arms::json::toJsonString(original);
            THEN("The JSON can be converted back to the variant")
            {
                auto restored = arms::json::makeWithJson<variant_type>(json, std::in_place_type<string_t>);
                REQUIRE(std::holds_alternative<string_t>(restored.data));
                CHECK(std::get<string_t>(restored.data).data == std::get<string_t>(original.data).data);
            }
        }
    }
    GIVEN("A variant that can hold a struct")
    {
        using struct_t = Wrapper<S>;
        using variant_type = arms::json::helpers::Variant<int_t, float_t, struct_t>;

        THEN("The forwarding constructor can be used to construct the struct")
        {
            variant_type v(std::in_place_type<struct_t>, 1, 2.3f);
            REQUIRE(std::holds_alternative<struct_t>(v.data));

            CHECK(std::get<struct_t>(v.data).data.i == 1);
            CHECK(std::get<struct_t>(v.data).data.f == 2.3f);
        }
    }
    GIVEN("A variant member with a single argument to initialise")
    {
        using variant_type = arms::json::helpers::Variant<string_t, int_t, float_t>;
        THEN("The forward constructor can be used to construct the member")
        {
            variant_type vs{std::in_place_type<string_t>, "a test"};
            REQUIRE(std::holds_alternative<string_t>(vs.data));

            variant_type vi{std::in_place_type<int_t>, 23};
            REQUIRE(std::holds_alternative<int_t>(vi.data));

            variant_type vf{std::in_place_type<float_t>, 24};
            REQUIRE(std::holds_alternative<float_t>(vf.data));
        }
        THEN("The forward constructor can be used to default-construct the member")
        {
            variant_type vs{std::in_place_type<string_t>};
            REQUIRE(std::holds_alternative<string_t>(vs.data));

            variant_type vi{std::in_place_type<int_t>};
            REQUIRE(std::holds_alternative<int_t>(vi.data));

            variant_type vf{std::in_place_type<float_t>};
            REQUIRE(std::holds_alternative<float_t>(vf.data));
        }
    }
}
