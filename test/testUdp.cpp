#include <algorithm>
#include <armoury/misc.hpp>
#include <armoury/udp.hpp>
#include <chrono>
#include <doctest/doctest.h>
#include <span>

using namespace arms::udp;

SCENARIO("Converting durations to ticks")
{
    GIVEN("A duration")
    {
        std::chrono::seconds duration{1};
        WHEN("It is converted to ticks")
        {
            auto ticks = arms::time::toNanoTicks(duration);
            THEN("There are the expected number of ticks")
            {
                CHECK((ticks == 1'000'000'000));
            }
        }
    }
    // Any negative duration typically means "wait forever" in the armoury.
    GIVEN("A negative duration")
    {
        std::chrono::seconds duration{-1};
        WHEN("It is converted to ticks")
        {
            auto ticks = arms::time::toNanoTicks(duration);
            THEN("There are the expected number of ticks")
            {
                CHECK((ticks == -1'000'000'000));
            }
        }
    }
    GIVEN("A negative nanosecond duration")
    {
        std::chrono::nanoseconds duration{-1};
        WHEN("It is converted to ticks")
        {
            auto ticks = arms::time::toNanoTicks(duration);
            THEN("It has -1 ticks")
            {
                CHECK((ticks == -1));
            }
        }
    }
    GIVEN("A const duration")
    {
        std::chrono::seconds const duration{1};
        WHEN("It is converted to ticks")
        {
            auto ticks = arms::time::toNanoTicks(duration);
            THEN("There are the expected number of ticks")
            {
                CHECK((ticks == 1'000'000'000));
            }
        }
    }
}

SCENARIO("Propagating errors through buffer items")
{
    GIVEN("A udp buffer owner")
    {
        auto bufferOwner = arms::udp::BufferOwner<arms::udp::IncomingBuffer::capacity()>();
        WHEN("A buffer item is given an error")
        {
            auto payloadRef = bufferOwner.getBufferScope();
            REQUIRE(payloadRef);
            (*payloadRef)->setError(std::make_error_code(std::errc::protocol_error));
            CHECK((*payloadRef)->hasError());

            THEN("The error is propagated to the buffer owner when the reference count goes out of scope")
            {
                REQUIRE_FALSE(bufferOwner.hasErrors());
                payloadRef.reset();

                CHECK(bufferOwner.hasErrors());
                CHECK((bufferOwner.getErrorCount() == 1));

                AND_WHEN("The item is retrieved again")
                {
                    auto payloadRef = bufferOwner.getBufferScope();
                    REQUIRE(payloadRef);
                    THEN("The item no longer has an error set on it")
                    {
                        CHECK_FALSE((*payloadRef)->hasError());
                    }
                }
            }
        }
    }
}

SCENARIO("Buffer references release items back to buffer")
{
    GIVEN("A udp buffer owner of 3 items")
    {
        arms::udp::BufferOwner<3u> bufferOwner{};
        WHEN("3 items are retrieved")
        {
            auto ref1 = bufferOwner.getBufferScope();
            auto ref2 = bufferOwner.getBufferScope();
            auto ref3 = bufferOwner.getBufferScope();

            REQUIRE(ref1);
            REQUIRE(ref2);
            REQUIRE(ref3);

            THEN("There should be no more items available")
            {
                auto refN = bufferOwner.getBufferScope(std::chrono::milliseconds{100});
                CHECK_FALSE(refN);
                AND_WHEN("3 reference counted items from the buffer owner have been released")
                {
                    ref1.reset();
                    ref2.reset();
                    ref3.reset();
                    THEN("There should be 3 items available")
                    {
                        auto ref1 = bufferOwner.getBufferScope(std::chrono::seconds{0});
                        auto ref2 = bufferOwner.getBufferScope(std::chrono::seconds{0});
                        auto ref3 = bufferOwner.getBufferScope(std::chrono::seconds{0});

                        CHECK(ref1);
                        CHECK(ref2);
                        CHECK(ref3);
                    }
                }
                AND_WHEN("The reference count for one item is bumped before the 3 original references are released")
                {
                    auto copy = ref1;
                    ref1.reset();
                    ref2.reset();
                    ref3.reset();
                    THEN("There should be 2 items available")
                    {
                        auto ref1 = bufferOwner.getBufferScope(std::chrono::seconds{0});
                        auto ref2 = bufferOwner.getBufferScope(std::chrono::seconds{0});
                        auto ref3 = bufferOwner.getBufferScope(std::chrono::seconds{0});

                        CHECK(ref1);
                        CHECK(ref2);
                        CHECK_FALSE(ref3);
                    }
                }
            }
        }
    }
}

SCENARIO("Setting data on a buffer and getting it back")
{
    GIVEN("A udp buffer")
    {
        arms::udp::Buffer buffer{};
        WHEN("A vector of bytes is copied into it")
        {
            std::vector<std::byte> vbs{};
            vbs.assign(1024u, std::byte{'x'});
            buffer.setData(vbs.begin(), vbs.end());

            THEN("The buffer holds a copy of the data")
            {
                CHECK((buffer.getSize() == vbs.size()));
                CHECK(std::equal(vbs.begin(), vbs.end(), buffer.getBegin()));

                AND_WHEN("A vector of uint64_t is subsequently copied into it")
                {
                    std::vector<uint64_t> vws{};
                    vws.assign(buffer.getCapacity() / sizeof(uint64_t), uint64_t{0xdeadbeeffacef00d});
                    buffer.setData(vws.begin(), vws.end());
                    THEN("The buffer properties change accordingly")
                    {
                        CHECK((buffer.getSize() == vws.size() * sizeof(uint64_t)));
                        uint64_t check{};
                        std::memcpy(&check, buffer.getBegin(), sizeof(uint64_t));
                        CHECK((check == 0xdeadbeeffacef00d));
                    }
                }
            }
        }
    }
}

SCENARIO("Compare Udp endpoints")
{
    GIVEN("A udp endpoint")
    {
        arms::udp::UdpEndpoint endpoint{"127.0.0.1", 1234};
        WHEN("Compared with an identical endpoint")
        {
            arms::udp::UdpEndpoint cf{endpoint};
            bool result = cf == endpoint;
            THEN("The comparison holds")
            {
                CHECK(result);
            }
        }
        WHEN("Compared with the same address and a different port")
        {
            arms::udp::UdpEndpoint cf{"127.0.0.1", 9876};
            bool result = cf == endpoint;
            THEN("The endpoints aren't equal")
            {
                CHECK(!result);
            }
        }
        WHEN("Compared with a different address and the same port")
        {
            arms::udp::UdpEndpoint cf{"127.0.0.2", 1234};
            bool result = cf == endpoint;
            THEN("The endpoints aren't equal")
            {
                CHECK(!result);
            }
        }
    }
}

SCENARIO("Assign udp buffer data")
{
    GIVEN("udp buffer data")
    {
        arms::udp::Buffer buffer{};
        WHEN("A second buffer of the same size is created")
        {
            arms::udp::Buffer::DataType buffer_array{};
            std::ranges::fill(buffer_array, std::byte{'$'});
            THEN("That second buffer can be assigned to the udp buffer data")
            {
                REQUIRE((buffer.getSize() == 0u));
                CHECK_NOTHROW(buffer.setData(buffer_array.begin(), buffer_array.end()));
                CHECK((buffer.getSize() == buffer_array.size()));

                REQUIRE(not buffer_array.empty());
                CHECK((*buffer.getBegin() == std::byte{'$'}));
            }
        }
        WHEN("A container comprising the same number of bytes as the udp buffer capacity, but containing a type larger "
             "type than byte, is created")
        {
            constexpr auto buffer_capacity = arms::udp::g_bufferElementCapacity;
            using wider_container = std::array<uint16_t, buffer_capacity / 2>;
            wider_container unsigned_array{};
            std::ranges::fill(unsigned_array, 0xbeef);
            REQUIRE((unsigned_array.size() * 2 == buffer_capacity));

            THEN("The container can be assigned to the udp buffer data")
            {
                REQUIRE((buffer.getSize() == 0u));
                CHECK_NOTHROW(buffer.setData(unsigned_array.begin(), unsigned_array.end()));
                CHECK((buffer.getSize() == buffer_capacity));

                AND_THEN("The udp buffer contains the expected byte values")
                {
                    uint16_t beef[1] = {0xbeef};
                    auto buffer_span = std::span{buffer.getBegin(), sizeof(uint16_t)};
                    auto beef_bytes_span = std::as_bytes(std::span{beef});
                    CHECK(std::ranges::equal(buffer_span, beef_bytes_span));

                    AND_THEN("The udp buffer data can be read back into the original container type")
                    {
                        wider_container more_beef{};
                        auto writable_beef_bytes = std::as_writable_bytes(std::span{more_beef});
                        std::copy(buffer.getBegin(), buffer.getEnd(), writable_beef_bytes.begin());
                        CHECK((unsigned_array == more_beef));
                    }
                }
            }
        }
        WHEN("A container comprising the more bytes than the udp buffer capacity is created")
        {
            constexpr auto buffer_capacity = arms::udp::g_bufferElementCapacity;
            std::array<uint16_t, buffer_capacity> unsigned_array{};
            std::ranges::fill(unsigned_array, 0xbeef);

            THEN("That container cannot be assigned to the udp buffer data")
            {
                REQUIRE((buffer.getSize() == 0u));
                CHECK_THROWS(buffer.setData(unsigned_array.begin(), unsigned_array.end()));
            }
        }
        WHEN("A negative range is passed to the udp buffer")
        {
            arms::udp::Buffer::DataType buffer_array{};
            auto begin = buffer_array.end();
            auto end = buffer_array.begin();
            THEN("An exception is not thrown")
            {
                CHECK_NOTHROW(buffer.setData(begin, end));
            }
        }
        WHEN("A negative range is passed to a full udp buffer")
        {
            arms::udp::Buffer::DataType buffer_array{};
            std::ranges::fill(buffer_array, std::byte{'$'});
            buffer.setData(buffer_array.begin(), buffer_array.end());
            REQUIRE(buffer.getSize() > 0u);

            auto begin = buffer_array.end();
            auto end = buffer_array.begin();
            THEN("The size is 0")
            {
                buffer.setData(begin, end);
                CHECK(buffer.getSize() == 0u);
            }
        }
        WHEN("An empty range is passed to a full udp buffer")
        {
            arms::udp::Buffer::DataType buffer_array{};
            std::ranges::fill(buffer_array, std::byte{'$'});
            buffer.setData(buffer_array.begin(), buffer_array.end());
            REQUIRE(buffer.getSize() > 0u);

            auto begin = buffer_array.begin();
            auto end = buffer_array.begin();
            THEN("The size is 0")
            {
                buffer.setData(begin, end);
                CHECK(buffer.getSize() == 0u);
            }
        }
    }
}

SCENARIO("Check alignment when assigning to udp buffer data")
{
    GIVEN("udp buffer data with default alignment")
    {
        arms::udp::Buffer buffers[2];
        auto &buffer = buffers[1];
        WHEN("A container of a non-trivially copyable UDT is created with a stricter alignment requirement than the "
             "udp buffer")
        {
            struct UDT
            {
                UDT() = default;
                UDT(int d) : data{d}
                {
                }

                // non-trivially copyable so that std::copy doesn't use memcpy
                UDT(UDT const &u) : data{u.data}
                {
                }
                UDT &operator=(UDT const &u)
                {
                    data = u.data;
                    return *this;
                }

                bool operator==(UDT const &) const = default;

                alignas(32) int data{};
            };
            REQUIRE_FALSE(std::is_trivially_copyable_v<UDT>);
            REQUIRE((alignof(UDT) > alignof(arms::udp::Buffer)));

            constexpr auto udt_container_size = arms::udp::g_bufferElementCapacity / sizeof(UDT);
            using udt_container_type = std::array<UDT, udt_container_size>;
            udt_container_type udt_container{};
            std::iota(std::begin(udt_container), std::end(udt_container), 23);

            THEN("The container can be assigned to the udp buffer data")
            {
                REQUIRE((buffer.getSize() == 0u));
                CHECK_NOTHROW(buffer.setData(udt_container.begin(), udt_container.end()));
                REQUIRE((buffer.getSize() > 0u));

                AND_THEN("The udp buffer contains the expected byte values")
                {
                    // Because the padding after data may be uninitialized, it shouldn't be included in any comparison
                    // of UDT bytes
                    constexpr auto comparison_size = offsetof(UDT, data) + sizeof(UDT::data);

                    REQUIRE((udt_container[0].data == 23));
                    UDT udt_compare[1] = {23};
                    auto comparison_span = std::as_bytes(std::span{udt_compare}).first(comparison_size);

                    auto buffer_span = std::span{buffer.getBegin(), comparison_size}.first(comparison_size);
                    CHECK(std::ranges::equal(buffer_span, comparison_span));

                    AND_THEN("The udp buffer data can be read back into the original container type")
                    {
                        udt_container_type read_back_udts{};
                        auto writable_udt_bytes = std::as_writable_bytes(std::span{read_back_udts});
                        std::copy(buffer.getBegin(), buffer.getEnd(), writable_udt_bytes.begin());
                        CHECK((udt_container == read_back_udts));
                    }
                }
            }
        }
    }
}
