# Rinicom Armoury

## Dependencies

### Debian/Ubuntu

The unit tests build with [`meson`](https://mesonbuild.com/) and depend on [`libmosquitto`](https://mosquitto.org/man/libmosquitto-3.html)

The [`doctest`](https://github.com/doctest/doctest) unit test framework is included in the source

    apt update && apt -y install meson ninja-build libmosquitto-dev

## Building and running tests

Build the unit tests like you would any other meson project

    meson setup build --buildtype=release
    cd build
    meson compile
    meson test

