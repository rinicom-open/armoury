option(
    'git_version_check',
    description : 'An error is raised if git version from tag does not match the project version',
    type : 'boolean',
    value : true
)
