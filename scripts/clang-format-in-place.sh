#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
ROOT_DIR=`readlink -f ${SCRIPT_DIR}/..`
CURRENT_DIR=`pwd`
FMT_CMD="find . -regex '.*\.\(cpp\|hpp\|cc\|cxx\|c\|h\|hh\)' -exec clang-format -style=file -i {} \;"

cd ${ROOT_DIR}/include
eval "$FMT_CMD"
cd ${ROOT_DIR}/test
eval "$FMT_CMD"
cd ${ROOT_DIR}/component_tests
eval "$FMT_CMD"
cd ${CURRENT_DIR}
