# Changelog

## 2025.1
### Features
- Use atomic_flag for progam exit requests, as used in signal handlers, and
    use functions to access the flag, to ensure listeners are always notified.
- Adds GenericJsonObject, an opaque type for holding any JSON data.
- Update to SPDLOG 1.15.1
- Allow aborting JSON-RPC procedure calls by throwing AbortProcedureCallHandling()
- Lockable now directly holds the object, instead of a unique_ptr to the object.
    This removes the need for the user to do a nullptr check, and also affects
    RingBuffer.

## 2023.0
### Features
- Switch to using git tag and CalVer for versions
- JSON arms::json::toJsonString(...) now accepts all compatible types
    (eg, std::vector), not only user defined classes with toJson member
    functions.
- Change the values of LogLevel enum, now higher values for more verbosity.
    The values make more sense when passed to other processes.
- Update SPDLOG to version 1.12.
- Adds some functionality to arms::files, including reading a file into a 
    generic container.
- JSONRPC A Batch can be composed of multiple APIs of Methods. The method
    processBatch can now process a batch containing different sets of methods.
    These must be compatible, i.e. any common method name must have the same
    parameters and return types. Typename Batch::M is changed to Batch::Methods.
- JSONRPC adds static checks with unit tests for incompatible API between Batch
    and Procedures, and duplicate method names.
- applyWithRecovery now does not terminate if doRecovery() or onSuccess() throw
    a NonErrorException. Also any type thrown by toApply() is rethrown intact,
    not just std::exception.

## 0.1.1
### Bugfixes
- Storing JSON Null type is fixed.
- JSONRPC void return types now produce "result":null, instead of neither a
    result or error.
- Fixed typo in type data of tts_datablocks.hpp (g_elementTypesData).

## 0.1.0

### Added
- meson.build files.
- VERSION file. This must contain just the armoury version in the format X.Y.Z
