## Third party header only libraries

### glm

```
Source : https://github.com/g-truc/glm
Release: 1.0.1
```

### Boost MP11

```
Source : https://github.com/boostorg/mp11
Branch : master
Commit : ec581685d89a21c62c3c6095d54065c5fa94253e
```

### RapidJSON

```
Source : https://github.com/Tencent/rapidjson
Branch : master
Commit : e0f68a435610e70ab5af44fc6a90523d69b210b3
```

### spdlog

```
Source : https://github.com/gabime/spdlog
Release: v1.15.1
```

### asio

```
Source : https://github.com/chriskohlhoff/asio.git
Branch : master
Commit : 08a7029cb10c911b6fb66fb015217a431206e52c
```

### doctest

```
Source : https://github.com/doctest/doctest.git
Branch : master
Commit : ae7a13539fb71f270b87eb2e874fbac80bc8dda2
Tag    : v2.4.11
```
