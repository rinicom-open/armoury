#include "Configuration.hpp"
#include <algorithm>
#include <armoury/files.hpp>
#include <armoury/udp.hpp>
#include <climits>
#include <functional>
#include <optional>
#include <random>
#include <string_view>
#include <thread>
#include <vector>

int main(int argc, char *argv[])
{
    arms::signals::initSignals();
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = udpTest::Configuration{configFile};
    arms::logger::setupLogging(config.logLevel, config.logAsync, config.logFile, config.logFileSizeMb,
                               config.logFileCount);

    using random_bytes_engine = std::independent_bits_engine<std::default_random_engine, CHAR_BIT, unsigned char>;
    random_bytes_engine rbe;
    std::vector<unsigned char> data(1'000);

    using Sender = arms::udp::BufferedSender<20u>;

    Sender sender{};
    arms::udp::UdpEndpoint endpoint{config.destinationAddress, config.udpPortOut};

    auto recovery_attempts = 5;
    while (recovery_attempts--)
    {
        auto iterations = 1000;
        while (iterations--)
        {
            std::generate(begin(data), end(data), std::ref(rbe));
            sender.sendData(endpoint, begin(data), end(data), std::chrono::seconds{0});
        }
        sender.attemptRecovery();
    }

    if (sender.hasErrors())
    {
        arms::throwLog("sender has errors after recovery");
    }

    std::generate(begin(data), end(data), std::ref(rbe));
    sender.sendData(endpoint, begin(data), end(data), std::chrono::seconds{0});
    std::this_thread::sleep_for(std::chrono::milliseconds{100});

    if (sender.hasErrors())
    {
        arms::throwLog("sender has errors after final send");
    }
}
