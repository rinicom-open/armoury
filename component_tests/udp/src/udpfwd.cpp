/**
 * Forward udp traffic from a source port (configured in defaults.conf) to a destination address and port
 **/

#include "Configuration.hpp"
#include <armoury/files.hpp>
#include <armoury/signals.hpp>
#include <armoury/time.hpp>
#include <armoury/udp.hpp>
#include <optional>
#include <string_view>
#include <thread>

int main(int argc, char *argv[])
{
    arms::signals::initSignals();

    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = udpTest::Configuration{configFile};
    arms::logger::setupLogging(config.logLevel, config.logAsync, config.logFile, config.logFileSizeMb,
                               config.logFileCount);

    arms::log<arms::LOG_INFO>("port in:{}, port out:{}", config.udpPortIn, config.udpPortOut);
    arms::log<arms::LOG_INFO>("bind address:{}, destination address:{}", config.bindAddress, config.destinationAddress);

    auto listenerPtr = std::make_unique<arms::udp::Receiver>(config.bindAddress, config.udpPortIn);
    auto &listener = *listenerPtr;

    arms::udp::Sender sender{};
    arms::udp::UdpEndpoint endpoint{config.destinationAddress, config.udpPortOut};

    std::atomic_size_t packetCount{};
    std::atomic_size_t totalBytesReceived{};
    arms::udp::BufferOwner<arms::udp::IncomingBuffer::capacity(), arms::udp::EndpointBuffer> bufferOwner{};

    std::thread bandwidthReport{[&, bytes = totalBytesReceived.load(), last = arms::time::steadyNow()]() mutable {
        while (!arms::signals::shouldExit())
        {
            auto now = arms::time::steadyNow();
            if (bytes != totalBytesReceived)
            {
                auto windowBytes = totalBytesReceived - bytes;
                auto windowDurationNs = now - last;
                auto windowDurationSecs =
                    std::chrono::duration_cast<std::chrono::seconds>(std::chrono::nanoseconds{windowDurationNs})
                        .count();
                auto bytesPerSecond = windowBytes / windowDurationSecs;
                arms::log<arms::LOG_INFO>("Rx {} bytes/sec", bytesPerSecond);
                arms::log<arms::LOG_INFO>("Payload count:{}", packetCount.load());
            }
            last = now;
            bytes = totalBytesReceived;
            std::this_thread::sleep_for(std::chrono::seconds{1});
        }
    }};

    for (int i = 0; i < 500; ++i)
    {
        if (arms::signals::shouldExit())
        {
            break;
        }
        enum ForwardResult
        {
            Success,
            FullSendBuffer,
            BadSendBuffer,
            CopyTimedOut,
            CopyError
        };

        ForwardResult forwardResult{Success};
        if (auto payloadRef = bufferOwner.getBufferScope(std::chrono::milliseconds{10}))
        {
            if (auto *bufferPtr = payloadRef->get())
            {
                using Result = arms::udp::OperationResult;
                if (auto result = listener.copyBuffer(*bufferPtr, std::chrono::milliseconds{10});
                    result == Result::Success)
                {
                    ++packetCount;
                    totalBytesReceived += bufferPtr->getSize();
                    sender.sendPayloadData(endpoint, *payloadRef);
                }
                else
                {
                    if (result == Result::TimedOut)
                    {
                        forwardResult = CopyTimedOut;
                    }
                    else
                    {
                        forwardResult = CopyError;
                    }
                }
            }
            else
            {
                forwardResult = BadSendBuffer;
            }
        }
        else
        {
            // empty payloadRef means "timed out"
            forwardResult = FullSendBuffer;
        }

        switch (forwardResult)
        {
        case Success: {
            break;
        }
        case FullSendBuffer: {
            arms::log<arms::LOG_INFO>("Full send buffer. Errors:{}", bufferOwner.getErrorCount());
            {
                sender.cancelSends();
                sender.checkNetworkThreadFailed();
                bufferOwner.clearErrors();
            }
            listener.checkNetworkThreadFailed();
            break;
        }
        case BadSendBuffer: {
            arms::throwLog("Fatal error in send buffers");
            break;
        }
        case CopyTimedOut: {
            // Not an error to not receive udp packets within a timeout period
            break;
        }
        case CopyError: {
            arms::throwLog("Fatal error in receive buffers");
            break;
        }
        }
    }

    arms::signals::requestProgramExit();

    arms::log<arms::LOG_INFO>("Packet count:{}, Bytes:{}, Errors:{}", packetCount.load(), totalBytesReceived.load(),
                              bufferOwner.getErrorCount());
    bandwidthReport.join();
}
