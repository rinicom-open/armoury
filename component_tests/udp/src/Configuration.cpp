#include "Configuration.hpp"

#include <armoury/logger.hpp>
#include <limits>

namespace
{
unsigned short limitSetting(unsigned short &value, unsigned int readValue, char const *setting)
{
    if (readValue > std::numeric_limits<unsigned short>::max())
    {
        arms::log<arms::LOG_ERROR>("Setting:{} value:{} exceeds maximum:{}.  Reverting to default:{}", setting,
                                   readValue, std::numeric_limits<unsigned short>::max(), value);
    }
    else
    {
        value = readValue;
    }
    return value;
}
unsigned short getSetting(udpTest::ConfigLoader &loader, unsigned short &value, char const *setting)
{
    unsigned int readValue = value;
    loader.getSetting(readValue, setting);
    return limitSetting(value, readValue, setting);
}
} // namespace

namespace udpTest
{
Configuration::Configuration(std::optional<std::string> const &configFile)
{
    if (configFile.has_value())
    {
        ConfigLoader loader{configFile.value()};
        loadAllSettings(loader);
    }
}

Configuration::Configuration(ConfigLoader &loader)
{
    loadAllSettings(loader);
}

void Configuration::loadAllSettings(ConfigLoader &loader)
{
    loader.getSetting(logLevel, "log_level");
    loader.getSetting(logFile, "log_file");
    loader.getSetting(logFileSizeMb, "log_file_size_mb");
    loader.getSetting(logFileCount, "log_file_count");
    loader.getSetting(logAsync, "log_async");

    loader.getSetting(bindAddress, "bind_ip_address");
    loader.getSetting(destinationAddress, "destination_ip_address");

    getSetting(loader, udpPortIn, "udp_port_in");
    getSetting(loader, udpPortOut, "udp_port_out");

    loader.getArray(additionalBinds, "additional_binds");

    loader.getSetting(payloadSize, "payload_size");
}

Configuration::AdditionalBind::AdditionalBind(libconfig::Setting const &setting)
{
    if (setting.isGroup() && setting.lookupValue("ip", address))
    {
        unsigned int readValue = port;
        setting.lookupValue("port", readValue);
        port = limitSetting(port, readValue, "port");
        return;
    }
    arms::throwLog("AdditionalBind config parse error");
}

} // namespace udpTest
