/**
 * Set the `payload_size` in `defaults.conf` to test the maximum payload size.  The received payload is compared with
 * the sent payload.
 **/

#include "Configuration.hpp"
#include <algorithm>
#include <armoury/files.hpp>
#include <armoury/udp.hpp>
#include <climits>
#include <functional>
#include <optional>
#include <random>
#include <spdlog/fmt/bundled/format.h>
#include <spdlog/fmt/bundled/ranges.h>
#include <string_view>
#include <thread>
#include <vector>

namespace
{
using random_bytes_engine = std::independent_bits_engine<std::default_random_engine, CHAR_BIT, unsigned char>;

using data_type = std::vector<unsigned char>;

} // namespace

int main(int argc, char *argv[])
{
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = udpTest::Configuration{configFile};
    arms::logger::setupLogging(config.logLevel, config.logAsync, config.logFile, config.logFileSizeMb,
                               config.logFileCount);

    random_bytes_engine rbe;
    std::vector<unsigned char> data(config.payloadSize);
    std::generate(begin(data), end(data), std::ref(rbe));

    arms::udp::BufferedSender<30u> sender{};
    arms::udp::UdpEndpoint endpoint{config.destinationAddress, config.udpPortIn};

    arms::udp::Payload::Data dataBytes{};
    for (auto c : data)
    {
        dataBytes.push_back(static_cast<std::byte>(c));
    }

    auto listenerPtr = std::make_unique<arms::udp::Receiver>(config.bindAddress, config.udpPortIn);
    auto &listener = *listenerPtr;
    std::this_thread::sleep_for(std::chrono::seconds{1}); // give the listener a chance to bind.

    std::thread sendThread([&]() { sender.sendData(endpoint, dataBytes.begin(), dataBytes.end()); });

    auto result{0};
    if (auto payload = listener.getPayload())
    {
        auto receivedWhatWeSent = payload->data == dataBytes;
        if (receivedWhatWeSent)
        {
            arms::log<arms::LOG_INFO>("Received back what was sent");
        }
        else
        {
            arms::log<arms::LOG_ERROR>("Sent/Received mismatch");
            arms::log<arms::LOG_INFO>("Outgoing:{}", fmt::join(dataBytes, " "));
            arms::log<arms::LOG_INFO>("Incoming:{}", fmt::join(payload->data, " "));
            if (auto anotherOne = listener.getPayload(std::chrono::seconds{1}))
            {
                arms::log<arms::LOG_INFO>("Addition payload available.  Length:{}", anotherOne->data.size());
            }
            else
            {
                arms::log<arms::LOG_INFO>("No additional payload");
            }
            result = -1;
        }
    }
    else
    {
        arms::log<arms::LOG_WARNING>("Didn't receive udp");
        result = -1;
    }
    sendThread.join();
    arms::log<arms::LOG_INFO>("Listener thread failed?:{}", listener.checkNetworkThreadFailed(false));
    arms::log<arms::LOG_INFO>("Send errors?:{}", sender.hasErrors());

    arms::log<arms::LOG_INFO>("Goodbye\n");
    return result;
}
