/**
 * Demonstrate retrieving data from a ring buffer and sending it over udp
 */

#include "Configuration.hpp"
#include <algorithm>
#include <armoury/files.hpp>
#include <armoury/udp.hpp>
#include <climits>
#include <functional>
#include <optional>
#include <random>
#include <string_view>
#include <thread>
#include <vector>

namespace
{
using random_bytes_engine = std::independent_bits_engine<std::default_random_engine, CHAR_BIT, unsigned char>;

} // namespace

int main(int argc, char *argv[])
{
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = udpTest::Configuration{configFile};
    arms::logger::setupLogging(config.logLevel, config.logAsync, config.logFile, config.logFileSizeMb,
                               config.logFileCount);

    random_bytes_engine rbe;
    using Data = std::vector<unsigned char>;

    arms::RingBuffer<Data, 30> incoming{};
    std::thread producer{[&rbe, &incoming]() {
        for (int i = 0; i < 10; ++i)
        {
            if (auto head = incoming.leader(false, 1'000'000'000))
            {
                head.value().get().assign(1000u, 0);
                std::generate(begin(head.value().get()), end(head.value().get()), std::ref(rbe));
            }
        }
    }};

    using Sender = arms::udp::BufferedSender<20u>;
    Sender sender{};
    arms::udp::UdpEndpoint endpoint{config.destinationAddress, config.udpPortOut};

    for (int i = 0; i < 10; ++i)
    {
        auto head = incoming.follower(false, 1'000'000'000);
        if (head.has_value())
        {
            sender.sendData(endpoint, head.value().get().begin(), head.value().get().end());
        }
        else
        {
            exit(1);
        }
    }

    producer.join();
    arms::log<arms::LOG_INFO>("Goodbye\n");
}
