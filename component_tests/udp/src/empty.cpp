#include "Configuration.hpp"
#include <algorithm>
#include <armoury/files.hpp>
#include <armoury/udp.hpp>
#include <climits>
#include <functional>
#include <optional>
#include <spdlog/fmt/bundled/format.h>
#include <spdlog/fmt/bundled/ranges.h>
#include <string_view>
#include <thread>
#include <vector>

namespace
{
using data_type = std::vector<unsigned char>;

} // namespace

int main(int argc, char *argv[])
{
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = udpTest::Configuration{configFile};
    arms::logger::setupLogging(config.logLevel, config.logAsync, config.logFile, config.logFileSizeMb,
                               config.logFileCount);

    arms::udp::BufferedSender<30u> sender{};
    arms::udp::UdpEndpoint endpoint{config.destinationAddress, config.udpPortIn};

    arms::udp::Payload::Data emptyPayload{};
    if (not emptyPayload.empty())
    {
        arms::log<arms::LOG_ERROR>("Payload not empty");
        exit(1);
    }

    auto listenerPtr = std::make_unique<arms::udp::Receiver>(config.bindAddress, config.udpPortIn);
    auto &listener = *listenerPtr;
    std::this_thread::sleep_for(std::chrono::seconds{1}); // give the listener a chance to bind.

    if (listener.getPayload(std::chrono::seconds{0}))
    {
        arms::log<arms::LOG_ERROR>("Received UDP data when we should not have");
        exit(1);
    }

    std::thread sendThread([&]() { sender.sendData(endpoint, emptyPayload.begin(), emptyPayload.end()); });

    auto result{0};
    if (auto payload = listener.getPayload())
    {
        auto receivedWhatWeSent = payload->data.empty();
        if (receivedWhatWeSent)
        {
            arms::log<arms::LOG_INFO>("Received back an empty udp packet");
        }
        else
        {
            arms::log<arms::LOG_ERROR>("Sent/Received mismatch");
            arms::log<arms::LOG_INFO>("Incoming:{}", fmt::join(payload->data, " "));
            arms::log<arms::LOG_INFO>("Size:{}", payload->data.size());
            result = -1;
        }
    }
    else
    {
        arms::log<arms::LOG_WARNING>("Didn't receive udp");
        result = -1;
    }
    sendThread.join();
    arms::log<arms::LOG_INFO>("Listener thread failed?:{}", listener.checkNetworkThreadFailed(false));
    arms::log<arms::LOG_INFO>("Send errors?:{}", sender.hasErrors());

    arms::log<arms::LOG_INFO>("Goodbye\n");
    return result;
}
