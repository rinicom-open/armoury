#pragma once

#include <armoury/logger.hpp>
#include <armoury/misc.hpp>
#include <array>
#include <cstdlib>
#include <libconfig.h++>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <vector>

namespace udpTest
{
class ConfigLoader
{
  public:
    explicit ConfigLoader(std::string const &configFile);

    /**
     * @brief Get a setting from the config
     * @tparam T a std::string, one of the arithmetic types supported by libconfig++, or optional thereof.
     * @param var The value to modify if a setting exists
     * @param setting The name of the setting to try and get
     * @return Whether or not var was modified
     *
     * @note Arithmetic types supported by libconfig++ are: bool, int, unsigned int, long, unsigned long, long long,
     * unsigned long long, double and float
     */
    template <typename T>
    bool getSetting(T &var, std::string const &setting) const
    {
        std::lock_guard<std::mutex> lock(m_cfgMutex);
        try
        {
            var = getValue<T>(m_cfg->lookup(setting));
        }
        catch (libconfig::SettingNotFoundException &)
        {
            // var keeps its default, all is OK.
            return false;
        }
        return true;
    }

    /**
     * @brief populate a vector of elements of type T
     * @tparam T a std::string, one of the arithmetic types supported by libconfig++, or optional thereof.
     * @param var The vector to populate
     * @param setting The expected name of the libconfig++ setting
     * @return whether or not the setting was found
     * @note if the setting is found, only the basic exception guarantee is offered.  ie. var may not be in its original
     * state if an exception is subsequently thrown.
     */
    template <typename T>
    bool getArray(std::vector<T> &var, std::string const &setting) const
    {
        std::lock_guard<std::mutex> lock(m_cfgMutex);
        try
        {
            libconfig::Setting const &items = m_cfg->lookup(setting);
            std::size_t length = static_cast<std::size_t>(items.getLength());
            var.clear();
            var.reserve(length);
            for (std::size_t i = 0; i < length; ++i)
            {
                try
                {
                    var.push_back(getValue<T>(items[i]));
                }
                catch (...)
                {
                    arms::throwLog("Error reading item {} of {} for setting {}", i + 1, length, setting);
                }
            }
        }
        catch (libconfig::SettingNotFoundException &)
        {
            // var keeps its default, all is OK.
            return false;
        }
        return true;
    }

    /**
     * @brief populate an array of elements of type T
     * @tparam T a std::string, one of the arithmetic types supported by libconfig++, or optional thereof.
     * @param var The array to populate
     * @param setting The expected name of the libconfig++ setting
     * @return whether or not the setting was found
     * @note if the setting is found, only the basic exception guarantee is offered.  ie. var may not be in its original
     * state if an exception is subsequently thrown.
     */
    template <typename T, std::size_t N>
    bool getArray(std::array<T, N> &var, std::string const &setting) const
    {
        std::lock_guard<std::mutex> lock(m_cfgMutex);
        try
        {
            const libconfig::Setting &items = m_cfg->lookup(setting);
            std::size_t length = static_cast<std::size_t>(items.getLength());
            if (length != N)
            {
                std::string msg{m_configFile + ", " + setting + ": Error reading data for array of size " +
                                std::to_string(N)};
                throw std::runtime_error(msg.c_str());
            }
            for (std::size_t i = 0; i < length; ++i)
            {
                try
                {
                    var.at(i) = getValue<T>(items[i]);
                }
                catch (...)
                {
                    arms::throwLog("Error reading item {} of {} for setting {}", i + 1, length, setting);
                }
            }
        }
        catch (libconfig::SettingNotFoundException &)
        {
            // var keeps its default, all is OK.
            return false;
        }
        return true;
    }

  private:
    template <typename T>
    struct SettingType
    {
        using Type = T;
    };

    template <typename T>
    struct SettingType<std::optional<T>>
    {
        using Type = T;
    };

    /**
     * @brief Metafunction that, if T is optional, returns the underlying type.  Else, it returns the type.
     * @tparam T A type that might be wrapped in a std::optional
     */
    template <typename T>
    using SettingTypeT = typename SettingType<T>::Type;

    template <typename T>
    auto getValue(libconfig::Setting &setting) const
    {
        using Type = SettingTypeT<std::remove_cv_t<T>>;
        static_assert(!std::is_const_v<T> && !std::is_const_v<Type>,
                      "Settings can only be assigned to non-const types");
        static_assert(
            !std::is_pointer_v<std::decay_t<Type>>,
            "The only supported settings are std::string and the arithmetic types supported by libconfig++ ie. bool, "
            "int, unsigned int, long, unsigned long, long long, unsigned long long, double and float");
        return static_cast<Type>(setting);
    }

    std::string m_configFile;
    std::unique_ptr<libconfig::Config> m_cfg;
    mutable std::mutex m_cfgMutex;
};

} // namespace udpTest
