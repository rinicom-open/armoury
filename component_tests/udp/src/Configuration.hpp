#pragma once

#include "ConfigLoader.hpp"
#include <armoury/udp.hpp>
#include <vector>

namespace udpTest
{
struct Configuration
{
    Configuration() = default;
    explicit Configuration(std::optional<std::string> const &configFile);
    explicit Configuration(ConfigLoader &loader);
    void loadAllSettings(ConfigLoader &loader);

    std::string logLevel{"info"};
    std::string logFile{""};
    std::size_t logFileSizeMb{5};
    std::size_t logFileCount{10};
    bool logAsync{false};

    std::string bindAddress{"0.0.0.0"};
    std::string destinationAddress{"127.0.0.1"};

    using UdpPort = arms::udp::UdpEndpoint::PortType;
    UdpPort udpPortIn{3000};
    UdpPort udpPortOut{3001};

    struct AdditionalBind
    {
        AdditionalBind(libconfig::Setting const &setting);

        std::string address{};
        UdpPort port{};
    };
    std::vector<AdditionalBind> additionalBinds{};

    std::size_t payloadSize{1'500};
};
} // namespace udpTest
