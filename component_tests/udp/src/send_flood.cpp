/**
 * Repeatedly send a payload.  This is to check that the memory isn't exhausted when repeatedly sending to eg. an
 * unreachable destination.
 */

#include "Configuration.hpp"
#include <algorithm>
#include <armoury/files.hpp>
#include <armoury/udp.hpp>
#include <climits>
#include <functional>
#include <optional>
#include <random>
#include <string_view>
#include <thread>
#include <vector>

namespace
{
// #define LEAKY_SEND

using random_bytes_engine = std::independent_bits_engine<std::default_random_engine, CHAR_BIT, unsigned char>;

// PayloadScope needs a reference counted type, with smart pointer-like interface to access an Element
// eg. shared_ptr<Element>
// The Element needs a setError(asio::error_code) method and a function to convert an
// Element to asio::buffer ie. getAsioBuffer(Element&) -> asio::buffer

template <typename T>
struct ElementAdapter
{
    using Error = asio::error_code;

    void setError(Error e)
    {
        error = std::move(e);
    }

    T data{};
    Error error{};
};

template <typename T>
auto makePayloadScope(T &&t)
{
    using Element = ElementAdapter<std::decay_t<T>>;
    return std::make_shared<Element>(std::forward<T>(t));
}

using data_type = std::vector<unsigned char>;

[[maybe_unused]] inline auto getAsioBuffer(ElementAdapter<data_type> &buffer)
{
    return asio::buffer(buffer.data);
}
} // namespace

int main(int argc, char *argv[])
{
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = udpTest::Configuration{configFile};
    arms::logger::setupLogging(config.logLevel, config.logAsync, config.logFile, config.logFileSizeMb,
                               config.logFileCount);

    random_bytes_engine rbe;
    std::vector<unsigned char> data(1'000);
    std::generate(begin(data), end(data), std::ref(rbe));

#ifdef LEAKY_SEND
    using Sender = arms::udp::Sender;
#else
    using Sender = arms::udp::BufferedSender<20u>;
#endif

    Sender sender{};
    arms::udp::UdpEndpoint endpoint{config.destinationAddress, config.udpPortOut};

    arms::udp::Payload::Data dataBytes{};
    for (auto c : data)
    {
        dataBytes.push_back(static_cast<std::byte>(c));
    }

#ifdef LEAKY_SEND
    for (int i = 0; i < 10'000; ++i)
    {
        sender.sendPayloadData(endpoint, makePayloadScope(data));
    }
#else
    for (int i = 0; i < 10'000; ++i)
    {
        sender.sendData(endpoint, dataBytes.begin(), dataBytes.end());
    }
#endif

    arms::log<arms::LOG_INFO>("Goodbye\n");
}
