/**
 * Echo received udp to the standard output
 */

#include "Configuration.hpp"
#include <armoury/files.hpp>
#include <armoury/udp.hpp>
#include <optional>
#include <string_view>
#include <thread>

int main(int argc, char *argv[])
{
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = udpTest::Configuration{configFile};
    arms::logger::setupLogging(config.logLevel, config.logAsync, config.logFile, config.logFileSizeMb,
                               config.logFileCount);

    arms::udp::UdpEndpoints endpoints{};
    endpoints.emplace_back(config.bindAddress, config.udpPortIn);
    for (auto endpoint : config.additionalBinds)
    {
        endpoints.emplace_back(endpoint.address, endpoint.port);
    }
    arms::udp::Receiver listener{endpoints};
    if (auto message = listener.getPayload(std::chrono::seconds(5)))
    {
        if (message->endpoint)
        {
            arms::log("Payload received on endpoint:{}", message->endpoint->toString());
        }
        auto payload = std::string_view{reinterpret_cast<char const *>(message->data.data()), message->data.size()};
        arms::log("UDP payload {}", payload);
    }
    else
    {
        arms::log("No UDP message received");
    }

    arms::log<arms::LOG_INFO>("Goodbye\n");
}
