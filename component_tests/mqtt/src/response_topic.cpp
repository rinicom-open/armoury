#include "ConfigLoader.hpp"
#include <armoury/files.hpp>
#include <armoury/jsonrpc.hpp>
#include <armoury/logger.hpp>
#include <armoury/misc.hpp>
#include <armoury/mqtt.hpp>
#include <armoury/mqtt/loadconfig.hpp>
#include <array>
#include <bitset>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <libconfig.h++>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <string_view>
#include <thread>
#include <vector>

namespace responseTopicTest
{
struct Configuration
{
    Configuration() = default;
    explicit Configuration(std::optional<std::string> const &configFile);
    explicit Configuration(selfTest::ConfigLoader &loader);
    void loadAllSettings(selfTest::ConfigLoader &loader);

    std::string requestTopic{"request"};
    std::string responseTopic{"response"};

    arms::mqtt::MqttInfo mqttInfo{};
};

Configuration::Configuration(std::optional<std::string> const &configFile)
{
    if (configFile.has_value())
    {
        selfTest::ConfigLoader loader{configFile.value()};
        loadAllSettings(loader);
    }
}

Configuration::Configuration(selfTest::ConfigLoader &loader)
{
    loadAllSettings(loader);
}

void Configuration::loadAllSettings(selfTest::ConfigLoader &loader)
{
    loader.getSetting(requestTopic, "request_topic");
    loader.getSetting(responseTopic, "response_topic");
    arms::mqtt::loadConfig(loader, mqttInfo);
}
} // namespace responseTopicTest

namespace
{
// NB. json Rpc requires c++20
// clang-format off
using mockApi = arms::json::Methods<
    arms::json::Rpc<"tv_on", int(arms::json::Param<"channel", int>, arms::json::Param<"subtitles", int>)>,
    arms::json::Rpc<"tv_off", int()>
>;
// clang-format on

template <arms::StringLiteral Method, typename... Args>
auto makeRequestJson(arms::json::RpcId id, Args &&...args)
{
    arms::json::Batch<mockApi> batch{};
    batch.addRequest<Method>(id, std::forward<Args>(args)...);
    return batch.makeRequestJson();
}

void server(int argc, char *argv[])
{
    auto handleRequest = [](auto const &json) {
        auto tvOn = [](int channel, int subtitles) -> int {
            arms::log<arms::LOG_INFO>("tv_on request received.  channel:{} subtitles:{}", channel, subtitles);
            return 1;
        };
        auto tvOff = []() -> int {
            arms::log<arms::LOG_INFO>("tv off request received.");
            return 2;
        };

        arms::json::Procedures<mockApi, 2> requestHandlers{
            arms::json::Func<mockApi, "tv_on">{tvOn},
            arms::json::Func<mockApi, "tv_off">{tvOff},
        };

        arms::json::Batch<mockApi> batch{json};
        requestHandlers.processBatch(batch);
        return batch.finaliseAndMakeResponseJson();
    };

    auto const *configPathString = argc > 1 ? argv[1] : "response.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = responseTopicTest::Configuration{configFile};

    arms::mqtt::MqttInfo info{};
    info = config.mqttInfo;
    info.clientName = "srv-";

    // subscribe to a request topic
    info.subscriptions.push_back(config.requestTopic);
    arms::mqtt::Subscriber requestTopicListener{info};
    waitForConnection(requestTopicListener);
    assert(requestTopicListener.isConnected());
    arms::log<arms::LOG_INFO>("Request topic listener connected");

    // Wait for the publisher for responses to connect to the broker
    arms::log<arms::LOG_INFO>("Connecting Response topic publisher");
    arms::mqtt::Publisher responder{info};
    waitForConnection(responder);
    assert(responder.isConnected());
    arms::log<arms::LOG_INFO>("Response topic publisher connected");

    // wait for request
    if (auto message = requestTopicListener.getMessage())
    {
        auto payload = message->payloadStringView();
        arms::log<arms::LOG_INFO>("topic:{}\tpayload:{}", message->topic, payload);

        if (auto responseJson = handleRequest(std::string{payload}))
        {
            if (message->responseData.hasResponseTopic())
            {
                arms::mqtt::PublishMessageProperties publishMessageProperties{};
                if (message->responseData.hasCorrelationData())
                {
                    publishMessageProperties.setCorrelationData(message->responseData.correlationDataView());
                }
                responder.publish(message->responseData.cloneResponseTopic(), *responseJson, publishMessageProperties);
            }
        }
    }
}
} // namespace

int main(int argc, char *argv[])
{
    using namespace responseTopicTest;
    std::thread serverThread{[argc, argv]() { server(argc, argv); }};

    auto const *configPathString = argc > 1 ? argv[1] : "response.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = Configuration{configFile};

    arms::mqtt::MqttInfo info{};
    info = config.mqttInfo;
    info.clientName = "rpc-";

    // subscribe to a response topic
    info.subscriptions.push_back(config.responseTopic);
    arms::mqtt::Subscriber responseTopicListener{info};
    waitForConnection(responseTopicListener);
    assert(responseTopicListener.isConnected());
    arms::log<arms::LOG_INFO>("Response topic subscriber connected");

    // make a request
    arms::mqtt::Publisher requester{info};
    waitForConnection(requester);
    assert(requester.isConnected());
    arms::log<arms::LOG_INFO>("Request publisher connected");

    // set response topic
    arms::mqtt::PublishMessageProperties publishMessageProperties{};
    publishMessageProperties.setResponseTopic(config.responseTopic);

    // add correlation data
    uint32_t correlationData{0xdeadbeef};
    publishMessageProperties.setCorrelationData(&correlationData, sizeof(correlationData));

    // send the request
    auto const responseId = 23;
    if (auto json = makeRequestJson<"tv_off">(responseId))
    {
        requester.publish(config.requestTopic, *json, publishMessageProperties);
    }

    // wait for the response
    if (auto message = responseTopicListener.getMessage())
    {
        auto payload = message->payloadStringView();
        arms::log<arms::LOG_INFO>("topic:{}\tpayload:{}", message->topic, payload);
        if (message->responseData.hasCorrelationData())
        {
            for (auto byte : message->responseData.correlationDataView())
            {
                arms::log<arms::LOG_INFO>("Correlation byte:{:x}", byte);
            }
            arms::log<arms::LOG_INFO>("Correlation data end");

            if (message->responseData.matchesCorrelationData(&correlationData))
            {
                arms::log<arms::LOG_INFO>("Correlation data matches");
                if (auto response = arms::json::extractResponse<mockApi, "tv_off">(responseId, std::string{payload}))
                {
                    arms::log<arms::LOG_INFO>("Response id matches");
                }
                else
                {
                    arms::log<arms::LOG_WARNING>(
                        "Response id doesn't match!  It should match, seeing as correlation id matches");
                }
            }
            else
            {
                arms::log<arms::LOG_WARNING>("Correlation data doesn't match!");
            }
        }
        else
        {
            arms::log<arms::LOG_INFO>("No correlation data");
        }
    }

    serverThread.join();
    arms::log<arms::LOG_INFO>("Goodbye\n");
} // namespace responseTopicTest
