#include "Configuration.hpp"

#include <armoury/logger.hpp>
#include <armoury/mqtt/loadconfig.hpp>

namespace selfTest
{
Configuration::Configuration(std::optional<std::string> const &configFile)
{
    if (configFile.has_value())
    {
        ConfigLoader loader{configFile.value()};
        loadAllSettings(loader);
    }
}

Configuration::Configuration(ConfigLoader &loader)
{
    loadAllSettings(loader);
}

void Configuration::loadAllSettings(ConfigLoader &loader)
{
    loader.getSetting(topic, "topic");
    loader.getSetting(message, "message");
    loader.getSetting(signalControl, "signal_control");
    loader.getSetting(brokerRetainsMessage, "broker_retains_message");
    loader.getSetting(clearRetainedMessage, "clear_retained_message");
    arms::mqtt::loadConfig(loader, mqttInfo);
}
} // namespace selfTest
