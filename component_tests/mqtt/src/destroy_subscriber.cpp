#include "Configuration.hpp"
#include <armoury/files.hpp>
#include <armoury/mqtt.hpp>
#include <armoury/signals.hpp>
#include <optional>
#include <string_view>
#include <thread>

int main(int argc, char *argv[])
{
    arms::signals::initSignals();

    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = selfTest::Configuration{configFile};

    arms::mqtt::MqttInfo info{};
    info = config.mqttInfo;
    info.clientName = "dtor-";
    info.subscriptions.push_back(config.topic);

    arms::mqtt::Subscriber sub{info};

    // To test, put a breakpoint before SubscriberWorker::work() blocks.  Then slowly step through
    // to let the Subscriber destructor get called in the main thread.
    std::chrono::milliseconds yuck{100};
    std::this_thread::sleep_for(yuck);

    arms::log<arms::LOG_INFO>("Goodbye\n");
}
