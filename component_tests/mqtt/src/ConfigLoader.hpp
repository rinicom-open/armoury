#pragma once

#include <armoury/logger.hpp>
#include <armoury/misc.hpp>
#include <array>
#include <cstdlib>
#include <libconfig.h++>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <vector>

namespace selfTest
{
class ConfigLoader
{
  public:
    explicit ConfigLoader(std::string const &configFile);

    template <typename T>
    bool getSetting(T &var, std::string const &setting) const
    {
        std::lock_guard<std::mutex> lock(m_cfgMutex);
        try
        {
            // LLVM's libc++ doesn't like linking with std::string interface
            if constexpr (std::is_same_v<T, std::string>)
            {
                var = static_cast<char const *>(m_cfg->lookup(setting));
            }
            else if constexpr (arms::isOptional<T>::value)
            {
                var = static_cast<typename T::value_type>(m_cfg->lookup(setting));
            }
            else
            {
                var = static_cast<T>(m_cfg->lookup(setting));
            }
        }
        catch (libconfig::SettingNotFoundException &)
        {
            // var keeps its default, all is OK.
            return false;
        }
        return true;
    }

    /**
     * @brief populate a vector of elements of type T
     * @param var The vector to populate
     * @param setting The expected name of the libconfig++ setting
     * @return whether or not the setting was found
     * @note if the setting is found, only the basic exception guarantee is offered.  ie. var may not be in its original
     * state if an exception is subsequently thrown.
     */
    template <typename T>
    bool getArray(std::vector<T> &var, std::string const &setting) const
    {
        std::lock_guard<std::mutex> lock(m_cfgMutex);
        try
        {
            libconfig::Setting const &items = m_cfg->lookup(setting);
            std::size_t length = static_cast<std::size_t>(items.getLength());
            var.clear();
            var.reserve(length);
            for (std::size_t i = 0; i < length; ++i)
            {
                try
                {
                    var.push_back(static_cast<T>(items[i]));
                }
                catch (...)
                {
                    arms::throwLog("Error reading item {} of {} for setting {}", i + 1, length, setting);
                }
            }
        }
        catch (libconfig::SettingNotFoundException &)
        {
            // var keeps its default, all is OK.
            return false;
        }
        return true;
    }

    /**
     * @brief populate an array of elements of type T
     * @param var The array to populate
     * @param setting The expected name of the libconfig++ setting
     * @return whether or not the setting was found
     * @note if the setting is found, only the basic exception guarantee is offered.  ie. var may not be in its original
     * state if an exception is subsequently thrown.
     */
    template <typename T, std::size_t N>
    bool getArray(std::array<T, N> &var, std::string const &setting) const
    {
        std::lock_guard<std::mutex> lock(m_cfgMutex);
        try
        {
            const libconfig::Setting &items = m_cfg->lookup(setting);
            std::size_t length = static_cast<std::size_t>(items.getLength());
            if (length != N)
            {
                std::string msg{m_configFile + ", " + setting + ": Error reading data for array of size " +
                                std::to_string(N)};
                throw std::runtime_error(msg.c_str());
            }
            for (std::size_t i = 0; i < length; ++i)
            {
                try
                {
                    var.at(i) = static_cast<T>(items[i]);
                }
                catch (...)
                {
                    arms::throwLog("Error reading item {} of {} for setting {}", i + 1, length, setting);
                }
            }
        }
        catch (libconfig::SettingNotFoundException &)
        {
            // var keeps its default, all is OK.
            return false;
        }
        return true;
    }

  private:
    std::string m_configFile;
    std::unique_ptr<libconfig::Config> m_cfg;
    mutable std::mutex m_cfgMutex;
};
} // namespace selfTest
