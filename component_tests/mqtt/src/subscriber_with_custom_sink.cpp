#include "ConfigLoader.hpp"
#include <armoury/mqtt.hpp>
#include <armoury/Semaphore.hpp>
#include <armoury/mqtt/SubscriberWithUnownedSink.hpp>
#include <chrono>
#include <mutex>
#include <string>
#include <string_view>

/**
 * Test:
 * 2 subscribers to 2 separate topics on the same host.
 * 1 publisher publishes to both topics.
 * Subscribers receive messages and transform them into custom shared buffer items
 *
 * Expected behaviour:
 * Both messages are read from the buffer.
 */

using namespace arms::mqtt;

namespace
{
void publisher(int, char *[])
{
    MqttInfo info{};
    info.clientName = "bfpub-";
    Publisher publisher{info};

    waitForConnection(publisher);

    publisher.publish("topic_1", std::string_view{"message_1"});
    publisher.publish("topic_2", std::string_view{"message_2"});
}

struct CustomItem
{
    std::string topic{};
    std::string message{};
};

class CustomItemBuffer
{
    arms::Semaphore semaphore;
    std::mutex m_lock{};
    std::list<CustomItem> m_items{};

  public:
    void add(CustomItem item)
    {
        std::lock_guard lock{m_lock};
        m_items.push_back(std::move(item));
        semaphore.signal();
    }

    CustomItem get()
    {
        if (not semaphore.waitFor(5'000'000'000))
        {
            arms::log<arms::LOG_ERROR>("Timeout waiting for message");
            exit(1);
        }

        std::lock_guard lock{m_lock};
        CustomItem result{};
        if (m_items.empty())
        {
            arms::log<arms::LOG_ERROR>("Buffer is empty");
            exit(1);
        }
        std::exchange(result, std::move(m_items.front()));
        m_items.pop_front();
        return result;
    }
};

inline bool addMessageToBuffer(CustomItemBuffer &messages, mosquitto_message const &message, mosquitto_property const *)
{
    auto message_string_view = std::string_view({static_cast<char const *>(message.payload), message.payloadlen + 0u});
    CustomItem item{.topic = message.topic, .message = std::string(message_string_view)};
    messages.add(std::move(item));
    return true;
}

template <typename Rep, typename Period>
[[nodiscard]] inline std::optional<CustomItem> getMessage(CustomItemBuffer &messages,
                                                          std::chrono::duration<Rep, Period>)
{
    return messages.get();
}

[[maybe_unused]] inline bool checkTopic(CustomItem const &item)
{
    if (item.topic != "topic_1" and item.topic != "topic_2")
    {
        arms::log<arms::LOG_ERROR>("Didn't find expected topic in message.  Topic was {}", item.topic);
        return false;
    }
    else
    {
        return true;
    }
}

[[maybe_unused]] inline bool checkMessage(CustomItem const &item)
{
    if (item.message != "message_1" and item.message != "message_2")
    {
        arms::log<arms::LOG_ERROR>("Didn't find expected payload in message.  Payload was {}", item.message);
        return false;
    }
    else
    {
        return true;
    }
}

[[maybe_unused]] inline bool checkTopic(arms::mqtt::Message const &message)
{
    if (message.topic != "topic_1" and message.topic != "topic_2")
    {
        arms::log<arms::LOG_ERROR>("Didn't find expected topic in message.  Topic was {}", message.topic);
        return false;
    }
    else
    {
        return true;
    }
}

[[maybe_unused]] inline bool checkMessage(arms::mqtt::Message const &message)
{
    if (message.payloadStringView() != "message_1" and message.payloadStringView() != "message_2")
    {
        arms::log<arms::LOG_ERROR>("Didn't find expected payload in message.  Payload was {}",
                                   message.payloadStringView());
        return false;
    }
    else
    {
        return true;
    }
}
} // namespace

int main(int argc, char *argv[])
{
    // using BufferType = MessageBuffer;
    using BufferType = CustomItemBuffer;
    BufferType messages{};

    MqttInfo subInfo1{};
    subInfo1.subscriptions = {"topic_1"};
    subInfo1.clientName = "sub1-";
    SubscriberWithUnownedSink subscriber1{subInfo1, messages};
    waitForConnection(subscriber1);

    MqttInfo subInfo2{};
    subInfo2.subscriptions = {"topic_2"};
    subInfo2.clientName = "sub2-";
    SubscriberWithUnownedSink subscriber2{subInfo2, messages};
    waitForConnection(subscriber2);

    std::thread publisherThread{[argc, argv]() { publisher(argc, argv); }};
    publisherThread.join();

    for (auto i = 0; i < 2; ++i)
    {
        if (auto message = getMessage(messages, std::chrono::seconds{5}))
        {
            if (not checkTopic(*message))
            {
                return 1;
            }
            if (not checkMessage(*message))
            {
                return 1;
            }
        }
    }
    arms::log<arms::LOG_INFO>("Goodbye");
}
