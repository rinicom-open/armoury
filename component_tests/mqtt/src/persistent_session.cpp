#include <armoury/logger.hpp>
#include <armoury/mqtt.hpp>
#include <string>
#include <string_view>

int main()
{
    using namespace arms::mqtt;

    arms::logger::setupLogging("debug", false, "", 0, 0);

    auto getPersistentSubscriberInfo = []() {
        MqttInfo info{};
        info.clientName = "persistent_session_test";
        info.sessionType = SessionType::Persistent;
        info.subscriptions.push_back("test_topic");
        return info;
    };
    // subscribe to a test topic
    {
        Subscriber persistentSubscriberSession{getPersistentSubscriberInfo()};
        waitForConnection(persistentSubscriberSession);
        assert(persistentSubscriberSession.isConnected());
        arms::log<arms::LOG_INFO>("Subscriber connected");

        // Give the subscriber a chance to subscribe after connection
        std::this_thread::sleep_for(std::chrono::seconds{1});

        [[maybe_unused]] auto message = persistentSubscriberSession.getMessage(std::chrono::seconds{0});
        assert(not message);

        // disconnect the subscriber at scope end
    }
    arms::log<arms::LOG_INFO>("Subscriber disconnected");

    // publish to the test topic
    {
        arms::log<arms::LOG_INFO>("Connecting topic publisher");
        MqttInfo publisherInfo{};
        publisherInfo.clientName = "pub-";
        Publisher publisher{publisherInfo};
        waitForConnection(publisher);
        assert(publisher.isConnected());
        arms::log<arms::LOG_INFO>("Response topic publisher connected");

        PublishMessageProperties publishMessageProperties{};
        publishMessageProperties.qos = 1; // qos needs to be > 0 for persistent subscriber sessions

        publisher.publish("test_topic", std::string_view{"test message"}, publishMessageProperties);
    }
    arms::log<arms::LOG_INFO>("Response topic publisher disconnected");

    // subscribe again, expecting to pick up the published message
    {
        // Don't actually need to set the 2nd subscription as persistent, as the last persistent setting carries over.
        // The *next* subscription would not pick up any old messages, however
        Subscriber persistentSubscriberSession{getPersistentSubscriberInfo()};
        waitForConnection(persistentSubscriberSession);
        assert(persistentSubscriberSession.isConnected());
        arms::log<arms::LOG_INFO>("Subscriber reconnected");

        // Give the subscriber a chance to subscribe after connection
        std::this_thread::sleep_for(std::chrono::seconds{1});

        auto message = persistentSubscriberSession.getMessage(std::chrono::seconds{0});
        assert(message);
        if (message)
        {
            arms::log<arms::LOG_INFO>("Subscriber received message:{}", message->payloadStringView());
            assert(message->payloadStringView() == "test message");
            arms::log<arms::LOG_INFO>("Goodbye\n");
            return 0;
        }
        else
        {
            arms::log<arms::LOG_INFO>("Oops\n");
        }
    }
    return 1;
}
