#include "Configuration.hpp"
#include <armoury/files.hpp>
#include <armoury/mqtt.hpp>
#include <armoury/signals.hpp>
#include <optional>
#include <string_view>
#include <thread>

namespace
{
// The publisher just runs through by default, but the code in this anonymous namespace enables the publisher
// to be driven by signals.
// Eg. advance the publisher with `pkill -SIGUSR1 simple-mqtt-pub` from the command line.
// And, if publication fails, you can retry with `pkill -SIGUSR2 simple-mqtt-pub`

/**
 * @brief The Event enum maps signals to state machine events.
 */
enum class Event
{
    Forwards,
    Backwards
};

/**
 * @brief blockUserSignals blocks SIGUSR1 and SIGUSR2 so that they can advance the publisher's state machine
 */
void blockUserSignals()
{
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGUSR1);
    sigaddset(&sigset, SIGUSR2);
    if (pthread_sigmask(SIG_BLOCK, &sigset, nullptr) != 0)
    {
        arms::throwLog("Error calling pthread_sigmask: {}", errno);
    }
}

/**
 * @brief waitForEvent wait to receive a signal, if configured to do so
 * @param waitForSignal whether or not we actually want to wait for a signal
 * @return whether to go forwards or backwards in the publisher's execution
 */
Event waitForEvent(bool waitForSignal)
{
    if (!waitForSignal)
    {
        return Event::Forwards;
    }
    else
    {
        sigset_t sigset;
        sigemptyset(&sigset);
        sigaddset(&sigset, SIGUSR1);
        sigaddset(&sigset, SIGUSR2);

        int signal{};
        if (auto result = sigwait(&sigset, &signal); result != 0)
        {
            arms::throwLog("Cannot wait on signals:{}", strerror(result));
        }

        if (signal == SIGUSR1)
        {
            return Event::Forwards;
        }
        else
        {
            assert(signal == SIGUSR2);
            return Event::Backwards;
        }
    }
}
} // namespace

int main(int argc, char const *const argv[])
{
    // Get program options from config file
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // Construct configuration
    auto const config = selfTest::Configuration{configFile};

    // If the publisher is configured to advance by signals, the default handling of those signals needs to be blocked.
    if (config.signalControl)
    {
        blockUserSignals();
    }

    arms::mqtt::MqttInfo info{};
    info = config.mqttInfo;
    info.clientName = "tls-";

    // Wait for the publisher to connect to the broker
    arms::mqtt::Publisher pub{info};
    pub.waitForConnection(std::chrono::milliseconds{50});
    while (!pub.isConnected())
    {
        arms::log<arms::LOG_INFO>("Not yet connected");
        pub.checkNetworkThreadFailed();
        pub.waitForConnection(std::chrono::milliseconds{50});
    }
    assert(pub.isConnected());

    arms::log<arms::LOG_INFO>("Connected");
    waitForEvent(config.signalControl);

    auto publish = [&](auto message, auto retain) {
        int result{};
        do
        {
            arms::log<arms::LOG_INFO>("Publishing");
            result = pub.publish(config.topic, message, -1, 0, retain);
            pub.attemptErrorRecovery(result);
        } while (waitForEvent(config.signalControl) == Event::Backwards);

        return result;
    };

    // Clear a retained message by publishing an empty retained message to the broker.
    if (config.clearRetainedMessage)
    {
        publish(std::string_view{""}, true);
    }

    // Publish the configured message
    auto result = publish(config.message, config.brokerRetainsMessage);
    arms::log<arms::LOG_INFO>("Publisher exiting with code:{}\t{}", result, mosquitto_strerror(result));
    return result;
}
