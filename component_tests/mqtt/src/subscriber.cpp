#include "Configuration.hpp"
#include <armoury/files.hpp>
#include <armoury/mqtt.hpp>
#include <optional>
#include <string_view>
#include <thread>

int main(int argc, char *argv[])
{
    auto const *configPathString = argc > 1 ? argv[1] : "defaults.conf";
    /*** Get program options from config file ***/
    std::optional<std::string> const configFile{arms::files::findConfigFile(configPathString)};
    if (!configFile)
    {
        arms::throwLog("cannot find config file {}", configPathString);
    }

    // construct configuration
    auto const config = selfTest::Configuration{configFile};

    arms::mqtt::MqttInfo info{};
    info = config.mqttInfo;
    info.clientName = "sub-";
    info.subscriptions.push_back(config.topic);

    arms::mqtt::Subscriber sub{info};

    sub.waitForConnection(std::chrono::milliseconds{50});
    while (!sub.isConnected())
    {
        arms::log<arms::LOG_INFO>("Not yet connected");
        sub.checkNetworkThreadFailed();
        sub.waitForConnection(std::chrono::milliseconds{50});
    }
    assert(sub.isConnected());

    if (auto message = sub.getMessage(std::chrono::seconds(8)))
    {
        auto payload = message->payloadStringView();
        arms::log<arms::LOG_INFO>("topic:{}\tpayload:{}", message->topic, payload);
    }
    else
    {
        arms::log<arms::LOG_INFO>("No message received");
    }

    arms::log<arms::LOG_INFO>("Goodbye\n");
}
