#pragma once

#include "ConfigLoader.hpp"
#include <armoury/mqtt.hpp>
#include <chrono>
#include <optional>
#include <string>
#include <vector>

namespace selfTest
{
struct Configuration
{
    Configuration() = default;
    explicit Configuration(std::optional<std::string> const &configFile);
    explicit Configuration(ConfigLoader &loader);
    void loadAllSettings(ConfigLoader &loader);

    std::string topic{"test"};
    std::string message{"test message"};
    bool signalControl{false};
    bool brokerRetainsMessage{false};
    bool clearRetainedMessage{false};

    arms::mqtt::MqttInfo mqttInfo{};
};
} // namespace selfTest
