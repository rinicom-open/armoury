#pragma once

#include "time.hpp"

#include <mutex>

namespace arms
{

class Watchdog
{
  public:
    void notify(int64_t reprieveNs = 0)
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        m_deadline = time::steadyNow() + reprieveNs;
    }
    /*
     * is_alive returns true until notify() is first called, then returns true if
     * notify() has been called within the last repreive_ns + timeout_ns nanoseconds.
     */
    bool expired(int64_t timeoutNs = 0)
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        return (m_deadline > 0) && (time::elapsedNanoseconds(m_deadline) > timeoutNs);
    }
    void reset()
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        m_deadline = 0;
    }

  private:
    int64_t m_deadline{0};
    std::mutex m_mtx;
};

} // namespace arms
