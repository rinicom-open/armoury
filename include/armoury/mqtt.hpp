#pragma once

#include <cstdlib>
#include <functional>
#include <list>
#include <optional>
#include <signal.h>
#include <span>
#include <string>
#include <variant>

#include <mosquitto.h>
#include <mqtt_protocol.h>

#include <armoury/RingBuffer.hpp>
#include <armoury/Semaphore.hpp>
#include <armoury/ThreadWarden.hpp>
#include <armoury/logger.hpp>
#include <armoury/misc.hpp>
#include <armoury/mqtt/authentication.hpp>
#include <armoury/mqtt/properties.hpp>
#include <armoury/time.hpp>

namespace arms::mqtt
{
class Context
{
  public:
    Context() = default;
    Context(Context const &) = delete;
    Context &operator=(Context const &) = delete;
    Context(Context &&other) noexcept
        : disconnectProps{std::exchange(other.disconnectProps, nullptr)},
          subscribeProps{std::exchange(other.subscribeProps, nullptr)},
          processMessages{std::exchange(other.processMessages, true)}, subOpts{std::exchange(other.subOpts, 0)},
          msgCount{std::exchange(other.msgCount, 0)}
    {
    }
    Context &operator=(Context &&other) noexcept
    {
        freeContext();
        disconnectProps = std::exchange(other.disconnectProps, nullptr);
        subscribeProps = std::exchange(other.subscribeProps, nullptr);
        processMessages = std::exchange(other.processMessages, true);
        subOpts = std::exchange(other.subOpts, 0);
        msgCount = std::exchange(other.msgCount, 0);
        return *this;
    }
    ~Context()
    {
        freeContext();
    }

    mosquitto_property *disconnectProps{nullptr};
    mosquitto_property *subscribeProps{nullptr};
    bool processMessages{true};
    int subOpts{0};
    int msgCount{0};

  private:
    void freeContext() noexcept
    {
        mosquitto_property_free_all(&disconnectProps);
        mosquitto_property_free_all(&subscribeProps);
    }
};

/**
 * @brief The SessionType enum
 */
enum class SessionType : bool
{
    Persistent = false,
    Clean = true
};

/**
 * @brief The ClientInfo
 * @note If cleanSession=true, first 6 characters of name are the prefix of a 23 character generated id.
 * @note If cleanSession=false, first 23 characters of name are used as the client id
 * @note QoS > 0 is required for persistent sessions to persist missed messages
 */
struct ClientInfo
{
    std::string name;
    bool cleanSession;

    static constexpr std::size_t idPrefixSize{6};
    static constexpr std::size_t idSize{23};

    std::string getId() const
    {
        if (cleanSession)
        {
            // Generate a unique name for non-persistent sessions
            std::string nameCopy = name;
            if (name.length() > idPrefixSize)
            {
                arms::log<arms::LOG_WARNING>(
                    "Only the first {} characters of {} will be used for the generated client id", idPrefixSize, name);
                nameCopy.resize(idPrefixSize);
            }
            arms::RandomIdGenerator idGenerator{};
            std::string id = idGenerator.newId(idSize);
            id.replace(0, nameCopy.length(), nameCopy);

            return id;
        }
        else
        {
            // Use the specified name for persistent sessions, unless beyond the limit defined in the MQTTv5 spec
            // section 3.1.3.1
            // Note that servers such as mosquitto are allowed by the spec to support longer ids.
            std::string nameCopy = name;
            if (name.length() > idSize)
            {
                arms::log<arms::LOG_WARNING>(
                    "Only the first {} characters of {} will be used for the persistent client id", idSize, name);
                nameCopy.resize(idSize);
            }
            return nameCopy;
        }
    }
};

class MqttInfo
{
  public:
    std::vector<std::string> subscriptions;
    std::string host = "localhost";
    std::string bind = "";
    /**
     * @brief clientName the client ID for peristent sessions and a client ID prefix for clean sessions
     * @note see ClientInfo for more details
     */
    std::string clientName = "wman-";
    SessionType sessionType{SessionType::Clean};

    std::optional<TLSSupport> tlsSupport{};
    std::optional<MqttLogin> login{};

    ClientInfo getClientInfo() const
    {
        if (sessionType == SessionType::Persistent && clientName.empty())
        {
            arms::throwLog("A persistent session MUST set a client name");
        }
        return ClientInfo{clientName, arms::enumUnderlyingValue(sessionType)};
    }

    /**
     * @brief getQoS
     * @return The required QoS for the session-type.
     * @note For persistent sessions to have any effect, QoS has to be > 0
     */
    int getQoS() const
    {
        return sessionType == SessionType::Persistent ? 1 : 0;
    }

    int getPort() const
    {
        if (port)
        {
            return *port;
        }
        else if (tlsSupport)
        {
            return 8883;
        }
        else
        {
            return 1883;
        }
    }

    void setPort(int p)
    {
        port = p;
    }

    /**
     * @brief Get keep-alive, in seconds
     * @return seconds to keep alive without contact before broker drops
     */
    int getKeepAlive() const
    {
        return keepalive;
    }

    /**
     * @brief Set keep-alive, in seconds
     * @param k seconds to keep alive without contact before broker drops
     * @note 0 disables keep-alive.  Must be between 5 and 65535
     */
    void setKeepAlive(int k)
    {
        if (k == 0)
        {
            // Keep-alive disabled
            keepalive = 0;
        }
        else
        {
            // Allowable range derived from mosquitto docs and source.
            keepalive = std::min(65535, std::max(5, k));
        }
    }

    void configure(mosquitto *mosq, std::string_view clientId, ClientKeyPasswordProvider pwCallback) const
    {
        if (login)
        {
            auto const rc = mosquitto_username_pw_set(mosq, login->username.c_str(), login->password.c_str());
            if (rc > 0)
            {
                arms::throwLog("MQTT: Unable to configure login: {}", mosquitto_strerror(rc));
            }
        }

        if (tlsSupport)
        {
            // If a client key is encrypted, the default behaviour is to try to get the password from stdin, so it's
            // better to provide a custom callback.  Any password stored in TLSClientInfo is provided by the callback.
            std::visit(
                [mosq, pwCallback, clientId](auto const &auth) {
                    auto const rc = auth.configure(mosq, clientId, pwCallback);
                    if (rc > 0)
                    {
                        arms::throwLog("MQTT: Unable to configure client authentication:{} client:{}",
                                       mosquitto_strerror(rc), clientId);
                    }
                },
                *tlsSupport);
        }
    }

    int handleKeyfilePasswordRequest(char *buf, int size) const
    {
        // A password request for a client key should only happen if certificate-based authentication is configured.
        if (tlsSupport)
        {
            return std::visit([buf, size](auto const &auth) { return auth.getClientKeyPassword(buf, size); },
                              *tlsSupport);
        }
        else
        {
            arms::log<arms::LOG_ERROR>(
                "Password requested for a client not configured to use TLS-based authentication");
            assert(false);
            return 0;
        }
    }

  private:
    std::optional<int> port{};
    int keepalive = 500;
};

inline bool operator==(MqttInfo const &lhs, MqttInfo const &rhs)
{
    auto lPort{lhs.getPort()}, rPort{rhs.getPort()};
    auto lKeep{lhs.getKeepAlive()}, rKeep{rhs.getKeepAlive()};
    return std::tie(lhs.subscriptions, lhs.host, lhs.bind, lhs.clientName, lPort, lKeep) ==
           std::tie(rhs.subscriptions, rhs.host, rhs.bind, rhs.clientName, rPort, rKeep);
}

/**
 * @brief The ResponseData class encapsulates any response topic and correlation data in a received message.
 */
class ResponseData
{
  public:
    explicit ResponseData(mosquitto_property const *properties)
        : m_responseTopic{properties}, m_correlationData{properties}
    {
    }

    ResponseData() = default;

    bool hasResponseTopic() const
    {
        return !m_responseTopic.getProperty().empty();
    }

    std::string cloneResponseTopic() const
    {
        return std::string{m_responseTopic.getProperty()};
    }

    /**
     * @brief responseTopicView returns a std::string_view of the response topic.
     * The caller is responsible for ensuring that this is not accessed after the
     * ResponseData goes out of scope.
     */
    std::string_view responseTopicView() const
    {
        return m_responseTopic.getProperty();
    }

    /**
     * @brief correlationDataView gets a span for any correlation data in the response.
     * @return an iterable collection of bytes (specifically, a BinaryDataSpan)
     */
    BinaryDataSpan correlationDataView() const
    {
        return m_correlationData.getProperty();
    }

    bool hasCorrelationData() const
    {
        return !m_correlationData.getProperty().empty();
    }

    /**
     * @brief matchesCorrelationData
     * @param data the data to match with the correlation data
     * @note If there is no correlation data, this function will always return true
     */
    template <typename T>
    bool matchesCorrelationData(T const *data) const
    {
        auto span = m_correlationData.getProperty();
        return 0 == std::memcmp(span.data(), data, span.size());
    }

  private:
    StringProperty<MQTT_PROP_RESPONSE_TOPIC> m_responseTopic;
    BinaryDataProperty<MQTT_PROP_CORRELATION_DATA> m_correlationData;
};

struct Message
{
    Message() = default;
    Message(mosquitto_message const &msg, mosquitto_property const *properties)
        : mid{msg.mid}, topic{msg.topic}, payload{static_cast<std::byte const *>(msg.payload),
                                                  static_cast<std::byte const *>(msg.payload) + msg.payloadlen},
          qos{msg.qos}, retain{msg.retain}, responseData{properties}
    {
    }

    /**
     * @brief payloadStringView treats the payload as a string
     * @return A non-owning string_view of the payload
     * @note Caller's reponsibility to ensure that the return value doesn't outlive the Message payload.
     */
    std::string_view payloadStringView() const
    {
        return {reinterpret_cast<char const *>(payload.data()), payload.size()};
    }

    int mid{0};
    std::string topic{};
    std::vector<std::byte> payload{};
    int qos{0};
    bool retain{false};
    ResponseData responseData{};
};

class MosqWrapper
{
  public:
    explicit MosqWrapper(ClientInfo const &clientInfo)
    {
        std::unique_lock<std::mutex> libLock{g_mqttLibMtx};
        init(clientInfo.getId(), clientInfo.cleanSession);
    }

    // can't deep-copy mosquitto instance, so explicitly delete copying
    MosqWrapper(MosqWrapper const &) = delete;
    MosqWrapper &operator=(MosqWrapper const &rhs) & = delete;

    ~MosqWrapper()
    {
        std::unique_lock<std::mutex> libLock{g_mqttLibMtx};
        mosquitto_destroy(m_mosq);
        mosquitto_lib_cleanup();
    }
    mosquitto *operator()() const
    {
        return m_mosq;
    }
    std::string const &clientId() const
    {
        return m_clientId;
    }

  private:
    /**
     * @brief init Initialises the mosquitto session
     * @param id The id for this moquitto client
     * @param cleanSession Whether or not previous sessions with the same id get cleaned-up, removing any queued
     * messages.
     * @note This method is not thread-safe.  g_mqttLibMtx should be locked when this method is called
     */
    void init(std::string const &id, bool const cleanSession)
    {
        // init and cleanup are ref counted, but not thread-safe.
        mosquitto_lib_init();
        m_clientId = id;
        m_mosq = mosquitto_new(m_clientId.c_str(), cleanSession, nullptr);
        if (!m_mosq)
        {
            mosquitto_lib_cleanup();
            std::string err;
            switch (errno)
            {
            case ENOMEM:
                err = "Error: Out of memory";
                break;
            case EINVAL:
                err = "Error: Invalid id and/or clean_session";
                break;
            }
            arms::throwLog("MQTT: failure initializing {}", err);
        }
        // set client to use MQTT v5
        else if (auto rc = mosquitto_int_option(m_mosq, MOSQ_OPT_PROTOCOL_VERSION, MQTT_PROTOCOL_V5);
                 rc != MOSQ_ERR_SUCCESS)
        {
            arms::throwLog("MQTT: Unable to configure client to use MQTT v5: {}", mosquitto_strerror(rc));
        }
        // set client to use threads
        else if (mosquitto_threaded_set(m_mosq, true) != 0)
        {
            // see https://github.com/eclipse/mosquitto/issues/1282#issuecomment-494341049
            assert(m_mosq); // mosquitto_threaded_set() should only return an error if m_mosq is null.
            arms::throwLog("MQTT: could not configure the mosquitto library to use threads.");
        }
    }
    inline static std::mutex g_mqttLibMtx{};

    std::string m_clientId;
    mosquitto *m_mosq{nullptr};
};

class Support
{
  public:
    Support(MqttInfo const &info, std::string clientId, mosquitto *mosq)
        : m_mosq{mosq}, m_clientId{std::move(clientId)}, m_info{info}
    {
        mosquitto_log_callback_set(m_mosq, logCallback);

        // For a persistent session to persist, mosquitto expects the session expiry to be non-zero
        // https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901048
        if (m_info.sessionType == SessionType::Persistent)
        {
            m_connectProperties.setSessionToNeverExpire();
        }
    }
    ~Support()
    {
        mosquitto_log_callback_set(m_mosq, nullptr);
    }
    int connect()
    {
        arms::log<arms::LOG_INFO>("MQTT: Client:'{}' attempting to connect to host:'{}' port:{}", m_clientId,
                                  m_info.host, m_info.getPort());
        int rc = mosquitto_connect_bind_v5(m_mosq, m_info.host.c_str(), m_info.getPort(), m_info.getKeepAlive(),
                                           m_info.bind.empty() ? nullptr : m_info.bind.c_str(), m_connectProperties());
        if (rc > 0)
        {
            if (rc == MOSQ_ERR_ERRNO)
            {
                std::string err = strerror(errno);
                arms::log<arms::LOG_ERROR>(
                    "MQTT: Client:'{}' unable to connect to host:'{}' port:{} errno:{} reason:{}", m_clientId,
                    m_info.host, m_info.getPort(), errno, err);
            }
            else
            {
                arms::log<arms::LOG_ERROR>(
                    "MQTT: Client:'{}' unable to connect to host:'{}' port:{} error:{} reason:{}", m_clientId,
                    m_info.host, m_info.getPort(), rc, mosquitto_strerror(rc));
            }
            return rc;
        }
        arms::log<arms::LOG_INFO>("MQTT: Client:'{}' connected to host:'{}' port:{}", m_clientId, m_info.host,
                                  m_info.getPort());
        return MOSQ_ERR_SUCCESS;
    }
    void disconnect(int reasonCode)
    {
        m_context.processMessages = false;
        if (m_mosq)
        {
            mosquitto_disconnect_v5(m_mosq, reasonCode, m_context.disconnectProps);
        }
    }

    MqttInfo const &getInfo() const
    {
        return m_info;
    }

    Context const &getContext() const
    {
        return m_context;
    }
    Context &getContext()
    {
        return m_context;
    }

    mosquitto *mosq() const
    {
        return m_mosq;
    }

    template <typename T>
    static int getTLSClientKeyPassword(char *buf, int size, int /*rwflag*/, void *userdata)
    {
        if (auto *client = static_cast<T const *>(mosquitto_userdata(static_cast<mosquitto *>(userdata))))
        {
            return client->handleKeyfilePasswordRequest(buf, size);
        }
        else
        {
            assert(false);
            return 0;
        }
    }

  private:
    static void logCallback(mosquitto * /*mosq*/, void * /*obj*/, int level, char const *str)
    {
        switch (level)
        {
        case MOSQ_LOG_INFO:
            arms::log<arms::LOG_INFO>("MQTT: {}", str);
            break;
        case MOSQ_LOG_NOTICE:
            arms::log<arms::LOG_INFO>("MQTT: {}", str);
            break;
        case MOSQ_LOG_WARNING:
            arms::log<arms::LOG_WARNING>("MQTT: {}", str);
            break;
        case MOSQ_LOG_ERR:
            arms::log<arms::LOG_ERROR>("MQTT: {}", str);
            break;
        case MOSQ_LOG_DEBUG:
            arms::log<arms::LOG_DEBUG>("MQTT: {}", str);
            break;
        default:
            arms::log("MQTT: {}", str);
            break;
        }
    }

    mosquitto *m_mosq{nullptr};
    std::string m_clientId{};
    Context m_context{};
    MqttInfo m_info{};
    ConnectProperties m_connectProperties{};
};

using MessageBuffer = arms::RingBuffer<Message, 1024>;

inline bool addMessageToBuffer(MessageBuffer &messages, mosquitto_message const &message,
                               mosquitto_property const *properties)
{
    auto buffer = messages.leader(true);
    if (buffer.has_value())
    {
        buffer.value() = Message(message, properties);
        return true;
    }
    return false;
}

[[nodiscard]] inline std::optional<Message> getMessageFromBuffer(MessageBuffer &messages, int64_t timeoutNs,
                                                                 bool skipToLatest = false)
{
    auto buffer = messages.follower(skipToLatest, timeoutNs);
    if (buffer.has_value())
    {
        return buffer.value();
    }
    else
    {
        return std::nullopt;
    }
}

template <typename MessageBufferType>
concept SubscriberSink = requires(MessageBufferType buffer) {
    {
        addMessageToBuffer(buffer, std::declval<mosquitto_message>(), static_cast<mosquitto_property *>(nullptr))
    } -> std::convertible_to<bool>;
};

template <SubscriberSink MessageBufferType = MessageBuffer>
class SubscriberWorkerImpl
{
  public:
    using ThisType = SubscriberWorkerImpl<MessageBufferType>;

    SubscriberWorkerImpl(MqttInfo info, std::string const &clientId, mosquitto *mosq, MessageBufferType &buffer,
                         arms::Semaphore &connected, std::function<void()> onDataReady)
        : m_support{std::move(info), clientId, mosq}, m_clientId{clientId}, m_buffer{buffer}, m_connected{connected},
          m_onDataReady{std::move(onDataReady)}, m_mosquittoCleanup{std::in_place, m_support.mosq(), this}
    {
        m_support.getInfo().configure(m_support.mosq(), m_clientId, m_support.getTLSClientKeyPassword<ThisType>);

        if (m_support.connect() != MOSQ_ERR_SUCCESS)
        {
            arms::throwLog("MQTT: failure connecting worker:{} client:{} host:'{}' port:{}", g_workerName, m_clientId,
                           m_support.getInfo().host, m_support.getInfo().getPort());
        }
    }
    ~SubscriberWorkerImpl()
    {
        m_mosquittoCleanup = std::nullopt;

        // Ordinarily, another disconnect call will be responsible for reaching the destructor.
        // But in the case where work() was never called, the constructor will have connected, and so a corresponding
        // disconnect() is required.
        // If work() finished due to some fatal error, it's expected that disconnect() will silently fail.
        m_support.disconnect(0);
    }
    int handleKeyfilePasswordRequest(char *buf, int size) const
    {
        return m_support.getInfo().handleKeyfilePasswordRequest(buf, size);
    }

    void onMessage(mosquitto_message const &message, mosquitto_property const *properties)
    {
        bool dataReady = addMessageToBuffer(m_buffer, message, properties);

        if (dataReady && m_onDataReady)
        {
            m_onDataReady();
        }
    }

    // thread_warden interface
    static constexpr char const *g_workerName{"mqtt_sub"};
    static constexpr bool g_copyDataOnce{true};
    struct Input
    {
    } dataIn;
    struct Output
    {
    } dataOut;
    int work()
    {
        int rc = mosquitto_loop_forever(m_support.mosq(), -1, 1);
        if (rc != 0)
        {
            arms::log<arms::LOG_ERROR>("MQTT: error:{} worker:{} client: {}", mosquitto_strerror(rc), g_workerName,
                                       m_clientId);
        }
        return rc;
    }
    // end of thread_warden interface

  private:
    static void messageCallback(mosquitto * /*mosq*/, void *obj, mosquitto_message const *message,
                                mosquitto_property const *properties)
    {
        assert(obj);
        auto &ths = *static_cast<ThisType *>(obj);
        if (ths.m_support.getContext().processMessages)
        {
            if (message)
            {
                ths.onMessage(*message, properties);
            }
            else
            {
                // This shouldn't be possible.  See implementation in mosquitto/lib/handle_publish.c
                arms::log<arms::LOG_ERROR>("MQTT: null message received");
                assert(false);
            }
        }
    }
    static void subscribeCallback(mosquitto * /*mosq*/, void *obj, int mid, int qosCount, int const *grantedQos)
    {
        assert(obj);
        auto &ths = *static_cast<ThisType *>(obj);
        std::string str = fmt::format("Client {0} subscribed (mid: {1}) QOS: ", ths.m_clientId, mid);
        for (int i{0}; i < qosCount; ++i)
        {
            str.append(std::to_string(grantedQos[i]) + " ");
        }
        arms::log("MQTT: {}", str);
    }

    static void connectCallback(mosquitto * /*mosq*/, void *obj, int result, int /*flags*/,
                                mosquitto_property const * /*properties*/)
    {
        assert(obj);
        auto &ths = *static_cast<ThisType *>(obj);
        if (result == 0)
        {
            ths.m_connected.binarySignal();
            int qos = ths.m_support.getInfo().getQoS();
            for (std::string const &s : ths.m_support.getInfo().subscriptions)
            {
                int subResult = mosquitto_subscribe_v5(ths.m_support.mosq(), nullptr, s.c_str(), qos,
                                                       ths.m_support.getContext().subOpts,
                                                       ths.m_support.getContext().subscribeProps);
                if (subResult != MOSQ_ERR_SUCCESS)
                {
                    arms::log<arms::LOG_ERROR>("Subscription error: {}", mosquitto_reason_string(subResult));
                }
            }
        }
        else
        {
            ths.m_connected.reset();
            if (result == MQTT_RC_UNSUPPORTED_PROTOCOL_VERSION)
            {
                arms::log<arms::LOG_ERROR>("Connection error: {}."
                                           "Try connecting to an MQTT v5 broker.",
                                           mosquitto_reason_string(result));
            }
            else
            {
                arms::log<arms::LOG_ERROR>("Connection error: {}", mosquitto_reason_string(result));
            }
            // A disconnect should cause mosquitto_loop_forever to return.  ThreadWarden will then recreate
            // the NetworkLoopWorker.
            ths.m_support.disconnect(0);
        }
    }

    static void disconnectCallback(mosquitto * /*mosq*/, void *obj, int result,
                                   mosquitto_property const * /*properties*/)
    {
        arms::log<arms::LOG_TRACE>("{} disconnect reason: {}", g_workerName, mosquitto_reason_string(result));
        assert(obj);
        auto &ths = *static_cast<ThisType *>(obj);
        ths.m_connected.reset();
    }

    /**
     * @brief A simple RAII type to ensure that callbacks are deregistered from mosquitto.
     * @note In C++ a class's destructor isn't called if its constructor throws an exception
     */
    class MosquittoCleanup
    {
      public:
        MosquittoCleanup(mosquitto *mosq, ThisType *owner) : m_mosq(mosq)
        {
            mosquitto_user_data_set(m_mosq, owner);
            mosquitto_subscribe_callback_set(m_mosq, subscribeCallback);
            mosquitto_connect_v5_callback_set(m_mosq, connectCallback);
            mosquitto_message_v5_callback_set(m_mosq, messageCallback);
            mosquitto_disconnect_v5_callback_set(m_mosq, disconnectCallback);
        }

        ~MosquittoCleanup()
        {
            mosquitto_subscribe_callback_set(m_mosq, nullptr);
            mosquitto_connect_v5_callback_set(m_mosq, nullptr);
            mosquitto_message_v5_callback_set(m_mosq, nullptr);
            mosquitto_disconnect_v5_callback_set(m_mosq, nullptr);
            mosquitto_user_data_set(m_mosq, nullptr);
        }

      private:
        mosquitto *m_mosq{nullptr};
    };

    Support m_support;
    std::string m_clientId;
    MessageBufferType &m_buffer;
    arms::Semaphore &m_connected;
    std::function<void()> m_onDataReady;
    std::optional<MosquittoCleanup> m_mosquittoCleanup;
};

using SubscriberWorker = SubscriberWorkerImpl<>;

template <typename Worker>
void ensureStop(Worker &worker, mosquitto *mosq)
{
    // The disconnect call stops the mosquitto_loop_forever running in the worker.
    arms::ensureStop(worker, [mosq]() { mosquitto_disconnect_v5(mosq, 0, nullptr); });
}

/**
 * @brief Wait until there's a message from a subscriber.
 * @param messages A (potentially shared) message buffer
 * @return A received message.
 */
[[nodiscard]] inline std::optional<Message> getMessage(MessageBuffer &messages)
{
    return arms::mqtt::getMessageFromBuffer(messages, -1);
}

/**
 * @brief Get a message from a subscriber.
 * @param messages A (potentially shared) message buffer
 * @param A std::chrono::duration within which to retrieve a message.  Pass a 0 duration for no timeout.
 * @return A received message.
 */
template <typename Rep, typename Period>
[[nodiscard]] inline std::optional<Message> getMessage(MessageBuffer &messages,
                                                       std::chrono::duration<Rep, Period> timeout)
{
    return arms::mqtt::getMessageFromBuffer(messages, arms::time::toNanoTicks(timeout));
}

/**
 * @brief Wait for the latest message from a subscriber.
 * @param messages A (potentially shared) message buffer
 * @return A received message.
 */
[[nodiscard]] inline std::optional<Message> getLatestMessage(MessageBuffer &messages)
{
    return arms::mqtt::getMessageFromBuffer(messages, -1, true);
}

/**
 * @brief Get the latest message from a subscriber.
 * @param messages A (potentially shared) message buffer
 * @param A std::chrono::duration within which to retrieve a message.  Pass a 0 duration for no timeout.
 * @return A received message.
 */
template <typename Rep, typename Period>
[[nodiscard]] inline std::optional<Message> getLatestMessage(MessageBuffer &messages,
                                                             std::chrono::duration<Rep, Period> timeout)
{
    return arms::mqtt::getMessageFromBuffer(messages, arms::time::toNanoTicks(timeout), true);
}

[[nodiscard]] inline bool hasMessages(MessageBuffer const &messages)
{
    return not messages.isEmpty();
}

class Subscriber
{
  public:
    Subscriber(MqttInfo info, std::function<void()> onDataReady = nullptr)
        : m_mosq{info.getClientInfo()}, m_buffer{},
          m_worker{info, m_mosq.clientId(), m_mosq(), m_buffer, m_connected, std::move(onDataReady)}
    {
        m_worker.start();
    }
    ~Subscriber()
    {
        ensureStop(m_worker, m_mosq());
    }

    bool isConnected() const
    {
        return m_connected.check();
    }

    template <typename Rep, typename Period>
    bool waitForConnection(std::chrono::duration<Rep, Period> timeout)
    {
        auto ticks = std::chrono::duration_cast<std::chrono::nanoseconds>(timeout).count();
        return m_connected.checkFor(ticks);
    }

    bool checkNetworkThreadFailed(bool restartOnFailure = true)
    {
        bool const failed = m_worker.checkAndRestartOnFailure(restartOnFailure);
        if (failed)
        {
            arms::log<LOG_WARNING>("MQTT: mosquitto subscriber thread failed");
        }
        return failed;
    }

    /**
     * @brief Wait until there's a message from a subscriber.
     * @return A received message.
     */
    [[nodiscard]] std::optional<Message> getMessage()
    {
        return arms::mqtt::getMessage(m_buffer);
    }

    /**
     * @brief Get a message from a subscriber.
     * @param A std::chrono::duration within which to retrieve a message.  Pass a 0 duration for no timeout.
     * @return A received message.
     */
    template <typename Rep, typename Period>
    [[nodiscard]] std::optional<Message> getMessage(std::chrono::duration<Rep, Period> timeout)
    {
        return arms::mqtt::getMessage(m_buffer, timeout);
    }

    /**
     * @brief Wait for the latest message from a subscriber.
     * @return A received message.
     */
    [[nodiscard]] std::optional<Message> getLatestMessage()
    {
        return arms::mqtt::getLatestMessage(m_buffer);
    }

    /**
     * @brief Get the latest message from a subscriber.
     * @param A std::chrono::duration within which to retrieve a message.  Pass a 0 duration for no timeout.
     * @return A received message.
     */
    template <typename Rep, typename Period>
    [[nodiscard]] std::optional<Message> getLatestMessage(std::chrono::duration<Rep, Period> timeout)
    {
        return arms::mqtt::getLatestMessage(m_buffer, timeout);
    }

    [[nodiscard]] bool hasMessages() const
    {
        return arms::mqtt::hasMessages(m_buffer);
    }

    [[nodiscard]] int32_t size() const
    {
        return m_buffer.size();
    }

  private:
    MosqWrapper m_mosq;
    MessageBuffer m_buffer;
    arms::Semaphore m_connected{};
    arms::ThreadWarden<SubscriberWorker, MqttInfo, std::string const &, mosquitto *, MessageBuffer &, arms::Semaphore &,
                       std::function<void()>>
        m_worker;
};

class NetworkLoopWorker
{
  public:
    NetworkLoopWorker(MqttInfo info, std::string const &clientId, mosquitto *mosq, arms::Semaphore &connected)
        : m_support{std::move(info), clientId, mosq}, m_clientId{clientId}, m_connected{connected},
          m_mosquittoCleanup{std::in_place, m_support.mosq(), this}
    {
        m_support.getInfo().configure(m_support.mosq(), m_clientId,
                                      m_support.getTLSClientKeyPassword<NetworkLoopWorker>);

        if (m_support.connect() != MOSQ_ERR_SUCCESS)
        {
            arms::throwLog("MQTT: failure connecting worker:{} client:{} host:'{}' port:{}", g_workerName,
                           m_support.getInfo().clientName, m_support.getInfo().host, m_support.getInfo().getPort());
        }
    }
    ~NetworkLoopWorker()
    {
        m_mosquittoCleanup = std::nullopt;

        // Ordinarily, another disconnect call will be responsible for reaching the destructor.
        // But in the case where work() was never called, the constructor will have connected, and so a corresponding
        // disconnect() is required.
        // If work() finished due to some fatal error, it's expected that disconnect() will silently fail.
        m_support.disconnect(0);
    }
    int handleKeyfilePasswordRequest(char *buf, int size) const
    {
        return m_support.getInfo().handleKeyfilePasswordRequest(buf, size);
    }

    // thread_warden interface
    static constexpr char const *g_workerName{"mqtt_net_loop"};
    static constexpr bool g_copyDataOnce{true};
    struct Input
    {
    } dataIn;
    struct Output
    {
    } dataOut;
    int work()
    {
        int rc = mosquitto_loop_forever(m_support.mosq(), -1, 1);
        if (rc != 0)
        {
            arms::log<arms::LOG_ERROR>("MQTT: error:{} worker:{} client:{}", mosquitto_strerror(rc), g_workerName,
                                       m_support.getInfo().clientName);
        }
        return rc;
    }
    // end of thread_warden interface

  private:
    static void connectCallback(mosquitto * /*mosq*/, void *obj, int result, int /*flags*/,
                                mosquitto_property const * /*properties*/)
    {
        assert(obj);
        NetworkLoopWorker &ths = *static_cast<NetworkLoopWorker *>(obj);
        if (result == MQTT_RC_SUCCESS)
        {
            ths.m_connected.binarySignal();
        }
        else
        {
            if (result == MQTT_RC_UNSUPPORTED_PROTOCOL_VERSION)
            {
                arms::log<arms::LOG_ERROR>("Connection error: {}."
                                           "Try connecting to an MQTT v5 broker.",
                                           mosquitto_reason_string(result));
            }
            else
            {
                arms::log<arms::LOG_ERROR>("Connection error: {}", mosquitto_reason_string(result));
            }
            // A disconnect should cause mosquitto_loop_forever to return.  ThreadWarden will then recreate
            // the NetworkLoopWorker.
            ths.m_support.disconnect(0);
        }
    }
    static void disconnectCallback(mosquitto * /*mosq*/, void *obj, int result,
                                   mosquitto_property const * /*properties*/)
    {
        arms::log<arms::LOG_TRACE>("{} disconnect reason: {}", g_workerName, mosquitto_reason_string(result));
        assert(obj);
        NetworkLoopWorker &ths = *static_cast<NetworkLoopWorker *>(obj);
        ths.m_connected.reset();
    }

    /**
     * @brief A simple RAII type to ensure that callbacks are deregistered from mosquitto.
     * @note In C++ a class's destructor isn't called if its constructor throws an exception
     */
    class MosquittoCleanup
    {
      public:
        MosquittoCleanup(mosquitto *mosq, NetworkLoopWorker *owner) : m_mosq(mosq)
        {
            mosquitto_user_data_set(m_mosq, owner);
            mosquitto_connect_v5_callback_set(m_mosq, connectCallback);
            mosquitto_disconnect_v5_callback_set(m_mosq, disconnectCallback);
        }

        ~MosquittoCleanup()
        {
            mosquitto_connect_v5_callback_set(m_mosq, nullptr);
            mosquitto_disconnect_v5_callback_set(m_mosq, nullptr);
            mosquitto_user_data_set(m_mosq, nullptr);
        }

      private:
        mosquitto *m_mosq{nullptr};
    };

    Support m_support;
    std::string m_clientId;
    arms::Semaphore &m_connected;
    std::optional<MosquittoCleanup> m_mosquittoCleanup;
};

class Publisher
{
  public:
    Publisher(MqttInfo info) : m_mosq{info.getClientInfo()}, m_worker{info, m_mosq.clientId(), m_mosq(), m_connected}
    {
        m_worker.start();
    }

    ~Publisher()
    {
        ensureStop(m_worker, m_mosq());
    }

    /**
     * @brief publish an mqtt payload
     * @param topic
     * @param payload
     * @param maxLen The maximum length of data to be published, if smaller than the actual payload size
     * @param qos the required mqtt qos (0, 1 or 2)
     * @param retain flags whether or not the published message should be retained by a broker.
     */
    template <typename P>
    int publish(std::string const &topic, P const &payload, std::size_t maxLen = -1, int qos = 0, bool retain = false)
    {
        return publish(topic, payload, maxLen, qos, retain, nullptr);
    }

    /**
     * @brief publish an mqtt payload with a response topic and correlation data
     * @param topic
     * @param payload
     * @param publishProperties the required maxLen, qos, retain, response topic and correlation data
     */
    template <typename P>
    int publish(std::string const &topic, P const &payload, PublishMessageProperties const &publishProperties)
    {
        return publish(topic, payload, publishProperties.maxLen, publishProperties.qos, publishProperties.retain,
                       publishProperties());
    }

    void attemptErrorRecovery(int error)
    {
        switch (error)
        {
        case MOSQ_ERR_SUCCESS:
            break;
        case MOSQ_ERR_NO_CONN:
            checkNetworkThreadFailed();
            break;
        case MOSQ_ERR_PROTOCOL:
        case MOSQ_ERR_ERRNO:
        default:
            attemptConnectionRetry();
            break;
        }
    }

    bool isConnected() const
    {
        return m_connected.check();
    }

    template <typename Rep, typename Period>
    bool waitForConnection(std::chrono::duration<Rep, Period> timeout)
    {
        auto ticks = std::chrono::duration_cast<std::chrono::nanoseconds>(timeout).count();
        return m_connected.checkFor(ticks);
    }

    bool checkNetworkThreadFailed(bool restartOnFailure = true)
    {
        bool const failed = m_worker.checkAndRestartOnFailure(restartOnFailure);
        if (failed)
        {
            arms::log<LOG_WARNING>("MQTT: mosquitto network thread failed");
        }
        return failed;
    }

    void attemptConnectionRetry()
    {
        m_connected.reset();
        ensureStop(m_worker, m_mosq());
        m_worker.start();
    }

  private:
    template <typename P>
    int publish(std::string const &topic, P const &payload, std::size_t maxLen, int qos, bool retain,
                mosquitto_property *publishProperties)
    {
        arms::log("MQTT: Publishing message...");
        int mid = 0;
        std::size_t size = sizeof(typename P::value_type) * std::min(maxLen, payload.size());
        int rc = mosquitto_publish_v5(m_mosq(), &mid, topic.c_str(), size, static_cast<void const *>(payload.data()),
                                      qos, retain, publishProperties);
        switch (rc)
        {
        case MOSQ_ERR_SUCCESS:
            arms::log("MQTT: Message published.");
            lastMid = mid;
            break;
        case MOSQ_ERR_NO_CONN:
            arms::log<arms::LOG_ERROR>("MQTT: Message publishing failed: No connection.");
            break;
        case MOSQ_ERR_PROTOCOL:
            arms::log<arms::LOG_ERROR>("MQTT: Message publishing failed: Protocol error.");
            break;
        case MOSQ_ERR_ERRNO:
            arms::log<arms::LOG_ERROR>("MQTT: Message publishing failed, system error: {}.", strerror(errno));
            break;
        default:
            arms::log<arms::LOG_ERROR>("MQTT: Message publishing failed: Error code {}.", rc);
            break;
        }

        return rc;
    }
    MosqWrapper m_mosq;
    arms::Semaphore m_connected{};
    arms::ThreadWarden<NetworkLoopWorker, MqttInfo, std::string const &, mosquitto *, arms::Semaphore &> m_worker;
    int lastMid{0};
};

/**
 * @brief waitForConnection blocks until the mqtt client is flagged as connected
 * @param client The Publisher or Subscriber to wait for
 * @param threadCheckInterval A std::chrono::duration to wait for
 */
template <typename MqttClient, typename Duration = std::chrono::milliseconds>
inline bool waitForConnection(MqttClient &client, Duration threadCheckInterval = Duration{50})
{
    client.waitForConnection(threadCheckInterval);
    while (!client.isConnected())
    {
        client.checkNetworkThreadFailed();
        client.waitForConnection(threadCheckInterval);
    }
    return client.isConnected();
}

} // namespace arms::mqtt
