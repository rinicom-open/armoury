#pragma once

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ctime>
#include <iostream>
#include <optional>
#include <string>
#include <thread>
#include <vector>

namespace arms::time
{
using Clock = std::chrono::system_clock;

using Timestamp = std::chrono::time_point<Clock, std::chrono::nanoseconds>;

inline Timestamp now()
{
    return Clock::now();
}

/*
 * Steady clock tick count in nanoseconds, usually since last boot.
 */
inline int64_t steadyNow()
{
    auto timeSinceEpoch = std::chrono::steady_clock::now().time_since_epoch();
    return std::chrono::duration_cast<std::chrono::nanoseconds>(timeSinceEpoch).count();
}

inline void microSleep(int64_t microseconds)
{
    std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
}

inline void nanoSleep(int64_t nanoseconds)
{
    std::this_thread::sleep_for(std::chrono::nanoseconds(nanoseconds));
}

inline int64_t nanosecondDuration(Timestamp startTimestamp, Timestamp endTimestamp)
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(endTimestamp - startTimestamp).count();
}

inline int64_t elapsedNanoseconds(Timestamp timestamp)
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(now() - timestamp).count();
}

inline int64_t elapsedNanoseconds(int64_t steadyNanosecondsCount)
{
    return steadyNow() - steadyNanosecondsCount;
}

inline Timestamp futureTime(int64_t nanoseconds)
{
    return now() + std::chrono::nanoseconds(nanoseconds);
}

inline Timestamp steadyClockToTimestamp(int64_t steadyNanosecondsCount)
{
    timespec ts;
    Timestamp currentTimestamp = now();
    clock_gettime(CLOCK_MONOTONIC, &ts);
    int64_t currentSteadyCount = ts.tv_sec * 1'000'000'000 + ts.tv_nsec;
    std::chrono::nanoseconds currentSteadyNanoseconds(currentSteadyCount);
    std::chrono::nanoseconds steadyNanoseconds(steadyNanosecondsCount);
    return currentTimestamp - currentSteadyNanoseconds + steadyNanoseconds;
}

inline std::string dateString(Timestamp timestamp)
{
    // auto t_time = std::chrono::system_clock::to_time_t(timestamp);
    auto sinceEpoch = timestamp.time_since_epoch();
    long tTime = std::chrono::duration_cast<std::chrono::seconds>(sinceEpoch).count();

    auto tSec = std::chrono::system_clock::from_time_t(tTime);
    std::chrono::microseconds us = std::chrono::duration_cast<std::chrono::microseconds>(timestamp - tSec);
    std::tm ttm;
    gmtime_r(&tTime, &ttm);
    constexpr std::size_t bufSize = 8;
    char usBuf[bufSize];
    snprintf(usBuf, bufSize, "%06d", static_cast<int>(us.count() % 1'000'000));
    char dateTimeFormat[] = "%Y-%m-%d %H:%M:%S";
    char timeStr[] = "yyyy-mm-dd HH:MM:SS.ffffff";
    std::strftime(timeStr, std::char_traits<char>::length(timeStr), dateTimeFormat, &ttm);
    std::string result(timeStr);
    result.append(".");
    result.append(usBuf);
    result.append(" GMT");
    return result;
}

inline std::string timeString(Timestamp timestamp)
{
    //    auto t_time = std::chrono::system_clock::to_time_t(timestamp);
    auto sinceEpoch = timestamp.time_since_epoch();
    long tTime = std::chrono::duration_cast<std::chrono::seconds>(sinceEpoch).count();

    std::tm ttm;
    gmtime_r(&tTime, &ttm);
    char dateTimeFormat[] = "%H:%M:%S";
    char timeStr[] = "HH:MM:SS";
    std::strftime(timeStr, std::char_traits<char>::length(timeStr) + 1, dateTimeFormat, &ttm);
    std::string result(timeStr);
    return result;
}

inline std::string durationString(int64_t durationNs, int subsecondPrecision = 0)
{
    constexpr int64_t nsInS{1'000'000'000};
    constexpr int64_t nsInMin{60 * nsInS};
    constexpr int64_t nsInHr{60 * nsInMin};
    constexpr int64_t nsInDay{24 * nsInHr};

    std::vector<std::tuple<int64_t, char const *>> values;
    bool negative{durationNs < 0};
    durationNs = std::abs(durationNs);

    int64_t days{durationNs / nsInDay};
    if (days > 0)
    {
        durationNs -= days * nsInDay;
        values.emplace_back(days, "d");
    }
    int64_t hours{durationNs / nsInHr};
    if (hours > 0)
    {
        durationNs -= hours * nsInHr;
        values.emplace_back(hours, "h");
    }
    int64_t mins{durationNs / nsInMin};
    if (mins > 0)
    {
        durationNs -= mins * nsInMin;
        values.emplace_back(mins, "m");
    }
    int64_t secs{durationNs / nsInS};
    if (secs > 0)
    {
        durationNs -= secs * nsInS;
        values.emplace_back(secs, "s");
    }
    if (subsecondPrecision > 0)
    {
        int64_t ms{durationNs / 1000};
        if (ms > 0)
        {
            durationNs -= ms * 1000;
            values.emplace_back(ms, "ms");
        }
        if (subsecondPrecision > 3)
        {
            int64_t us{durationNs / 1000};
            if (us > 0)
            {
                durationNs -= us * 1000;
                values.emplace_back(us, "us");
            }
            if (subsecondPrecision > 6)
            {
                values.emplace_back(durationNs, "ns");
            }
        }
    }

    std::string result{negative ? "-" : ""};
    for (auto it = values.begin(); it != values.end(); ++it)
    {
        auto const &[val, unit] = *it;
        result += std::to_string(val) + " " + unit;
        if (it + 1 != values.end())
        {
            result += ", ";
        }
    }
    if (values.size() == 0)
    {
        result = "0 s";
    }

    return result;
}

class ScopedTimer
{
  public:
    static void defaultLoggerCallback(std::string const &msg)
    {
        std::cout << msg << std::endl;
    }

    ScopedTimer(std::string text = "ScopedTimer", void (*loggerCb)(std::string const &) = defaultLoggerCallback)
        : m_text{text}, m_loggerCb{loggerCb}
    {
        start();
    }
    void start()
    {
        asm volatile("" ::: "memory");
        m_startTime = steadyNow();
        asm volatile("" ::: "memory");
    }
    ~ScopedTimer()
    {
        try
        {
            int64_t endTime;

            asm volatile("" ::: "memory");
            endTime = steadyNow();
            asm volatile("" ::: "memory");

            m_text += " took " + std::to_string(endTime - m_startTime) + " ns.";
            m_loggerCb(m_text);
        }
        catch (...)
        {
        }
    }

  private:
    std::string m_text;
    void (*m_loggerCb)(std::string const &);
    int64_t m_startTime;
};

class RateLimiter
{
  public:
    RateLimiter(int64_t intervalNs, bool catchUp = true)
        : m_previousTime{0}, m_intervalNs{intervalNs}, m_catchUp{catchUp}
    {
    }

    // TODO: Check if catch-up should be limited. At the moment it can spin a lot after a pause.
    //       Is it possible for catch-up to go wrong?
    // return true if we are ahead of time, false if no wait was required.
    bool waitCycle()
    {
        if (m_previousTime == 0)
        {
            m_previousTime = steadyNow();
            return true;
        }
        int64_t now = steadyNow();
        int64_t desiredNext = m_intervalNs + m_previousTime;
        bool inTime = (desiredNext > now);
        if (inTime)
        {
            // std::cout << "RateLimiter: Sleeping for " << desiredNext - now << std::endl;
            nanoSleep(desiredNext - now);
            m_previousTime = m_catchUp ? desiredNext : steadyNow() + m_intervalNs;
        }
        else
        {
            // std::cout << "RateLimiter: Out of time" << std::endl;
            m_previousTime = m_catchUp ? desiredNext : now + m_intervalNs;
        }
        return inTime;
    }

  private:
    int64_t m_previousTime;
    int64_t m_intervalNs;
    bool m_catchUp;
};

class Happening
{
  public:
    // If an activation is currently active, will keep the previous last observation time
    // and use that to compute the delta time. The elapsed time between the previous observation
    // and this new activation is limited to the new lifetime, it cannont grow forever.
    void activate(double value = 1.0, int64_t lifetimeMs = 3000)
    {
        int64_t newActivationTime = arms::time::steadyNow();
        int64_t newLifetimeNs = std::max(int64_t(1), lifetimeMs) * 1'000'000;
        if (newActivationTime >= m_activationTime + m_lifetimeNs)
        {
            m_observationTime = newActivationTime;
        }
        else
        {
            m_observationTime = std::max(newActivationTime - newLifetimeNs, m_observationTime);
        }
        m_value = value;
        m_lifetimeNs = newLifetimeNs;
        m_activationTime = newActivationTime;
    }
    void deactivate()
    {
        m_activationTime = -1;
    }

    double rateDependent()
    {
        int64_t dt = updateObservationTime();
        if (dt == 0)
        {
            return 0.0;
        }
        return m_value * static_cast<double>(dt) / m_lifetimeNs;
    }

    double rateIndependent() const
    {
        int64_t elapsedNs = arms::time::elapsedNanoseconds(m_activationTime);
        if (elapsedNs >= m_lifetimeNs)
        {
            return 0.0;
        }
        return m_value;
    }

    double rateDependentDecay()
    {
        int64_t dt = updateObservationTime();
        if (dt == 0)
        {
            return 0.0;
        }
        double decay{
            std::sqrt(std::max(0.0, 1.0 - static_cast<double>(m_observationTime - m_activationTime) / m_lifetimeNs))};
        return decay * m_value * static_cast<double>(dt) / m_lifetimeNs;
    }

    double rateIndependentDecay() const
    {
        int64_t elapsedNs = arms::time::elapsedNanoseconds(m_activationTime);
        if (elapsedNs >= m_lifetimeNs)
        {
            return 0.0;
        }
        double decay{std::sqrt(std::max(0.0, 1.0 - static_cast<double>(elapsedNs) / m_lifetimeNs))};
        return decay * m_value;
    }

    double rateIndependentPulsating(int64_t periodMs = 500) const
    {
        int64_t elapsedNs = arms::time::elapsedNanoseconds(m_activationTime);
        if (elapsedNs >= m_lifetimeNs)
        {
            return 0.0;
        }
        double pulse{(1.0 + std::cos(static_cast<double>(elapsedNs / 1'000'000) / periodMs * 2.0 * M_PI)) / 2.0};
        double decayScale{std::max(0.0, 1.0 - static_cast<double>(elapsedNs) / m_lifetimeNs)};
        double decay{std::sqrt(decayScale)};
        return m_value * std::clamp(pulse * decay, 0.0, 1.0);
    }

  private:
    int64_t updateObservationTime()
    {
        int64_t currentTime = steadyNow();
        int64_t dt = currentTime - m_observationTime;
        m_observationTime = currentTime;
        if (currentTime >= m_activationTime + m_lifetimeNs)
        {
            return 0;
        }
        return dt;
    }

    int64_t m_lifetimeNs{1};
    double m_value{0.0};
    int64_t m_activationTime{-1};
    int64_t m_observationTime{0};
};

/**
 * @brief Convert a duration type to nanosecond ticks
 * @returns int64_t ticks
 */
template <typename Rep, typename Period>
inline int64_t toNanoTicks(std::chrono::duration<Rep, Period> timeout)
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(timeout).count();
}

} // namespace arms::time
