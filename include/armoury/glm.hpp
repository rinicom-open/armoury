#pragma once

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/string_cast.hpp>

#include <utility>

namespace glm
{
template <typename T>
GLM_FUNC_QUALIFIER tmat4x4<T, defaultp> vulkanInfinitePerspective(T fovy, T aspect, T zNear)
{
    constexpr T perspectiveFlipYArr[16] = {1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
    auto const perspectiveFlipY = glm::make_mat4(perspectiveFlipYArr);
    return perspectiveFlipY * infinitePerspectiveRH(fovy, aspect, zNear);
}

} // namespace glm
