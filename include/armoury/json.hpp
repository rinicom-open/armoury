#pragma once

#include "logger.hpp"
#include "misc.hpp"

#include <boost/mp11.hpp>

#define RAPIDJSON_HAS_STDSTRING 1
#include <rapidjson/document.h>
#include <rapidjson/error/en.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <optional>
#include <type_traits>
#include <variant>

namespace arms::json
{

struct Null
{
    constexpr bool operator==(Null) const noexcept
    {
        return true;
    }
};

enum class ErrorCode : int
{
    PARSE_ERROR = -32700,
    INVALID_REQUEST = -32600,
    METHOD_NOT_FOUND = -32601,
    INVALID_PARAMS = -32602,
    INTERNAL_ERROR = -32603,
    //-32099 to -32000 are implementation-defined server-errors.
    SERVER_ERRORS_BEGIN = -32099,
    SERVER_ERRORS_END = -32000,
    TYPE_ERROR = -32000,
    VALUE_ERROR = -32001,
    // All other values in the range -32768 to -32000 are reserved for future pre-defined errors.
    // Other values in this space (-31999 to +32767) are implementation-defined application errors.
    APPLICATION_ERRORS_BEGIN = -31999,
    APPLICATION_ERRORS_END = 32767
};

inline constexpr std::string_view errorCodeToString(ErrorCode code)
{
    if (code == ErrorCode::PARSE_ERROR)
        return "Parse error";
    else if (code == ErrorCode::INVALID_REQUEST)
        return "Invalid Request";
    else if (code == ErrorCode::METHOD_NOT_FOUND)
        return "Method not found";
    else if (code == ErrorCode::INVALID_PARAMS)
        return "Invalid params";
    else if (code == ErrorCode::INTERNAL_ERROR)
        return "Internal error";
    else if (code == ErrorCode::TYPE_ERROR)
        return "Type error";
    else if (code == ErrorCode::VALUE_ERROR)
        return "Value error";
    else if ((code >= ErrorCode::SERVER_ERRORS_BEGIN) && (code <= ErrorCode::SERVER_ERRORS_END))
        return "Server error";
    else
        return "Application defined error";
}

/*!
 * \brief GenericJsonObject is an opaque type for holding any JSON data. It can be used in load/store
 * calls, allowing JSONs with some unknown structure to be preserved between conversion to and from
 * C++ Objects.
 */
class GenericJsonObject;

/*!
 * \brief Accessor enables the Support base class to access private members of its derived
 * classes. It also has helper members (hasToJson / hasFromJson) that are used for template selection.
 * If toJson and fromJson member functions in the derived class of Support are private,
 * then that class must friend Accessor. Otherwise befriending is not neccessary.
 */
template <typename T>
class Accessor
{
  public:
    template <typename D>
    static void toJson(T const &derived, D &dom)
    {
        return derived.toJson(dom);
    }

    template <typename D>
    static void fromJson(T &derived, D const &dom)
    {
        return derived.fromJson(dom);
    }

    struct ArbitraryClass
    {
    };

    static constexpr bool g_hasToJson = requires(T v, ArbitraryClass a) { v.toJson(a); };

    static constexpr bool g_hasFromJson = requires(T v, ArbitraryClass a) { v.fromJson(a); };
};

// Note that the type parameter to the concept is decayed to a value type as Serialisable types won't typically
// befriend Accessor<T&> etc.
template <typename T>
concept ConvertibleToJson = Accessor<std::remove_cvref_t<T>>::g_hasToJson;

template <typename T>
concept ConvertibleFromJson = Accessor<std::remove_cvref_t<T>>::g_hasFromJson;

template <typename T>
concept Serialisable = ConvertibleFromJson<T> and ConvertibleToJson<T>;

namespace internal
{
template <typename D>
struct Context
{
    D &dom;
    rapidjson::Document::AllocatorType &allocator;
};
} // namespace internal

enum class Requirement
{
    REQUIRED,
    OPTIONAL,
    IGNORE_ERRORS
};

// Note : When loading, null are treated as missing members. In arrays, etc. a null will throw.
template <Requirement requirement = Requirement::REQUIRED, typename D, typename F>
inline bool load(D const &dom, char const *name, F &&valOut);

// Note : A null can be stored by storing a struct Null
template <Requirement requirement = Requirement::REQUIRED, typename D, typename T>
inline void store(internal::Context<D> &ctx, char const *name, T const &valIn);

struct JsonError : public std::exception
{
    JsonError() : JsonError(ErrorCode::INTERNAL_ERROR)
    {
    }

    JsonError(ErrorCode code) : code{static_cast<int>(code)}, message{errorCodeToString(code)}
    {
    }

    template <typename... Args>
    JsonError(ErrorCode code, std::string const &message, Args &&...args)
        : code{static_cast<int>(code)}, message{std::string(errorCodeToString(code)) + ": " +
                                                strings::tryFormat(message, std::forward<Args>(args)...)}
    {
    }

    int code{};
    std::string message{};
    std::optional<std::string> data{};

    [[nodiscard]] virtual char const *what() const noexcept override
    {
        return message.c_str();
    }

  private:
    friend class Accessor<JsonError>;

    template <typename D>
    void toJson(D &dom) const
    {
        store(dom, "code", code);
        store(dom, "message", message);
        store(dom, "data", data);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        load(dom, "code", code);
        load(dom, "message", message);
        load<Requirement::OPTIONAL>(dom, "data", data);
    }
};

namespace internal
{

[[nodiscard]] inline auto documentFromString(std::string_view jsonStr) -> rapidjson::Document
{
    rapidjson::Document d;
    rapidjson::ParseResult pRes = d.Parse<rapidjson::kParseDefaultFlags>(jsonStr.data(), jsonStr.size());
    if (!pRes)
    {
        throw JsonError{ErrorCode::PARSE_ERROR, "{0} at {1}", rapidjson::GetParseError_En(pRes.Code()), pRes.Offset()};
    }
    return d;
}

/*
 * RapidJSON does not provide some functionality for some integer types
 * This includes .Is<T>() and .Get<T>() member functions of DOM values,
 * where T is a short integer.
 * We have to treat these integer types as special cases, loading the
 * values as int64, and checking ranges where appropriate.
 */
template <typename T>
inline constexpr bool isUnsupportedInteger()
{
    return std::disjunction<std::is_same<T, char>, std::is_same<T, signed char>, std::is_same<T, unsigned char>,
                            std::is_same<T, std::int8_t>, std::is_same<T, std::uint8_t>, std::is_same<T, std::int16_t>,
                            std::is_same<T, std::uint16_t>>::value;
}

template <typename F>
inline void loadValue(rapidjson::Value const &domVal, char const *name, F &&val)
{
    using T = std::remove_reference_t<F>;
    if (domVal.IsNull())
    {
        throw JsonError{ErrorCode::PARSE_ERROR, "null not supported for json member {}", name};
    }

    if constexpr (not isUnsupportedInteger<T>())
    {
        if (domVal.Is<T>())
        {
            val = domVal.Get<T>();
            return;
        }
    }

    if (domVal.Is<std::int64_t>())
    {
        if constexpr (std::is_integral_v<T>)
        {
            std::int64_t newVal = domVal.GetInt64();
            if (inRange<T>(newVal))
            {
                val = static_cast<T>(newVal);
                return;
            }
            else
            {
                throw JsonError{ErrorCode::VALUE_ERROR, "out of range: {} ({})", name, newVal};
            }
        }
        else if constexpr (std::is_floating_point_v<T>)
        {
            static_assert(std::numeric_limits<T>::max() > std::numeric_limits<std::int64_t>::max(),
                          "Float range error");
            // The smallest float available can represent an int64 (assuming IEEE-754 32 bit float)
            val = static_cast<T>(domVal.GetInt64());
            return;
        }
    }
    else if (domVal.Is<double>())
    {
        [[maybe_unused]] double dblVal = domVal.GetDouble();
        if constexpr (std::is_integral_v<T>)
        {
            auto optVal = safeFloatingToIntegerConversion<T>(dblVal);
            if (optVal.has_value())
            {
                val = optVal.value();
                return;
            }
            else
            {
                throw JsonError{ErrorCode::VALUE_ERROR, "out of range {} ({})", name, dblVal};
            }
        }
        else if constexpr (std::is_floating_point_v<T>)
        {
            if (dblVal >= std::numeric_limits<T>::lowest() && dblVal <= std::numeric_limits<T>::max())
            {
                val = static_cast<T>(dblVal);
                return;
            }
            else
            {
                throw JsonError{ErrorCode::VALUE_ERROR, "out of range: {} ({})", name, dblVal};
            }
        }
    }
    throw JsonError{ErrorCode::TYPE_ERROR, "{}", name};
}

template <ConvertibleFromJson F>
inline void loadValue(rapidjson::Value const &domVal, char const * /*name*/, F &&val)
{
    Accessor<std::remove_cvref_t<F>>::fromJson(std::forward<F>(val), domVal);
}

template <typename F>
inline void doLoad(rapidjson::Value const &dom, char const *name, F &&valOut);

template <typename F>
inline void loadArray(rapidjson::Value const &dom, char const *name, F &&arrayOut)
{
    using T = std::remove_reference_t<F>;
    auto jArr = dom.Get<rapidjson::Value::ConstArray>();

    if constexpr (isVector<T>::value)
    {
        arrayOut.clear();
        for (auto const &val : jArr)
        {
            typename T::value_type newVal{};
            doLoad(val, name, newVal);
            arrayOut.push_back(newVal);
        }
    }
    else if constexpr (std::is_array_v<T>)
    {
        static_assert(std::is_bounded_array_v<T>, "Array must be bounded");
        static_assert(std::rank_v<T>, "Array must be single rank");
        static_assert(std::extent_v<T, 0>, "Array must not be zero sized");
        if (jArr.Size() != std::extent_v<T, 0>)
        {
            throw JsonError{ErrorCode::TYPE_ERROR, "array size mismatch: {} (expected size {}, got {})", name,
                            std::size(arrayOut), jArr.Size()};
        }
        for (std::size_t i{}; i < std::extent_v<T, 0>; ++i)
        {
            doLoad(jArr[i], name, arrayOut[i]);
        }
    }
    else if constexpr (isSpan<T>::value)
    {
        if (jArr.Size() != std::size(arrayOut))
        {
            throw JsonError{ErrorCode::TYPE_ERROR, "array size mismatch: {} (expected size {}, got {})", name,
                            std::size(arrayOut), jArr.Size()};
        }
        for (std::size_t i{0}; i < std::size(arrayOut); ++i)
        {
            doLoad(jArr[i], name, arrayOut[i]);
        }
    }
    else
    {
        static_assert(std::tuple_size_v<T> > 0, "For types std::array and std::tuple, size must be greater than 0.");
        if (jArr.Size() != std::tuple_size_v<T>)
        {
            throw JsonError{ErrorCode::TYPE_ERROR, "array size mismatch: {} (expected size {}, got {})", name,
                            std::tuple_size_v<T>, jArr.Size()};
        }

        if constexpr (isTuple<T>::value)
        {
            std::size_t i{0};
            std::apply(
                [&](auto &&...val) {
                    // Note : the comma operator has a strict sequencing guarantee. Therefore the order of
                    // evalution of each call that includes jArr[i++] is as expected.
                    ((doLoad(jArr[i++], name, val)), ...);
                },
                std::forward<F>(arrayOut));
        }
        else
        {
            for (std::size_t i{0}; i < std::tuple_size_v<T>; ++i)
            {
                doLoad(jArr[i], name, arrayOut[i]);
            }
        }
    }
}

template <typename F>
inline void doLoad(rapidjson::Value const &dom, char const *name, F &&valOut)
{
    using T = std::remove_reference_t<F>;
    if constexpr (std::is_same_v<T, GenericJsonObject>)
    {
        valOut.value.CopyFrom(dom, valOut.value.GetAllocator());
    }
    else if constexpr (isOptional<T>::value)
    {
        doLoad(dom, name, valOut.has_value() ? valOut.value() : valOut.emplace());
    }
    else if constexpr (isArray<T>::value || isTuple<T>::value || isVector<T>::value || isSpan<T>::value ||
                       std::is_array_v<T>)
    {
        if (not dom.IsArray())
        {
            throw JsonError{ErrorCode::PARSE_ERROR, "expected an array type"};
        }
        loadArray(dom, name, std::forward<F>(valOut));
    }
    else
    {
        loadValue(dom, name, std::forward<F>(valOut));
    }
}

template <ConvertibleToJson T>
[[nodiscard]] inline rapidjson::Value makeDomValue(T const &t, rapidjson::Document::AllocatorType &allocator)
{
    rapidjson::Value object{};
    object.SetObject();
    Context<rapidjson::Value> newCtx{object, allocator};
    Accessor<T>::toJson(t, newCtx);
    return object;
}

// makeDomValue for non-custom types (ie not structs with .toJson)
// This must come after the definition of makeDomValue for custom types so that its recursive calls
// to make arrays of arrays, etc, may make arrays of custom types.
template <typename T>
[[nodiscard]] inline rapidjson::Value makeDomValue(T const &t, rapidjson::Document::AllocatorType &allocator)
{
    if constexpr (std::is_same_v<T, GenericJsonObject>)
    {
        return {t.value, allocator};
    }
    else if constexpr (isOptional<T>::value)
    {
        if (t.has_value())
        {
            return makeDomValue(t.value(), allocator);
        }
        else
        {
            rapidjson::Value result;
            result.SetNull();
            return result;
        }
    }
    else if constexpr (isTuple<T>::value)
    {
        rapidjson::Value jArr{rapidjson::kArrayType};
        std::apply([&](auto &&...v) { ((jArr.PushBack(makeDomValue(v, allocator), allocator)), ...); }, t);
        return jArr;
    }
    else if constexpr (isArray<T>::value || isVector<T>::value || isSpan<T>::value)
    {
        rapidjson::Value jArr{rapidjson::kArrayType};
        for (auto const &v : t)
        {
            jArr.PushBack(makeDomValue(v, allocator), allocator);
        }
        return jArr;
    }
    else if constexpr (std::is_array_v<T>)
    {
        static_assert(std::is_bounded_array_v<T>, "Array must be bounded");
        static_assert(std::rank_v<T>, "Array must be single rank");
        static_assert(std::extent_v<T, 0>, "Array must not be zero sized");
        rapidjson::Value jArr{rapidjson::kArrayType};
        for (std::size_t i{}; i < std::extent_v<T, 0>; ++i)
        {
            jArr.PushBack(makeDomValue(t[i], allocator), allocator);
        }
        return jArr;
    }
    else
    {
        rapidjson::Value val{};
        if constexpr (std::is_same_v<T, Null>)
        {
            val.SetNull();
        }
        else
        {
            if constexpr (isUnsupportedInteger<T>())
            {
                if constexpr (std::is_signed_v<T>)
                {
                    val.Set<int64_t>(t);
                }
                else
                {
                    val.Set<uint64_t>(t);
                }
            }
            else if constexpr (std::is_same_v<T, std::string_view>)
            {
                // Deep copy of the string data.
                val.SetString(t.data(), t.size(), allocator);
            }
            else
            {
                // The allocator will be ignored for arithmetic types, and used to copy c-style and c++ strings.
                val.Set(t, allocator);
            }
        }
        return val;
    }
}
} // namespace internal

template <typename T, typename J>
[[nodiscard]] inline std::string toJsonString(J const &data)
{
    rapidjson::Document doc;
    rapidjson::Value val = internal::makeDomValue(data, doc.GetAllocator());
    doc.CopyFrom(val, doc.GetAllocator());
    rapidjson::StringBuffer buffer;
    T writer{buffer};
    if (not doc.Accept(writer))
    {
        throw JsonError{ErrorCode::PARSE_ERROR};
    }
    return buffer.GetString();
}

template <typename J>
[[nodiscard]] inline std::string toJsonString(J const &data)
{
    return toJsonString<rapidjson::Writer<rapidjson::StringBuffer>, J>(data);
}

template <typename J>
[[nodiscard]] inline std::string toPrettyJsonString(J const &data)
{
    return toJsonString<rapidjson::PrettyWriter<rapidjson::StringBuffer>>(data);
}

template <typename J, typename... Params>
[[nodiscard]] inline J makeWithJson(std::string_view jsonStr, Params &&...p)
{
    rapidjson::Document doc = internal::documentFromString(jsonStr);
    J newData{std::forward<Params>(p)...};
    Accessor<J>::fromJson(newData, doc);
    return newData;
}

// Note : When loading, null are treated as missing members. In arrays, etc. a null will throw.
template <Requirement requirement, typename D, typename F>
inline bool load(D const &dom, char const *name, F &&valOut)
{
    using T = std::remove_reference_t<F>;
    if (not dom.IsObject())
    {
        throw JsonError{ErrorCode::PARSE_ERROR, "expected an object type ({})", name};
    }
    rapidjson::Value::ConstMemberIterator i = dom.FindMember(name);
    bool success = (i != dom.MemberEnd()) && !(i->value.IsNull());

    if constexpr (requirement == Requirement::OPTIONAL)
    {
        if (success)
        {
            internal::doLoad(i->value, name, std::forward<F>(valOut));
        }
    }
    else if constexpr (requirement == Requirement::IGNORE_ERRORS)
    {
        if (success)
        {
            try
            {
                // If an error is thrown, don't accept a partial update of the member.
                T valCpy{valOut};
                internal::doLoad(i->value, name, valCpy);
                valOut = valCpy;
            }
            catch (...)
            {
                success = false;
            }
        }
    }
    else if constexpr (requirement == Requirement::REQUIRED)
    {
        if (success)
        {
            internal::doLoad(i->value, name, std::forward<F>(valOut));
        }
        else
        {
            throw JsonError{ErrorCode::PARSE_ERROR, "required member {} not present", name};
        }
    }
    return success;
}

template <Requirement requirement, typename D, typename T>
inline void store(internal::Context<D> &ctx, char const *name, T const &valIn)
{
    if constexpr (requirement == Requirement::OPTIONAL &&
                  (isVector<T>::value || isSpan<T>::value || isMap<T>::value || std::is_same_v<T, std::string>))
    {
        if (std::size(valIn) == 0)
        {
            return;
        }
    }
    rapidjson::Value domValue;
    try
    {
        domValue = internal::makeDomValue(valIn, ctx.allocator);
    }
    catch (std::exception const &e)
    {
        if constexpr (requirement == Requirement::IGNORE_ERRORS)
        {
            return;
        }
        throw;
    }
    if constexpr (not std::is_same_v<T, Null>)
    {
        if (domValue.IsNull())
        {
            return;
        }
    }
    ctx.dom.AddMember(rapidjson::StringRef(name), domValue, ctx.allocator);
}

class GenericJsonObject
{
  public:
    GenericJsonObject() = default;
    ~GenericJsonObject() = default;
    GenericJsonObject(GenericJsonObject const &other)
    {
        *this = other;
    }
    GenericJsonObject &operator=(GenericJsonObject const &other)
    {
        this->value.CopyFrom(other.value, this->value.GetAllocator());
        return *this;
    }

  private:
    rapidjson::Document value;

    template <typename F>
    friend inline void internal::doLoad(rapidjson::Value const &dom, char const *name, F &&valOut);

    template <typename T>
    friend inline rapidjson::Value internal::makeDomValue(T const &t, rapidjson::Document::AllocatorType &allocator);
};

} // namespace arms::json
