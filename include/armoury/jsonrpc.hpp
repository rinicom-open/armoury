#pragma once

#include "json.hpp"

namespace arms::json
{

/*
 * @brief AbortProcedureCallHandling is type of std::exception.
 * If a AbortProcedureCallHandling is thrown while handling an RPC method call,
 * it will be caught and the RPC request will not be removed from the batch,
 * nor a response added.
 */
struct AbortProcedureCallHandling final
{
};

template <typename R>
class RpcResponse;

template <typename... Ts>
struct Methods;

template <StringLiteral Method, typename... Fs>
class Rpc;

template <typename... Ms>
class Batch;

namespace helpers
{
template <typename>
struct isResponse : std::false_type
{
};

template <typename R>
struct isResponse<RpcResponse<R>> : std::true_type
{
};

template <typename T>
inline constexpr bool isResponseV = isResponse<T>::value;

template <typename T>
inline constexpr bool isMethodsV = isTemplateType<Methods, T>::value;

template <typename>
struct isRpc : std::false_type
{
};

template <StringLiteral S, typename... Ts>
struct isRpc<Rpc<S, Ts...>> : std::true_type
{
};

template <typename T>
inline constexpr bool isRpcV = isRpc<T>::value;

template <Serialisable... V>
class Variant
{
  public:
    using Type = std::variant<V...>;

    template <typename T, typename... Args>
        requires(not std::same_as<std::remove_cvref_t<T>, Variant>)
    explicit Variant(T &&first, Args &&...args) : data{std::forward<T>(first), std::forward<Args>(args)...}
    {
    }

    Variant() = delete;

    Variant(Variant const &) = default;
    Variant &operator=(Variant const &) = default;

    Variant(Variant &&) noexcept = default;
    Variant &operator=(Variant &&) noexcept = default;

    Type data;

  private:
    friend class Accessor<Variant>;

    template <typename... Ms>
    friend class arms::json::Batch;

    void toJson(auto &dom) const
    {
        std::visit([&dom](auto const &v) { v.toJson(dom); }, data);
    }

    void fromJson(auto const &dom)
    {
        std::visit([&dom](auto &v) { v.fromJson(dom); }, data);
    }
};

template <typename M, StringLiteral Str>
using RpcRequestType =
    typename std::remove_pointer_t<std::tuple_element_t<M::getIndex(Str.get()), typename M::RpcPtrs>>;

template <typename M, StringLiteral Str>
using RpcFunctionType =
    typename std::remove_pointer_t<std::tuple_element_t<M::getIndex(Str.get()), typename M::RpcPtrs>>::FunctionType;

template <typename M, StringLiteral Str>
using RpcResponseType =
    typename std::remove_pointer_t<std::tuple_element_t<M::getIndex(Str.get()), typename M::RpcPtrs>>::ResponseType;

template <typename M>
typename M::RequestVariant makeRequestVariant(std::string_view method)
{
    std::size_t idx = M::getIndex(method);
    if (idx < M::size())
    {
        return boost::mp11::mp_with_index<M::size()>(
            idx, [&](auto I) { return typename M::RequestVariant{std::in_place_index<I>}; });
    }
    throw JsonError{ErrorCode::METHOD_NOT_FOUND};
}

template <typename M>
typename M::ResponseVariant makeResponseVariant(std::string_view method)
{
    std::size_t idx = M::getIndex(method);
    if (idx < M::size())
    {
        return boost::mp11::mp_with_index<M::size()>(
            idx, [&](auto I) { return typename M::ResponseVariant{std::in_place_index<I>}; });
    }
    throw JsonError{ErrorCode::METHOD_NOT_FOUND};
}
} // namespace helpers

class RpcId
{
  public:
    using VariantId = std::variant<Null, std::string, std::int64_t>;

    RpcId() = default;
    RpcId(RpcId const &other) = default;
    RpcId &operator=(RpcId const &other) = default;
    RpcId(RpcId &&other) noexcept = default;
    RpcId &operator=(RpcId &&other) noexcept = default;

    RpcId(std::convertible_to<std::string> auto newId)
    {
        id.emplace<std::string>(newId);
    }

    RpcId(std::integral auto newId)
    {
        id.emplace<int64_t>(newId);
    }

    template <typename T>
    RpcId &operator=(T newId)
    {
        id = newId;
        return *this;
    }

    [[nodiscard]] bool isNull() const noexcept
    {
        return std::holds_alternative<Null>(id);
    }

    VariantId id{Null()};

  private:
    friend class Accessor<RpcId>;

    template <StringLiteral Method, typename... Fs>
    friend class Rpc;

    template <typename R>
    friend class RpcResponse;

    template <typename D>
    void toJson(D &dom) const
    {
        std::visit([&](auto &&theId) { store(dom, "id", theId); }, id);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        // Throw a JsonError if the id is present and not null but cannot be
        // loaded as a string or int64_t
        std::string idStr{};
        if (load<Requirement::IGNORE_ERRORS>(dom, "id", idStr))
        {
            id = idStr;
        }
        else
        {
            std::int64_t idNum;
            if (load<Requirement::OPTIONAL>(dom, "id", idNum))
            {
                id = idNum;
            }
        }
    }
};

inline bool operator==(RpcId const &lhs, RpcId const &rhs)
{
    return lhs.id == rhs.id;
}

template <typename R>
class RpcResponse
{
  public:
    using ResultType = R;
    using VoidType = Null;

    static constexpr bool isVoidResult()
    {
        return std::is_void_v<ResultType>;
    }

    static constexpr char const *jsonrpc{"2.0"};
    // Note : std::optional<void> is not possible. struct Null would be translated to Json Null
    std::optional<std::conditional_t<isVoidResult(), VoidType, R>> result;
    std::optional<JsonError> error;
    RpcId id;

  private:
    friend class Accessor<RpcResponse>;

    template <Serialisable... V>
    friend class helpers::Variant;

    template <typename D>
    void toJson(D &dom) const
    {
        store(dom, "jsonrpc", jsonrpc);

        if (error.has_value())
        {
            store(dom, "error", error.value());
        }
        else
        {
            if constexpr (isVoidResult())
            {
                store(dom, "result", VoidType{});
            }
            else
            {
                if (result.has_value())
                {
                    store(dom, "result", result.value());
                }
                else
                {
                    store(dom, "error", JsonError{ErrorCode::INTERNAL_ERROR});
                }
            }
        }
        id.toJson(dom);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        std::string rpcVersion{};
        load(dom, "jsonrpc", rpcVersion);
        if (rpcVersion != jsonrpc)
        {
            throw JsonError{ErrorCode::INVALID_REQUEST, "unsupported JsonRPC version"};
        }
        load<Requirement::OPTIONAL>(dom, "error", error);
        if (not error.has_value())
        {
            if constexpr (isVoidResult())
            {
                result = VoidType();
            }
            else
            {
                load(dom, "result", result);
            }
        }
        id.fromJson(dom);
    }
};

template <StringLiteral PName, typename P>
struct Param
{
    constexpr Param() = default;
    explicit Param(P const &arg) : arg{arg}
    {
    }
    [[nodiscard]] constexpr std::string_view name() const
    {
        return PName.get();
    }

    P arg{};
};

template <typename... Ps>
struct Params;
template <StringLiteral... PNames, typename... Ps>
struct Params<Param<PNames, Ps>...>
{
    using ArgsContainer = std::tuple<std::decay_t<Ps>...>;
    using ParamContainer = std::tuple<Param<PNames, std::decay_t<Ps>>...>;

    Params() = default;

    static std::size_t size()
    {
        return sizeof...(Ps);
    }

    template <typename... FRs>
    void setArgs(FRs &&...frs)
    {
        static_assert(sizeof...(FRs) == sizeof...(Ps), "Wrong number of arguments.");
        if constexpr (sizeof...(Ps) > 0)
        {
            params = ParamContainer{std::forward<FRs>(frs)...};
        }
    }

    ArgsContainer getArgs()
    {
        return std::apply([](auto... p) { return ArgsContainer{(p.arg)...}; }, params);
    };

    ParamContainer params;

  private:
    friend class Accessor<Params>;

    template <typename D>
    void toJson(D &dom) const
    {
        std::apply([&](auto &...p) { (store(dom, p.name().data(), p.arg), ...); }, params);
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        std::apply([&](auto &...p) { (load(dom, p.name().data(), p.arg), ...); }, params);
    }
};

template <StringLiteral Method, typename R, typename... Args>
class Rpc<Method, R(Args...)>;
template <StringLiteral Method, typename R, StringLiteral... PNames, typename... Ps>
class Rpc<Method, R(Param<PNames, Ps>...)>
{
  public:
    using ReturnType = R;
    using ParamTypes = Params<Param<PNames, Ps>...>;
    using ResponseType = RpcResponse<ReturnType>;
    using FunctionType = std::function<R(Ps...)>;

    template <typename T>
        requires helpers::isRpcV<T>
    static consteval bool isCompatible()
    {
        return getMethod().get() != T::getMethod().get() || (std::is_same_v<ReturnType, typename T::ReturnType> &&
                                                             std::is_same_v<ParamTypes, typename T::ParamTypes>);
    }

    template <typename T>
        requires helpers::isMethodsV<T>
    static consteval bool isCompatible()
    {
        return std::apply([](auto... r) { return (isCompatible<std::remove_pointer_t<decltype(r)>>() && ...); },
                          typename T::RpcPtrs());
    }

    /***
     * @brief Provides the number of RPCs in std::tuple<RpcPtrs...> that have a matching method name with this Rpc.
     */
    template <typename T>
        requires helpers::isMethodsV<T>
    static consteval std::size_t countMatchingMethodNames()
    {
        return std::apply(
            [](auto... r) {
                return ((std::remove_pointer_t<decltype(r)>::getMethod().get() == getMethod().get()) + ...);
            },
            typename T::RpcPtrs());
    }

    static_assert(not std::disjunction_v<std::is_pointer<Ps>...>,
                  "No pointers or C-style arrays can be stored in RpcRequest");

    template <typename... FRs>
    void setArgs(FRs &&...frs)
    {
        static_assert(sizeof...(FRs) == sizeof...(Ps),
                      "RcpRequest::setArgs - wrong number of arguments for this method.");
        if constexpr (sizeof...(Ps) > 0)
        {
            ParamTypes params;
            params.setArgs(std::forward<FRs>(frs)...);
            m_params = params;
        }
    }

    static constexpr decltype(Method) getMethod() noexcept
    {
        return Method;
    }

    ResponseType call(FunctionType &fun)
    {
        ResponseType response{};

        if (id.has_value())
        {
            response.id = id.value();
        }

        try
        {
            if constexpr (std::is_same_v<void, ReturnType>)
            {
                doCall(fun);
            }
            else
            {
                response.result = doCall(fun);
            }
        }
        catch (AbortProcedureCallHandling)
        {
            // handled by the caller
            throw;
        }
        catch (JsonError const &e)
        {
            response.error = e;
        }
        catch (std::exception const &e)
        {
            response.error = JsonError{ErrorCode::INTERNAL_ERROR, "{}", e.what()};
        }
        catch (...)
        {
            response.error = JsonError{ErrorCode::INTERNAL_ERROR};
        }

        return response;
    }

    ReturnType doCall(FunctionType &fun)
    {
        if constexpr (sizeof...(Ps) > 0)
        {
            if (m_params.has_value())
            {
                return std::apply(fun, m_params.value().getArgs());
            }
            else
            {
                throw JsonError{ErrorCode::INVALID_PARAMS, "method {}", Method.get()};
            }
        }
        else
        {
            return fun();
        }
    }

    static constexpr char const *jsonrpc{"2.0"};
    std::optional<RpcId> id;

  private:
    friend class Accessor<Rpc>;

    template <Serialisable... V>
    friend class helpers::Variant;

    template <typename D>
    void toJson(D &dom) const
    {
        store(dom, "jsonrpc", jsonrpc);
        store(dom, "method", std::string(getMethod().get()));
        store(dom, "params", m_params);
        if (id.has_value())
        {
            id->toJson(dom);
        }
    }

    template <typename D>
    void fromJson(D const &dom)
    {
        std::string rpcVersion{};
        load(dom, "jsonrpc", rpcVersion);
        if (rpcVersion != jsonrpc)
        {
            throw JsonError{ErrorCode::INVALID_REQUEST, "unsupported jsonrpc version"};
        }

        std::string method{};
        load(dom, "method", method);
        if (method != getMethod().get())
        {
            throw JsonError{ErrorCode::INTERNAL_ERROR, "method mismatch ({} != {})", getMethod().get(), method};
        }

        // If the rpc takes no arguments, there's no args to load.
        // Prevent loading zero-length arrays and tuples,
        // as that would normally be a programming error.
        if constexpr (sizeof...(Ps) > 0)
        {
            // Check if the parameters element is an object
            // We would not get this far if dom is not an object, previous load calls will throw.
            rapidjson::Value::ConstMemberIterator i = dom.FindMember("params");
            bool paramsIsObject = (i != dom.MemberEnd()) && (i->value.IsObject());

            if (paramsIsObject)
            {
                load(dom, "params", m_params);
            }
            else
            {
                // Fall back to loading the parameters as an in-order array
                typename ParamTypes::ArgsContainer args;
                load(dom, "params", args);
                std::apply([&](auto &...a) { setArgs(a...); }, args);
            }
        }

        if (dom.HasMember("id"))
        {
            RpcId newId;
            newId.fromJson(dom);
            id = newId;
        }
    }

    std::optional<ParamTypes> m_params{};
};

template <StringLiteral... S, typename... F>
struct Methods<Rpc<S, F>...>
{
    using RpcPtrs = std::tuple<Rpc<S, F> *...>;
    using RpcFunctionV = std::variant<std::remove_cvref_t<typename Rpc<S, F>::FunctionType>...>;
    using RequestVariant = helpers::Variant<std::remove_cvref_t<Rpc<S, F>>...>;
    using ResponseVariant = helpers::Variant<RpcResponse<std::remove_cvref_t<typename Rpc<S, F>::ReturnType>>...>;

    static_assert(
        std::apply(
            [](auto... rpc) {
                return (
                    (std::remove_pointer_t<decltype(rpc)>::template countMatchingMethodNames<Methods<Rpc<S, F>...>>() ==
                     1) &&
                    ...);
            },
            RpcPtrs()),
        "Method names must be unique in API");

    static constexpr std::size_t size()
    {
        return sizeof...(S);
    }

    /***
     * @brief Check if Methods type M2 is compatible with Methods.
     * @tparam M2
     * @return true if there is no StringLiteral Rpc method name common between both Method types that has a different
     * function signature in each. Otherwise returns false.
     */
    template <typename M2>
    static consteval bool isCompatible()
    {
        return std::apply(
            [&](auto... rpc) { return (std::remove_pointer_t<decltype(rpc)>::template isCompatible<M2>() && ...); },
            RpcPtrs());
    }

    static constexpr std::size_t getIndex(std::string_view key)
    {
        for (std::size_t idx{}; idx < size(); ++idx)
        {
            StringLiteral str = boost::mp11::mp_with_index<size()>(idx, [&](auto I) {
                auto ptr = std::tuple_element_t<I, RpcPtrs>{nullptr};
                return std::remove_pointer_t<decltype(ptr)>::getMethod();
            });
            if (str.get() == key)
            {
                return idx;
            }
        }
        return size();
    }

    template <StringLiteral Method>
    static constexpr std::optional<RequestVariant> makeRequest()
    {
        constexpr std::size_t idx = getIndex(Method.get());
        if (idx == size())
        {
            return {};
        }
        else
        {
            return RequestVariant{std::in_place_index<idx>};
        }
    }

    static ResponseVariant makeErrorResponseVariant(JsonError const &error, RpcId const &id = {})
    {
        static_assert(std::variant_size_v<typename ResponseVariant::Type> > 0);
        ResponseVariant errorResponse(std::in_place_index<0>);
        auto &response = std::get<0>(errorResponse.data);
        response.error = error;
        response.id = id;
        return errorResponse;
    }

    template <typename C>
    static ResponseVariant call(RequestVariant &requestVariant, C &&fun)
    {
        return std::visit(
            [&](auto rpc) -> ResponseVariant {
                if constexpr (std::is_same_v<typename decltype(rpc)::FunctionType, std::remove_cvref_t<C>>)
                {
                    auto response = rpc.call(std::forward<C>(fun));
                    return ResponseVariant{std::in_place_index<getIndex(rpc.getMethod().get())>, response};
                }
                // Should never get here, but needs to be valid code.
                assert(false);
                return makeErrorResponseVariant({ErrorCode::INTERNAL_ERROR, "Mismatched requests and responses."});
            },
            requestVariant.data);
    }
};

// A batch is specific to particular sets of Api1
template <typename... Ms>
class Batch
{
  public:
    using Methods = typename TemplateCat<Ms...>::type;

    Batch() = default;

    explicit Batch(std::string_view jsonStr)
    {
        initFromRequestJson(jsonStr);
    }

    void initFromRequestJson(std::string_view jsonStr)
    {
        requests.clear();
        responses.clear();
        try
        {
            rapidjson::Document doc = internal::documentFromString(jsonStr);
            if (doc.IsArray())
            {
                auto jArr = doc.Get<rapidjson::Value::ConstArray>();
                if (jArr.Size() == 0)
                {
                    throw JsonError{ErrorCode::INVALID_REQUEST};
                }
                for (auto const &val : jArr)
                {
                    addRequestFromDomValue(val);
                }
            }
            else
            {
                addRequestFromDomValue(doc);
            }
        }
        catch (JsonError const &e)
        {
            responses.push_back(Methods::makeErrorResponseVariant(e));
        }
        catch (std::exception const &e)
        {
            responses.push_back(Methods::makeErrorResponseVariant({ErrorCode::PARSE_ERROR, e.what()}));
        }
        catch (...)
        {
            responses.push_back(Methods::makeErrorResponseVariant({ErrorCode::PARSE_ERROR}));
        }
    }

    template <StringLiteral Method, typename... Args>
    void addRequest(RpcId const &id, Args &&...args)
    {
        constexpr std::size_t idx = Methods::getIndex(Method.get());
        typename Methods::RequestVariant request(std::in_place_index<idx>);
        auto &rpcRequest = std::get<idx>(request.data);
        rpcRequest.setArgs(std::forward<Args>(args)...);
        rpcRequest.id = id;
        requests.push_back(request);
    }

    // Retreive the first response in the batch matching a given id.
    // This may be used to check responses before sending a reply.
    // The client should instead use the free function extractResponse.
    template <StringLiteral Method>
    std::optional<helpers::RpcResponseType<Methods, Method>> getResponse(RpcId id)
    {
        try
        {
            constexpr std::size_t idx = Methods::getIndex(Method.get());
            for (auto const &r : responses)
            {
                if (r.data.index() == idx)
                {
                    auto &response = std::get<idx>(r.data);
                    if (response.id == id)
                    {
                        return response;
                    }
                }
            }
        }
        catch (...)
        {
        }
        return std::nullopt;
    }

    void finaliseResponse()
    {
        // Convert any unhandled requests to responses
        for (auto const &request : requests)
        {
            std::optional<RpcId> id = std::visit([&](auto &r) { return r.id; }, request.data);
            if (id.has_value())
            {
                responses.push_back(Methods::makeErrorResponseVariant({ErrorCode::METHOD_NOT_FOUND}, id.value()));
            }
        }
        requests.clear();
    }

    [[nodiscard]] std::optional<std::string> makeRequestJson() const
    {
        return makeJson(requests);
    }

    std::optional<std::string> finaliseAndMakeResponseJson()
    {
        finaliseResponse();
        return makeJson(responses);
    }

    std::vector<typename Methods::RequestVariant> requests;
    std::vector<typename Methods::ResponseVariant> responses;

  private:
    template <typename D>
    void addRequestFromDomValue(D const &val) noexcept
    {
        try
        {
            RpcId id{};
            bool shouldSendErrorResponse = true;
            try
            {
                if (not val.IsObject())
                {
                    throw JsonError{ErrorCode::TYPE_ERROR, "expected an object"};
                }
                std::string method;
                load(val, "method", method);

                if (val.HasMember("id"))
                {
                    RpcId newId;
                    Accessor<RpcId>::fromJson(newId, val);
                    id = newId;
                }
                else
                {
                    shouldSendErrorResponse = false;
                }

                typename Methods::RequestVariant request = helpers::makeRequestVariant<Methods>(method);
                Accessor<typename Methods::RequestVariant>::fromJson(request, val);
                requests.push_back(request);
            }
            catch (JsonError const &e)
            {
                if (shouldSendErrorResponse)
                {
                    responses.push_back(Methods::makeErrorResponseVariant(e, id));
                }
            }
            catch (std::exception const &e)
            {
                if (shouldSendErrorResponse)
                {
                    responses.push_back(Methods::makeErrorResponseVariant({ErrorCode::PARSE_ERROR, e.what()}, id));
                }
            }
            catch (...)
            {
                if (shouldSendErrorResponse)
                {
                    responses.push_back(Methods::makeErrorResponseVariant({ErrorCode::PARSE_ERROR}, id));
                }
            }
        }
        catch (...)
        {
            // although unlikely push_back could throw
            arms::log<LogLevel::LOG_WARNING>("Rogue exception caught during addRequestFromDomValue");
        }
    }

    template <typename T>
    static std::optional<std::string> makeJson(T const &data)
    {
        if (data.empty())
        {
            return {};
        }

        rapidjson::Document doc;
        internal::Context<rapidjson::Document> ctx{doc, doc.GetAllocator()};
        if (data.size() > 1)
        {
            ctx.dom.SetArray();
            for (auto &val : data)
            {
                ctx.dom.PushBack(internal::makeDomValue(val, ctx.allocator), ctx.allocator);
            }
        }
        else if (data.size() == 1)
        {
            ctx.dom.SetObject();
            data[0].toJson(ctx);
        }

        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer{buffer};
        if (not ctx.dom.Accept(writer))
        {
            if constexpr (helpers::isResponseV<T>)
            {
                return R"(
                {"jsonrpc":"2.0", "id":null, "error":{"code":-32603, "message":"Unknown internal error"}}
                )";
            }
            else
            {
                return {};
            }
        }
        return buffer.GetString();
    }
};

namespace internal
{
template <typename M, StringLiteral Method, typename D>
std::optional<helpers::RpcResponseType<M, Method>> extractResponseFromDomValue(D const &val, RpcId const &id) noexcept
{
    try
    {
        if (val.IsObject())
        {
            RpcId domId{};
            Accessor<RpcId>::fromJson(domId, val);
            if (id == domId)
            {
                helpers::RpcResponseType<M, Method> response;
                Accessor<helpers::RpcResponseType<M, Method>>::fromJson(response, val);
                return response;
            }
        }
    }
    catch (...)
    {
    }
    return std::nullopt;
}
} // namespace internal

// Retreive the first response in the batch matching a given id
template <typename P, StringLiteral Method>
std::optional<helpers::RpcResponseType<P, Method>> extractResponse(RpcId id, std::string_view jsonStr)
{
    try
    {
        rapidjson::Document doc = internal::documentFromString(jsonStr);
        if (doc.IsArray())
        {
            auto jArr = doc.Get<rapidjson::Value::ConstArray>();
            for (auto const &val : jArr)
            {
                auto response = internal::extractResponseFromDomValue<P, Method>(val, id);
                if (response.has_value())
                {
                    return response;
                }
            }
        }
        else
        {
            return internal::extractResponseFromDomValue<P, Method>(doc, id);
        }
    }
    catch (...)
    {
    }
    return std::nullopt;
}

template <typename M, StringLiteral Method>
struct Func
{
    using FuncType = helpers::RpcFunctionType<M, Method>;

    template <typename C, typename S = void>
    explicit Func(C callable, [[maybe_unused]] S *s = nullptr)
    {
        if constexpr (std::is_member_function_pointer_v<C>)
        {
            static_assert(not std::is_same_v<void, S>, "An instance must be passed when calling a member function.");
            assert(s);
            function = std::bind_front(callable, s);
        }
        else
        {
            function = callable;
        }
    }

    FuncType function;
};

template <typename M, std::size_t N = M::size()>
class Procedures
{
  public:
    using FunctionV = typename M::RpcFunctionV;
    using Keys = std::array<StringLiteral, N>;
    using Data = std::array<FunctionV, N>;

    template <StringLiteral... Methods>
    explicit Procedures(Func<M, Methods>... args)
        : m_keys{Methods...}, m_funcs{FunctionV{std::in_place_index<M::getIndex(Methods.get())>, args.function}...}
    {
        static_assert(sizeof...(Methods) == N, "Incorrect number of callees");
    }

    static constexpr std::size_t size() noexcept
    {
        return N;
    }

    [[nodiscard]] constexpr std::size_t getIndex(std::string_view key) const noexcept
    {
        for (std::size_t i{}; i < size(); ++i)
        {
            if (m_keys[i].get() == key)
            {
                return i;
            }
        }
        return size();
    }

    template <typename... Ms>
    void processBatch(Batch<Ms...> &batch) noexcept
    {
        using ArgMethods = typename Batch<Ms...>::Methods;
        static_assert(M::template isCompatible<ArgMethods>(), "Batch and procedure APIs are not compatible!");
        for (auto request{batch.requests.begin()}; request < batch.requests.end();)
        {
            try
            {
                StringLiteral str = std::visit([](auto &r) { return r.getMethod(); }, request->data);
                std::size_t calleeIdx = getIndex(str.get());
                std::size_t m2Idx = ArgMethods::getIndex(str.get());
                if (calleeIdx < size() && m2Idx < ArgMethods::size())
                {
                    std::visit(
                        [&](auto &&f, auto const &r) {
                            // This is always true if the method sets in the Api1 and batch are compatible, ie:
                            //  - The StringLiteral method names are unique within each set
                            //  - The method names correspond to the same function types in both sets
                            if constexpr (std::is_same_v<std::remove_cvref_t<decltype(f)>,
                                                         typename std::remove_cvref_t<decltype(r)>::FunctionType>)
                            {
                                typename ArgMethods::ResponseVariant response = ArgMethods::call(*request, f);
                                if (r.id.has_value())
                                {
                                    batch.responses.push_back(response);
                                }
                            }
                        },
                        m_funcs.at(calleeIdx), request->data);

                    request = batch.requests.erase(request);
                    continue;
                }
            }
            catch (AbortProcedureCallHandling)
            {
                // When a handler throws AbortProcedureCallHandling, the current request is not deleted, nor is a
                // response created. The request will remain in the request buffer. No other exceptions can escape
                // the ArgMethods::call function.
            }
            catch (std::exception const &e)
            {
                arms::log<LogLevel::LOG_ERROR>("Exception caught while processing RPC batch: {}", e.what());
            }
            catch (...)
            {
                arms::log<LogLevel::LOG_ERROR>("Unknown exception caught while processing RPC batch!");
            }

            ++request;
        }
    }

  private:
    Keys m_keys;
    Data m_funcs;
};

} // namespace arms::json
