#pragma once

#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <random>
#include <span>
#include <stdexcept>
#include <type_traits>
#include <vector>

namespace arms
{

struct StringLiteral
{
    static constexpr std::size_t capacity = 50;

    template <std::size_t N>
    constexpr StringLiteral(char const (&str)[N]) : n{N - 1}
    {
        static_assert(N <= capacity, "StringLiteral capacity is too small for the string!");
        for (std::size_t i{}; i < N; ++i)
        {
            value[i] = str[i];
        }
    }

    constexpr StringLiteral(StringLiteral const &other) = default;
    constexpr StringLiteral &operator=(StringLiteral const &other) = default;
    constexpr StringLiteral(StringLiteral &&other) noexcept = default;
    constexpr StringLiteral &operator=(StringLiteral &&other) noexcept = default;

    [[nodiscard]] constexpr std::string_view get() const
    {
        return {value, n};
    }

    std::size_t n{};
    char value[capacity]{};
};

template <typename>
struct isTuple : std::false_type
{
};
template <typename... T>
struct isTuple<std::tuple<T...>> : std::true_type
{
};

template <typename>
struct isArray : std::false_type
{
};
template <typename T, std::size_t N>
struct isArray<std::array<T, N>> : std::true_type
{
};

template <typename>
struct isVector : std::false_type
{
};
template <typename T>
struct isVector<std::vector<T>> : std::true_type
{
};

template <typename>
struct isSpan : std::false_type
{
};
template <typename T, std::size_t N>
struct isSpan<std::span<T, N>> : std::true_type
{
};

template <typename>
struct isMap : std::false_type
{
};
template <typename K, typename T>
struct isMap<std::map<K, T>> : std::true_type
{
};

template <typename>
struct isOptional : std::false_type
{
};
template <typename T>
struct isOptional<std::optional<T>> : std::true_type
{
};

template <template <typename...> typename, typename...>
struct isTemplateType : std::false_type
{
};

template <template <typename...> typename Template, typename... Type>
struct isTemplateType<Template, Template<Type...>> : std::true_type
{
};

template <template <typename...> typename Template, typename... Type>
inline constexpr bool isTemplateTypeV = isTemplateType<Template, Type...>::value;

template <typename T>
inline constexpr bool isSharedPtrV = isTemplateType<std::shared_ptr, T>::value;

template <typename T>
inline constexpr bool isTupleV = isTemplateType<std::tuple, T>::value;

template <typename T>
inline constexpr bool isVectorV = isTemplateType<std::vector, T>::value;

template <typename T>
inline constexpr bool isMapV = isTemplateType<std::map, T>::value;

template <typename T>
inline constexpr bool isOptionalV = isTemplateType<std::optional, T>::value;

template <typename T>
inline constexpr bool isFunctionV = isTemplateType<std::function, T>::value;

template <typename, typename = void>
struct IsBasicLockable : std::false_type
{
};

template <typename T>
struct IsBasicLockable<T, std::void_t<decltype(std::declval<T>().lock()), decltype(std::declval<T>().unlock())>>
    : std::true_type
{
};

template <typename T>
inline constexpr bool isBasicLockable = IsBasicLockable<T>::value;

template <typename T>
struct function_traits;

template <typename R, typename... Args>
struct function_traits<std::function<R(Args...)>>
{
    static constexpr size_t nArgs = sizeof...(Args);

    using result_type = R;
    using tuple_args_type = std::tuple<Args...>;

    template <size_t i>
    struct Arg
    {
        using type = typename std::tuple_element<i, tuple_args_type>::type;
    };
};

namespace detail
{
template <typename T, size_t... Is, typename... Args>
inline constexpr std::array<T, sizeof...(Is)> makeArrayOfValueHelper(std::index_sequence<Is...>, Args const &...args)
{
    return {(static_cast<void>(Is), T{args...})...};
}
} // namespace detail

/**
 * @brief make a std::array containing multiples of the same value, constructed using args.
 * This has the advantage (compared to a loop) of supporting non-default contructable types.
 * However, the type must be move-constructable.
 */
template <typename T, size_t N, typename... Args>
inline constexpr std::array<T, N> makeArrayOfValue(Args const &...args)
{
    return detail::makeArrayOfValueHelper<T>(std::make_index_sequence<N>{}, args...);
}

/**
 * @brief Concatenate template arguments. Used in JSON-RPC Batch for combining Methods' arguments:
 * Methods<A,B>, Methods<C,D>  ->  Methods<A,B,C,D>
 */
template <typename T, typename... Empty>
struct TemplateCat
{
    static_assert(sizeof...(Empty) == 0, "TemplateCat couldn't concatenate the templates parameters!");
    using type = T;
};
template <template <typename...> class T, typename... Args1, typename... Args2, typename... Other>
struct TemplateCat<T<Args1...>, T<Args2...>, Other...> : public TemplateCat<T<Args1..., Args2...>, Other...>
{
};

template <typename K, typename V, std::size_t Size>
struct StaticMap
{
    using key_type = K;
    using value_type = V;

    [[nodiscard]] constexpr value_type at(key_type const &key) const
    {
        auto const iter =
            std::find_if(std::begin(data), std::end(data), [&key](auto const &v) { return v.first == key; });
        if (iter != end(data))
        {
            return iter->second;
        }
        else
        {
            throw std::range_error("Item not found");
        }
    }

    std::array<std::pair<K, V>, Size> data{};
};

/**
 * @brief RandomIdGenerator is designed to produce strings that can be used as client ids for MQTT.
 * MQTT servers must accept client id's up to 23 characters long of simple alpha-numeric characters,
 * includes upper and lower case.
 */
class RandomIdGenerator
{
  public:
    std::string newId(std::size_t len = 23)
    {
        std::string id;
        id.reserve(len);
        for (std::size_t i{}; i < len; ++i)
        {
            id.push_back(getChar());
        }
        return id;
    }

  private:
    char getChar()
    {
        return characters[distribution(device)];
    }

    static constexpr std::string_view characters = "0123456789"
                                                   "abcdefghijklmnopqrstuvwxyz"
                                                   "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::uniform_int_distribution<> distribution{0, static_cast<int>(characters.length() - 1)};
    std::mt19937 device{std::random_device{}()};
};

/**
 * @brief relaxedEqual compares the values of two numbers and returns if they are within the precision of
 * the least precise type. It will return true if the values are separated by a single unit of that numerical precision.
 * It is not usually recommended to compare equality of floating point numbers, with or without this function.
 */
template <typename T1, typename T2>
inline constexpr bool relaxedEqual(T1 f1, T2 f2)
{
    if constexpr (std::is_integral_v<T1> && std::is_integral_v<T2>)
    {
        return f1 == f2;
    }
    else if constexpr (std::is_integral_v<T1>)
    {
        return relaxedEqual(static_cast<long double>(f1), f2);
    }
    else if constexpr (std::is_integral_v<T2>)
    {
        return relaxedEqual(f1, static_cast<long double>(f2));
    }
    else if constexpr (sizeof(T1) >= sizeof(T2))
    {
        return (std::nextafter(f2, std::numeric_limits<T2>::lowest()) <= f1 &&
                std::nextafter(f2, std::numeric_limits<T2>::max()) >= f1);
    }
    else
    {
        return (std::nextafter(f1, std::numeric_limits<T1>::lowest()) <= f2 &&
                std::nextafter(f1, std::numeric_limits<T1>::max()) >= f2);
    }
}

/*
 * @brief additionInRange checks if the value of t1 and t2 added together is in the range of type T.
 */
template <typename T, typename T1, typename T2>
    requires std::is_integral_v<T> && std::is_integral_v<T1> && std::is_integral_v<T2>
static bool additionInRange(T1 t1, T2 t2)
{
    static_assert(std::rank_v<std::intmax_t> <= std::rank_v<std::uintmax_t>,
                  "additionInRange assumes uintmax_t has (equal) highest rank of all integers");

    bool st1{t1 < 0};
    std::uintmax_t ut1{st1 ? std::uintmax_t(0) - t1 : t1};

    bool st2{t2 < 0};
    std::uintmax_t ut2{st2 ? std::uintmax_t(0) - t2 : t2};

    std::uintmax_t uLowest = std::uintmax_t(0) - std::numeric_limits<T>::lowest();
    std::uintmax_t uMax = static_cast<std::uintmax_t>(std::numeric_limits<T>::max());

    if (!st1 && !st2)
    {
        // positive t1 and t2
        return ut2 <= uMax - ut1;
    }
    else if (st1 && st2)
    {
        // negative t1, negative t2
        return ut2 <= uLowest - ut1;
    }
    else if (!st1 && st2)
    {
        // positive t1, negative t2
        if (ut1 >= ut2)
        {
            return (ut1 - ut2) <= uMax;
        }
        else
        {
            return (ut2 - ut1) <= uLowest;
        }
    }
    else
    {
        // negative t1, positive t2
        if (ut1 >= ut2)
        {
            return (ut1 - ut2) <= uLowest;
        }
        else
        {
            return (ut2 - ut1) <= uMax;
        }
    }
}

/**
 * @brief moduloSum computes modulo arithmetic sum of any number of integer type arguments.
 * Rationale:
 * Although the % operator with negative arguments is well defined since c++11, the result can be negative,
 * and implicit signed -> unsigned conversions can lead to bugs.
 * @param b The first argument is the base of the modulo addition.
 * @param x0, x1, x2, ... If any of these arguments are signed, then every argument must be representable by a 64 bit
 * signed integer without overflowing.
 * @returns unsigned (positive including zero, and less than b) modulo sum of the integer arguments after the first.
 * The return value is zero if b is zero.
 */
template <typename... Is>
inline constexpr std::uint64_t moduloSum(std::uint64_t b, Is... x) noexcept
{
    static_assert(std::conjunction_v<std::is_integral<Is>...>, "Arguments must be integer type.");
    if constexpr (std::disjunction_v<std::is_signed<Is>...>)
    {
        std::int64_t sB = static_cast<std::int64_t>(b);
        auto z = (b == 0) ? 0 : (... + (static_cast<std::int64_t>(x) % sB)) % sB;
        return z + ((z >= 0) ? 0 : b);
    }
    else
    {
        return (b == 0) ? 0 : (... + (static_cast<std::uint64_t>(x) % b)) % b;
    }
}

/**
 * @brief Check if an integral value is in range for representation by another integral type.
 * This functionality will be present in C++20
 * @param integer - an integral number to check.
 * @returns true if the integer can be represented by the other type.
 */
template <typename I, typename J>
inline constexpr bool inRange([[maybe_unused]] J integer)
{
    static_assert(std::is_integral_v<I> && std::is_integral_v<J>, "inRange: types must be integers");
    if constexpr (std::is_unsigned_v<I> && std::is_signed_v<J>)
    {
        if constexpr (sizeof(I) >= sizeof(J))
        {
            return integer >= 0;
        }
        else
        {
            return integer >= 0 && integer <= static_cast<J>(std::numeric_limits<I>::max());
        }
    }
    else if constexpr (std::is_unsigned_v<J>)
    {
        if constexpr (std::numeric_limits<I>::max() >= std::numeric_limits<J>::max())
        {
            return true;
        }
        else
        {
            return integer <= static_cast<J>(std::numeric_limits<I>::max());
        }
    }
    else
    {
        return integer >= std::numeric_limits<I>::lowest() && integer <= std::numeric_limits<I>::max();
    }
}

/**
 * @brief Converts a floating point value to an integer without undefined behaviour.
 * Returns std::nullopt if the value is out of range.
 * @param floating - a floating point number.
 * @returns Nearest integer value to the input floating point value, or std::nullopt.
 */
template <typename I, typename F>
inline constexpr std::optional<I> safeFloatingToIntegerConversion(F floating) noexcept
{
    static_assert(std::is_floating_point_v<F>, "Argument must be floating point type.");
    static_assert(std::is_integral_v<I>, "Return must be integral type.");

    using BigInt = std::conditional_t<std::is_signed_v<I>, std::int64_t, std::uint64_t>;

    std::optional<I> result{};

    if constexpr (std::numeric_limits<F>::has_quiet_NaN || std::numeric_limits<F>::has_signaling_NaN ||
                  std::numeric_limits<F>::has_infinity)
    {
        if (not std::isfinite(floating))
        {
            return result;
        }
    }

    F rounded{std::round(floating)};
    if (rounded < std::nextafter(static_cast<F>(std::numeric_limits<BigInt>::lowest()), static_cast<F>(0.0)) ||
        rounded > std::nextafter(static_cast<F>(std::numeric_limits<BigInt>::max()), static_cast<F>(0.0)))
    {
        return result;
    }

    auto resultBigInt{static_cast<BigInt>(rounded)};
    if (inRange<I>(resultBigInt))
    {
        result = static_cast<I>(resultBigInt);
    }

    return result;
}

/**
 * @brief Get the underlying value for an enumeration.
 * @note As you can specify the underlying type of an enumeration when declaring it, you cannot assume that casting an
 * enum value to int is safe.  Use this method instead.
 */
template <typename Enum>
constexpr auto enumUnderlyingValue(Enum e) noexcept
{
    static_assert(std::is_enum_v<Enum>);
    return static_cast<std::underlying_type_t<Enum>>(e);
}

} // namespace arms
