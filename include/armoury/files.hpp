#pragma once

#ifdef USE_EXPERIMENTAL_FILESYSTEM

#include <experimental/filesystem>

namespace arms::files
{
namespace fs = std::experimental::filesystem;
}
#else
#include <filesystem>
namespace arms::files
{
namespace fs = std::filesystem;
}
#endif

#include <algorithm>
#include <cstring>
#include <fstream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

#ifndef SEARCH_PATH_ENV_VAR
#define SEARCH_PATH_ENV_VAR RINICOM_SEARCH_PATH
#endif
#define FILETOOLS_STRINGIFY_HELPER(S) #S
#define FILETOOLS_STRINGIFY(X) FILETOOLS_STRINGIFY_HELPER(X)

namespace arms::files
{

inline constexpr char const *const g_searchPathEnvVar{FILETOOLS_STRINGIFY(SEARCH_PATH_ENV_VAR)};

inline std::vector<std::string> splitPaths(std::string const &paths)
{
    std::vector<std::string> pathVec;
    std::string path;
    std::istringstream pathStream(paths);
    while (std::getline(pathStream, path, ':'))
    {
        pathVec.push_back(path);
    }
    return pathVec;
}

inline bool isFile(std::string const &file)
{
    return fs::is_regular_file(file);
}

inline bool isDir(std::string const &dir)
{
    return fs::is_directory(dir);
}

inline std::optional<std::string> findFile(std::string const &file, std::string const &paths)
{
    std::vector<std::string> pathVec = splitPaths(paths);
    fs::path fsFile(file);
    for (std::string const &path : pathVec)
    {
        fs::path fsPath(path);
        fs::path fullPath(fsPath / fsFile);
        if (fs::is_regular_file(fullPath))
        {
            return fullPath.string();
        }
    }
    return std::nullopt;
}

inline std::optional<std::string> findDataFile(std::string const &file)
{
    std::string paths("/:");
    paths.append(fs::current_path().string() + "/:");
    paths.append(fs::current_path().string() + "/share/:");
    if (char const *prefix = std::getenv(g_searchPathEnvVar))
    {
        paths.append(std::string(prefix) + "/:");
    }
    if (char const *homePath = std::getenv("HOME"))
    {
        paths.append(std::string(homePath) + "/.local/share/:");
        paths.append(std::string(homePath) + ":");
    }
    paths.append("/usr/local/share/:/usr/share/:");
    if (char const *xdgPaths = std::getenv("XDG_DATA_DIRS"))
    {
        paths.append(std::string(xdgPaths) + ":");
    }
    return findFile(file, paths);
}

inline std::optional<std::string> findLibraryFile(std::string const &file)
{
    std::string paths("/:");
    if (char const *prefix = std::getenv(g_searchPathEnvVar))
    {
        paths.append(std::string(prefix) + "/:");
        paths.append(std::string(prefix) + "/lib/:");
    }
    paths.append("/lib/:/usr/lib/:/usr/lib/x86_64-linux-gnu/:/usr/lib64/:");
    paths.append(fs::current_path().string() + "/:");
    paths.append(fs::current_path().string() + "/lib/:");
    return findFile(file, paths);
}

inline std::optional<std::string> findConfigFile(std::string const &file)
{
    std::string paths("/:");
    if (char const *prefix = std::getenv(g_searchPathEnvVar))
    {
        paths.append(std::string(prefix) + "/:");
    }
    paths.append(fs::current_path().string() + "/:");
    paths.append(fs::current_path().string() + "/config/:");
    if (char const *homePath = std::getenv("HOME"))
    {
        paths.append(std::string(homePath) + "/.config/:");
    }
    if (char const *xdgPaths = std::getenv("XDG_CONFIG_HOME"))
    {
        paths.append(std::string(xdgPaths) + ":");
    }
    paths.append("/etc/foxd/:/etc/:/etc/config/:");
    return findFile(file, paths);
}

inline std::string combinePaths(std::string const &path1, std::string const &path2)
{
    fs::path fsPath1(path1);
    fs::path fsPath2(path2);
    fs::path fsFullPath = fsPath1 / fsPath2;
    return fsFullPath.string();
}

inline std::string readFile(std::string const &filename)
{
    std::optional<std::string> fullPathFilename = arms::files::findDataFile(filename);
    if (!fullPathFilename.has_value())
    {
        throw std::runtime_error("File not found: " + filename);
    }
    std::ifstream file{fullPathFilename.value(), std::ios::ate | std::ios::binary};
    if (!file.is_open())
    {
        throw std::runtime_error("Failure opening file: " + fullPathFilename.value());
    }
    std::size_t fileSize = static_cast<size_t>(file.tellg());
    std::string buffer(fileSize, '\0');
    file.seekg(0);
    file.read(buffer.data(), static_cast<long>(fileSize));
    file.close();
    return buffer;
}

template <typename Container>
    requires std::contiguous_iterator<typename Container::const_iterator>
inline void readFile(std::string const &filename, Container &container)
{
    if constexpr (std::is_same_v<std::string, std::remove_cvref_t<Container>>)
    {
        std::ifstream file(filename, std::ios_base::binary | std::ios_base::in | std::ios_base::ate);
        if (not file.is_open() || not file.good())
        {
            throw std::runtime_error("Cannot open file for reading: " + filename);
        }
        auto size = file.tellg();
        container.resize(size);
        file.seekg(0);
        file.read(container.data(), size);
        if (file.gcount() != size)
        {
            throw std::runtime_error("Could not complete reading: " + filename);
        }
    }
    else
    {
        std::string str;
        readFile(filename, str);
        std::size_t elements = str.size() / sizeof(typename Container::value_type) +
                               ((str.size() % sizeof(typename Container::value_type) == 0) ? 0 : 1);
        container.clear();
        container.resize(elements);
        std::memcpy(container.data(), str.data(), str.size());
    }
}

/**
 * @brief areFiles checks the paths given, and return those that correspond to a file that exists.
 * @param files
 * @return vector of files that are found to exist
 */
inline std::vector<std::string> areFiles(std::vector<std::string> files)
{
    files.erase(std::remove_if(files.begin(), files.end(),
                               [](std::string const &s) {
                                   fs::path p = s;
                                   bool is_good = false;
                                   try
                                   {
                                       is_good = fs::exists(p) && not fs::is_directory(p);
                                   }
                                   catch (...)
                                   {
                                   }
                                   return not is_good;
                               }),
                files.end());
    return files;
}

} // namespace arms::files
