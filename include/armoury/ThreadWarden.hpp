#pragma once

#include "Lockable.hpp"
#include "Semaphore.hpp"
#include "Watchdog.hpp"
#include "logger.hpp"
#include "misc.hpp"
#include "signals.hpp"

#include <sstream>

namespace arms
{

struct ThreadInteraction
{
    void reset()
    {
        watchdog.reset();
        endWork.reset();
        workEnded.reset();
        beginCleanup.reset();
        threadEnding.reset();
    }
    Watchdog watchdog;
    Semaphore endWork;
    Semaphore workEnded;
    Semaphore beginCleanup;
    Semaphore threadEnding;
};

// threadInteraction provides a mechanism for worker threads to check their status, ie if they should end.
// If workers do not return quickly, they should periodically check the_threadInteraction->endWork.check().
// They may notify the watchdog in order to have the master thread check for timeouts.
// However, if the master thread is to end them, they must be interruptible.
inline thread_local ThreadInteraction *g_threadInteraction = nullptr;

namespace thread_warden_internal
{
class ThreadException
{
  public:
    void set(std::exception_ptr const &exceptPtr)
    {
        std::unique_lock<std::shared_mutex> lock{m_mtx};
        m_exPtr = exceptPtr;
    }

    std::exception_ptr exception() const
    {
        std::shared_lock<std::shared_mutex> lock{m_mtx};
        return m_exPtr;
    }

    void checkAndRethrow() const
    {
        std::shared_lock<std::shared_mutex> lock{m_mtx};
        if (m_exPtr)
        {
            std::rethrow_exception(m_exPtr);
        }
    }

  private:
    std::exception_ptr m_exPtr{nullptr};
    mutable std::shared_mutex m_mtx;
};

struct Instructions
{
    bool refreshDataOut{true};
};

template <typename T>
inline void copyLocalData(T &worker, Lockable<typename T::Input> const &dataIn, Lockable<typename T::Output> &dataOut,
                          Lockable<Instructions> &instruction)
{
    auto [lockedIn, lockedOut, lockedInstruction] =
        makeLocked<ReadLock, WriteLock, WriteLock>(dataIn, dataOut, instruction);
    if (lockedInstruction.get().refreshDataOut)
    {
        worker.dataIn = lockedIn;
        worker.dataOut = lockedOut;
        lockedInstruction.get().refreshDataOut = false;
    }
    else
    {
        worker.dataIn = lockedIn;
        lockedOut = worker.dataOut;
    }
}

template <class T, class... Is>
inline void threadMain(ThreadException &threadException, std::tuple<Is...> const &initializer,
                       Lockable<typename T::Input> const &dataIn, Lockable<typename T::Output> &dataOut,
                       Lockable<Instructions> &instruction, ThreadInteraction &interaction)
{
    g_threadInteraction = &interaction;

    try
    {
        std::ostringstream id;
        id << std::this_thread::get_id();
        std::string threadId = id.str();
        arms::log("{0:s} thread has started (id {1:s}).", T::g_workerName, threadId);

        while (true)
        {
            T worker{std::make_from_tuple<T>(initializer)};

            bool dataCopied{false};
            while (!interaction.endWork.check())
            {
                if (!T::g_copyDataOnce || !dataCopied)
                {
                    copyLocalData(worker, dataIn, dataOut, instruction);
                    dataCopied = true;
                }
                if (worker.work() != 0)
                {
                    arms::log<arms::LOG_TRACE>("In thread {0}, work "
                                               "returned non-zero.",
                                               T::g_workerName);
                    break;
                }
            }

            copyLocalData(worker, dataIn, dataOut, instruction);

            // If end was not requested, wait 1s before retrying
            if (interaction.endWork.waitFor(1'000'000'000))
            {
                interaction.workEnded.binarySignal();
                interaction.beginCleanup.wait();
                break;
            }
        }
        arms::log("{0:s} thread is ending (id {1:s}).", T::g_workerName, threadId);
    }
    catch (...)
    {
        // Stop the thread from restarting too quickly.
        // Sleeping here shouldn't pause the main thread.
        for (int n{}; n < 50 && !interaction.endWork.check(); ++n)
        {
            time::microSleep(10'000);
        }

        // Exiting thread *must* signal before stop() can finish
        interaction.workEnded.binarySignal();
        threadException.set(std::current_exception());
    }
    interaction.threadEnding.signal();
}
} // namespace thread_warden_internal

template <typename T, typename... Is>
class ThreadWarden
{
  public:
    ThreadWarden(ThreadWarden const &) = delete;
    ThreadWarden &operator=(ThreadWarden const &) = delete;
    ThreadWarden(ThreadWarden &&) noexcept = delete;
    ThreadWarden &operator=(ThreadWarden &&) noexcept = delete;
    template <typename... FRs>
    explicit ThreadWarden(FRs &&...init) : inputData{}, outputData{}, m_initializer{std::forward<FRs>(init)...}
    {
    }

    ~ThreadWarden()
    {
        stop();
    }

    bool isRunning() const
    {
        return m_isRunning;
    }

    void start()
    {
        if (!m_isRunning || !m_workerThread.joinable())
        {
            m_exception.set(nullptr);
            m_interaction.reset();
            m_instructions = thread_warden_internal::Instructions();
            m_workerThread = std::thread(thread_warden_internal::threadMain<T, Is...>, std::ref(m_exception),
                                         std::ref(m_initializer), std::ref(inputData), std::ref(outputData),
                                         std::ref(m_instructions), std::ref(m_interaction));
            std::ostringstream id;
            id << m_workerThread.get_id();
            m_workerThreadId = id.str();
            pthread_setname_np(m_workerThread.native_handle(), T::g_workerName);
        }
        m_isRunning = true;
    }

    void initiateStop()
    {
        if (m_isRunning && m_workerThread.joinable())
        {
            arms::log("Initiating thread stop");
            m_interaction.endWork.binarySignal();
        }
    }

    void stop()
    {
        if (m_isRunning && m_workerThread.joinable())
        {
            arms::log("Stopping thread");
            // indicate to worker not to continue loop
            m_interaction.endWork.binarySignal();

            // avoid race-like conditions, eg a blocking system call
            // starts after interrupt signal is sent to the thread,
            // by sending interrupt signal until thread is ending.
            // Allow an initial wait period to let the worker stop
            // cleanly.
            constexpr int64_t wait{5'000'000};
            while (!m_interaction.workEnded.waitFor(wait))
            {
                signals::interruptWaitingThread(m_workerThread);
            }

            // signal to the thread that we have stopped sending
            // interruption signals, now it can begin to cleanup
            m_interaction.beginCleanup.binarySignal();

            constexpr int64_t cleanupWait{1'000'000'000};
            m_interaction.threadEnding.checkFor(cleanupWait);

            while (!m_interaction.threadEnding.waitFor(wait))
            {
                signals::interruptWaitingThread(m_workerThread);
            }

            m_workerThread.join();
            m_exception.set(nullptr);
            m_interaction.reset();
        }
        m_isRunning = false;
    }

    void restart()
    {
        arms::log("Restarting {0:s}", T::g_workerName);
        stop();
        start();
    }

    void resetOutputData()
    {
        auto [lockedOutput, lockedInstruction] = makeLocked<WriteLock, WriteLock>(outputData, m_instructions);
        lockedInstruction.get().refreshDataOut = true;
        lockedOutput = typename T::Output();
    }

    /**
     * @brief check_and_restart_on_failure checks for exceptions and an expired watchdog in worker thread.
     * The watchdog will not be expired if the worker has never called notify(..) on it.
     * @param restart_on_failure (default true)
     * @return true if worker thread failed.
     */
    bool checkAndRestartOnFailure(bool restartOnFailure = true)
    {
        bool threadFailed{false};
        try
        {
            m_exception.checkAndRethrow();
        }
        catch (std::exception const &e)
        {
            threadFailed = true;
            arms::log<arms::LOG_WARNING>("{0:s} (thread id {1:s}) stopped unexpectedly. Error: {2:s}", T::g_workerName,
                                         m_workerThreadId, e.what());
        }
        if (m_interaction.watchdog.expired())
        {
            threadFailed = true;
            arms::log<arms::LOG_WARNING>("{0:s} (thread id {1:s}) watchdog expired.", T::g_workerName,
                                         m_workerThreadId);
        }
        if (threadFailed)
        {
            stop();
            if (restartOnFailure)
            {
                start();
            }
        }
        return threadFailed;
    }

    /**
     * @brief Check if the type held by the thread warden has finished working.
     * @param timeout a timeout argument eg. std::chrono::milliseconds
     * @return whether or not work has ended
     */
    template <typename Rep, typename Period>
    bool checkForWorkEnded(std::chrono::duration<Rep, Period> timeout) const
    {
        auto timeoutNs = std::chrono::duration_cast<std::chrono::nanoseconds>(timeout);
        return m_interaction.workEnded.checkFor(timeoutNs.count());
    }

    /**
     * @brief Check if the thread is ending
     * @param timeout a timeout argument eg. std::chrono::milliseconds
     * @return whether or not the thread is ending
     */
    template <typename Rep, typename Period>
    bool checkForThreadEnding(std::chrono::duration<Rep, Period> timeout) const
    {
        auto timeoutNs = std::chrono::duration_cast<std::chrono::nanoseconds>(timeout);
        return m_interaction.threadEnding.checkFor(timeoutNs.count());
    }

    Lockable<typename T::Input> inputData;
    Lockable<typename T::Output> outputData;

    using Ward = T;

  private:
    std::tuple<Is...> m_initializer;

    std::thread m_workerThread{};
    std::string m_workerThreadId{};
    thread_warden_internal::ThreadException m_exception{};
    bool m_isRunning{false}; // Note: Unlike .joinable(), this is set by main
                             // thread, avoiding possible data races when
                             // queried by main thread.

    ThreadInteraction m_interaction;

    Lockable<thread_warden_internal::Instructions> m_instructions;
};

/**
 * @brief ensureStop Stops a worker with a blocking call.
 * @param worker a ThreadWarden for a worker with a blocking call in its work method
 * @param stopWorker a thread-safe method that causes the blocking worker method to return
 */
template <typename Worker, typename Fn>
void ensureStop(Worker &worker, Fn const &stopWorker)
{
    static_assert(arms::isTemplateType<ThreadWarden, Worker>::value);
    using Ward = typename Worker::Ward;

    // Signal to the worker that we don't want to continue working.
    worker.initiateStop();

    // stopWorker is called is to cause a return from a blocking function called by Worker::Ward.work()
    stopWorker();

    // If the worker hadn't got as far as calling a blocking function, but had gotten past its last check to stop
    // working, then, in this brief window, the worker will have ignored the stop request and will start blocking.
    //
    // Hence, the worker is checked periodically to see that it's finished.  If not, disconnect is called again in
    // the hope that it will cause the blocking function to return and for the work to finish.
    int attempts{0};
    while (!worker.checkForWorkEnded(std::chrono::milliseconds{200}))
    {
        if (++attempts == 5)
        {
            arms::log<arms::LOG_ERROR>("Unable to stop worker thread:{} after 4 attempts.  Will continue to try.",
                                       Ward::g_workerName);
        }
        stopWorker();
    }
    if (attempts > 1)
    {
        arms::log<arms::LOG_WARNING>("It took {} attempts to stop the worker thread:{}", attempts, Ward::g_workerName);
    }

    // Finally, wait for the worker to stop
    worker.stop();
}
} // namespace arms
