#pragma once

#include <optional>
#include <string>
#include <variant>

#include <mosquitto.h>

#include "../logger.hpp"

namespace arms::mqtt
{
struct ClientCredentials
{
    std::string certfile;
    std::string keyfile;
    std::string password;
};

using ClientKeyPasswordProvider = int (*)(char *buf, int size, int rwflag, void *userdata);

/**
 * @brief The CACertificateFile struct
 * @note A path to a PEM-encoded CA certificate
 */
struct CACertificateFile
{
    std::string path;
    /**
     * @brief configure applies the CA certificate file path to mosquitto_tls_set
     * @param mosq The mosquitto instance
     * @param certfile The client certificate file, or null
     * @param keyfile The client key file, or null
     * @param pwCallback A callback to provide a password to mosquitto
     * @return see mosquitto_tls_set
     */
    int configure(mosquitto *mosq, char const *certfile, char const *keyfile,
                  ClientKeyPasswordProvider pwCallback) const
    {
        return mosquitto_tls_set(mosq, path.c_str(), nullptr, certfile, keyfile, pwCallback);
    }
};

/**
 * @brief The CACertificatesPath struct
 * @note A path to a directory containing .pem files.  See the mosquitto.conf man page and its documentation for capath
 * for more details.
 */
struct CACertificatesPath
{
    std::string path;
    /**
     * @brief configure applies the CA certificate directory to mosquitto_tls_set
     * @param mosq The mosquitto instance
     * @param certfile The client certificate file, or null
     * @param keyfile The client key file, or null
     * @param pwCallback A callback to provide a password to mosquitto
     * @return see mosquitto_tls_set
     */
    int configure(mosquitto *mosq, char const *certfile, char const *keyfile,
                  ClientKeyPasswordProvider pwCallback) const
    {
        return mosquitto_tls_set(mosq, nullptr, path.c_str(), certfile, keyfile, pwCallback);
    }
};

using CertificateAccess = std::variant<CACertificateFile, CACertificatesPath>;

class CertificateBasedTLSSupport
{
  public:
    /**
     * @brief CertificateBasedClientAuthentication constructor
     * @param caInfo  How to get Certificate Authority information
     * @param insecure Disable hostname checks on broker certificates
     * @note Insecure should only be set in debug environments
     */
    CertificateBasedTLSSupport(CertificateAccess caInfo, bool insecure)
        : caCertificateAccess{std::move(caInfo)}, clientCertificateInfo{}, insecure{insecure}
    {
    }
    /**
     * @brief CertificateBasedClientAuthentication constructor
     * @param caInfo  How to get Certificate Authority information
     * @param clientInfo Client certificate and key (and password, if required)
     * @param insecure Disable hostname checks on broker certificates
     * @note Insecure should only be set in debug environments
     */
    CertificateBasedTLSSupport(CertificateAccess caInfo, ClientCredentials clientInfo, bool insecure)
        : caCertificateAccess{std::move(caInfo)}, clientCertificateInfo{std::move(clientInfo)}, insecure{insecure}
    {
    }

    /**
     * @brief configure key and certificate for an MQTT TLS connection
     * @param mosq The mosquitto instance to be configured
     * @param clientName An identifier for the client for logging purposes
     * @param pwCallback A callback to populate a password buffer for an encrypted client key
     * @return see mosquitto_tls_set
     */
    int configure(mosquitto *mosq, std::string_view clientName, ClientKeyPasswordProvider pwCallback) const
    {
        if (isInsecure())
        {
            arms::log<arms::LOG_WARNING>("Disabling host name verification on the broker certificate.  Client:{}",
                                         clientName);
            mosquitto_tls_insecure_set(mosq, true);
        }
        return std::visit(
            [this, mosq, pwCallback](auto const &caCertificate) {
                if (clientCertificateInfo)
                    return caCertificate.configure(mosq, clientCertificateInfo->certfile.c_str(),
                                                   clientCertificateInfo->keyfile.c_str(), pwCallback);
                else
                    return caCertificate.configure(mosq, nullptr, nullptr, pwCallback);
            },
            caCertificateAccess);
    }

    /**
     * @brief Copies any password that encrypts the client key to a buffer provided by the mosquitto library.
     * @param buf where to write the password to
     * @param size the length of the password buffer
     * @return the length of the password
     */
    int getClientKeyPassword(char *buf, int size) const
    {
        if (buf && size > 0 && clientCertificateInfo)
        {
            // There's no point checking whether the password is empty or not, as we'll get an authentication failure if
            // a password was needed.
            strncpy(buf, clientCertificateInfo->password.c_str(), size - 1);
            buf[size - 1] = '\0';
            return strlen(buf);
        }
        else
        {
            arms::log<arms::LOG_ERROR>(
                "Bad parameters provided by mosquitto callback.  Null buffer?:{} , buffer size:{}", buf == nullptr,
                size);
            assert(false);
            return 0;
        }
    }

    /**
     * @brief isInsecure An option intended for intial testing to disable certificate-based TLS security
     * @return Whether or not host name checks on broker certificates are disabled
     */
    bool isInsecure() const
    {
        return insecure;
    }

  private:
    CertificateAccess caCertificateAccess;
    std::optional<ClientCredentials> clientCertificateInfo;
    bool insecure;
};

/**
 * @brief Pre-shared Key support for MQTT
 * @note Can be used instead of certificate-based TLS.
 */
struct PSKBasedTLSSupport
{
    std::string key;
    std::string identity;
    int configure(mosquitto *mosq, std::string_view, ClientKeyPasswordProvider) const
    {
        return mosquitto_tls_psk_set(mosq, key.c_str(), identity.c_str(), nullptr);
    }
    int getClientKeyPassword(char *, int) const
    {
        arms::log<arms::LOG_ERROR>("Password requested for a PSK-authenticated client.  There should be no "
                                   "password-protected client key to unlock.");
        assert(false);
        return 0;
    }
};

using TLSSupport = std::variant<CertificateBasedTLSSupport, PSKBasedTLSSupport>;

/**
 * @brief Username and password for MQTT
 * @note Depending upon the mosquitto configuration, the password may not actually be used for authentication
 */
struct MqttLogin
{
    std::string username{};
    std::string password{};
};

} // namespace arms::mqtt
