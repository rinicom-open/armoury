#pragma once

#include "../mqtt.hpp"
#include <string>

namespace arms::mqtt
{
/**
 * @brief A Subscriber that does not own its message buffer
 * @tparam Sink Where this subscriber will write messages to
 * @note This lets you completely customise how mosquitto messages are stored, with an implementation of
 * getMessageBuffer() for your custom Sink
 */
template <SubscriberSink Sink>
class SubscriberWithUnownedSink
{
  public:
    SubscriberWithUnownedSink(MqttInfo info, Sink &buffer, std::function<void()> onDataReady = nullptr)
        : m_mosq{info.getClientInfo()}, m_buffer{buffer},
          m_worker{info, m_mosq.clientId(), m_mosq(), m_buffer, m_connected, std::move(onDataReady)}
    {
        m_worker.start();
    }

    ~SubscriberWithUnownedSink()
    {
        ensureStop(m_worker, m_mosq());
    }

    bool isConnected() const
    {
        return m_connected.check();
    }

    template <typename Rep, typename Period>
    bool waitForConnection(std::chrono::duration<Rep, Period> timeout)
    {
        auto ticks = std::chrono::duration_cast<std::chrono::nanoseconds>(timeout).count();
        return m_connected.checkFor(ticks);
    }

    bool checkNetworkThreadFailed(bool restartOnFailure = true)
    {
        bool const failed = m_worker.checkAndRestartOnFailure(restartOnFailure);
        if (failed)
        {
            arms::log<arms::LOG_WARNING>("MQTT: mosquitto subscriber thread failed");
        }
        return failed;
    }

  private:
    MosqWrapper m_mosq;
    Sink &m_buffer;
    arms::Semaphore m_connected{};
    arms::ThreadWarden<SubscriberWorkerImpl<Sink>, MqttInfo, std::string const &, mosquitto *, Sink &,
                       arms::Semaphore &, std::function<void()>>
        m_worker;
};

/**
 * @brief SubscriberWithBufferReference is a default Subscriber where you provide a reference to a MessageBuffer as an
 * additional parameter on construction.
 *
 * @note There are a collection of methods in the main mqtt header to get a message from a message buffer, which are
 * also used in the main Subscriber implementation.
 */
using SubscriberWithBufferReference = SubscriberWithUnownedSink<MessageBuffer>;

} // namespace arms::mqtt
