#pragma once

#include <limits>
#include <span>
#include <string>
#include <string_view>
#include <variant>

#include <mosquitto.h>
#include <mqtt_protocol.h>

#include "../misc.hpp"
#include "../logger.hpp"

namespace arms::mqtt
{
/**
 * @brief Factory function for a list of mqtt property identifiers
 * @tparam The property identifiers to be grouped together.
 */
template <int... Args>
constexpr auto makePropertyTypeList()
{
    return std::array<int, sizeof...(Args)>{Args...};
}
/**
 * @brief stringProperties are property identifers from mqtt_protocol.h for string properties.
 */
inline constexpr auto stringProperties =
    makePropertyTypeList<MQTT_PROP_CONTENT_TYPE, MQTT_PROP_RESPONSE_TOPIC, MQTT_PROP_ASSIGNED_CLIENT_IDENTIFIER,
                         MQTT_PROP_AUTHENTICATION_METHOD, MQTT_PROP_RESPONSE_INFORMATION, MQTT_PROP_SERVER_REFERENCE,
                         MQTT_PROP_REASON_STRING>();
/**
 * @brief binaryDataProperties are property identifers from mqtt_protocol.h for binary data properties.
 */
inline constexpr auto binaryDataProperties =
    makePropertyTypeList<MQTT_PROP_CORRELATION_DATA, MQTT_PROP_AUTHENTICATION_DATA>();

/**
 * @brief uint32Properties are property identifers from mqtt_protocol.h for 4-byte int properties.
 */
inline constexpr auto uint32Properties =
    makePropertyTypeList<MQTT_PROP_MESSAGE_EXPIRY_INTERVAL, MQTT_PROP_SESSION_EXPIRY_INTERVAL,
                         MQTT_PROP_WILL_DELAY_INTERVAL, MQTT_PROP_MAXIMUM_PACKET_SIZE>;

/**
 * @brief The PublishMessagePropertyList struct holds mqtt5_property enums from mqtt_protocol.h valid for CMD_PUBLISH messages
 */
struct PublishMessagePropertyList
{
    inline static constexpr auto properties =
        makePropertyTypeList<MQTT_PROP_PAYLOAD_FORMAT_INDICATOR, MQTT_PROP_MESSAGE_EXPIRY_INTERVAL,
                             MQTT_PROP_CONTENT_TYPE, MQTT_PROP_RESPONSE_TOPIC, MQTT_PROP_CORRELATION_DATA,
                             MQTT_PROP_SUBSCRIPTION_IDENTIFIER, MQTT_PROP_TOPIC_ALIAS>();

    inline static constexpr auto MessageType = CMD_PUBLISH; ///< The MQTT Protocol message type that the property list is for
};

/**
 * @brief The ConnectPropertyList struct holds mqtt5_property enums from mqtt_protocol.h valid for CMD_CONNECT messages
 */
struct ConnectPropertyList
{
    inline static constexpr auto properties =
        makePropertyTypeList<MQTT_PROP_SESSION_EXPIRY_INTERVAL, MQTT_PROP_AUTHENTICATION_METHOD,
                             MQTT_PROP_AUTHENTICATION_DATA, MQTT_PROP_REQUEST_PROBLEM_INFORMATION,
                             MQTT_PROP_REQUEST_RESPONSE_INFORMATION, MQTT_PROP_RECEIVE_MAXIMUM,
                             MQTT_PROP_TOPIC_ALIAS_MAXIMUM, MQTT_PROP_MAXIMUM_PACKET_SIZE>();

    inline static constexpr auto MessageType = CMD_CONNECT; ///< The MQTT Protocol message type that the property list is for
};

/**
 * @brief findIdentifier allows correct usage of mqtt property identifiers to be checked at compile time.
 */
template <int I, typename PropertyList>
constexpr bool findIdentifier(PropertyList const &p)
{
    for (auto i : p)
    {
        if (i == I)
            return true;
    }
    return false;
}

/**
 * @brief A collection of mqtt properties
 *
 * @tparam ValidProperties A struct with a properties member array containing valid properties for a particular mqtt
 * message type.  Enables compile-time checks of mqtt properties.
 */
template <typename ValidProperties>
class Properties
{
    static_assert(arms::isArray<std::remove_cv_t<decltype(ValidProperties::properties)>>::value);

  public:
    Properties() = default;
    Properties &operator=(Properties const &rhs)
    {
        if (auto rc = mosquitto_property_copy_all(&m_properties, rhs.m_properties); rc != MOSQ_ERR_SUCCESS)
        {
            // Documented error codes are:
            // MOSQ_ERR_INVAL	if dest is NULL
            // MOSQ_ERR_NOMEM	on out of memory (dest will be set to NULL)
            // So only oom should be possible, and there's nothing that can be done to recover from that.

            arms::throwLog("MQTT: Unable to copy properties: {}", mosquitto_strerror(rc));
        }
        return *this;
    }

    Properties &operator=(Properties &&source) noexcept
    {
        clearAllProperties();
        m_properties = source.m_properties;
        source.m_properties = nullptr;
        return *this;
    }

    Properties(Properties const &source)
    {
        *this = source;
    }
    Properties(Properties &&source) noexcept
    {
        *this = std::move(source);
    }

    ~Properties() noexcept
    {
        clearAllProperties();
    }

    /**
     * @brief checkProperties
     * @retval MOSQ_ERR_SUCCESS	all properties are valid
     * @retval MOSQ_ERR_DUPLICATE_PROPERTY a property is duplicated where it is forbidden.
     * @retval MOSQ_ERR_PROTOCOL any property is invalid
     */
    int checkProperties() const noexcept
    {
        return mosquitto_property_check_all(ValidProperties::MessageType, m_properties);
    }

    /**
     * @brief conversion operator
     * @return property for use with mosquitto library
     * @throws std::runtime_error checkProperties() does not return MOSQ_ERR_SUCCESS
     */
    mosquitto_property *operator()() const
    {
        if (auto check = checkProperties(); check != MOSQ_ERR_SUCCESS)
        {
            arms::throwLog("MQTT: invalid properties: {}", mosquitto_strerror(check));
        }
        return m_properties;
    }

    /**
     * @brief addStringProperty
     * @tparam identifier A valid identifier from mqtt_protocol.h
     * @param property The string value for the property
     * @retval MOSQ_ERR_SUCCESS	on success
     * @retval MOSQ_ERR_INVAL	if identifier is invalid, if value is NULL, or if proplist is NULL
     * @retval MOSQ_ERR_NOMEM	on out of memory
     * @retval MOSQ_ERR_MALFORMED_UTF8	value is not valid UTF-8.
     */
    template <int Identifier>
    int addStringProperty(std::string const &property)
    {
        static_assert(findIdentifier<Identifier>(ValidProperties::properties));
        return mosquitto_property_add_string(&m_properties, Identifier, property.c_str());
    }

    /**
     * @brief addBinaryDataProperty
     * @tparam identifier A valid identifier from mqtt_protocol.h
     * @param property The binary data value for the property
     * @param length The length of the property data in bytes
     * @retval MOSQ_ERR_SUCCESS	on success
     * @retval MOSQ_ERR_INVAL	if identifier is invalid, if value is NULL, or if proplist is NULL
     * @retval MOSQ_ERR_NOMEM	on out of memory
     */
    template <int Identifier>
    int addBinaryDataProperty(void const *property, uint16_t length)
    {
        static_assert(findIdentifier<Identifier>(ValidProperties::properties));
        return mosquitto_property_add_binary(&m_properties, Identifier, property, length);
    }

    /**
     * @brief addUint32Property
     * @tparam identifier A valid identifier from mqtt_protocol.h
     * @param property The integer value for the property
     * @retval MOSQ_ERR_SUCCESS	on success
     * @retval MOSQ_ERR_INVAL	if identifier is invalid, if value is NULL, or if proplist is NULL
     * @retval MOSQ_ERR_NOMEM	on out of memory
     */
    template <int Identifier>
    int addUint32Property(uint32_t property)
    {
        static_assert(findIdentifier<Identifier>(ValidProperties::properties));
        return mosquitto_property_add_int32(&m_properties, Identifier, property);
    }

    /**
     * @brief clearAllProperties
     */
    void clearAllProperties()
    {
        mosquitto_property_free_all(&m_properties);
        m_properties = nullptr;
    }

  private:
    mosquitto_property *m_properties{nullptr};
};

/**
 * @brief The PublishMessageProperties can be passed to a Publisher to set data length, mqtt qos, the retain flag, a
 * response topic and any correlation data.
 */
struct PublishMessageProperties : Properties<PublishMessagePropertyList>
{
    /**
     * @brief maxLen  is the maximum length of data to be published, if smaller than the actual payload size
     */
    std::size_t maxLen = -1;
    /**
     * @brief qos is the required mqtt qos (0, 1 or 2)
     */
    int qos{0};
    /**
     * @brief retain flags whether or not the published message should be retained by a broker.
     */
    bool retain{false};

    /**
     * @brief setResponseTopic
     * @param topic
     * @retval MOSQ_ERR_SUCCESS	on success
     * @retval MOSQ_ERR_INVAL	if identifier is invalid, if value is NULL, or if proplist is NULL
     * @retval MOSQ_ERR_NOMEM	on out of memory
     * @retval MOSQ_ERR_MALFORMED_UTF8	value is not valid UTF-8.
     *
     * @note this property should not be set more than once on a PublishMessageProperties instance
     */
    int setResponseTopic(std::string const &topic)
    {
        return addStringProperty<MQTT_PROP_RESPONSE_TOPIC>(topic.c_str());
    }

    /**
     * @brief setCorrelationData
     * @param data
     * @param length
     * @retval MOSQ_ERR_SUCCESS	on success
     * @retval MOSQ_ERR_INVAL	if identifier is invalid, if value is NULL, or if proplist is NULL
     * @retval MOSQ_ERR_NOMEM	on out of memory
     *
     * @note this property should not be set more than once on a PublishMessageProperties instance
     */
    int setCorrelationData(void const *data, uint16_t length)
    {
        return addBinaryDataProperty<MQTT_PROP_CORRELATION_DATA>(data, length);
    }
    /**
     * @brief setCorrelationData
     * @param data A standard container-like structure holding the correlation data
     * @param maxLen the maximum length of data to be published, if smaller than the actual correlation data size
     * @retval MOSQ_ERR_SUCCESS	on success
     * @retval MOSQ_ERR_INVAL	if identifier is invalid, if value is NULL, or if proplist is NULL
     * @retval MOSQ_ERR_NOMEM	on out of memory
     *
     * @tparam CorrelationData should be deduced and so no need for the caller to specify it.  Must store data in a
     * contiguous block of storage, like std::vector or std::array, and have size() and data() members.
     *
     * @note this property should not be set more than once on a PublishMessageProperties instance
     */
    template <typename CorrelationData>
    auto setCorrelationData(CorrelationData const &data, std::size_t maxLen = -1)
        -> decltype(data.size(), data.data(), int{})
    {
        auto requestedDataLength = sizeof(typename CorrelationData::value_type) * std::min(maxLen, data.size());
        uint16_t length = std::min<std::common_type_t<decltype(requestedDataLength), uint16_t>>(
            requestedDataLength, std::numeric_limits<uint16_t>::max());
        return setCorrelationData(static_cast<void const *>(data.data()), length);
    }
};

/**
 * @brief The ConnectProperties are for setting the session expiry of an MQTT connection.
 */
struct ConnectProperties : Properties<ConnectPropertyList>
{
    /**
     * @brief setSessionToNeverExpire
     * @return see Properties::addUint32Property
     *
     * @note When set on a persistent session, this mirrors the MQTT v3.1.1 behaviour for a connection with CleanSession
     * set to 0
     * @note once set, session expiry cannot be reconfigured unless clearAllProperties() is called
     */
    int setSessionToNeverExpire()
    {
        return setSessionExpiry(std::numeric_limits<uint32_t>::max());
    }

    /**
     * @brief setSessionToExpireOnDisconnect
     * @return see Properties::addUint32Property
     *
     * @note This is the default behaviour for MQTT v5, even on a persistent session
     * @note once set, session expiry cannot be reconfigured unless clearAllProperties() is called
     */
    int setSessionToExpireOnDisconnect()
    {
        return setSessionExpiry(0);
    }

    /**
     * @brief setSessionExpiry
     * @param seconds Seconds before a session expires.  This may not be entirely accurate if, for example, the broker
     * is down at the time of expiry.
     * @return see Properties::addUint32Property
     *
     * @note It can be useful to customise the session expiry when looking to minimise the use of broker resources
     * @note once set, session expiry cannot be reconfigured unless clearAllProperties() is called
     */
    int setSessionExpiry(uint32_t seconds)
    {
        return addUint32Property<MQTT_PROP_SESSION_EXPIRY_INTERVAL>(seconds);
    }
};

namespace detail
{
struct Free
{
    void operator()(void *ptr) const
    {
        std::free(ptr);
    }
};
} // namespace detail

/**
 * RAII wrapper for property data allocated by mosquitto
 * @tparam T The desired pointed-to type returned by eg. PropertyData.get()
 */
template <typename T>
using PropertyData = std::unique_ptr<T, detail::Free>;

/**
 * Deep copy of PropertyData
 */
template <typename T>
inline PropertyData<T> copyPropertyData(PropertyData<T> const &rhs, std::size_t length)
{
    auto *destination = static_cast<T *>(std::calloc(length, sizeof(T)));
    std::memcpy(destination, rhs.get(), length);
    return PropertyData<T>{destination};
}

/**
 * A wrapper around a mosquitto-allocated string property, handling copying, clean-up and providing access via a
 * string_view
 * @tparam Identifier a property identifier found in stringProperties
 */
template <int Identifier>
class StringProperty final
{
  public:
    static_assert(findIdentifier<Identifier>(stringProperties), "Identifier isn't an mqtt5 string property");
    StringProperty() = default;
    explicit StringProperty(mosquitto_property const *properties)
    {
        char *data{};
        if (mosquitto_property_read_string(properties, Identifier, &data, false))
        {
            m_data.reset(data);
        }
    }

    StringProperty(StringProperty const &source)
    {
        *this = source;
    }

    StringProperty &operator=(StringProperty const &rhs) &
    {
        if (this != &rhs)
        {
            if (!rhs.m_data)
            {
                m_data.reset();
            }
            else
            {
                auto length = rhs.getProperty().size() + 1; // +1 for the null character
                m_data = copyPropertyData(rhs.m_data, length);
            }
        }
        return *this;
    }

    /**
     * @brief getProperty
     * @return A non-owning view of the property.
     * @note Caller's reponsibility to ensure that the return value doesn't outlive the StringProperty
     */
    std::string_view getProperty() const
    {
        if (m_data)
        {
            return std::string_view{m_data.get()};
        }
        else
        {
            return std::string_view{};
        }
    }

  private:
    PropertyData<char> m_data{};
};

/**
 * @brief BinaryDataSpan An iterable view of binary data with a size accessor.
 */
using BinaryDataSpan = std::span<const std::byte>;

/**
 * A wrapper around a mosquitto-allocated binary data property, handling copying, clean-up and providing container-like
 * access via a BinaryDataSpan
 * @tparam Identifier a property identifier found in binaryDataProperties
 */
template <int Identifier>
class BinaryDataProperty final
{
  public:
    static_assert(findIdentifier<Identifier>(binaryDataProperties), "Identifier isn't an mqtt5 binary data property");
    BinaryDataProperty() = default;
    explicit BinaryDataProperty(mosquitto_property const *properties)
    {
        void *data{};
        if (mosquitto_property_read_binary(properties, Identifier, &data, &m_size, false))
        {
            m_data.reset(static_cast<std::byte *>(data));
        }
    }

    BinaryDataProperty(BinaryDataProperty const &source)
    {
        *this = source;
    }

    BinaryDataProperty &operator=(BinaryDataProperty const &rhs) &
    {
        if (this != &rhs)
        {
            if (!rhs.m_data)
            {
                m_data.reset();
                m_size = 0u;
            }
            else
            {
                auto sourceSpan = rhs.getProperty();
                m_size = sourceSpan.size();
                m_data = copyPropertyData(rhs.m_data, m_size);
            }
        }
        return *this;
    }

    /**
     * @brief getProperty
     * @return A non-owning view of the property.
     * @note Caller's reponsibility to ensure that the return value doesn't outlive the BinaryDataProperty
     */
    BinaryDataSpan getProperty() const
    {
        if (m_data)
        {
            return BinaryDataSpan{m_data.get(), m_size};
        }
        else
        {
            return BinaryDataSpan{};
        }
    }

  private:
    PropertyData<std::byte> m_data{};
    uint16_t m_size{};
};

} // namespace arms::mqtt
