#pragma once

#include <optional>
#include <string>

#include "../mqtt.hpp"
#include "authentication.hpp"

namespace arms::mqtt
{
/**
 * @brief Wrapper class to load TLS support from a configuration
 */
struct TLSSupportFromConfig
{
    std::optional<TLSSupport> auth{};

    /**
     * @note SettingType will typically be libconfig::Setting from libconfig++.  The SettingType is templatized so it
     * can be restored from a test harness.
     */
    template <typename SettingType>
    explicit TLSSupportFromConfig(SettingType const &setting)
    {
        auto getCertificateInfo = [](std::string const &type, std::string path) -> std::optional<CertificateAccess> {
            if (type == "file")
            {
                return CACertificateFile{std::move(path)};
            }
            else if (type == "directory")
            {
                return CACertificatesPath{std::move(path)};
            }
            else
            {
                return std::nullopt;
            }
        };

        auto getTLSInfo =
            [getCertificateInfo](SettingType const &setting) -> std::optional<CertificateBasedTLSSupport> {
            std::string typeString{}, path{};
            if (setting.lookupValue("ca_path_type", typeString) && setting.lookupValue("ca_path", path))
            {
                if (auto type = getCertificateInfo(typeString, std::move(path)))
                {
                    bool hasClientKey = setting.exists("client_key");
                    bool hasClientCertificate = setting.exists("client_cert");
                    bool isInsecure{false};
                    if (setting.exists("insecure"))
                    {
                        setting.lookupValue("insecure", isInsecure);
                    }
                    if (hasClientKey && hasClientCertificate)
                    {
                        ClientCredentials client{};
                        setting.lookupValue("client_key", client.keyfile);
                        setting.lookupValue("client_cert", client.certfile);

                        // client_key_password is an optional field
                        setting.lookupValue("client_key_password", client.password);
                        return CertificateBasedTLSSupport{*std::move(type), std::move(client), isInsecure};
                    }
                    else if (!hasClientKey && !hasClientCertificate)
                    {
                        return CertificateBasedTLSSupport{*std::move(type), isInsecure};
                    }
                    else if (hasClientKey)
                    {
                        arms::throwLog("missing client_cert");
                    }
                    else if (hasClientCertificate)
                    {
                        arms::throwLog("missing client_key");
                    }
                }
            }
            return std::nullopt;
        };

        auto getPSKInfo = [](SettingType const &setting) -> std::optional<PSKBasedTLSSupport> {
            PSKBasedTLSSupport result{};
            if (setting.lookupValue("key", result.key))
            {
                if (setting.lookupValue("identity", result.identity))
                {
                    return result;
                }
                else
                {
                    arms::throwLog("missing PSK identity");
                }
            }
            else
            {
                arms::throwLog("missing PSK key");
            }
            return std::nullopt;
        };

        std::string typeString{};
        if (setting.isGroup() && setting.lookupValue("type", typeString))
        {
            if (typeString == "cert")
            {
                auth = getTLSInfo(setting).value();
                return;
            }
            else if (typeString == "psk")
            {
                auth = getPSKInfo(setting).value();
                return;
            }
        }

        arms::throwLog("Client auth config parse error");
    }
    TLSSupportFromConfig(TLSSupportFromConfig &&) noexcept = default;
    TLSSupportFromConfig &operator=(TLSSupportFromConfig &&) & noexcept = default;
};

/**
 * @brief A utility method to restore MqttInfo properties from a config file
 */
template <typename ConfigLoaderType>
void loadConfig(ConfigLoaderType &loader, MqttInfo &info)
{
    loader.getSetting(info.host, "mqtt_host");

    std::optional<int> mqttPort{};
    loader.getSetting(mqttPort, "mqtt_port");
    if (mqttPort)
    {
        info.setPort(*mqttPort);
    }

    loader.getSetting(info.bind, "mqtt_bind_address");

    std::optional<int> mqttKeepAlive{};
    loader.getSetting(mqttKeepAlive, "mqtt_keep_alive");
    if (mqttKeepAlive)
    {
        info.setKeepAlive(*mqttKeepAlive);
    }

    MqttLogin login{};
    if (loader.getSetting(login.username, "mqtt_username"))
    {
        // If `use_identity_as_username` or `use_subject_as_username` are configured on mosquitto, the
        // password won't actually be used.  Therefore, a missing password won't be treated as an error.
        loader.getSetting(login.password, "mqtt_password");
        info.login = std::move(login);
    }

    std::optional<TLSSupportFromConfig> maybeAuth{};
    loader.getSetting(maybeAuth, "mqtt_tls_client_auth");
    if (maybeAuth)
    {
        info.tlsSupport = std::move(maybeAuth->auth);
    }

    bool cleanSession{true};
    loader.getSetting(cleanSession, "mqtt_clean_session");
    info.sessionType = cleanSession ? SessionType::Clean : SessionType::Persistent;
}

namespace detail
{
template <typename ConfigLoaderType>
struct GroupPrefixConfigLoaderAdapter
{
    ConfigLoaderType &configLoader;
    std::string prefix;

    template <typename T>
    bool getSetting(T &var, std::string const &setting) const
    {
        return configLoader.getSetting(var, prefix + '.' + setting);
    }
};
} // namespace detail

template <typename ConfigLoaderType>
void loadConfig(ConfigLoaderType &loader, arms::mqtt::MqttInfo &info, std::string setting)
{
    detail::GroupPrefixConfigLoaderAdapter<ConfigLoaderType> adaptedLoader{loader, std::move(setting)};
    loadConfig(adaptedLoader, info);
}

} // namespace arms::mqtt
