#pragma once

#include "strings.hpp"

#include <spdlog/async.h>
#include <spdlog/common.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <sys/wait.h>
#include <unistd.h>

namespace arms
{

enum LogLevel
{
    LOG_NONE = 0,
    LOG_CRITICAL = 1,
    LOG_ERROR = 2,
    LOG_WARNING = 3,
    LOG_INFO = 4,
    LOG_DEBUG = 5,
    LOG_TRACE = 6
};

namespace logger
{

inline std::shared_ptr<spdlog::logger> g_theLogger{spdlog::stdout_color_mt("logger")};

/**
 * @brief iToLogLevel, safely convert from integer to LogLevel
 * @param i integer value of the log level
 * @return LogLevel enum value
 */
inline constexpr LogLevel iToLogLevel(int i)
{
    if (i > 0 && i <= 6)
    {
        return static_cast<LogLevel>(i);
    }
    return LOG_NONE;
}

inline constexpr LogLevel stringToLogLevel(std::string_view logLevel)
{
    std::string level(logLevel);
    std::transform(level.begin(), level.end(), level.begin(), ::tolower);
    if (level == "trace")
    {
        return LOG_TRACE;
    }
    else if (level == "debug")
    {
        return LOG_DEBUG;
    }
    else if (level == "info")
    {
        return LOG_INFO;
    }
    else if (level == "warning" || level == "warn")
    {
        return LOG_WARNING;
    }
    else if (level == "error" || level == "err")
    {
        return LOG_ERROR;
    }
    else if (level == "critical" || level == "crit")
    {
        return LOG_CRITICAL;
    }
    else if (level == "none" || level == "off")
    {
        return LOG_NONE;
    }
    return LOG_NONE;
}

inline constexpr spdlog::level::level_enum logLevelToSpdLogLevel(LogLevel logLevel)
{
    switch (logLevel)
    {
    case LOG_NONE:
        return spdlog::level::off;
    case LOG_TRACE:
        return spdlog::level::trace;
    case LOG_DEBUG:
        return spdlog::level::debug;
    case LOG_INFO:
        return spdlog::level::info;
    case LOG_WARNING:
        return spdlog::level::warn;
    case LOG_ERROR:
        return spdlog::level::err;
    case LOG_CRITICAL:
        return spdlog::level::critical;
    default:
        return spdlog::level::off;
    }
}

inline constexpr LogLevel spdLogLevelToLogLevel(spdlog::level::level_enum logLevel)
{
    switch (logLevel)
    {
    case spdlog::level::off:
        return LOG_NONE;
    case spdlog::level::trace:
        return LOG_TRACE;
    case spdlog::level::debug:
        return LOG_DEBUG;
    case spdlog::level::info:
        return LOG_INFO;
    case spdlog::level::warn:
        return LOG_WARNING;
    case spdlog::level::err:
        return LOG_ERROR;
    case spdlog::level::critical:
        return LOG_CRITICAL;
    default:
        return LOG_NONE;
    }
}

inline void setupLogging(LogLevel logLevel, bool logAsync, std::string logFile, std::size_t logFileSizeMb,
                         std::size_t logFileCount)
{
    g_theLogger.reset();
    spdlog::drop("logger");
    // if no log filename is set, output the log to the terminal
    if (logFile.size() != 0)
    {
        if (logAsync)
        {
            g_theLogger = spdlog::rotating_logger_mt<spdlog::async_factory>("logger", logFile, 1048576 * logFileSizeMb,
                                                                            logFileCount);
        }
        else
        {
            g_theLogger = spdlog::rotating_logger_mt("logger", logFile, 1048576 * logFileSizeMb, logFileCount);
        }
        spdlog::set_pattern("[%Y-%m-%d %T.%f] (%t) [%l] %v");
    }
    else
    {
        if (logAsync)
        {
            g_theLogger = spdlog::stdout_color_mt<spdlog::async_factory>("logger");
        }
        else
        {
            g_theLogger = spdlog::stdout_color_mt("logger");
        }
        spdlog::set_pattern("[%T.%f] (%t) [%l] %v");
    }

    spdlog::set_level(logLevelToSpdLogLevel(logLevel));
}

inline void setupLogging(std::string logLevel, bool logAsync, std::string logFile, std::size_t logFileSizeMb,
                         std::size_t logFileCount)
{
    setupLogging(stringToLogLevel(logLevel), logAsync, logFile, logFileSizeMb, logFileCount);
}

} // namespace logger

/**
 * @brief Check whether or not a log level is enabled.
 * @note This method can be useful when an argument to a logging format string is expensive to evaluate.
 * @note One alternative is to encapsulate the expensive argument's evaluation in a stream insertion operator.  See
 * https://fmt.dev/latest/api.html#std-ostream-support for further info.
 */
template <arms::LogLevel L>
inline bool shouldLog() noexcept
{
    if constexpr (L != LOG_NONE)
    {
        constexpr spdlog::level::level_enum level = logger::logLevelToSpdLogLevel(L);
        if (logger::g_theLogger)
        {
            return logger::g_theLogger->should_log(level);
        }
    }
    return false;
}

inline arms::LogLevel getLogLevel()
{
    if (logger::g_theLogger)
    {
        return logger::spdLogLevelToLogLevel(logger::g_theLogger->level());
    }
    else
    {
        return LOG_NONE;
    }
}

template <arms::LogLevel L = LOG_DEBUG, typename... Args>
inline void log(char const *const formatStr, Args &&...args) noexcept
{
    assert(formatStr);
    try
    {
        constexpr spdlog::level::level_enum level = logger::logLevelToSpdLogLevel(L);
#ifndef NDEBUG
        // Provide better debug messages if there are issues formatting the message,
        // but probably slower.
        assert(logger::g_theLogger);
        if (logger::g_theLogger && logger::g_theLogger->should_log(level))
        {
            std::string msg = strings::tryFormat(formatStr, std::forward<Args>(args)...);
            logger::g_theLogger->log(level, msg);
        }
#else
        if (logger::g_theLogger)
        {
            logger::g_theLogger->log(level, fmt::runtime(formatStr), std::forward<Args>(args)...);
        }
#endif
    }
    catch (...)
    {
    }
}

template <arms::LogLevel L = LOG_DEBUG, typename... Args>
inline void log(std::string const &formatStr, Args &&...args) noexcept
{
    arms::log<L>(formatStr.c_str(), std::forward<Args>(args)...);
}

/*
 * throwLog will format and throw a log message even if the log level is
 * below the configured value. The template argument determines whether
 * the message is logged.
 */
template <arms::LogLevel L = LOG_ERROR, typename... Args>
[[noreturn]] inline void throwLog(char const *const formatStr, Args &&...args)
{
    assert(formatStr);
    std::string msg = strings::tryFormat(formatStr, std::forward<Args>(args)...);
    arms::log<L>(msg);
    throw std::runtime_error(msg);
}

/*
 * popupLog will not display a log message on the screen if the log level is
 * below the configured value.
 */
template <arms::LogLevel L = LOG_ERROR, typename... Args>
inline void popupLog(char const *const formatStr, Args &&...args)
{
    assert(formatStr);

    constexpr auto kdialogBoxType = []() -> char const * {
        char const *boxType;
        if constexpr (L == LOG_WARNING || L == LOG_ERROR || L == LOG_CRITICAL)
        {
            boxType = "--error";
        }
        else
        {
            boxType = "--msgbox";
        }
        return boxType;
    };

    constexpr auto zenityBoxType = []() -> char const * {
        char const *boxType;
        if constexpr (L == LOG_ERROR || L == LOG_CRITICAL)
        {
            boxType = "--error";
        }
        else if constexpr (L == LOG_WARNING)
        {
            boxType = "--warning";
        }
        else
        {
            boxType = "--info";
        }
        return boxType;
    };

    constexpr spdlog::level::level_enum level = logger::logLevelToSpdLogLevel(L);
    if (logger::g_theLogger->should_log(level))
    {
        std::string msg = strings::tryFormat(formatStr, std::forward<Args>(args)...);
        arms::log<L>(msg);
        int pid{fork()};
        if (pid)
        {
            int status{0};
            waitpid(pid, &status, 0);
        }
        else
        {
            if (-1 == execlp("kdialog", "kdialog", kdialogBoxType(), msg.c_str(), static_cast<char *>(nullptr)))
            {
                execlp("zenity", "zenity", zenityBoxType(), "--no-markup", "--text", msg.c_str(), "--width=320",
                       static_cast<char *>(nullptr));
            }
        }
    }
}
} // namespace arms
