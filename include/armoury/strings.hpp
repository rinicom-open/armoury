#pragma once

#include <iomanip>
#include <map>
#include <spdlog/fmt/fmt.h>
#include <string>

namespace arms::strings
{

template <typename... Args>
inline std::string tryFormat(std::string_view formatStr, Args &&...args) noexcept
{
    if (not formatStr.data())
    {
        return {};
    }

    std::string msg;
    try
    {
        if constexpr (sizeof...(args) > 0)
        {
            // msg = std::vformat(formatStr, std::make_format_args(std::forward<Args>(args)...));
            msg = fmt::format(fmt::runtime(formatStr), std::forward<Args>(args)...);
        }
        else
        {
            msg = formatStr;
        }
    }
    catch (std::exception const &e)
    {
        try
        {
            msg = "An unknown error occurred! ";
            msg.append(formatStr);
            msg.append(" - Formatting error: ");
            msg.append(e.what());
        }
        catch (...)
        {
        }
    }
    catch (...)
    {
        try
        {
            msg = "An unknown error occurred!";
        }
        catch (...)
        {
        }
    }
    return msg;
}

inline const std::string urlEncode(std::string const &s)
{
    std::ostringstream os;
    for (std::string::const_iterator ci = s.begin(); ci != s.end(); ++ci)
    {
        if ((*ci >= 'a' && *ci <= 'z') || (*ci >= 'A' && *ci <= 'Z') || (*ci >= '0' && *ci <= '9'))
        { // allowed
            os << *ci;
        }
        else if (*ci == ' ')
        {
            os << '+';
        }
        else
        {
            os << '%' << std::hex << std::uppercase << (*ci >> 4) << (*ci % 16);
        }
    }
    return os.str();
}

class Registry
{
  public:
    void insertOrAssign(std::string const &key, std::string const &value, bool withUrlEncoded = true)
    {
        m_archive.insert_or_assign(key, value);
        if (withUrlEncoded)
        {
            m_archive.insert_or_assign("url_" + key, urlEncode(value));
        }
    }

    void erase(std::string const &key)
    {
        m_archive.erase(key);
        m_archive.erase("url_" + key);
    }

    std::string at(std::string const &key) const
    {
        return m_archive.at(key);
    }

    std::string replacePlaceholders(std::string const &input) const
    {
        // TODO : make constexpr in c++20
        const std::string phStart{"<-{"};
        const std::string phEnd{"}->"};

        std::string result = input;
        for (auto const &[key_str, replacement] : m_archive)
        {
            std::string key = phStart + key_str + phEnd;
            for (std::size_t pos = result.find(key); pos != std::string::npos; pos = result.find(key))
            {
                result.replace(pos, key.length(), replacement);
            }
        }

        while (true)
        {
            std::size_t phStartIdx = result.find(phStart);
            std::size_t phEndIdx = result.find(phEnd);
            if (phStartIdx == std::string::npos && phEndIdx == std::string::npos)
            {
                break;
            }
            if (phStartIdx == std::string::npos)
            {
                phStartIdx = 0;
            }
            if (phEndIdx == std::string::npos)
            {
                result.erase(phStartIdx);
                continue;
            }
            result.erase(phStartIdx, phEndIdx + phEnd.size() - phStartIdx);
        }
        return result;
    }

  private:
    std::map<std::string, std::string> m_archive;
};

} // namespace arms::strings
