#pragma once

#include <armoury/Lockable.hpp>
#include <armoury/misc.hpp>
#include <armoury/time.hpp>

#include <condition_variable>
#include <mutex>
#include <optional>

namespace arms
{

template <typename TDATA, int32_t N>
class RingBuffer
{
    static_assert(N >= 2, "RingBuffer must have at least a capacity of 2.");

  public:
    template <typename... Args>
    explicit RingBuffer(Args const &...args)
    {
        static_assert(std::is_constructible_v<TDATA, Args...>,
                      "RingBuffer: type is not constructible with arguments provided.");
        for (auto &d : m_buf)
        {
            d = std::move(TDATA(args...));
        }
    }

    RingBuffer() = default;

    template <template <class> class LockType>
    using LockedDataType = LockedData<TDATA, LockType>;

    using DataType = TDATA;

    std::optional<LockedData<TDATA, WriteLock>> leader(bool jumpFollower = false, int64_t timeoutNs = -1)
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        int32_t nextWIdx = moduloSum(N, m_leaderIdx, 1);
        if (m_followerIdx == nextWIdx)
        {
            if (jumpFollower)
            {
                nextWIdx = moduloSum(N, m_followerIdx, 1);
            }
            else if (timeoutNs == 0)
            {
                return std::nullopt;
            }
            else if (timeoutNs < 0)
            {
                m_leaderCv.wait(lock, [&]() { return m_followerIdx != nextWIdx; });
            }
            else if (!m_leaderCv.wait_for(lock, std::chrono::nanoseconds(timeoutNs),
                                          [&]() { return m_followerIdx != nextWIdx; }))
            {
                return std::nullopt;
            }
        }
        m_leaderIdx = nextWIdx;
        auto [LockedData] = makeLocked<WriteLock>(m_buf[m_leaderIdx]);
        lock.unlock();
        m_followerCv.notify_one();
        return std::move(LockedData);
    }

    template <template <class> class LockType = ReadLock>
    auto follower(bool skipToLatest = false, int64_t timeoutNs = -1)
        -> std::optional<std::conditional_t<std::is_same_v<LockType<TDATA>, ReadLock<TDATA>>,
                                            LockedData<TDATA, ReadLock>, LockedData<TDATA, WriteLock>>>
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        if (skipToLatest && (m_followerIdx != m_leaderIdx))
        {
            m_followerIdx = moduloSum(N, m_leaderIdx, -1);
        }
        if (m_followerIdx == m_leaderIdx)
        {
            if (timeoutNs == 0)
            {
                return std::nullopt;
            }
            else if (timeoutNs < 0)
            {
                m_followerCv.wait(lock, [&]() { return m_followerIdx != m_leaderIdx; });
            }
            else if (!m_followerCv.wait_for(lock, std::chrono::nanoseconds(timeoutNs),
                                            [&]() { return m_followerIdx != m_leaderIdx; }))
            {
                return std::nullopt;
            }
        }
        m_followerIdx = moduloSum(N, m_followerIdx, 1);
        auto [LockedData] = makeLocked<LockType>(m_buf[m_followerIdx]);
        lock.unlock();
        m_leaderCv.notify_one();
        return std::move(LockedData);
    }

    /**
     * @brief is_empty.
     * @return bool indicating whether data exists in the ring buffer to be read.
     * Returns false if the leader currently has locked data but has not yet
     * returned it to the ring buffer.
     */
    bool isEmpty() const
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        return (m_followerIdx == m_leaderIdx);
    }

    int32_t size() const
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        int32_t diff = m_leaderIdx - m_followerIdx;
        if (diff >= 0)
        {
            return diff;
        }
        return N + diff;
    }

    static constexpr int32_t capacity()
    {
        return N;
    }

  private:
    std::array<Lockable<TDATA>, N> m_buf;

    int32_t m_leaderIdx{-1};
    std::condition_variable m_leaderCv;

    int32_t m_followerIdx{-1};
    std::condition_variable m_followerCv;

    mutable std::mutex m_mtx;
};

} // namespace arms
