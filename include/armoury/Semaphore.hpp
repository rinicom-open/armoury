#pragma once

#include "time.hpp"

#include <condition_variable>
#include <cstdint>
#include <mutex>

namespace arms
{

class Semaphore
{
  public:
    explicit Semaphore(bool signal = false) : m_count(signal ? 1 : 0)
    {
    }
    Semaphore(Semaphore const &) = delete;
    Semaphore &operator=(Semaphore const &) = delete;
    Semaphore(Semaphore &&) noexcept = delete;
    Semaphore &operator=(Semaphore &&) noexcept = delete;

    void signal()
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        ++m_count;
        m_cv.notify_one();
    }

    void binarySignal()
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        m_count = 1;
        m_cv.notify_one();
    }

    void wait()
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        m_cv.wait(lock, [this]() { return m_count > 0; });
        --m_count;
    }

    bool check() const
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        return (m_count > 0);
    }

    bool waitFor(int64_t timeoutNs)
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        bool result = m_cv.wait_for(lock, std::chrono::nanoseconds(timeoutNs), [this]() { return m_count > 0; });
        if (result)
        {
            --m_count;
        }
        return result;
    }

    bool checkFor(int64_t timeoutNs) const
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        return m_cv.wait_for(lock, std::chrono::nanoseconds(timeoutNs), [this]() { return m_count > 0; });
    }

    bool waitUntil(time::Timestamp timestamp)
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        bool result = m_cv.wait_until(lock, timestamp, [this]() { return m_count > 0; });
        if (result)
        {
            --m_count;
        }
        return result;
    }

    bool checkUntil(time::Timestamp timestamp) const
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        return m_cv.wait_until(lock, timestamp, [this]() { return m_count > 0; });
    }

    bool tryWithoutWait()
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        if (m_count > 0)
        {
            --m_count;
            return true;
        }
        return false;
    }

    void reset()
    {
        std::unique_lock<std::mutex> lock(m_mtx);
        m_count = 0;
    }

  private:
    mutable std::mutex m_mtx;
    mutable std::condition_variable m_cv;
    int m_count{0};
};

} // namespace arms
