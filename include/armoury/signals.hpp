#pragma once

#include "logger.hpp"

#include <csignal>
#include <thread>
extern "C"
{
#include <sys/signalfd.h>
}

namespace arms::signals
{
namespace detail
{
inline std::atomic_flag g_exitProgram = ATOMIC_FLAG_INIT;
}

inline bool shouldExit()
{
    return detail::g_exitProgram.test();
}

// requestProgramExit is lock-free, reentrant, signal handler safe.
inline void requestProgramExit()
{
    detail::g_exitProgram.test_and_set();
    detail::g_exitProgram.notify_all();
}

inline void waitForProgramExitRequest()
{
    detail::g_exitProgram.wait(false);
}

extern "C" inline void quitHandler(int /*signal*/)
{
    requestProgramExit();
}

inline thread_local std::atomic<void (*)(int)> g_customThreadSigHandler = nullptr;

static_assert(decltype(g_customThreadSigHandler)::is_always_lock_free, "Atomic function pointer must be lock-free!");

extern "C" inline void customSignalHandlerCaller(int signal)
{
    void (*func)(int) = g_customThreadSigHandler.load();
    if (func)
    {
        func(signal);
    }
}

constexpr inline int g_customInterruptSignal{SIGUSR2};

inline void initSignals()
{
    struct sigaction sa = {};
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = quitHandler;

    if (!(sigaction(SIGINT, &sa, NULL) == 0 && sigaction(SIGTERM, &sa, NULL) == 0 &&
          sigaction(SIGQUIT, &sa, NULL) == 0))
    {
        arms::throwLog("Error setting signal handler");
    }
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = customSignalHandlerCaller;
    if (sigaction(g_customInterruptSignal, &sa, nullptr) != 0)
    {
        arms::throwLog("Error calling sigaction: {}", errno);
    }
}

inline void interruptWaitingThread(std::thread &thr)
{
    pthread_kill(thr.native_handle(), g_customInterruptSignal);
}

} // namespace arms::signals
