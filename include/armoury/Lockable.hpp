#pragma once

#include <condition_variable>
// #include <memory>
#include <mutex>
#include <shared_mutex>
#include <utility>

namespace arms
{

/**
 * @brief SharedMutex is intended to be used where the mutex needs to be unlocked in
 * a different thread to the one that locked it. It will likely be a bit slower than
 * std::shared_mutex.
 */
class SharedMutex
{
  public:
    void lock()
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        m_cv.wait(lock, [&]() { return m_sharedState == 0; });
        m_sharedState = -1;
    }
    bool try_lock()
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        if (m_sharedState == 0)
        {
            m_sharedState = -1;
            return true;
        }
        return false;
    }
    void unlock()
    {
        {
            std::unique_lock<std::mutex> lock{m_mtx};
            m_sharedState = 0;
        }
        m_cv.notify_all();
    }
    void lock_shared()
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        m_cv.wait(lock, [&]() { return m_sharedState >= 0; });
        ++m_sharedState;
    }
    bool try_lock_shared()
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        if (m_sharedState >= 0)
        {
            ++m_sharedState;
            return true;
        }
        return false;
    }
    void unlock_shared()
    {
        {
            std::unique_lock<std::mutex> lock{m_mtx};
            --m_sharedState;
        }
        m_cv.notify_all();
    }

  private:
    std::mutex m_mtx;
    std::condition_variable m_cv;
    int m_sharedState{};
};

using lockable_mutex_t = SharedMutex;

struct BadLockableAccess : public std::exception
{
};

template <class S>
class Lockable;

template <class T>
class ReadLock
{
  public:
    operator T const &() const
    {
        return get();
    }

    T const &get() const
    {
        if (!data)
        {
            throw BadLockableAccess();
        }
        return *data;
    }

  protected:
    std::shared_lock<lockable_mutex_t> lockObj{};
    T const *data{};
    ReadLock(lockable_mutex_t &mtx, T const *data) : lockObj{mtx, std::defer_lock}, data{data}
    {
    }
    ReadLock() = default;
};

template <class T, template <class> class V>
class LockedData;

template <class T>
class WriteLock
{
  public:
    operator T &()
    {
        return get();
    }

    T &get()
    {
        if (!data)
        {
            throw BadLockableAccess();
        }
        return *data;
    }

  protected:
    std::unique_lock<lockable_mutex_t> lockObj;
    T *data{};
    WriteLock(lockable_mutex_t &mtx, T *data) : lockObj{mtx, std::defer_lock}, data{data}
    {
    }
    WriteLock() = default;
};

template <class T, template <class> class V>
class LockedData : public V<T>
{
    static_assert(std::is_same<V<T>, ReadLock<T>>::value || std::is_same<V<T>, WriteLock<T>>::value,
                  "LockedData type can only be ReadLock or WriteLock.");

  public:
    void lock()
    {
        V<T>::lockObj.lock();
    }

    void unlock()
    {
        V<T>::lockObj.unlock();
    }

    // Required naming for compatibility with standard library
    bool try_lock() // NOLINT
    {
        return V<T>::lockObj.try_lock();
    }

    LockedData() = delete;
    LockedData(LockedData const &) = delete;
    LockedData(LockedData &&other) noexcept
    {
        this->data = std::exchange(other.data, nullptr);
        this->lockObj = std::move(other.lockObj);
    }
    LockedData &operator=(LockedData const &) = delete;
    LockedData &operator=(LockedData &&other) noexcept
    {
        if (this != &other)
        {
            this->data = std::move(other.data);
            this->lockObj = std::move(other.lockObj);
        }
        return *this;
    }
    ~LockedData() = default;

    template <std::convertible_to<T> U>
    LockedData &operator=(U &&other) requires std::same_as<V<T>, WriteLock<T>>
    {
        *this->data = std::forward<U>(other);
        return *this;
    }

  private:
    template <class S>
    friend class Lockable;

    LockedData(lockable_mutex_t &mtx, T *data) : V<T>(mtx, data)
    {
    }

    LockedData(lockable_mutex_t &mtx, T const *data) : V<T>(mtx, data)
    {
    }
};

template <class S>
class Lockable
{
  public:
    explicit operator LockedData<S, WriteLock>() &
    {
        return LockedData<S, WriteLock>(this->m_mtx, &this->m_data);
    }

    operator LockedData<S, ReadLock>() const && = delete;
    explicit operator LockedData<S, ReadLock>() const &
    {
        return LockedData<S, ReadLock>(this->m_mtx, &this->m_data);
    }

    Lockable() = default;
    Lockable &operator=(Lockable &&) noexcept = delete;
    Lockable(Lockable &&) noexcept = delete;
    Lockable &operator=(Lockable const &) = delete;
    Lockable(Lockable const &) = delete;

    explicit Lockable(S &&data) : m_data{std::move(data)}
    {
    }

    /**
     * @brief The forwarding constructor passes the arguments after the first tag to the Lockable's unique_ptr that
     * holds the data.
     * @note The standard library provides the std::in_place constant that can be used as the first argument
     */
    template <typename... Ts>
    explicit Lockable(std::in_place_t, Ts &&...ts) : m_data{std::forward<Ts>(ts)...}
    {
    }

    Lockable &operator=(S &&data)
    {
        std::unique_lock lock{m_mtx};
        this->m_data = std::move(data);
        return *this;
    }

  private:
    mutable lockable_mutex_t m_mtx{};
    S m_data{};
};

namespace detail
{

template <class>
struct LockableTraits;

template <template <class T> class Lockable, typename T>
struct LockableTraits<Lockable<T>>
{
    using ValueType = T;
};

} // namespace detail

template <template <class> class... Vs, typename... Ts>
auto makeLocked(Ts &&...data)
{
    static_assert(sizeof...(data) > 0, "makeLocked<...>( args... ) called with no arguments.");
    static_assert(sizeof...(Vs) == sizeof...(Ts), "makeLocked<...>( ... ) must be called with the same number of "
                                                  "ReadLock|WriteLock template arguments as function arguments "
                                                  "(Lockable objects).");

    auto result = std::tuple<LockedData<typename detail::LockableTraits<std::decay_t<Ts>>::ValueType, Vs>...>{
        std::forward<Ts>(data)...};

    if constexpr (sizeof...(data) == 1)
    {
        std::get<0>(result).lock();
    }
    else if constexpr (sizeof...(data) > 1)
    {
        std::apply(std::lock<LockedData<typename detail::LockableTraits<std::decay_t<Ts>>::ValueType, Vs>...>, result);
    }

    return result;
}

} // namespace arms
