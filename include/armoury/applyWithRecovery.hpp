#pragma once

#include "logger.hpp"

namespace arms
{

/*
 * @brief NonErrorException is type of std::runtime_error, specifically intended to use with applyWithRecovery.
 * If a NonErrorException is thrown by toRecover(), it will be caught and the program will not terminate.
 */
class NonErrorException : public std::runtime_error
{
  public:
    explicit NonErrorException(char const *message) : std::runtime_error(message)
    {
    }
    explicit NonErrorException(std::string const &message) : std::runtime_error(message)
    {
    }
    NonErrorException(NonErrorException const &) = default;
    NonErrorException &operator=(NonErrorException const &) = default;
    NonErrorException(NonErrorException &&) = default;
    NonErrorException &operator=(NonErrorException &&) = default;
    ~NonErrorException() = default;
};

/*
 * @brief applyWithRecovery will invoke the first argument, toApply, and if no exception is thrown, forward it's return
 * value after then invoking onSuccess. If an exception was thrown by toApply, then toRecover will be invoked before the
 * exception is rethrown; onSuccess will not be invoked.
 * If either toRecover or onSuccess throw uncaught exceptions (except NonErrorException, which will be caught and
 * ignored,) then applyWithRecovery will call std::terminate().
 */
inline decltype(auto) applyWithRecovery(auto toApply, auto toRecover, auto onSuccess)
{
    struct OnScopeExit
    {
        ~OnScopeExit()
        {
            if (enabled)
            {
                // Any action taken by onSuccess that is not mandatory, and which might throw
                // should be placed in a try block.
                try
                {
                    static_cast<void>(onExit());
                }
                catch (NonErrorException const &e)
                {
                }
                catch (std::exception const &e)
                {
                    arms::log<arms::LOG_CRITICAL>("Unrecoverable failure finalizing change: {}", e.what());
                    std::terminate();
                }
                catch (...)
                {
                    arms::log<arms::LOG_CRITICAL>("Unrecoverable failure finalizing change");
                    std::terminate();
                }
            }
        }
        decltype(onSuccess) onExit;
        bool enabled{true};
    };

    OnScopeExit invokeOnSuccess{std::move(onSuccess), true};

    auto doRecovery = [&]() -> void {
        try
        {
            static_cast<void>(toRecover());
        }
        catch (NonErrorException const &e)
        {
        }
        catch (std::exception const &e)
        {
            arms::log<arms::LOG_CRITICAL>("Unrecoverable failure: {}", e.what());
            std::terminate();
        }
        catch (...)
        {
            arms::log<arms::LOG_CRITICAL>("Unrecoverable failure");
            std::terminate();
        }
    };

    try
    {
        return toApply();
    }
    catch (...)
    {
        invokeOnSuccess.enabled = false;
        doRecovery();
        throw;
    }
}

} // namespace arms
