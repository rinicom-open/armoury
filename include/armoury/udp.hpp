#pragma once

#include <armoury/RingBuffer.hpp>
#include <armoury/ThreadWarden.hpp>
#include <armoury/logger.hpp>
#include <armoury/time.hpp>
#include <asio.hpp>
#include <bitset>
#include <span>
#include <spdlog/fmt/ostr.h>
#include <string>

namespace arms::udp
{
inline constexpr std::size_t g_bufferElementCapacity = 1550;

/**
 * @brief The Buffer class.  A fixed-size byte array + a variable length of in-use data.
 */
class Buffer
{
  public:
    using DataType = std::array<std::byte, g_bufferElementCapacity>;

    auto getBegin() const
    {
        return m_data.begin();
    }

    auto getEnd() const
    {
        return std::next(m_data.begin(), getSize());
    }

    std::size_t getSize() const
    {
        return m_used;
    }

    constexpr std::size_t getCapacity() const
    {
        return g_bufferElementCapacity;
    }

    /**
     * @brief setData copy the provided sequence of data into the buffer
     * @param start An iterator to the start of a sequence of data
     * @param end An iterator to the end of a sequence of data
     *
     * @note negative or empty ranges will set the buffer size to zero
     */
    template <typename InputIterator>
    void setData(InputIterator start, InputIterator end)
    {
        using Type = typename std::iterator_traits<InputIterator>::value_type;
        using Diff = typename std::iterator_traits<InputIterator>::difference_type;

        if (Diff const range_size = std::distance(start, end); range_size < 0)
        {
            // std::copy does nothing if you pass it a negative range, so mirroring that behaviour here
            arms::log<arms::LOG_WARNING>("Start iterator comes after end.  Range size:{}", range_size);
            m_used = 0u;
        }
        else if (std::size_t const size = sizeof(Type) * range_size; size <= getCapacity())
        {
            auto source_bytes = std::as_bytes(std::span{start, end});
            std::copy(source_bytes.begin(), source_bytes.end(), m_data.data());
            m_used = size;
        }
        else
        {
            arms::throwLog("Buffer data capacity exceeded.  Capacity:{}.  Requested size:{}", getCapacity(), size);
        }
    }

    /**
     * @brief getAsioBuffer
     * @param buffer The data to be converted
     * @returns A asio::buffer
     *
     * @note For custom types held in a PayloadScope, provide an overload of this function in the same namespace as the
     * custom type.
     */
    inline friend auto getAsioBuffer(Buffer &buffer)
    {
        return asio::buffer(buffer.getData(), buffer.getSize());
    }

  private:
    DataType const &getData() const
    {
        return m_data;
    }
    DataType &getData()
    {
        return m_data;
    }

    DataType m_data{};
    std::size_t m_used{};
};

/**
 * @brief The UdpEndpoint class encapsulates an address and a port.
 */
class UdpEndpoint
{
  public:
    using AddressType = std::string;
    using PortType = unsigned short;

    /**
     * @brief UdpEndpoint
     * @param address an ip address
     * @param udpPort the port
     */
    UdpEndpoint(AddressType const &address, PortType udpPort = 3000)
        : m_endpoint{asio::ip::udp::endpoint{asio::ip::make_address(address), udpPort}}
    {
    }

    /**
     * @brief Construct an empty UdpEndpoint
     */
    UdpEndpoint() = default;

    asio::ip::udp::endpoint const &get() const
    {
        return m_endpoint;
    }

    std::string toString() const
    {
        return m_endpoint.address().to_string() + ":" + std::to_string(m_endpoint.port());
    }

    bool operator==(UdpEndpoint const &rhs) const
    {
        return m_endpoint == rhs.m_endpoint;
    }

    bool operator!=(UdpEndpoint const &rhs) const
    {
        return m_endpoint != rhs.m_endpoint;
    }

  private:
    asio::ip::udp::endpoint m_endpoint{};
};

using UdpEndpoints = std::vector<UdpEndpoint>;

template <typename InputIterator>
inline void setData(UdpEndpoint const &, Buffer &buffer, InputIterator start, InputIterator end)
{
    buffer.setData(start, end);
}

/**
 * @brief The EndpointBuffer records a buffer and an endpoint
 * @note Inheritance is used here just for convenience of implementation.  It's not intended for an EndpointBuffer to be
 * referred to by a base class pointer or reference.
 */
class EndpointBuffer : public Buffer
{
  public:
    UdpEndpoint const &getEndpoint() const
    {
        return m_endpoint;
    }

    void setEndpoint(UdpEndpoint endpoint)
    {
        m_endpoint = std::move(endpoint);
    }

  private:
    UdpEndpoint m_endpoint{};
};

template <typename InputIterator>
inline void setData(UdpEndpoint const &endpoint, EndpointBuffer &buffer, InputIterator start, InputIterator end)
{
    buffer.setData(start, end);
    buffer.setEndpoint(endpoint);
}

/**
 * @brief Ring buffer for incoming udp data.
 * @note Change the buffer type to Buffer if endpoint information isn't required on received messages.
 */
using IncomingBuffer = arms::RingBuffer<EndpointBuffer, 300>;

/**
 * @brief The SingleReceiver class continually schedules operations on an asio io_service to receive data from a single
 * endpoint
 *
 * @note The entire interface is private as the type cannot be sensibly moved or copied, due to the `this` capture in
 * its registered asio callback.  Instead, the type is held by the friend class, SingleReceiverProxy
 */
class SingleReceiver
{
  private:
    SingleReceiver(IncomingBuffer &incomingBuffer, asio::io_service &ioservice, UdpEndpoint endpoint,
                   std::function<void()> onHandleReceive)
        : m_buffer{incomingBuffer}, m_receiverEndpoint{std::move(endpoint)}, m_onDataReady{std::move(onHandleReceive)},
          m_socket{ioservice}
    {
        m_socket.open(asio::ip::udp::v4());
        m_socket.bind(m_receiverEndpoint.get());
        asyncWait();
    }

    void handleReceive(std::error_code const &error, std::size_t bytesTransferred)
    {
        arms::log<arms::LOG_TRACE>("UDP rx client: endpoint {}, packet size {}", m_receiverEndpoint.toString(),
                                   bytesTransferred);
        if (!error || error == asio::error::message_size)
        {
            bool dataReady{false};
            assert(!error);
            if (auto lckBuf = m_buffer.leader(true); lckBuf.has_value())
            {
                auto rxBegin = m_recvBuffer.begin();
                setData(m_receiverEndpoint, lckBuf.value().get(), rxBegin, std::next(rxBegin, bytesTransferred));
                dataReady = true;
            }
            if (dataReady && m_onDataReady)
            {
                m_onDataReady();
            }
        }

        if (error)
        {
            arms::log<LOG_WARNING>("UDP receive failed: endpoint {}, message {}", m_receiverEndpoint.toString(),
                                   error.message());
        }
        asyncWait();
    }

    void asyncWait()
    {
        m_socket.async_receive_from(asio::buffer(m_recvBuffer), m_endpoint,
                                    [this](auto const &error, auto bytes) { handleReceive(error, bytes); });
    }

    IncomingBuffer &m_buffer;

    UdpEndpoint m_receiverEndpoint;
    asio::ip::udp::endpoint m_endpoint;
    std::function<void()> m_onDataReady;

    asio::ip::udp::socket m_socket;
    std::array<std::byte, g_bufferElementCapacity> m_recvBuffer;

    friend class SingleReceiverProxy;
};

class SingleReceiverProxy
{
  public:
    SingleReceiverProxy(IncomingBuffer &incomingBuffer, asio::io_service &ioservice, UdpEndpoint endpoint,
                        std::function<void()> onHandleReceive)
        : m_receiver{makeReceiver(incomingBuffer, ioservice, std::move(endpoint), std::move(onHandleReceive))}
    {
    }

    // Disable copying
    SingleReceiverProxy(SingleReceiverProxy const &) = delete;
    SingleReceiverProxy &operator=(SingleReceiverProxy const &) = delete;

    // Move-only
    SingleReceiverProxy(SingleReceiverProxy &&) noexcept = default;
    SingleReceiverProxy &operator=(SingleReceiverProxy &&) & noexcept = default;

  private:
    using SingleReceiverPtr = std::unique_ptr<SingleReceiver>;
    static SingleReceiverPtr makeReceiver(IncomingBuffer &incomingBuffer, asio::io_service &ioservice,
                                          UdpEndpoint endpoint, std::function<void()> onHandleReceive)
    {
        return SingleReceiverPtr{
            new SingleReceiver(incomingBuffer, ioservice, std::move(endpoint), std::move(onHandleReceive))};
    }
    SingleReceiverPtr m_receiver{};
};

/**
 * @brief Receivers holds scheduled tasks on asio to receive udp data from specific endpoints
 *
 * @note It's intended for the Receivers container to reserve the storage it needs for each endpoint before it's
 * populated with receivers.  However, the SingleReceiverProxy acts as a safety net in-case the storage isn't reserved,
 * as it can be safely move-constructed.
 */
using Receivers = std::vector<SingleReceiverProxy>;

/**
 * @brief The ReceiverWorker class is created by a ThreadWarden in the Receiver class.  It retrieves messages
 * received on the specified endpoints
 */
class ReceiverWorker
{
  public:
    static constexpr char const *g_workerName{"udp_rx"};
    static constexpr bool g_copyDataOnce{true};
    struct Input
    {
    } dataIn;
    struct Output
    {
    } dataOut;

    /**
     * @brief ReceiverWorker Constructor
     * @param incomingBuffer Where received messages are put
     * @param ioservice The asio execution context for udp operations
     * @param onHandleReceive An optional thread-safe callback, called when data is received
     * @param endpoints A collection of one or more endpoints
     */
    ReceiverWorker(IncomingBuffer &incomingBuffer, asio::io_service &ioservice, std::function<void()> onHandleReceive,
                   UdpEndpoints endpoints)
        : m_ioservice{ioservice}
    {
        if (endpoints.empty())
        {
            arms::throwLog("At least one endpoint should be given to a ReceiverWorker");
        }
        m_receivers.reserve(endpoints.size());
        for (auto endpoint : endpoints)
        {
            m_receivers.emplace_back(incomingBuffer, m_ioservice, std::move(endpoint), onHandleReceive);
        }
        // This function must be called prior to any second or later set of invocations of m_ioservice.run()
        // https://think-async.com/Asio/asio-1.21.0/doc/asio/reference/io_context/restart.html
        m_ioservice.restart();
    }

    int work()
    {
        arms::log("UDP client: Running");
        m_ioservice.run(); // shouldn't return until m_ioservice.stop() is called
        arms::log("UDP client: Work ended");

        // The service shouldn't run out of work from the receivers, and so should only return after a call to
        // m_ioservice.stop().
        //
        // The endWork semaphore should have been signalled before the m_ioservice.stop() call.
        // If not, this is an error.
        if (!arms::g_threadInteraction->endWork.check())
        {
            arms::throwLog("UDP Receiver worker thread ended unexpectedly");
        }
        return 0;
    }

  private:
    asio::io_service &m_ioservice;
    Receivers m_receivers{};
};

/**
 * @brief The Payload struct.  Used by Receiver to return copies of the last received payload.
 */
struct Payload
{
    using Data = std::vector<std::byte>;
    Payload() = default;
    explicit Payload(Data bytes) : data{std::move(bytes)}
    {
    }
    explicit Payload(Buffer const &source) : data{source.getBegin(), source.getEnd()}
    {
    }
    explicit Payload(EndpointBuffer const &source)
        : data{source.getBegin(), source.getEnd()}, endpoint{source.getEndpoint()}
    {
    }
    Data data{};
    std::optional<UdpEndpoint> endpoint{};
};

/**
 * @brief The OperationResult enum.  Used as a return type for some operations that can time out.
 */
enum class OperationResult
{
    Success,
    TimedOut,
    Error
};

template <typename Worker>
void ensureStop(Worker &worker, asio::io_service &m_ioservice)
{
    // The m_ioservice.stop() call causes m_ioservice.run() in the worker to return.
    arms::ensureStop(worker, [&]() { m_ioservice.stop(); });
}

/**
 * @brief The Receiver class receives udp packets from the specified endpoint(s).
 */
class Receiver
{
  public:
    using BufferType = IncomingBuffer::DataType;

    explicit Receiver(UdpEndpoints toBind, std::function<void()> onDataReady = nullptr)
        : m_ioservice{}, m_worker{m_buffer, m_ioservice, onDataReady, std::move(toBind)}
    {
        m_worker.start();
    }

    explicit Receiver(UdpEndpoint toBind, std::function<void()> onDataReady = nullptr)
        : Receiver{UdpEndpoints{std::move(toBind)}, std::move(onDataReady)}
    {
    }

    Receiver(UdpEndpoint::AddressType const &address, UdpEndpoint::PortType port,
             std::function<void()> onDataReady = nullptr)
        : Receiver{UdpEndpoint{address, port}, std::move(onDataReady)}
    {
    }

    ~Receiver()
    {
        ensureStop(m_worker, m_ioservice);
    }

    /**
     * @brief Copy a udp buffer from a udp listener into pre-allocated storage.
     * @param destination The buffer to copy the received udp data into.
     * @param timeout A std::chrono::duration within which to retrieve udp.  Pass a 0 duration for no timeout.
     * @note Negative values of std::chrono::duration will block.
     * @return Whether or not the destination was copied to.
     */
    template <typename Rep, typename Period>
    OperationResult copyBuffer(BufferType &destination, std::chrono::duration<Rep, Period> timeout)
    {
        return copyBuffer(destination, arms::time::toNanoTicks(timeout));
    }

    /**
     * @brief Wait indefinitely for data from a udp listener and then copy it into pre-allocated storage.
     * @param destination The buffer to copy the received data into.
     * @return Whether or not the destination was copied to.
     */
    OperationResult copyBuffer(BufferType &destination)
    {
        return copyBuffer(destination, -1);
    }

    /**
     * @brief Get a payload from a udp listener.
     * @param A std::chrono::duration within which to retrieve a payload.  Pass a 0 duration for no timeout.
     * @return A heap-allocated copy of the received payload.
     */
    template <typename Rep, typename Period>
    [[nodiscard]] std::optional<Payload> getPayload(std::chrono::duration<Rep, Period> timeout)
    {
        return getPayload(arms::time::toNanoTicks(timeout));
    }
    /**
     * @brief Wait indefinitely for a payload from a udp listener.
     * @return A heap-allocated copy of the received payload.
     */
    [[nodiscard]] std::optional<Payload> getPayload()
    {
        return getPayload(-1);
    }

    bool checkNetworkThreadFailed(bool restartOnFailure = true)
    {
        bool const failed = m_worker.checkAndRestartOnFailure(restartOnFailure);
        if (failed)
        {
            arms::log<LOG_WARNING>("UDP: listener thread failed");
        }
        return failed;
    }

    [[nodiscard]] int32_t size() const
    {
        return m_buffer.size();
    }

  private:
    OperationResult copyBuffer(BufferType &destination, int64_t timeoutNs)
    {
        if (auto buffer = m_buffer.follower(false, timeoutNs); buffer.has_value())
        {
            destination = buffer.value().get();
            return OperationResult::Success;
        }
        else
        {
            return OperationResult::TimedOut;
        }
    }

    [[nodiscard]] std::optional<Payload> getPayload(int64_t timeoutNs)
    {
        std::optional<Payload> result{};
        if (auto buffer = m_buffer.follower(false, timeoutNs); buffer.has_value())
        {
            result = Payload{buffer.value().get()};
        }
        return result;
    }

    IncomingBuffer m_buffer;
    asio::io_service m_ioservice;
    arms::ThreadWarden<ReceiverWorker, IncomingBuffer &, asio::io_service &, std::function<void()>, UdpEndpoints>
        m_worker;
};

/**
 * @brief The BufferOwner class holds a fixed number of Buffer objects.  Each Buffer can be reserved by a
 * ReferenceCountedElement
 * @tparam Capacity The number of Buffer objects the owner holds
 */
template <std::size_t Capacity, typename BufferType = Buffer>
class BufferOwner
{
  public:
    using ElementType = BufferType;

    /**
     * @brief The ElementAdapter class allows a send operation to propagate errors back to the BufferOwner
     */
    class ElementAdapter : public ElementType
    {
      public:
        using Error = asio::error_code;

        void setError(Error e)
        {
            m_error = std::move(e);
        }

        bool hasError() const
        {
            return m_error.value() != 0;
        }

        void clearError()
        {
            m_error = Error{};
        }

      private:
        Error m_error{};
    };

    /**
     * @brief The ReferenceCountedElement class provides access to an ElementAdapter, releasing it when the last
     * reference to the element has gone out of scope.
     */
    class ReferenceCountedElement
    {
      public:
        ReferenceCountedElement(BufferOwner &owner, ElementAdapter &element, std::size_t index)
            : m_element(element),
              m_referenceCount(std::shared_ptr<void>{static_cast<void *>(nullptr),
                                                     [&owner, index](auto const *) { owner.releaseReference(index); }})
        {
        }

        ReferenceCountedElement(ReferenceCountedElement const &) = default;
        ReferenceCountedElement &operator=(ReferenceCountedElement const &) & = default;

        ElementAdapter const *operator->() const &
        {
            return get();
        }

        ElementAdapter *operator->() &
        {
            return get();
        }

        ElementAdapter const &operator*() const &
        {
            return m_element;
        }

        ElementAdapter &operator*() &
        {
            return m_element;
        }

        ElementAdapter const *get() const &
        {
            assert(hasReferences());
            return &m_element;
        }

        ElementAdapter *get() &
        {
            assert(hasReferences());
            return &m_element;
        }

      private:
        bool hasReferences() const &
        {
            // From en.cppreference.com
            // "In multithreaded environment, this does not imply that the destructor of the managed object has
            // completed."
            return m_referenceCount.use_count() != 0;
        }
        ElementAdapter &m_element;
        std::shared_ptr<void> m_referenceCount{};
    };

    /**
     * Get access to a buffer item, protected by a reference count
     * @param timeout A std::chrono::duration
     * @returns An optional ReferenceCountedElement.  Will be nullopt if the timeout expires.
     */
    template <typename Rep, typename Period>
    [[nodiscard]] std::optional<ReferenceCountedElement> getBufferScope(std::chrono::duration<Rep, Period> timeout)
    {
        return getBufferScopeImpl(timeout);
    }

    /**
     * Get access to a buffer item, protected by a reference count
     * @returns An optional ReferenceCountedElement.  Should always contain an item.
     */
    [[nodiscard]] std::optional<ReferenceCountedElement> getBufferScope()
    {
        auto result = getBufferScopeImpl();
        assert(result);
        return result;
    }

    bool hasErrors() const
    {
        return m_errorCount;
    }

    uint64_t getErrorCount() const
    {
        return m_errorCount;
    }

    void clearErrors()
    {
        m_errorCount.store(0u);
    }

  private:
    void releaseReference(std::size_t index) noexcept // intentionally terminate on exception
    {
        {
            std::unique_lock<std::mutex> lock{m_mtx};
            m_inUse[index] = false;
            if (m_buffer[index].hasError())
            {
                ++m_errorCount;
                m_buffer[index].clearError();
            }
        }
        m_freeIndex.notify_one();
    }

    template <typename... WaitUntilFreeArgs>
    [[nodiscard]] std::optional<ReferenceCountedElement> getBufferScopeImpl(WaitUntilFreeArgs &&...waitArgs)
    {
        std::unique_lock<std::mutex> lock{m_mtx};
        auto getFreeIndex = [this]() {
            std::optional<std::size_t> result{};
            for (auto i = 0u; i < m_inUse.size(); ++i)
            {
                if (!m_inUse[i])
                {
                    result = i;
                    break;
                }
            }
            return result;
        };

        if (m_inUse.all())
        {
            arms::log<arms::LOG_TRACE>("Buffer is full.");
            if (!waitUntilFree(lock, std::forward<WaitUntilFreeArgs>(waitArgs)...))
            {
                return std::nullopt;
            }
        }

        assert(!m_inUse.all());
        auto i = getFreeIndex();
        m_inUse[*i] = true;
        return ReferenceCountedElement{*this, m_buffer.at(*i), *i};
    }

    template <typename Rep, typename Period>
    bool waitUntilFree(std::unique_lock<std::mutex> &lock, std::chrono::duration<Rep, Period> timeout)
    {
        auto test = [this] { return !m_inUse.all(); };
        return m_freeIndex.wait_for(lock, timeout, test);
    }

    bool waitUntilFree(std::unique_lock<std::mutex> &lock)
    {
        auto test = [this] { return !m_inUse.all(); };
        m_freeIndex.wait(lock, test);
        return true;
    }

    mutable std::mutex m_mtx;
    std::condition_variable m_freeIndex;

    using BufferList = std::array<ElementAdapter, Capacity>;
    BufferList m_buffer{};
    std::bitset<Capacity> m_inUse{};
    std::atomic_uint64_t m_errorCount{};
};

/**
 * @brief The AsioWorker class is created by a ThreadWarden in the Sender class.  It runs
 * the asio loop.
 */
class AsioWorker
{
  public:
    static constexpr char const *g_workerName{"asio_loop"};
    static constexpr bool g_copyDataOnce{true};
    struct Input
    {
    } dataIn;
    struct Output
    {
    } dataOut;

    AsioWorker(asio::io_service &ioservice) : m_ioservice{ioservice}, m_work{ioservice.get_executor()}
    {
        // This function must be called prior to any second or later set of invocations of m_ioservice.run()
        // https://think-async.com/Asio/asio-1.21.0/doc/asio/reference/io_context/restart.html
        m_ioservice.restart();
    }

    int work()
    {
        arms::log("ASIO worker: Running");
        m_ioservice.run(); // shouldn't return until m_ioservice.stop() is called
        arms::log("ASIO worker: Work ended");

        // The io_service shouldn't run out of work, because of the executor_work_guard.  So it should only return after
        // m_ioservice.stop() has been called
        //
        // The endWork semaphore should have been signalled before the m_ioservice.stop() call.
        // If not, this is an error.
        if (!arms::g_threadInteraction->endWork.check())
        {
            arms::throwLog("ASIO worker work ended unexpectedly");
        }
        return 0;
    }

  private:
    asio::io_service &m_ioservice;
    asio::executor_work_guard<decltype(m_ioservice.get_executor())> m_work;
};

/**
 * @brief The Sender class is a simple sender for udp packets.
 */
class Sender
{
  public:
    Sender() : m_socket{m_ioservice}, m_worker{m_ioservice}
    {
        m_socket.open(asio::ip::udp::v4());
        m_worker.start();
    }

    ~Sender()
    {
        ensureStop(m_worker, m_ioservice);
    }

    /**
     * @brief sendPayloadData
     * @param endpoint The udp endpoint to sent to.
     * @param payloadScope The payload to send.  The scope type is used to keep the payload alive for the lifetime of
     * the operation.
     * @note Memory can be exhausted by repeatedly issuing sends that don't complete eg. sending to a bad endpoint.
     */
    template <typename PayloadScope>
    void sendPayloadData(UdpEndpoint const &endpoint, PayloadScope payloadScope)
    {
        static_assert(!arms::isBasicLockable<PayloadScope>,
                      "Mutexes should be acquired and released on the same thread.  The payload passed here may be "
                      "copied many times and released on a different thread.");
        if (auto *payloadPtr = payloadScope.get())
        {
            m_socket.async_send_to(getAsioBuffer(*payloadPtr), endpoint.get(),
                                   [payloadScope](asio::error_code const &error, std::size_t bytesTransferred) mutable {
                                       if (!error)
                                       {
                                           arms::log<LOG_TRACE>("UDP sent {} bytes", bytesTransferred);
                                       }
                                       else
                                       {
                                           payloadScope->setError(error);
                                           arms::log<LOG_WARNING>("UDP send failed:{}", error.message());
                                       }
                                   });
        }
    }

    bool checkNetworkThreadFailed(bool restartOnFailure = true)
    {
        bool const failed = m_worker.checkAndRestartOnFailure(restartOnFailure);
        if (failed)
        {
            arms::log<LOG_WARNING>("UDP: sender thread failed");
        }
        return failed;
    }

    void cancelSends()
    {
        m_socket.close();
        m_socket.open(asio::ip::udp::v4());
    }

  private:
    asio::io_service m_ioservice;
    asio::ip::udp::socket m_socket;
    arms::ThreadWarden<AsioWorker, asio::io_service &> m_worker;
};

/**
 * @brief The BufferedSender class is a udp sender that limits the number of concurrent requests and, therefore, the
 * amount of memory captured by asio callbacks.
 *
 * @tparam Capacity The maximum number of concurrent send requests.
 */
template <std::size_t Capacity>
class BufferedSender : private Sender
{
  public:
    /**
     * @brief sendData
     * @param endpoint The udp endpoint to sent to.
     * @param start An iterator to the start of a sequence of data
     * @param end An iterator to the end of a sequence of data
     * @param timeout A std::chrono::duration within which to copy the data sequence to the send buffers.  Pass a 0
     * duration for no timeout.
     *
     * @returns OperationResult indicating success or failure
     *
     * @note Negative values of std::chrono::duration will block.
     * @note An operation can timeout if the Capacity is taken up with send requests.
     */
    template <typename InputIterator, typename Rep, typename Period>
    OperationResult sendData(UdpEndpoint const &endpoint, InputIterator start, InputIterator end,
                             std::chrono::duration<Rep, Period> timeout)
    {
        return sendDataImpl(endpoint, start, end, timeout);
    }

    /**
     * @brief sendData overload that waits indefinitely for space in the send buffer while it's full.
     * @param endpoint The udp endpoint to sent to.
     * @param start An iterator to the start of a sequence of data
     * @param end An iterator to the end of a sequence of data
     *
     * @returns OperationResult indicating success or failure
     */
    template <typename InputIterator>
    OperationResult sendData(UdpEndpoint const &endpoint, InputIterator start, InputIterator end)
    {
        return sendDataImpl(endpoint, start, end);
    }

    void attemptRecovery()
    {
        arms::log<arms::LOG_TRACE>("Error count:{}", m_buffers.getErrorCount());
        cancelSends();
        checkNetworkThreadFailed();
        m_buffers.clearErrors();
    }

    bool hasErrors()
    {
        return m_buffers.hasErrors() || checkNetworkThreadFailed(false);
    }

  private:
    template <typename InputIterator, typename... BufferScopeArgs>
    OperationResult sendDataImpl(UdpEndpoint const &endpoint, InputIterator start, InputIterator end,
                                 BufferScopeArgs &&...args)
    {
        if (auto payloadRef = m_buffers.getBufferScope(std::forward<BufferScopeArgs>(args)...))
        {
            // BufferOwner guarantees that a reference count is associated with a Buffer
            assert(payloadRef->get());

            auto &payload = *payloadRef;
            payload->setData(start, end);
            sendPayloadData(endpoint, payload);
            return OperationResult::Success;
        }
        else
        {
            assert(sizeof...(args) > 0); // Should block until free if no timeout
            return OperationResult::TimedOut;
        }
    }
    BufferOwner<Capacity> m_buffers{};
};

} // namespace arms::udp
