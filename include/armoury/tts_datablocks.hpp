#pragma once

#include "json.hpp"

#include <algorithm>
#include <iomanip>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <variant>

namespace arms::datablock
{

/*!
 * \brief The ElementType enum has values for the 19 data types that can be stored
 * in a datablock, and the 3 container types (datablock, struct, and UDT).
 */
enum class ElementType : int
{
    DATABLOCK = -3,
    STRUCT = -2,
    UDT = -1,
    BOOL = 0,
    BYTE = 1,
    WORD = 2,
    DWORD = 3,
    LWORD = 4,
    SINT = 5,
    INT = 6,
    DINT = 7,
    LINT = 8,
    USINT = 9,
    UINT = 10,
    UDINT = 11,
    ULINT = 12,
    REAL = 13,
    LREAL = 14,
    TIME = 15,
    LTIME = 16,
    DTL = 17,
    CHAR = 18,
    end = 19
};

/*!
 * \brief The DateTime struct represents a timestamp accurate to 1 nanosecond.
 * The member variables mirror the data in a DTL element type.
 * It does not contain timezone/locale information.
 */
struct DateTime
{
    static_assert(sizeof(long long int) == sizeof(std::int64_t),
                  "DateTime assumes long long int to be the same size as std::int64_t.");

    [[nodiscard]] std::string toString() const
    {
        // note: in C++11 str[str.size()] is well defined: '\0'
        TmAndNs rawTmAndNs = toTmAndNs();
        TmAndNs tmAndNs = fixTmAndNs(rawTmAndNs).value_or(rawTmAndNs);
        std::string nsBuf(9, ' ');
        snprintf(nsBuf.data(), nsBuf.size() + 1, "%09lld", static_cast<long long int>(tmAndNs.second));
        static constexpr char const *dateTimeFormat = "%Y-%m-%d %a %H:%M:%S.";
        std::string timeStr = "yyyy-mm-dd aaa HH:MM:SS.";
        std::strftime(timeStr.data(), timeStr.size() + 1, dateTimeFormat, &tmAndNs.first);
        return timeStr + nsBuf;
    }

    /*!
     * \brief isValid checks the validity of the DateTime data. Ie if the member variables represent
     * a real date and time.
     * \return true if it the program is able to check the validity of the timestamp, and it is valid.
     * Otherwise returns false.
     */
    [[nodiscard]] bool isValid() const noexcept
    {
        TmAndNs tmAndNs = toTmAndNs();
        std::optional<TmAndNs> fixedTmAndNs = fixTmAndNs(tmAndNs);
        return fixedTmAndNs.has_value() && areSame(tmAndNs, fixedTmAndNs.value());
    }

    std::uint16_t year{};
    std::uint8_t month{};
    std::uint8_t day{};
    std::uint8_t weekday{};
    std::uint8_t hour{};
    std::uint8_t minute{};
    std::uint8_t second{};
    std::uint32_t nanosecond{};

  private:
    using TmAndNs = std::pair<std::tm, std::int64_t>;

    static constexpr bool areSame(TmAndNs const &tm1, TmAndNs const &tm2) noexcept
    {
        return (tm1.first.tm_year == tm2.first.tm_year && tm1.first.tm_mon == tm2.first.tm_mon &&
                tm1.first.tm_mday == tm2.first.tm_mday && tm1.first.tm_wday == tm2.first.tm_wday &&
                tm1.first.tm_hour == tm2.first.tm_hour && tm1.first.tm_min == tm2.first.tm_min &&
                tm1.first.tm_sec == tm2.first.tm_sec && tm1.first.tm_isdst == tm2.first.tm_isdst &&
                tm1.second == tm2.second);
    }

    static std::optional<TmAndNs> fixTmAndNs(TmAndNs const &tmAndNs) noexcept
    {
        TmAndNs fixed = tmAndNs;
        std::lldiv_t nsDiv = std::lldiv(static_cast<long long int>(tmAndNs.second), 1'000'000'000LL);

        if (additionInRange<decltype(std::tm::tm_sec)>(fixed.first.tm_sec, nsDiv.quot) &&
            std::in_range<decltype(fixed.second)>(nsDiv.rem))
        {
            fixed.first.tm_sec =
                static_cast<std::intmax_t>(fixed.first.tm_sec) + static_cast<std::intmax_t>(nsDiv.quot);
            fixed.second = nsDiv.rem;
        }
        else
        {
            return std::nullopt;
        }

        // timegm is non-portable. For a portable version
        // set the TZ environment variable to UTC, call std::mktime
        // and restore the value of TZ
        time_t const epoch = timegm(&fixed.first);

        if (epoch != -1)
        {
            return fixed;

            // It's also possible to use the epoch value to fix the timestamp.
            // If it's possible to represent years as int, this should work
            // Unlike mktime, it does not guarantee unchanged tm on failure.
            //
            // if (gmtime_r(&epoch, &fixed.first))
            //{
            //    return fixed;
            //}
        }

        // Unable to validate/fix, but could be OK
        return std::nullopt;
    }

    [[nodiscard]] TmAndNs toTmAndNs() const noexcept
    {
        std::tm tm{};
        tm.tm_year = static_cast<int>(year) + 70;
        tm.tm_mon = month - 1;
        tm.tm_mday = day;
        // Convert from Mon(1) to Sun(7) to Sun(0) to Sat(6)
        tm.tm_wday = weekday % 7;
        tm.tm_hour = hour;
        tm.tm_min = minute;
        tm.tm_sec = second;
        tm.tm_isdst = 0;
        return {tm, nanosecond};
    }
};

using CppTypeVariant =
    std::variant<std::monostate, bool, std::byte, std::array<std::byte, 2>, std::array<std::byte, 4>,
                 std::array<std::byte, 8>, std::int8_t, std::int16_t, std::int32_t, std::int64_t, std::uint8_t,
                 std::uint16_t, std::uint32_t, std::uint64_t, float, double, char, DateTime>;

struct TypeParams
{
    std::string_view typeStr;
    ElementType type;
    CppTypeVariant cppType;
    std::size_t size;
    std::size_t alignment;
};

/*!
 * \brief g_elementTypesData stores information about the types that can be stored in
 * a datablock.
 * Although the size of container types alone is 0, the output may indicate
 * a non-zero size and pad because of any elements they contain.
 */
constexpr std::array<TypeParams, 22> g_elementTypesData{
    {{"Data block", ElementType::DATABLOCK, {}, 0, 16},
     {"Struct", ElementType::STRUCT, {}, 0, 16},
     {"User Defined Type", ElementType::UDT, {}, 0, 16},
     {"Bool", ElementType::BOOL, bool{}, 1, 1},
     {"Byte", ElementType::BYTE, std::byte{}, 8, 8},
     {"Word", ElementType::WORD, std::array<std::byte, 2>{}, 16, 16},
     {"DWord", ElementType::DWORD, std::array<std::byte, 4>{}, 32, 16},
     {"LWord", ElementType::LWORD, std::array<std::byte, 8>{}, 64, 16},
     {"SInt", ElementType::SINT, std::int8_t{}, 8, 8},
     {"Int", ElementType::INT, std::int16_t{}, 16, 16},
     {"DInt", ElementType::DINT, std::int32_t{}, 32, 16},
     {"LInt", ElementType::LINT, std::int64_t{}, 64, 16},
     {"USInt", ElementType::USINT, std::uint8_t{}, 8, 8},
     {"UInt", ElementType::UINT, std::uint16_t{}, 16, 16},
     {"UDInt", ElementType::UDINT, std::uint32_t{}, 32, 16},
     {"ULInt", ElementType::ULINT, std::uint64_t{}, 64, 16},
     {"Real", ElementType::REAL, float{}, 32, 16},
     {"LReal", ElementType::LREAL, double{}, 64, 16},
     {"Time", ElementType::TIME, std::int32_t{}, 32, 16},
     {"LTime", ElementType::LTIME, std::int64_t{}, 64, 16},
     {"DTL", ElementType::DTL, DateTime{}, 96, 16},
     {"Char", ElementType::CHAR, char{}, 8, 8}}};

/*!
 * \brief elementAlignmentBits extracts the alignment property of the type.
 * From C++ 20 it will be possible to make this function constexpr
 * \param type is the ElementType enum
 * \return aligment property of the type in bits. Returns 0 if the element
 * type is not found
 */
inline std::size_t elementAlignmentBits(ElementType type) noexcept
{
    auto it = std::find_if(g_elementTypesData.begin(), g_elementTypesData.end(),
                           [&](TypeParams p) { return p.type == type; });
    if (it != g_elementTypesData.end())
    {
        return (*it).alignment;
    }
    return 0;
}

/*!
 * \brief elementSizeBits extracts the size of the type from g_elementTypesData.
 * From C++ 20 it will be possible to make this function constexpr
 * \param type is the ElementType enum
 * \return size of the type in bits. Returns 0 if the element
 * type is not found
 */
inline std::size_t elementSizeBits(ElementType type) noexcept
{
    auto it = std::find_if(g_elementTypesData.begin(), g_elementTypesData.end(),
                           [&](TypeParams p) { return p.type == type; });
    if (it != g_elementTypesData.end())
    {
        return (*it).size;
    }
    return 0;
}

/*!
 * \brief stringToElementType converts the std::string representation of a type
 * to its enumeration value.
 * From C++ 20 it will be possible to make this function constexpr
 * \param string representation of the type
 * \return ElementType enumeration of the type. Returns ElementType::end if the
 * element type is not found
 */
inline ElementType stringToElementType(std::string const &str) noexcept
{
    auto it = std::find_if(g_elementTypesData.begin(), g_elementTypesData.end(),
                           [&](TypeParams p) { return p.typeStr == str; });
    if (it != g_elementTypesData.end())
    {
        return (*it).type;
    }
    // User defined types are arbitrary strings,
    // distinguished by having double quotes.
    if (str.find('\"') < str.size())
    {
        return ElementType::UDT;
    }
    return ElementType::end;
}

/*!
 * \brief elementTypeToString converts the enumeration value of a type to its
 * std::string representation.
 * From C++ 20 it will be possible to make this function constexpr
 * \param ElementType enumeration of the type
 * \return string representation of the type. Returns an empty string if the
 * element type is not valid
 */
inline std::string elementTypeToString(ElementType type)
{
    auto it = std::find_if(g_elementTypesData.begin(), g_elementTypesData.end(),
                           [&](TypeParams p) { return p.type == type; });
    if (it != g_elementTypesData.end())
    {
        return std::string((*it).typeStr);
    }
    return {};
}

/*!
 * \brief hexDump constructs a pretty representation of the raw data contained
 * in a datablock
 * \param dataBlock is the raw datablock data, as a vector of bytes.
 * \return pretty string representation of the data
 */
inline std::string hexDump(std::vector<std::byte> const &dataBlock)
{
    auto makeAddrStr = [](std::size_t i) -> std::string { return "[" + std::to_string(i) + "] "; };
    std::stringstream stream;
    for (std::size_t i{}; i < dataBlock.size(); ++i)
    {
        if (i % 20 == 0)
        {
            if (i > 0)
            {
                stream << "\n";
            }
            std::string addrStr = makeAddrStr(i);
            std::size_t padSize = makeAddrStr(dataBlock.size()).size() - addrStr.size();
            std::string paddedAddr{std::string(padSize, ' ') + addrStr};
            stream << paddedAddr;
        }
        stream << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(dataBlock[i]);
        if (i + 1 < dataBlock.size())
        {
            stream << " ";
        }
    }
    stream << std::endl;
    return stream.str();
}

/*!
 * @brief isCompatibleType checks if the template type T is compatible with the
 * given enumeration of the type
 */
template <typename T>
inline bool isCompatibleType(ElementType type) noexcept
{
    auto it = std::find_if(g_elementTypesData.begin(), g_elementTypesData.end(),
                           [&](TypeParams p) { return p.type == type; });
    if (it != g_elementTypesData.end())
    {
        return std::holds_alternative<T>((*it).cppType) && !std::is_same_v<T, std::monostate>;
    }
    return false;
}

/*!
 * \brief The Descriptor class holds the metadata needed to access an element in
 * the datablock
 */
class Descriptor
{
  public:
    /*!
     * \brief Descriptor constructor is not usually called by the user, but used by the parser
     * to instantiate descriptors based on a datablock description file (.db file)
     * \param size is ignored for non-container types that have a pre-defined size
     */
    Descriptor(std::string const &basePath, std::string const &name, std::array<int, 2> addr, ElementType typeEnum,
               int size, int pad);
    Descriptor() = default;
    Descriptor(Descriptor const &) = default;
    Descriptor &operator=(Descriptor const &) = default;
    Descriptor(Descriptor &&) noexcept = default;
    Descriptor &operator=(Descriptor &&) noexcept = default;

    /*!
     * \brief descriptorIsValid checks if the descriptor is valid for a data element of the template type T
     */
    template <typename T>
    [[nodiscard]] bool descriptorIsValid(std::vector<std::byte> const &dataBlock) const noexcept
    {
        if (static_cast<ElementType>(m_typeId) == ElementType::end)
        {
            std::cout << "UNINITIALIZED DESCRIPTOR!" << std::endl;
            return false;
        }
        if (!isCompatibleType<T>(static_cast<ElementType>(m_typeId)))
        {
            std::cout << "TYPE MISMATCH!" << std::endl;
            return false;
        }
        std::size_t typeSize = sizeof(T);
        if constexpr (std::is_same_v<T, DateTime>)
        {
            typeSize = 12;
        }
        // smallest size of T is one (byte) in cpp
        if ((dataBlock.size() < typeSize + m_addr[0]) || (m_addr[1] < 0) || (m_addr[1] > 7))
        {
            std::cout << "OUT OF RANGE!" << std::endl;
            return false;
        }
        return true;
    }

    /*!
     * \brief Descriptor::read checks that the descriptor is valid for type T, then uses the
     * metadata in the Descriptor object to extract the value from dataBlock, and write it to out
     * \param dataBlock is a vector of bytes, containing the data
     * \param out is the value of the element, with template type T, extracted from the
     * datablock
     */
    template <typename T>
    void read(std::vector<std::byte> const &dataBlock, T &out) const
    {
        if (!descriptorIsValid<T>(dataBlock))
        {
            return;
        }
        if constexpr (std::is_same_v<T, bool>)
        {
            out = ((dataBlock[m_addr[0]] & std::byte(1u << m_addr[1])) != std::byte(0));
        }
        else if constexpr (std::is_same_v<T, DateTime>)
        {
            Descriptor("", m_name, m_addr, ElementType::UINT, 0, 0).read(dataBlock, out.year);
            Descriptor("", m_name, {m_addr[0] + 2, m_addr[1]}, ElementType::USINT, 0, 0).read(dataBlock, out.month);
            Descriptor("", m_name, {m_addr[0] + 3, m_addr[1]}, ElementType::USINT, 0, 0).read(dataBlock, out.day);
            Descriptor("", m_name, {m_addr[0] + 4, m_addr[1]}, ElementType::USINT, 0, 0).read(dataBlock, out.weekday);
            Descriptor("", m_name, {m_addr[0] + 5, m_addr[1]}, ElementType::USINT, 0, 0).read(dataBlock, out.hour);
            Descriptor("", m_name, {m_addr[0] + 6, m_addr[1]}, ElementType::USINT, 0, 0).read(dataBlock, out.minute);
            Descriptor("", m_name, {m_addr[0] + 7, m_addr[1]}, ElementType::USINT, 0, 0).read(dataBlock, out.second);
            Descriptor("", m_name, {m_addr[0] + 8, m_addr[1]}, ElementType::UDINT, 0, 0)
                .read(dataBlock, out.nanosecond);
        }
        else
        {
            std::array<std::byte, sizeof(T)> outData;
            std::copy_n(dataBlock.begin() + m_addr[0], sizeof(T), outData.begin());
            if constexpr (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
            {
                std::reverse(outData.begin(), outData.end());
            }
            std::memcpy(&out, outData.data(), sizeof(T));
        }
    }

    /*!
     * \brief Descriptor::write checks that the descriptor is valid for type T, then uses the metadata
     * in the Descriptor object to write the value 'in' to dataBlock
     * \param dataBlock is a vector of bytes, containing the data
     * \param in is the value of type T, to be written to the datablock
     */
    template <typename T>
    void write(std::vector<std::byte> &dataBlock, T const &in) const
    {
        if (!descriptorIsValid<T>(dataBlock))
        {
            return;
        }
        if constexpr (std::is_same_v<T, bool>)
        {
            std::byte change = std::byte(static_cast<uint32_t>(in) << m_addr[1]);
            std::byte mask = ~std::byte(1u << m_addr[1]);
            dataBlock[m_addr[0]] = (dataBlock[m_addr[0]] & mask) | change;
        }
        else if constexpr (std::is_same_v<T, DateTime>)
        {
            Descriptor("", m_name, m_addr, ElementType::UINT, 0, 0).write(dataBlock, in.year);
            Descriptor("", m_name, {m_addr[0] + 2, m_addr[1]}, ElementType::USINT, 0, 0).write(dataBlock, in.month);
            Descriptor("", m_name, {m_addr[0] + 3, m_addr[1]}, ElementType::USINT, 0, 0).write(dataBlock, in.day);
            Descriptor("", m_name, {m_addr[0] + 4, m_addr[1]}, ElementType::USINT, 0, 0).write(dataBlock, in.weekday);
            Descriptor("", m_name, {m_addr[0] + 5, m_addr[1]}, ElementType::USINT, 0, 0).write(dataBlock, in.hour);
            Descriptor("", m_name, {m_addr[0] + 6, m_addr[1]}, ElementType::USINT, 0, 0).write(dataBlock, in.minute);
            Descriptor("", m_name, {m_addr[0] + 7, m_addr[1]}, ElementType::USINT, 0, 0).write(dataBlock, in.second);
            Descriptor("", m_name, {m_addr[0] + 8, m_addr[1]}, ElementType::UDINT, 0, 0)
                .write(dataBlock, in.nanosecond);
        }
        else
        {
            std::array<std::byte, sizeof(T)> inData;
            std::memcpy(inData.data(), &in, sizeof(T));
            if constexpr (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
            {
                std::reverse(inData.begin(), inData.end());
            }
            std::copy_n(inData.begin(), sizeof(T), dataBlock.begin() + m_addr[0]);
        }
    }

    [[nodiscard]] std::string const &path() const noexcept
    {
        return m_path;
    }
    [[nodiscard]] std::string const &name() const noexcept
    {
        return m_name;
    }
    [[nodiscard]] std::string const &type() const noexcept
    {
        return m_type;
    }
    [[nodiscard]] std::array<int, 2> addr() const noexcept
    {
        return m_addr;
    }
    [[nodiscard]] int typeId() const noexcept
    {
        return m_typeId;
    }
    [[nodiscard]] int size() const noexcept
    {
        return m_size;
    }
    [[nodiscard]] int pad() const noexcept
    {
        return m_pad;
    }

  private:
    friend class arms::json::Accessor<Descriptor>;
    template <typename D>
    void toJson(D &dom) const
    {
        using arms::json::store;
        store(dom, "path", m_path);
        store(dom, "name", m_name);
        store(dom, "type", m_type);
        store(dom, "addr", m_addr);
        store(dom, "type_id", m_typeId);
        store(dom, "size", m_size);
        store(dom, "pad", m_pad);
    }
    template <typename D>
    void fromJson(D const &dom)
    {
        using arms::json::load;
        load(dom, "path", m_path);
        load(dom, "name", m_name);
        load(dom, "type", m_type);
        load(dom, "addr", m_addr);
        load(dom, "type_id", m_typeId);
        load(dom, "size", m_size);
        load(dom, "pad", m_pad);
    }

    std::string m_path;
    std::string m_name;
    std::string m_type;
    std::array<int, 2> m_addr{0, 0};
    int m_typeId{static_cast<int>(ElementType::end)};
    int m_size{0};
    int m_pad{0};
};

/*!
 * \brief The Parser class stores a set of Descriptors, each of which holds metadata
 * allowing the user to read and write values to a datablock.
 */
class Parser
{
  public:
    /*!
     * \brief Construct a Parser using a
     * \param buffer
     */
    explicit Parser(std::string_view buffer);
    Parser() = default;

    /*!
     * \brief The minimum size in bytes required to hold all of the data.
     */
    std::size_t dataSize() const noexcept
    {
        return m_dataSize;
    }

    /*!
     * \brief findDescriptor retreives the descriptor at the requested path
     * \param path
     * \return the descriptor at the given path, or std::nullopt if no matching
     * descriptor is found
     */
    [[nodiscard]] std::optional<Descriptor> findDescriptor(std::string const &path) const noexcept;

  private:
    friend class arms::json::Accessor<Parser>;
    template <typename D>
    void toJson(D &dom) const
    {
        using arms::json::store;
        store(dom, "descriptor_set", m_descriptorSet);
	store(dom, "data_size", m_dataSize);
    }
    template <typename D>
    void fromJson(D const &dom)
    {
        using arms::json::load;
        load(dom, "descriptor_set", m_descriptorSet);
	load(dom, "data_size", m_dataSize);
        calculateNewDataSize();
    }
    void calculateNewDataSize() noexcept;

    std::vector<Descriptor> m_descriptorSet;
    std::size_t m_dataSize{0};
};

namespace internal
{

class Element
{
  public:
    Element() = delete;
    Element(ElementType type, std::string name, std::vector<std::size_t> dims, std::vector<std::string> const &tokens,
            std::map<std::string, Element> const &udtMap);
    Element(ElementType type, std::string name, std::vector<std::size_t> dims, std::vector<Element> children = {})
        : m_type{type}, m_name{std::move(name)}, m_dims{std::move(dims)}, m_children{std::move(children)}
    {
    }

    void fillDescriptorSet(std::vector<Descriptor> &descriptorSet, std::size_t &bitAddr,
                           std::string const &nameBase = "") const;

  private:
    ElementType m_type{ElementType::end};
    std::string m_name;
    std::vector<std::size_t> m_dims;
    std::vector<Element> m_children;
};

inline std::vector<std::string> split(std::string_view buffer, char delim)
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream{std::string(buffer)};
    while (std::getline(tokenStream, token, delim))
    {
        tokens.push_back(token);
    }
    return tokens;
}

inline void removeSingleLineComments(std::vector<std::string> &tokens, std::string const &commentStart)
{
    for (std::string &tok : tokens)
    {
        std::size_t cmtStartIdx = tok.find(commentStart);
        if (cmtStartIdx < tok.size())
        {
            tok.erase(cmtStartIdx);
        }
    }
}

inline void removeMultilineComments(std::vector<std::string> &tokens, std::string const &commentStart,
                                    std::string const &commentEnd)
{
    bool inComment{false};
    for (std::string &tok : tokens)
    {
        while (true)
        {
            // note: always breaks/continues after erase, which refreshes idx positions.
            std::size_t cmtStartIdx = tok.find(commentStart);
            std::size_t cmtEndingIdx = tok.find(commentEnd);
            std::size_t cmtEndIdx = cmtEndingIdx + commentEnd.size();
            if (inComment)
            {
                if (cmtEndingIdx < tok.size())
                {
                    tok.erase(0, cmtEndIdx);
                    inComment = false;
                    continue;
                }
                else
                {
                    tok.clear();
                    break;
                }
            }
            if (cmtStartIdx < tok.size())
            {
                if (cmtEndingIdx < tok.size())
                {
                    tok.erase(cmtStartIdx, cmtEndIdx - cmtStartIdx);
                    continue;
                }
                tok.erase(cmtStartIdx);
                inComment = true;
            }
            break;
        }
    }
}

inline std::vector<std::string> semicolonsToNewLines(std::vector<std::string> const &tokens)
{
    bool inQuotes{false};
    std::vector<std::string> result;
    for (std::string tok : tokens)
    {
        for (char &c : tok)
        {
            if (c == '"')
            {
                inQuotes = !inQuotes;
            }
            if (c == ';' && !inQuotes)
            {
                c = '\n';
            }
        }
        std::vector<std::string> splitTok = split(tok, '\n');
        result.insert(result.end(), splitTok.begin(), splitTok.end());
    }
    return result;
}

/*!
 * \brief trimWhitespaceAndUnprintable removes unprintable characters (eg tab \t) from
 * the token. Also trim spaces from the beginning and end of the token
 * \param token is the string to be modified in-place
 */
inline void trimWhitespaceAndUnprintable(std::string &token)
{
    token.erase(remove_if(token.begin(), token.end(), [](unsigned char c) { return !std::isprint(c); }), token.end());
    static constexpr std::string_view whiteSpace = " \t\v\r\n";
    std::size_t startIdx = token.find_first_not_of(whiteSpace);
    if (startIdx < token.size())
    {
        token.erase(0, startIdx);
    }
    std::size_t endIdx = token.find_last_not_of(whiteSpace);
    if (endIdx < token.size())
    {
        // note erase argument must be <= size()
        token.erase(endIdx + 1);
    }
    if (token.find_first_not_of(whiteSpace) >= token.size())
    {
        token.clear();
    }
}

inline void trimWhitespaceAndUnprintable(std::vector<std::string> &tokens)
{
    for (std::string &tok : tokens)
    {
        trimWhitespaceAndUnprintable(tok);
    }
    tokens.erase(std::remove(tokens.begin(), tokens.end(), std::string()), tokens.end());
}

inline std::vector<std::string> preprocess(std::string_view buffer)
{
    std::vector<std::string> tokens = split(buffer, '\n');
    removeSingleLineComments(tokens, "//");
    removeMultilineComments(tokens, "(*", "*)");
    removeMultilineComments(tokens, "{", "}");
    tokens = semicolonsToNewLines(tokens);
    trimWhitespaceAndUnprintable(tokens);
    return tokens;
}

struct NamedBlock
{
    std::string name;
    std::vector<std::string> tokens;
};

inline std::vector<NamedBlock> extractNamedBlocks(std::vector<std::string> const &tokens, std::string const &startsWith,
                                                  std::string const &endsWith)
{
    std::vector<NamedBlock> result;
    bool inBlock{false};
    NamedBlock block{};
    static constexpr std::string_view nameDelim{"\""};
    for (std::string const &tok : tokens)
    {
        if (tok.substr(0, startsWith.size()) == startsWith)
        {
            inBlock = true;
            std::size_t nameStartIdx = tok.find_first_of(nameDelim);
            std::size_t nameEndIdx = tok.find_last_of(nameDelim) + nameDelim.size();
            if (nameStartIdx < tok.size())
            {
                block.name = tok.substr(nameStartIdx, nameEndIdx - nameStartIdx);
            }
            else
            {
                block.name = "\"NO NAME\"";
            }
            continue;
        }
        if (tok.substr(0, endsWith.size()) == endsWith)
        {
            inBlock = false;
            result.push_back(block);
            block = NamedBlock{};
            continue;
        }
        if (inBlock)
        {
            block.tokens.push_back(tok);
        }
    }
    return result;
}

inline bool stringStartsWith(std::string const &str, std::string const &start)
{
    return (str.substr(0, start.size()) == start);
}

inline std::vector<std::string> extractOutermostBlock(std::vector<std::string> const &tokens,
                                                      std::string const &startsWith, std::string const &endsWith)
{
    int startIdx{0};
    int endIdx{0};
    int tokensMinusOne = static_cast<int>(tokens.size()) - 1;
    for (int idx{0}; idx < tokensMinusOne; ++idx)
    {
        if (stringStartsWith(tokens[idx], startsWith))
        {
            startIdx = idx + 1;
            break;
        }
    }
    for (int idx{tokensMinusOne}; idx >= 0; --idx)
    {
        if (stringStartsWith(tokens[idx], endsWith))
        {
            endIdx = idx;
            break;
        }
    }
    return {tokens.begin() + startIdx, tokens.begin() + endIdx};
}

// Process variable definition such as "MYVAR : Array[0..2,0..5] of "UDT1""
// returns "MYVAR", a vector containing {3, 6}, and ""UDT1""
inline std::tuple<std::string, std::vector<std::size_t>, std::string> processDefinition(std::string const &def)
{
    std::size_t colonIdx = def.find_first_of(':');
    std::string elementName = def.substr(0, colonIdx);
    trimWhitespaceAndUnprintable(elementName);
    std::string typeDesc = def.substr(colonIdx + 1);
    trimWhitespaceAndUnprintable(typeDesc);
    std::vector<std::size_t> arrayDims;
    if (stringStartsWith(typeDesc, "Array"))
    {
        std::size_t dimStartIdx = typeDesc.find_first_of('[') + 1;
        std::size_t dimEndIdx = typeDesc.find_first_of(']');
        std::string dimsStr = typeDesc.substr(dimStartIdx, dimEndIdx - dimStartIdx);
        std::vector<std::string> dims = split(dimsStr, ',');
        for (std::string const &d : dims)
        {
            std::vector<std::string> vals = split(d, '.');
            arrayDims.push_back(1 + std::stoi(vals.at(2)) - std::stoi(vals.at(0)));
        }
        std::size_t endOfArrayDescIdx = typeDesc.find(" of ") + 4;
        typeDesc = typeDesc.substr(endOfArrayDescIdx);
        trimWhitespaceAndUnprintable(typeDesc);
    }
    return {elementName, arrayDims, typeDesc};
}

inline std::map<std::string, Element> makeUdtMap(std::vector<std::string> const &tokens)
{
    std::map<std::string, Element> udts;
    std::vector<NamedBlock> udtBlocks = extractNamedBlocks(tokens, "TYPE", "END_TYPE");
    for (NamedBlock const &udt : udtBlocks)
    {
        std::vector<std::string> udtTokens = extractOutermostBlock(udt.tokens, "STRUCT", "END_STRUCT");
        Element e{ElementType::UDT, udt.name, {}, udtTokens, udts};
        udts.insert_or_assign(udt.name, e);
    }
    return udts;
}

inline std::map<std::string, Element> makeDatablockMap(std::vector<std::string> const &tokens)
{
    std::map<std::string, Element> udtMap = makeUdtMap(tokens);
    std::map<std::string, Element> datablockMap;
    std::vector<NamedBlock> blocks = extractNamedBlocks(tokens, "DATA_BLOCK", "END_DATA_BLOCK");
    for (NamedBlock const &block : blocks)
    {
        std::vector<std::string> dataBlockTokens = extractOutermostBlock(block.tokens, "STRUCT", "END_STRUCT");
        Element e{ElementType::DATABLOCK, block.name, {}, dataBlockTokens, udtMap};
        datablockMap.insert_or_assign(block.name, e);
    }
    return datablockMap;
}

// If dims is empty, returns size 1.
inline std::size_t size1d(std::vector<std::size_t> const &dims)
{
    std::size_t multiple{1};
    for (std::size_t d : dims)
    {
        multiple *= d;
    }
    return multiple;
}

inline std::vector<std::size_t> ndFrom1dIndex(std::vector<std::size_t> dims, std::size_t idx)
{
    std::vector<std::size_t> multiples;
    std::size_t multiple{1};
    for (std::size_t i{0}; i < dims.size(); ++i)
    {
        std::size_t ri = dims.size() - 1 - i;
        multiples.push_back(multiple);
        multiple *= dims[ri];
    }
    for (std::size_t i{0}; i < dims.size(); ++i)
    {
        std::size_t ri = dims.size() - 1 - i;
        dims[i] = idx / multiples[ri];
        idx -= dims[i] * multiples[ri];
    }
    return dims;
}

template <typename T>
inline std::string vectorToString(std::vector<T> const &vec)
{
    std::ostringstream oss;
    if (!vec.empty())
    {
        oss << "[";
        // Convert all but the last element to avoid a trailing ","
        std::copy(vec.begin(), vec.end() - 1, std::ostream_iterator<T>(oss, ", "));
        // Now add the last element with no delimiter
        oss << vec.back();
        oss << "]";
    }
    return oss.str();
}

inline std::size_t getAlignedAddress(std::size_t addr, std::size_t alignment)
{
    assert(alignment > 0);
    std::size_t remainder = addr % alignment;
    if (remainder > 0)
    {
        addr += alignment - remainder;
    }
    return addr;
}

inline std::array<int, 2> byteAddressFromBitAddress(std::size_t bitAddr)
{
    int bytes = bitAddr / 8;
    int bits = static_cast<int>(bitAddr) - bytes * 8;
    return {bytes, bits};
}

inline std::string concatenateNames(std::string const &base, std::string const &name)
{
    return base + (base.empty() ? "" : ".") + name;
}

inline Element::Element(ElementType type, std::string name, std::vector<std::size_t> dims,
                        std::vector<std::string> const &tokens, std::map<std::string, Element> const &udtMap)
    : m_type{type}, m_name{std::move(name)}, m_dims{std::move(dims)}
{
    std::size_t tokenIdx{0};
    while (tokenIdx < tokens.size())
    {
        std::string const &tokenRef = tokens[tokenIdx];
        auto [elementName, arrayDims, typeStr] = processDefinition(tokenRef);
        ElementType tokenType = stringToElementType(typeStr);
        if (tokenType == ElementType::end)
        {
            throw std::runtime_error("Error parsing datablock. Unknown tokenType: " + typeStr);
        }
        else if (tokenType == ElementType::STRUCT)
        {
            std::vector<std::string> structTokens;
            ++tokenIdx; // start inside the Struct block
            int nesting = 0;
            while (true)
            {
                std::string const &tokenRefInner = tokens.at(tokenIdx);
                if (stringStartsWith(tokenRefInner, "END_STRUCT"))
                {
                    --nesting;
                }
                else if (tokenRefInner.find("Struct") != std::string::npos)
                {
                    ++nesting;
                }
                if (nesting < 0)
                {
                    break;
                }
                structTokens.push_back(tokenRefInner);
                ++tokenIdx;
            }
            m_children.emplace_back(tokenType, elementName, arrayDims, structTokens, udtMap);
        }
        else if (tokenType == ElementType::UDT)
        {
            m_children.emplace_back(tokenType, elementName, arrayDims, udtMap.at(typeStr).m_children);
        }
        else
        {
            m_children.emplace_back(tokenType, elementName, arrayDims);
        }
        ++tokenIdx;
    }
}

inline void Element::fillDescriptorSet(std::vector<Descriptor> &descriptorSet, std::size_t &bitAddr,
                                       std::string const &nameBase) const
{
    std::size_t dataAlignment = elementAlignmentBits(m_type);
    std::size_t containerAlignment = dataAlignment;
    static constexpr std::size_t arrayAlignment = 16;
    if (!m_dims.empty())
    {
        containerAlignment = std::max(containerAlignment, arrayAlignment);
    }

    bitAddr = getAlignedAddress(bitAddr, containerAlignment);

    std::size_t s1d = size1d(m_dims);
    for (std::size_t i{0}; i < s1d; ++i)
    {
        std::string fullName = m_name + vectorToString(ndFrom1dIndex(m_dims, i));
        std::size_t sizeBits = elementSizeBits(m_type);
        bitAddr = getAlignedAddress(bitAddr, dataAlignment);
        std::size_t startAddr = bitAddr;

        // Note : no type has children and size_bits > 0, so order
        // of incrementing bit_addr and traversing children is not
        // important
        bitAddr += sizeBits;

        std::vector<Descriptor> childrenDescription;
        for (Element const &e : m_children)
        {
            e.fillDescriptorSet(childrenDescription, bitAddr, concatenateNames(nameBase, fullName));
        }

        std::size_t endAddr = bitAddr;

        if (i + 1 == s1d)
        {
            bitAddr = getAlignedAddress(bitAddr, containerAlignment);
        }

        int descSize = endAddr - startAddr;
        int descPad = bitAddr - endAddr;
        descriptorSet.emplace_back(nameBase, fullName, byteAddressFromBitAddress(startAddr), m_type, descSize, descPad);

        descriptorSet.insert(descriptorSet.end(), childrenDescription.begin(), childrenDescription.end());
    }
}
} // namespace internal

inline Descriptor::Descriptor(std::string const &basePath, std::string const &name, std::array<int, 2> addr,
                              ElementType typeEnum, int size, int pad)
    : m_path{internal::concatenateNames(basePath, name)}, m_name{name}, m_type{elementTypeToString(typeEnum)},
      m_addr{addr}, m_typeId{static_cast<int>(typeEnum)}, m_size{size}, m_pad{pad}
{
    int typeSize = elementSizeBits(typeEnum);
    if (typeSize > 0)
    {
        m_size = typeSize;
    }
}

inline Parser::Parser(std::string_view buffer)
{
    std::vector<std::string> tokens = internal::preprocess(buffer);
    std::map<std::string, internal::Element> dataBlockMap = internal::makeDatablockMap(tokens);
    for (auto const &[name, e] : dataBlockMap)
    {
        std::size_t baseBitAddr = 0;
        e.fillDescriptorSet(m_descriptorSet, baseBitAddr);
    }
    calculateNewDataSize();
}

inline std::optional<Descriptor> Parser::findDescriptor(std::string const &path) const noexcept
{
    auto it = std::find_if(m_descriptorSet.begin(), m_descriptorSet.end(),
                           [&](Descriptor const &d) { return (d.path() == path); });
    if (it != m_descriptorSet.end())
    {
        return *it;
    }
    return std::nullopt;
}

// Increase the datasize to at least that required by all the descriptors.
inline void Parser::calculateNewDataSize() noexcept
{
    for (Descriptor const &d : m_descriptorSet)
    {
        std::array<int, 2> address = d.addr();
        std::size_t bits = address[0] * 8 + d.size();
        std::size_t bytes = (bits + 7) / 8;
        m_dataSize = (bytes > m_dataSize) ? bytes : m_dataSize;
    }
}

} // namespace arms::datablock
